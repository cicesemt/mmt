function output = thousandsep(input, digits)
  import java.text.DecimalFormat;
  
  pattern = '#,##0';
  
  for ii = 1 : digits
    if ii == 1
      pattern = sprintf('%s.0', pattern);
    else
      pattern = sprintf('%s0', pattern);
    end
  end
  
  decimalFormat = DecimalFormat(pattern);
  
  output = char(decimalFormat.format(input));
end