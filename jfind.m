function [varargout] = jfind(path)
  if nargout > 0
    varargout{1} = [];
  end
  
  files = dir(path);
  
  if ~isempty(files)
    for ii = 1 : length(files)
      file = files(ii);
      
      if ~strcmp(file.name, '.') && ~strcmp(file.name, '..') && file.isdir
        if nargout > 0
          varargout{1} = [varargout{1}; jfind(sprintf('%s%s%s', path, filesep, file.name))];
        else
          jfind(sprintf('%s%s%s', path, filesep, file.name));
        end
      end
    end
    
    for ii = 1 : length(files)
      file = files(ii);
      
      if ~strcmp(file.name, '.') && ~strcmp(file.name, '..') && ~file.isdir
        f = jfile(sprintf('%s%s%s', path, filesep, file.name));
        
        if ~isempty(f)
          if nargout > 0
            varargout{1} = [varargout{1}; f];
          else
            fprintf('%s%s%s\n', path, filesep, file.name);
          end
        end
      end
    end
  end
  
end