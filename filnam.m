function [varargout] = filnam(file)
  if nargout > 0
    varargout{1} = [];
  end
  
  if ~isempty(file)
    if file.aduSerialNumber < 10
      aduSerialNumber = sprintf('00%d', file.aduSerialNumber);
    elseif file.aduSerialNumber < 100
      aduSerialNumber = sprintf('0%d', file.aduSerialNumber);
    else
      aduSerialNumber = sprintf('%d', file.aduSerialNumber);
    end
    
    if file.xmlVersion < 10
      xmlVersion = sprintf('0%d', file.xmlVersion);
    else
      xmlVersion = sprintf('%d', file.xmlVersion);
    end
    
    if file.channelNumber < 10
      channelNumber = sprintf('0%d', file.channelNumber);
    else
      channelNumber = sprintf('%d', file.channelNumber);
    end
    
    if file.runNumber < 10
      runNumber = sprintf('00%d', file.runNumber);
    elseif file.runNumber < 100
      runNumber = sprintf('0%d', file.runNumber);
    else
      runNumber = sprintf('%d', file.runNumber);
    end
    
    if strcmpi(file.boardType, 'LF')
      boardType = 'L';
    elseif strcmpi(file.boardType, 'HF')
      boardType = 'H';
    else
      boardType = 'M';
    end
    
    fnam = sprintf('%s_%s_V%s_C%s_R%s_B%s_%d%s.ats', file.channelType, aduSerialNumber, xmlVersion, channelNumber, runNumber, boardType, file.samplingRate, file.measurementUnit);
    
    if nargout > 0
      varargout{1} = fnam;
    else
      fprintf('%s\n', fnam);
    end
  end
end