function surveybands(varargin)
  currentSurvey = [];
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSurvey)
    currentSurvey = survey(varargin{:});
  end
  
  if ~isempty(currentSurvey)
    printBands(buildBands(currentSurvey));
  end
end

function bands = buildBands(currentSurvey)
  bands = struct(...
    'name', {}, ...
    'folder', {}, ...
    'bytes', {}, ...
    'samplingRate', {}, ...
    'measurementUnit', {}, ...
    'numberOfSamples', {}, ...
    'frequency', {}, ...
    'latitude', {}, ...
    'longitude', {}, ...
    'elevation', {}, ...
    'startTime', {}, ...
    'stopTime', {}, ...
    'channels', {} ...
  );
  
  frequencies = zeros(0);
  
  for ii = 1 : length(currentSurvey.sites)
    for jj = 1 : length(currentSurvey.sites(ii).bands)
      bands(length(bands) + 1) = currentSurvey.sites(ii).bands(jj);
      frequencies(length(frequencies) + 1) = currentSurvey.sites(ii).bands(jj).frequency;
    end
  end

  [~, index] = sort(frequencies);
  
  swp = bands;
  
  for ii = 1 : length(index)
    bands(ii) = swp(index(ii));
  end
end

function printBands(bands)
  names = cell(0);
  
  for ii = 1 : length(bands)
    ignore = false;
    
    for jj = 1 : length(names)
      if strcmpi(names{jj}, bands(ii).name)
        ignore = true;
        break;
      end
    end
    
    if ~ignore
      names{length(names) + 1} = bands(ii).name;
    end
  end
  
  width = 0;
  
  for ii = 1 : length(names)
    if width < length(names{ii})
      width = length(names{ii});
    end
  end
  
  for ii = 1 : length(names)
    if length(names{ii}) < width
      names{ii} = sprintf(sprintf('%%%ds%%s', width - length(names{ii})), ' ', names{ii});
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s\n', names{ii});
  end
end