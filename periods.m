function [varargout] = periods(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    if strcmpi(varargin{1}, 'preview')
      varargin(:, 1) = [];
      periodspreview(varargin{:});
      return;
    end
  end
  
  sites = cell(0);
  bands = cell(0);
  
  onCommand = false;
  onAll = false;
  onIndexes = zeros(0);
  
  offCommand = false;
  offAll = false;
  offIndexes = zeros(0);
  
  onStatus = false;
  offStatus = false;
  
  ascendingOrder = false;
  
  if nargin > 0
    if strcmpi(varargin{1}, 'on')
      varargin(:, 1) = [];
      onCommand = true;
    elseif strcmpi(varargin{1}, 'off')
      varargin(:, 1) = [];
      offCommand = true;
    end
    
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-site')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          sites{length(sites) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          bands{length(bands) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-status')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          if strcmpi(varargin{k}, 'on')
            onStatus = true;
            offStatus = false;
          elseif strcmpi(varargin{k}, 'off')
            onStatus = false;
            offStatus = true;
          end
          
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-asc')
        varargin(:, k) = [];
        ascendingOrder = true;
      else
        k = k + 1;
      end
    end
  end
  
  if ~isempty(varargin)
    if onCommand || offCommand
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, 'all')
          varargin(:, k) = [];
          
          if onCommand
            onAll = true;
          else
            offAll = true;
          end
          
          break;
        else
          k = k + 1;
        end
      end
      
      if ~isempty(varargin)
        if ~onAll && ~offAll
          k = 1;
          
          while k <= length(varargin)
            index = varargin{k};
            
            if ischar(index)
              index = str2double(index);
            end
            
            if ~isnan(index)
              varargin(:, k) = [];
              
              if onCommand
                onIndexes(length(onIndexes) + 1) = index;
              else
                offIndexes(length(offIndexes) + 1) = index;
              end
            else
              k = k + 1;
            end
          end
        end
      end
    end
  end
  
  if ~isempty(varargin)
    path = varargin;
  else
    path = {pwd()};
  end
  
  jfiles = jselect(path, sites, bands);
  
  if ~isempty(jfiles)
    p = cell(0);
    o = zeros(0);
    
    for ii = 1 : length(jfiles)
      jfile = jfiles{ii};
      mask = pmask('-path', jfile.folder);
      
      for jj = 1 : length(jfile.zxx.period)
        period.globalIndex = 0;
        period.localIndex = jj;
        period.folder = jfile.folder;
        period.site = jfile.site;
        period.band = jfile.band;
        period.value = jfile.zxx.period(jj);
        period.mask = 'on';
        period.duplicated = false;
        period.jfile = jfile;
        
        for kk = 1 : length(mask)
          if period.localIndex == mask(kk)
            period.mask = 'off';
            break;
          end
        end
        
        p{length(p) + 1} = period;
        o(length(o) + 1) = period.band;
      end
    end
    
    [~, idx] = sort(o);
    swp = cell(length(idx));
    
    for ii = 1 : length(idx)
      swp{length(idx) - ii + 1} = p{idx(ii)};
    end
    
    p = swp;
    o = zeros(length(p));
    
    for ii = 1 : length(o)
      o(ii) = p{ii}.value;
    end
    
    [~, idx] = sort(o);
    swp = cell(length(idx));
    
    for ii = 1 : length(idx)
      swp{ii} = p{idx(ii)};
      swp{ii}.globalIndex = ii;
    end
    
    p = swp;
    
    for ii = 1 : length(p)
      if strcmp(p{ii}.mask, 'off')
        continue;
      end
      
      for jj = 1 : length(p)
        if ii ~= jj
          if strcmp(p{jj}.mask, 'off')
            continue;
          end
          
          if strcmp(sprintf('%f', p{ii}.value), sprintf('%f', p{jj}.value))
            p{ii}.duplicated = true;
            break;
          end
        end
      end
    end
    
    if ~ascendingOrder
      swp = cell(length(p));
      
      for ii = 1 : length(swp)
        swp{ii} = p{length(swp) - ii + 1};
      end
      
      p = swp;
    end
    
    if onCommand
      if onAll
        for ii = 1 : length(p)
          pmaskclear(p{ii}.folder);
        end
      else
        for ii = 1 : length(onIndexes)
          for jj = 1 : length(p)
            if onIndexes(ii) == p{jj}.globalIndex
              pmaskremove(p{jj}.folder, p{jj}.localIndex);
              break;
            end
          end
        end
      end
    elseif offCommand
      if offAll
        for ii = 1 : length(p)
          pmaskclear(p{ii}.folder);
        end
        
        for ii = 1 : length(p)
          pmaskadd(p{ii}.folder, p{ii}.localIndex);
        end
      else
        for ii = 1 : length(offIndexes)
          for jj = 1 : length(p)
            if offIndexes(ii) == p{jj}.globalIndex
              pmaskadd(p{jj}.folder, p{jj}.localIndex);
              break;
            end
          end
        end
      end
    else
      if nargout > 0
        for ii = 1 : length(p)
          if onStatus
            if ~strcmp(p{ii}.mask, 'on')
              continue;
            end
          elseif offStatus
            if ~strcmp(p{ii}.mask, 'off')
              continue;
            end
          end
          
          varargout{1} = [varargout{1} p{ii}];
        end
      else
        globalIndexWidth = 0;
        siteWidth = 0;
        bandWidth = 0;
        valueWidth = 0;
        maskWidth = 0;
        
        for ii = 1 : length(p)
          if onStatus
            if ~strcmp(p{ii}.mask, 'on')
              continue;
            end
          elseif offStatus
            if ~strcmp(p{ii}.mask, 'off')
              continue;
            end
          end
          
          global_index_width = length(sprintf('%d', p{ii}.globalIndex));
          site_width = length(sprintf('%s', p{ii}.site));
          band_width = length(sprintf('%d', p{ii}.band));
          value_width = length(sprintf('%f', p{ii}.value));
          mask_width = length(sprintf('%s', p{ii}.mask));
          
          if globalIndexWidth < global_index_width
            globalIndexWidth = global_index_width;
          end
          
          if siteWidth < site_width
            siteWidth = site_width;
          end
          
          if bandWidth < band_width
            bandWidth = band_width;
          end
          
          if valueWidth < value_width
            valueWidth = value_width;
          end
          
          if maskWidth < mask_width
            maskWidth = mask_width;
          end
        end
        
        for ii = 1 : length(p)
          if onStatus
            if ~strcmp(p{ii}.mask, 'on')
              continue;
            end
          elseif offStatus
            if ~strcmp(p{ii}.mask, 'off')
              continue;
            end
          end
          
          fprintf(sprintf('%%%dd\t', globalIndexWidth), p{ii}.globalIndex);
          fprintf(sprintf('%%%ds\t', siteWidth), p{ii}.site);
          fprintf(sprintf('%%%dd\t', bandWidth), p{ii}.band);
          fprintf(sprintf('%%%df\t', valueWidth), p{ii}.value);
          fprintf(sprintf('%%%ds', maskWidth), p{ii}.mask);
          
          if p{ii}.duplicated
            fprintf('\t@');
          end
          
          fprintf('\n');
        end
      end
    end
  end
end