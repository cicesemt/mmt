function [varargout] = ofil(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  name = 'ofil';
  defaultValue = 'ofil';
  
  value = sparam(name, defaultValue);
  
  if ~isempty(value)
    s = site();
    
    if ~isempty(s)
      defaultValue = s.name;
    end
    
    resetFlag = false;
    errorFlag = false;
    
    if nargin > 0
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-reset')
          varargin(:, k) = [];
          resetFlag = true;
        else
          if length(varargin{k}) > 1 && varargin{k}(1) == '-' && isnan(str2double(varargin{k}))
            cprintf('err', 'unknown option ''%s'' \n', varargin{k});
            errorFlag = true;
            break;
          end
          
          k = k + 1;
        end
      end
    end
    
    if ~errorFlag
      if resetFlag
        if ~isempty(varargin)
          cprintf('err', 'too many arguments\n');
        elseif ~strcmp(value, defaultValue)
          value = sparam(name, value, defaultValue);
        end
      elseif ~isempty(varargin)
        if length(varargin) > 1
          cprintf('err', 'too many arguments\n');
        else
          value = sparam(name, value, varargin{1});
        end
      elseif nargout == 0
        fprintf('%s\n', value);
      end
    end
    
    if nargout > 0
      varargout{1} = value;
    end
  end
end