function coh(varargin)
  red     = '[0.6350, 0.0780, 0.1840]';
  green   = '[0.4660, 0.6740, 0.1880]';
  blue    = '[0.0000, 0.4470, 0.7410]';
  orange  = '[0.8500, 0.3250, 0.0980]';
  yellow  = '[0.9290, 0.6940, 0.1250]';
  %purple  = '[0.4940, 0.1840, 0.5560]';
  %skyblue = '[0.3010, 0.7450, 0.9330]';
  
  sites = cell(0);
  bands = cell(0);
  
  exflag = false;
  eyflag = false;
  hxflag = false;
  hyflag = false;
  hzflag = false;
  nrflag = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-site')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          sites{length(sites) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          bands{length(bands) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-ex')
        exflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-ey')
        eyflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-hx')
        hxflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-hy')
        hyflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-hz')
        hzflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-nonrobust')
        nrflag = true;
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if ~isempty(varargin)
    args = varargin;
  else
    args = {pwd()};
  end
  
  if ~exflag && ~eyflag && ~hxflag && ~hyflag && ~hzflag
    exflag = true;
    eyflag = true;
    hzflag = true;
  end
  
  c2files = c2select(args, sites, bands, exflag, eyflag, hxflag, hyflag, hzflag, nrflag);
  
  if ~isempty(c2files)
    sites = cell(0);
    
    exflag = false;
    eyflag = false;
    hxflag = false;
    hyflag = false;
    hzflag = false;
    
    f = figure;
    f.set('NumberTitle', 'off');
    f.set('MenuBar', 'figure');
    f.set('ToolBar', 'figure');
    
    f.UserData.id = 'mmt-coh-plot';
    f.UserData.freezed = false;
    
    f.UserData.Series          = {};
    f.UserData.MaskSeries      = {};
    f.UserData.RepaintSeries   = {};
    f.UserData.SelectionSeries = {};
    
    set(f, 'KeyPressFcn', @keypress);
    
    for ii = 1 : 5
      for jj = 1 : length(c2files)
        c2file = c2files{jj};
        
        plotflag = false;
        visibilityflag = false;
        
        if c2file.stage == 1
          if c2file.input == 1
            if ii == 4
              plotflag = true;
              
              if ~hxflag
                visibilityflag = true;
              end
              
              hxflag = true;
              color  = orange;
            end
          elseif c2file.input == 2
            if ii == 5
              plotflag = true;
              
              if ~hyflag
                visibilityflag = true;
              end
              
              hyflag = true;
              color  = yellow;
            end
          end
        elseif c2file.stage == 2
          if c2file.output == 1
            if ii == 1
              plotflag = true;
              
              if ~exflag
                visibilityflag = true;
              end
              
              exflag = true;
              color  = red;
            end
          elseif c2file.output == 2
            if ii == 2
              plotflag = true;
              
              if ~eyflag
                visibilityflag = true;
              end
              
              eyflag = true;
              color  = blue;
            end
          elseif c2file.output == 3
            if ii == 3
              plotflag = true;
              
              if ~hzflag
                visibilityflag = true;
              end
              
              hzflag = true;
              color  = green;
            end
          end
        end
        
        if plotflag
          siteflag = true;
          
          for kk = 1 : length(sites)
            if strcmpi(sites{kk}, sprintf('%s[%s]', c2file.site, c2file.frequencyBand))
              siteflag = false;
            end
          end
          
          if siteflag
            sites{length(sites) + 1} = sprintf('%s[%s]', c2file.site, c2file.frequencyBand);
          end
          
          value  = c2file.coh.value;
          period = c2file.coh.period;
          upper  = c2file.coh.upper;
          lower  = c2file.coh.lower;
          
          if visibilityflag
            series = semilogx(period, value, 'Color', color, 'LineStyle', 'none', 'Marker', 'o');
          else
            series = semilogx(period, value, 'Color', color, 'LineStyle', 'none', 'Marker', 'o', 'HandleVisibility', 'off');
          end
          
          hold on;
          
          xe = zeros(2, length(period));
          xe(1, :) = period;
          xe(2, :) = period;
          ye = zeros(2, length(period));
          ye(1, :) = lower;
          ye(2, :) = upper;
          
          eseries = semilogx(xe, ye, 'Color', color, 'LineStyle', '-', 'HandleVisibility', 'off');
          
          set(series.Parent, 'ButtonDownFcn', @axesmousedown);
          set(series, 'ButtonDownFcn', @seriesmousedown);
          set(eseries, 'ButtonDownFcn', @seriesmousedown);
          
          series.UserData.SeriesType  = 'data';
          series.UserData.SeriesFile  = c2file;
          series.UserData.SeriesError = eseries;
          
          for kk = 1 : length(eseries)
            eseries(kk).UserData.SeriesType = 'error';
            eseries(kk).UserData.SeriesFile = c2file;
            eseries(kk).UserData.SeriesData = series;
            eseries(kk).UserData.XData      = eseries(kk).XData(1);
            eseries(kk).UserData.YData      = series.YData(kk);
          end
          
          f.UserData.Series{length(f.UserData.Series) + 1} = series;
        end
      end
    end
    
    for ii = 1 : length(sites)
      if ii == 1
        f.set('Name', sprintf('%s', sites{ii}));
      else
        f.set('Name', sprintf('%s, %s', f.get('Name'), sprintf('%s', sites{ii})));
      end
    end
    
    if nrflag
      title('Coherence (nonrobust)');
    else
      title('Coherence');
    end
    
    ylim([0, 1]);
    yticks(0 : 0.25 : 1);
    ylabel('Coherence');
    xlabel('Period [seconds]');
    
    legends = cell(0);
    
    if exflag
      legends{length(legends) + 1} = 'Ex';
    end
    
    if eyflag
      legends{length(legends) + 1} = 'Ey';
    end
    
    if hzflag
      legends{length(legends) + 1} = 'Hz';
    end
    
    if hxflag
      legends{length(legends) + 1} = 'Hx';
    end
    
    if hyflag
      legends{length(legends) + 1} = 'Hy';
    end
    
    legend(gca, legends);
    
    grid on;
    
    enmask(f);
    repaint(f);
  end
end

function enmask(figure)
  maskSeries = figure.UserData.MaskSeries;
  
  figure.UserData.MaskSeries = {};
  
  for ii = 1 : length(figure.UserData.Series)
    series = figure.UserData.Series{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = false(1, length(series.XData));
    flag = false;
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = true;
          flag = true;
          break;
        end
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      mdata = semilogx(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      mdata.UserData.SeriesType  = 'mdata';
      mdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      mdata.UserData.SeriesError = [];
      
      for jj = 1 : length(filter)
        if filter(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          merror = semilogx(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          merror.UserData.SeriesType = 'merror';
          merror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          merror.UserData.SeriesData = mdata;
          merror.UserData.XData      = errorSeries.UserData.XData;
          merror.UserData.YData      = errorSeries.UserData.YData;
          
          set(merror, 'ButtonDownFcn', @seriesmousedown);
          
          mdata.UserData.SeriesError(length(mdata.UserData.SeriesError) + 1) = merror;
        end
      end
      
      set(mdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.MaskSeries{length(figure.UserData.MaskSeries) + 1} = mdata;
    end
  end
  
  for ii = 1 : length(maskSeries)
    for jj = 1 : length(maskSeries{ii}.UserData.SeriesError)
      delete(maskSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(maskSeries{ii});
  end
end

function repaint(figure)
  repaintSeries = figure.UserData.RepaintSeries;
  
  figure.UserData.RepaintSeries = {};
  
  for ii = 1 : length(figure.UserData.Series)
    series = figure.UserData.Series{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = true(1, length(series.XData));
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = false;
          break;
        end
      end
    end
    
    flag = false;
    
    for jj = 1 : length(filter)
      if filter(jj)
        flag = true;
        break;
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      Color     = series.Color;
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      rdata = semilogx(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      rdata.UserData.SeriesType  = 'rdata';
      rdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      rdata.UserData.SeriesError = [];
      
      for jj = 1 : length(filter)
        if filter(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          Color     = errorSeries.Color;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          rerror = semilogx(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          rerror.UserData.SeriesType = 'rerror';
          rerror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          rerror.UserData.SeriesData = rdata;
          rerror.UserData.XData      = errorSeries.UserData.XData;
          rerror.UserData.YData      = errorSeries.UserData.YData;
          
          set(rerror, 'ButtonDownFcn', @seriesmousedown);
          
          rdata.UserData.SeriesError(length(rdata.UserData.SeriesError) + 1) = rerror;
        end
      end
      
      set(rdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.RepaintSeries{length(figure.UserData.RepaintSeries) + 1} = rdata;
    end
  end
  
  for ii = 1 : length(repaintSeries)
    for jj = 1 : length(repaintSeries{ii}.UserData.SeriesError)
      delete(repaintSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(repaintSeries{ii});
  end
end

function select(figure, x1, y1, x2, y2)
  selectionSeries = figure.UserData.SelectionSeries;
  
  figure.UserData.SelectionSeries = {};
  
  seriesFilter = false(1, length(figure.UserData.Series));
  dataFilter = cell(1, length(figure.UserData.Series));
  dataIndex = cell(1, length(figure.UserData.Series));
  
  for ii = 1 : length(figure.UserData.Series)
    series = figure.UserData.Series{ii};
    
    dataFilter{ii} = false(1, length(series.XData));
    dataIndex{ii} = zeros(0);
    
    for jj = 1 : length(series.XData)
      if series.XData(jj) >= x1 && series.XData(jj) <= x2 && series.YData(jj) >= y1 && series.YData(jj) <= y2
        seriesFilter(ii) = true;
        dataFilter{ii}(jj) = true;
        dataIndex{ii}(length(dataIndex{ii}) + 1) = jj;
      end
    end
    
    if seriesFilter(ii)
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.SelectionSeries{length(figure.UserData.SelectionSeries) + 1} = sdata;
    end
  end
  
  for ii = 1 : length(selectionSeries)
    for jj = 1 : length(selectionSeries{ii}.UserData.SeriesError)
      delete(selectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(selectionSeries{ii});
  end
end

function deselect(figure)
  for ii = 1 : length(figure.UserData.SelectionSeries)
    for jj = 1 : length(figure.UserData.SelectionSeries{ii}.UserData.SeriesError)
      delete(figure.UserData.SelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(figure.UserData.SelectionSeries{ii});
  end
  
  figure.UserData.SelectionSeries = {};
end

function keypress(figure, event)
  shift = false;
  
  for ii = 1 : length(event.Modifier)
    if strcmp(event.Modifier{ii}, 'shift')
      shift = true;
    end
  end
  
  if strcmp(event.Key, 'r') && ~figure.UserData.freezed
    enmask(figure);
    repaint(figure);
    deselect(figure);
  elseif strcmp(event.Key, 'd')
    figure.UserData.freezed = true;
    flag = false;
    
    for ii = 1 : length(figure.UserData.SelectionSeries)
      for jj = 1 : length(figure.UserData.SelectionSeries{ii}.UserData.SeriesIndex)
        [changed, ~] = pmaskadd(figure.UserData.SelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.SelectionSeries{ii}.UserData.SeriesIndex(jj));
        
        if changed
          flag = true;
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'u')
    figure.UserData.freezed = true;
    flag = false;
    
    for ii = 1 : length(figure.UserData.SelectionSeries)
      for jj = 1 : length(figure.UserData.SelectionSeries{ii}.UserData.SeriesIndex)
        [changed, ~] = pmaskremove(figure.UserData.SelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.SelectionSeries{ii}.UserData.SeriesIndex(jj));
        
        if changed
          flag = true;
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'delete') || strcmp(event.Key, 'backspace')
    figure.UserData.freezed = true;
    flag = false;
    
    if shift
      for ii = 1 : length(figure.UserData.SelectionSeries)
        for jj = 1 : length(figure.UserData.SelectionSeries{ii}.UserData.SeriesIndex)
          [changed, ~] = pmaskremove(figure.UserData.SelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.SelectionSeries{ii}.UserData.SeriesIndex(jj));
          
          if changed
            flag = true;
          end
        end
      end
    else
      for ii = 1 : length(figure.UserData.SelectionSeries)
        for jj = 1 : length(figure.UserData.SelectionSeries{ii}.UserData.SeriesIndex)
          [changed, ~] = pmaskadd(figure.UserData.SelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.SelectionSeries{ii}.UserData.SeriesIndex(jj));
          
          if changed
            flag = true;
          end
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'escape')
    deselect(figure);
  end
end

function seriesmousedown(series, ~)
  mousedown(series.Parent, series);
end

function axesmousedown(axes, ~)
  mousedown(axes, []);
end

function mousedown(axes, ~)
  figure = axes.Parent;
  
  if strcmp(figure.SelectionType, 'normal')
    currentPoint = axes.CurrentPoint;
    
    x1 = currentPoint(1, 1);
    y1 = currentPoint(1, 2);
    
    if x1 >= axes.XLim(1) && x1 <= axes.XLim(2)
      if y1 >= axes.YLim(1) && y1 <= axes.YLim(2)
        top = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        left = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        right = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        bottom = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        
        figure.Pointer = 'crosshair';
        figure.WindowButtonMotionFcn = @mousemotion;
        figure.WindowButtonUpFcn = @mouseup;
      end
    end
  end
  
  function mousemotion(~, ~)
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = currentPoint(1, 2);
    
    if x2 < axes.XLim(1)
      x2 = axes.XLim(1);
    elseif x2 > axes.XLim(2)
      x2 = axes.XLim(2);
    end
    
    if y2 < axes.YLim(1)
      y2 = axes.YLim(1);
    elseif y2 > axes.YLim(2)
      y2 = axes.YLim(2);
    end
    
    top.XData = [x1, x2];
    top.YData = [y1, y1];
    
    left.XData = [x1, x1];
    left.YData = [y1, y2];
    
    right.XData = [x2, x2];
    right.YData = [y1, y2];
    
    bottom.XData = [x1, x2];
    bottom.YData = [y2, y2];
    
    drawnow();
  end
  
  function mouseup(~, ~)
    figure.Pointer = 'arrow';
    figure.WindowButtonMotionFcn = [];
    figure.WindowButtonUpFcn = [];
    
    delete(top);
    delete(left);
    delete(right);
    delete(bottom);
    
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = currentPoint(1, 2);
    
    if x1 ~= x2 && y1 ~= y2
      if x1 > x2
        x  = x1;
        x1 = x2;
        x2 = x;
      end
      
      if y1 > y2
        y  = y1;
        y1 = y2;
        y2 = y;
      end
      
      if x1 < axes.XLim(1)
        x1 = axes.XLim(1);
      end
      
      if x2 > axes.XLim(2)
        x2 = axes.XLim(2);
      end
      
      if y1 < axes.YLim(1)
        y1 = axes.YLim(1);
      end
      
      if y2 > axes.YLim(2)
        y2 = axes.YLim(2);
      end
      
      select(figure, x1, y1, x2, y2);
    else
      deselect(figure);
    end
  end
end