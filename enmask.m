function [output] = enmask(input, frequency, startTime, stopTime, mask)
  if isempty(input) || isempty(mask)
    output = input;
  else
    start = mask(:, 1);
    stop  = mask(:, 2);
    
    k = length(start) + 1;
    
    for ii = 1 : length(start)
      if startTime <= start(ii)
        k = ii;
        break;
      end
    end
    
    if k > 1
      if startTime < stop(k - 1)
        k = k - 1;
      end
    end
    
    m = true((stopTime - startTime) * frequency, 1);
    
    for ii = k : length(start)
      if stopTime <= start(ii)
        break;
      end
      
      k1 = (max(start(ii), startTime) - startTime) * frequency;
      k2 = (min(stop(ii), stopTime)   - startTime) * frequency;
      
      m(k1 + 1 : k2, 1) = false;
    end
    
    output = input(m);
  end
end