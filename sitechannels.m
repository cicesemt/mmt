function sitechannels(varargin)
  currentSite = [];
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSite = varargin{k};
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSite)
    currentSite = site(varargin{:});
  end
  
  if ~isempty(currentSite)
    printChannels(currentSite);
  end
end

function printChannels(currentSite)
  names = cell(0);
  
  for ii = 1 : length(currentSite.bands)
    for jj = 1 : length(currentSite.bands(ii).channels)
      ignore = false;
      
      for kk = 1 : length(names)
        if strcmpi(names{kk}, currentSite.bands(ii).channels(jj).channelType)
          ignore = true;
          break;
        end
      end
      
      if ~ignore
        names{length(names) + 1} = currentSite.bands(ii).channels(jj).channelType;
      end
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s\n', names{ii});
  end
end