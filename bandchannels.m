function bandchannels(varargin)
  currentBand = [];
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentBand = varargin{k};
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentBand)
    currentBand = band(varargin{:});
  end
  
  if ~isempty(currentBand)
    for ii = 1 : length(currentBand.channels)
      fprintf('%s\n', currentBand.channels(ii).channelType);
    end
  end
end