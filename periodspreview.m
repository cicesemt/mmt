function [varargout] = periodspreview(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  nfftColumn = false;
  nsectColumn = false;
  nf1Column = false;
  ascendingOrder = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-nfft')
        varargin(:, k) = [];
        nfftColumn = true;
      elseif strcmpi(varargin{k}, '-nsect')
        varargin(:, k) = [];
        nsectColumn = true;
      elseif strcmpi(varargin{k}, '-nf1')
        varargin(:, k) = [];
        nf1Column = true;
      elseif strcmpi(varargin{k}, '-asc')
        varargin(:, k) = [];
        ascendingOrder = true;
      else
        k = k + 1;
      end
    end
  end
  
  info = measinfo();
  
  if ~isempty(info)
    tbwValue = tbw();
    deltatValue = deltat();
    
    nfftValue = nfft();
    nsctincValue = nsctinc();
    nsctmaxValue = nsctmax();
    
    nf1Value = nf1();
    nfincValue = nfinc();
    nfsectValue = nfsect();
    
    nos = info.numberOfSamples;
    offs = 0.57 - 0.07 * tbwValue;
    nfftmax = nfftValue / nsctincValue^(nsctmaxValue - 1);
    
    indexWidth = length(sprintf('%d', nsctmaxValue * nfsectValue));
    nfftWidth = length(sprintf('%d', nfftValue));
    nsectWidth = length(sprintf('%d', fix((nos - nfftmax) / (nfftmax * offs)) + 1));
    nf1Width = length(sprintf('%d', nf1Value + (nfsectValue - 1) * nfincValue));
    periodWidth = length(sprintf('%f', (deltatValue * nfftValue) / nf1Value));
    
    if ascendingOrder
      index = 0;

      for ii = nsctmaxValue : -1 : 1
        nfftCurrentValue = nfftValue / nsctincValue^(ii - 1);

        for jj = nfsectValue : -1 : 1
          nf1CurrentValue = nf1Value + (jj - 1) * nfincValue;

          index = index + 1;
          period = (deltatValue * nfftCurrentValue) / nf1CurrentValue;
          
          if nargout > 0
            varargout{1} = [varargout{1} period];
          else
            fprintf(sprintf('%%%dd\t', indexWidth), index);
            
            if nfftColumn
              if jj == nfsectValue
                fprintf(sprintf('%%%dd\t', nfftWidth), nfftCurrentValue);
              else
                fprintf(sprintf('%%%ds\t', nfftWidth), ' ');
              end
            end
            
            if nsectColumn
              if jj == nfsectValue
                fprintf(sprintf('%%%dd\t', nsectWidth), fix((nos - nfftCurrentValue) / (nfftCurrentValue * offs)) + 1);
              else
                fprintf(sprintf('%%%ds\t', nsectWidth), ' ');
              end
            end
            
            if nf1Column
              fprintf(sprintf('%%%dd\t', nf1Width), nf1CurrentValue);
            end
            
            fprintf(sprintf('%%%df\n', periodWidth), period);
          end
        end
      end
    else
      index = nsctmaxValue * nfsectValue + 1;
      
      for ii = 1 : nsctmaxValue
        nfftCurrentValue = nfftValue / nsctincValue^(ii - 1);
        
        for jj = 1 : nfsectValue
          nf1CurrentValue = nf1Value + (jj - 1) * nfincValue;
          
          index = index - 1;
          period = (deltatValue * nfftCurrentValue) / nf1CurrentValue;
          
          if nargout > 0
            varargout{1} = [varargout{1} period];
          else
            fprintf(sprintf('%%%dd\t', indexWidth), index);
            
            if nfftColumn
              if jj == 1
                fprintf(sprintf('%%%dd\t', nfftWidth), nfftCurrentValue);
              else
                fprintf(sprintf('%%%ds\t', nfftWidth), ' ');
              end
            end
            
            if nsectColumn
              if jj == 1
                fprintf(sprintf('%%%dd\t', nsectWidth), fix((nos - nfftCurrentValue) / (nfftCurrentValue * offs)) + 1);
              else
                fprintf(sprintf('%%%ds\t', nsectWidth), ' ');
              end
            end
            
            if nf1Column
              fprintf(sprintf('%%%dd\t', nf1Width), nf1CurrentValue);
            end
            
            fprintf(sprintf('%%%df\n', periodWidth), period);
          end
        end
      end
    end
    
    if nargout > 0
      varargout{1} = varargout{1}';
    end
  end
end