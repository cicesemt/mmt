function atsfile = atsfile(filename)
  atsfile = [];
  
  file = dir(filename);
  
  if length(file) == 1 && ~file.isdir && length(file.name) >= 30
    filetype = file.name(length(file.name) - 3 : length(file.name));
    
    if strcmpi(filetype, '.ATS')
      u0 = file.name( 4 :  4);
      u1 = file.name( 8 :  8);
      u2 = file.name(12 : 12);
      u3 = file.name(17 : 17);
      u4 = file.name(21 : 21);
      u5 = file.name(24 : 24);
      
      if strcmp(u0, '_') && strcmp(u1, '_') && strcmp(u2, '_') && strcmp(u3, '_') && strcmp(u4, '_') && strcmp(u5, '_')
        v = file.name( 5 :  5);
        c = file.name( 9 :  9);
        r = file.name(13 : 13);
        t = file.name(18 : 18);
        b = file.name(22 : 22);
        u = file.name(length(file.name) - 4 : length(file.name) - 4);
        
        if strcmpi(v, 'V') && strcmpi(c, 'C') && strcmpi(r, 'R') && strcmpi(t, 'T') && strcmpi(b, 'B') && (strcmpi(u, 'H') || strcmpi(u, 'S'))
          aduSerialNumber = str2double(file.name( 1 :  3));
          xmlVersion      = str2double(file.name( 6 :  7));
          channelNumber   = str2double(file.name(10 : 11));
          runNumber       = str2double(file.name(14 : 16));
          channelType     = file.name(19 : 20);
          boardType       = file.name(23 : 23);
          samplingRate    = str2double(file.name(25 : length(file.name) - 5));
          
          if ~isnan(aduSerialNumber) && ~isnan(xmlVersion) && ~isnan(channelNumber) && ~isnan(runNumber) && ~isnan(samplingRate)
            if strcmpi(u, 'S')
              measurementUnit = 'S';
              frequency = 1 / samplingRate;
            else
              frequency = samplingRate;
              measurementUnit = 'H';
            end
            
            if strcmpi(channelType, 'EX')
              channelType = 'Ex';
            elseif strcmpi(channelType, 'EY')
              channelType = 'Ey';
            elseif strcmpi(channelType, 'HX')
              channelType = 'Hx';
            elseif strcmpi(channelType, 'HY')
              channelType = 'Hy';
            elseif strcmpi(channelType, 'HZ')
              channelType = 'Hz';
            end
            
            if strcmp(channelType, 'Ex') || strcmp(channelType, 'Ey') || strcmp(channelType, 'Hx') || strcmp(channelType, 'Hy') || strcmp(channelType, 'Hz')
              if strcmpi(boardType, 'L')
                boardType = 'LF';
              elseif strcmpi(boardType, 'H')
                boardType = 'HF';
              elseif strcmpi(boardType, 'M')
                boardType = 'MF';
              end
              
              if strcmp(boardType, 'LF') || strcmp(boardType, 'MF') || strcmp(boardType, 'HF')
                if file.bytes >= 1024 && mod(file.bytes - 1024, 4) == 0
                  fp = fopen(sprintf('%s%s%s', file.folder, filesep, file.name), 'r');
                  
                  if fp ~= -1
                    header.length  = fread(fp, 1, 'int16');
                    header.version = fread(fp, 1, 'int16');
                    
                    if header.length == 1024 && (header.version == 80 || header.version == 75)
                      header.numberOfSamples    = fread(fp,   1, 'int32');
                      header.frequency          = fread(fp,   1, 'float32');
                      header.startTime          = fread(fp,   1, 'int32');
                      header.mvFactor           = fread(fp,   1, 'float64');
                      header.gmtOffset          = fread(fp,   1, 'int32');
                      header.originalFrequency  = fread(fp,   1, 'float32');
                      header.aduSerialNumber    = fread(fp,   1, 'int16');
                      header.adcSerialNumber    = fread(fp,   1, 'int16');
                      header.channelNumber      = fread(fp,   1, 'int8');
                      header.chopper            = fread(fp,   1, 'int8');
                      header.channelType        = fread(fp,   2, 'int8');
                      header.sensorType         = fread(fp,   6, 'int8');
                      header.sensorSerialNumber = fread(fp,   1, 'int16');
                      header.posX1              = fread(fp,   1, 'float32');
                      header.posY1              = fread(fp,   1, 'float32');
                      header.posZ1              = fread(fp,   1, 'float32');
                      header.posX2              = fread(fp,   1, 'float32');
                      header.posY2              = fread(fp,   1, 'float32');
                      header.posZ2              = fread(fp,   1, 'float32');
                      header.dipoleLength       = fread(fp,   1, 'float32');
                      header.angle              = fread(fp,   1, 'float32');
                      header.probeRes           = fread(fp,   1, 'float32');
                      header.dcOffset           = fread(fp,   1, 'float32');
                      header.gainStage1         = fread(fp,   1, 'float32');
                      header.gainStage2         = fread(fp,   1, 'float32');
                      header.latitude           = fread(fp,   1, 'int32');
                      header.longitude          = fread(fp,   1, 'int32');
                      header.elevation          = fread(fp,   1, 'int32');
                      header.latLongType        = fread(fp,   1, 'int8');
                      header.addCoordType       = fread(fp,   1, 'int8');
                      header.gaussRefMeridian   = fread(fp,   1, 'int16');
                      header.hochwert           = fread(fp,   1, 'float64');
                      header.rechtswert         = fread(fp,   1, 'float64');
                      header.gpsStat            = fread(fp,   1, 'int8');
                      header.gpsAccuracy        = fread(fp,   1, 'int8');
                      header.utcOffset          = fread(fp,   1, 'int16');
                      header.systemType         = fread(fp,  12, 'int8');
                      header.surveyHeaderName   = fread(fp,  12, 'int8');
                      header.measurementType    = fread(fp,   4, 'int8');
                      header.dcOffsetCorrValue  = fread(fp,   1, 'float64');
                      header.dcOffsetCorrOn     = fread(fp,   1, 'int8');
                      header.inputDivOn         = fread(fp,   1, 'int8');
                      header.notUsedVar         = fread(fp,   1, 'int16');
                      header.selfTestResult     = fread(fp,   2, 'int8');
                      header.reserved5          = fread(fp,   2, 'int8');
                      header.calFreqs           = fread(fp,   1, 'int16');
                      header.calEntryLength     = fread(fp,   1, 'int16');
                      header.calVersion         = fread(fp,   1, 'int16');
                      header.calStartAddress    = fread(fp,   1, 'int16');
                      header.lfFilters          = fread(fp,   8, 'int8');
                      header.aduCalFilename     = fread(fp,  12, 'int8');
                      header.aduCalTime         = fread(fp,   1, 'int32');
                      header.sensorCalFilename  = fread(fp,  12, 'int8');
                      header.sensorCalTime      = fread(fp,   1, 'int32');
                      header.powerlineFreq1     = fread(fp,   1, 'float32');
                      header.powerlineFreq2     = fread(fp,   1, 'float32');
                      header.hfFilters          = fread(fp,   8, 'int8');
                      header.originalMvFactor   = fread(fp,   1, 'float64');
                      header.unsedVar           = fread(fp,   1, 'int32');
                      header.boardType          = fread(fp,   4, 'int8');
                      header.client             = fread(fp,  16, 'int8');
                      header.contractor         = fread(fp,  16, 'int8');
                      header.area               = fread(fp,  16, 'int8');
                      header.surveyID           = fread(fp,  16, 'int8');
                      header.operator           = fread(fp,  16, 'int8');
                      header.reserved           = fread(fp, 112, 'int8');
                      header.xmlHeader          = fread(fp,  64, 'int8');
                      header.comments           = fread(fp, 512, 'int8');
                      
                      if header.lfFilters(1) == 8
                        header.lfFilters(1) = 0;
                      end
                      
                      header.channelType       = deblank(sprintf('%s', header.channelType));
                      header.sensorType        = deblank(sprintf('%s', header.sensorType));
                      header.latLongType       = deblank(sprintf('%s', header.latLongType));
                      header.gpsStat           = deblank(sprintf('%s', header.gpsStat));
                      header.systemType        = deblank(sprintf('%s', header.systemType));
                      header.surveyHeaderName  = deblank(sprintf('%s', header.surveyHeaderName));
                      header.measurementType   = deblank(sprintf('%s', header.measurementType));
                      header.selfTestResult    = deblank(sprintf('%s', header.selfTestResult));
                      header.reserved5         = deblank(sprintf('%s', header.reserved5));
                      header.lfFilters         = deblank(sprintf('%s', header.lfFilters));
                      header.aduCalFilename    = deblank(sprintf('%s', header.aduCalFilename));
                      header.sensorCalFilename = deblank(sprintf('%s', header.sensorCalFilename));
                      header.hfFilters         = deblank(sprintf('%s', header.hfFilters));
                      header.boardType         = deblank(sprintf('%s', header.boardType));
                      header.client            = deblank(sprintf('%s', header.client));
                      header.contractor        = deblank(sprintf('%s', header.contractor));
                      header.area              = deblank(sprintf('%s', header.area));
                      header.surveyID          = deblank(sprintf('%s', header.surveyID));
                      header.operator          = deblank(sprintf('%s', header.operator));
                      header.reserved          = deblank(sprintf('%s', header.reserved));
                      header.xmlHeader         = deblank(sprintf('%s', header.xmlHeader));
                      header.comments          = deblank(sprintf('%s', header.comments));
                      
                      if ~isempty(strfind(header.comments, 'weather:'))
                        header.weather  = deblank(sprintf('%s', extractAfter(header.comments, 'weather:')));
                        header.comments = deblank(sprintf('%s', extractBefore(header.comments, 'weather:')));
                      else
                        header.weather = '';
                      end
                      
                      if header.version == 75
                        if channelNumber == 5
                          channelNumber = 0;
                        elseif channelNumber == 6
                          channelNumber = 1;
                        elseif channelNumber == 7
                          channelNumber = 2;
                        elseif channelNumber == 8
                          channelNumber = 3;
                        elseif channelNumber == 9
                          channelNumber = 4;
                        end
                      end
                      
                      if header.aduSerialNumber == aduSerialNumber && header.channelNumber == channelNumber && strcmpi(header.channelType, channelType) && strcmpi(header.boardType, boardType) && header.frequency == frequency
                        if strcmpi(header.systemType, 'ADU07') && (strcmpi(header.measurementType, 'MT') || strcmpi(header.measurementType, 'CSAM'))
                          numberOfSamples = (file.bytes - 1024) / 4;
                          totalSeconds    = fix(numberOfSamples / frequency);
                          numberOfSamples = totalSeconds * frequency;
                          
                          startTime = header.startTime;
                          stopTime  = startTime + totalSeconds;
                          
                          seconds = totalSeconds;
                          days    = fix(seconds/86400);
                          seconds = mod(seconds,86400);
                          hours   = fix(seconds/3600);
                          seconds = mod(seconds,3600);
                          minutes = fix(seconds/60);
                          seconds = mod(seconds,60);
                          
                          atsfile.name              = file.name;
                          atsfile.folder            = file.folder;
                          atsfile.bytes             = file.bytes;
                          atsfile.aduSerialNumber   = aduSerialNumber;
                          atsfile.xmlVersion        = xmlVersion;
                          atsfile.channelNumber     = channelNumber;
                          atsfile.runNumber         = runNumber;
                          atsfile.channelType       = channelType;
                          atsfile.boardType         = boardType;
                          atsfile.samplingRate      = samplingRate;
                          atsfile.measurementUnit   = measurementUnit;
                          atsfile.numberOfSamples   = numberOfSamples;
                          atsfile.frequency         = frequency;
                          atsfile.startTime         = startTime;
                          atsfile.stopTime          = stopTime;
                          atsfile.days              = days;
                          atsfile.hours             = hours;
                          atsfile.minutes           = minutes;
                          atsfile.seconds           = seconds;
                          atsfile.totalSeconds      = totalSeconds;
                          atsfile.header            = header;
                        end
                      end
                    end
                    
                    fclose(fp);
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end