function value = fparam(name, defaultValue, varargin)
  value = [];
  
  globalParam = false;
  
  if strcmpi(name, 'npcsm')
    globalParam = true;
  elseif strcmpi(name, 'nptsm')
    globalParam = true;
  elseif strcmpi(name, 'nptssm')
    globalParam = true;
  elseif strcmpi(name, 'noutm')
    globalParam = true;
  elseif strcmpi(name, 'ninpm')
    globalParam = true;
  elseif strcmpi(name, 'nrefm')
    globalParam = true;
  elseif strcmpi(name, 'nrsitem')
    globalParam = true;
  elseif strcmpi(name, 'nsectm')
    globalParam = true;
  elseif strcmpi(name, 'nblk')
    globalParam = true;
  elseif strcmpi(name, 'nfsm')
    globalParam = true;
  elseif strcmpi(name, 'nfm')
    globalParam = true;
  elseif strcmpi(name, 'gfortran')
    globalParam = true;
  end
  
  dname = [];
  fname = [];
  
  if globalParam
    dname = mfilename('fullpath');
    dname = dname(1 : length(dname) - length(mfilename()));
    
    if length(dname) > 1 && strcmp(dname(length(dname)), filesep)
      dname = dname(1 : length(dname) - 1);
    end
    
    dname = sprintf('%s%s.mt', dname, filesep);
    fname = sprintf('%s%s%s', dname, filesep, name);
  else
    files = local();
    
    if ~isempty(files)
      dname = '.mt';
      fname = sprintf('%s%s%s', dname, filesep, name);
    end
  end
  
  if ~isempty(fname)
    value = defaultValue;
    
    if nargin > 2
      v = value;
      
      if ischar(varargin{1}) || isstring(varargin{1})
        v = str2double(varargin{1});
      elseif isnumeric(varargin{1})
        v = varargin{1};
      end
      
      if ~isnan(v)
        value = v;
      end
      
      [mtdir, ~] = mkdir(dname);
      
      if mtdir
        fp = fopen(fname, 'wt');
        
        if fp ~= -1
          fprintf(fp, '%.12f\n', value);
          fclose(fp);
        end
      end
    else
      fp = fopen(fname, 'rt');
      
      if fp ~= -1
        v = fgetl(fp);
        
        if ischar(v)
          v = str2double(v);
          
          if ~isnan(v)
            value = v;
          end
        end
        
        fclose(fp);
      end
    end
  end
end