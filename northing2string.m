function string = northing2string(northing)
  string = '';
  len = length(sprintf('%d', fix(northing)));
  
  if len < 7
    for ii = 1 : 7 - len
      string = sprintf('%s0', string);
    end
  end
  
  string = sprintf('%s%.2f m N', string, northing);
end