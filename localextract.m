function localextract(varargin)
  files = local();
  
  if ~isempty(files)
    info = localinfo();
    
    if ~isempty(info)
      startTime = [];
      stopTime  = [];
      
      if nargin > 0
        if nargin == 2
          if isnumeric(varargin{1}) && isnumeric(varargin{2})
            startTime = varargin{1};
            stopTime  = varargin{2};
          end
        end
      end
      
      if isempty(startTime)
        startTime = info.startTime;
      end
      
      if isempty(stopTime)
        stopTime = info.stopTime;
      end
      
      folder = atsfolder(pwd());
      
      if ~isempty(folder)
        if folder.defaultFormat || folder.longFormat
          year   = datestr(datetime(startTime, 'convertfrom','posixtime'), 'yyyy');
          month  = datestr(datetime(startTime, 'convertfrom','posixtime'), 'mm');
          day    = datestr(datetime(startTime, 'convertfrom','posixtime'), 'dd');
          hour   = datestr(datetime(startTime, 'convertfrom','posixtime'), 'HH');
          minute = datestr(datetime(startTime, 'convertfrom','posixtime'), 'MM');
          second = datestr(datetime(startTime, 'convertfrom','posixtime'), 'SS');
          
          name = sprintf('%s_%s-%s-%s_%s-%s-%s', folder.meas, year, month, day, hour, minute, second);
          
          if folder.longFormat
            name = sprintf('%s_%d%s', name, folder.samplingRate, folder.measurementUnit);
          end
        else
          name = folder.name;
        end
        
        parent = folder.parent;
        
        if strcmp(parent(length(parent)), filesep)
          parent = parent(1 : length(parent) - 1);
        end
        
        path = sprintf('%s%s%s', parent, filesep, name);
        pathSuccess = false;
        k = 0;
        
        while true
          if isempty(dir(path))
            [pathSuccess, ~] = mkdir(path);
            break;
          end
          
          k = k + 1;
          
          if k > 1
            path = sprintf('%s%s%s_extract_%d', parent, filesep, name, k);
          else
            path = sprintf('%s%s%s_extract', parent, filesep, name);
          end
        end
        
        if pathSuccess
          for ii = 1 : length(files)
            atsextract(sprintf('%s%s%s', files(ii).folder, filesep, files(ii).name), startTime, stopTime, path);
          end
        end
      end
    end
  end
end