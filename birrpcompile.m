function birrpcompile()
  mmtpath = mfilename('fullpath');
  mmtpath = mmtpath(1 : length(mmtpath) - length(mfilename()));
  
  if length(mmtpath) > 1 && strcmp(mmtpath(length(mmtpath)), filesep)
    mmtpath = mmtpath(1 : length(mmtpath) - 1);
  end
  
  homepath = sprintf('%s%s%s', mmtpath, filesep, 'birrp');
  sourcepath = sprintf('%s%s%s', homepath, filesep, 'src');
  parameters = sprintf('%s%s%s', sourcepath, filesep, 'parameters.h');
  success = false;

  fp = fopen(parameters, 'wt');
  
  if fp ~= -1
    nrsitem('-reset');
    
    fprintf(fp, 'c  birrp adjustable parameters\n');
    fprintf(fp, 'c  compile time logical switch, .true. for stand-alone interactive version,\n');
    fprintf(fp, 'c  .false. for non-interactive use\n');
    fprintf(fp, '        logical inter\n');
    fprintf(fp, '        parameter (inter=.true.)\n');
    fprintf(fp, 'c  \n');
    fprintf(fp, 'c  npcsm = maximum number of data pieces\n');
    fprintf(fp, 'c  nptsm = maximum number of data\n');
    fprintf(fp, 'c  nptssm = maximum fft section length\n');
    fprintf(fp, 'c  noutm = maximum number of output time series\n');
    fprintf(fp, 'c  ninpm = maximum number of input time series\n');
    fprintf(fp, 'c  nrefm = maximum number of reference time series, must be .ge. ninpm\n');
    fprintf(fp, 'c  nrsitem = maximum number of reference sites\n');
    fprintf(fp, 'c  nsectm = maximum number of data sections\n');
    fprintf(fp, 'c  \n');
    fprintf(fp, '        parameter (npcsm=%d,nptsm=%d,nptssm=%d)\n', npcsm(), nptsm(), nptssm());
    fprintf(fp, '        parameter (noutm=%d,ninpm=%d,nrefm=%d,nrsitem=%d)\n', noutm(), ninpm(), nrefm(), nrsitem());
    fprintf(fp, '        parameter (nsectm=%d)\n', nsectm());
    
    nblk('-reset');
    nfsm('-reset');
    nfm('-reset');
    
    success = true;
    
    fclose(fp);
  end
  
  if success
    input01 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'birrp.f');
    input02 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'coherence.f');
    input03 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'dataft.f');
    input04 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'diagnostic.f');
    input05 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'fft.f');
    input06 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'filter.f');
    input07 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'header.f');
    input08 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'input.f');
    input09 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'math.f');
    input10 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'rarfilt.f');
    input11 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'response.f');
    input12 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'rtpss.f');
    input13 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'utils.f');
    input14 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'weight.f');
    input15 = sprintf('%s%s%s%s%s', sourcepath, filesep, 'zlinpack.f');
    
    output = sprintf('%s%s%s', homepath, filesep, 'birrp');
    
    command = sprintf('%s -std=legacy -o %s -I%s', gfortran(), output, sourcepath);
    command = sprintf('%s %s', command, input01);
    command = sprintf('%s %s', command, input02);
    command = sprintf('%s %s', command, input03);
    command = sprintf('%s %s', command, input04);
    command = sprintf('%s %s', command, input05);
    command = sprintf('%s %s', command, input06);
    command = sprintf('%s %s', command, input07);
    command = sprintf('%s %s', command, input08);
    command = sprintf('%s %s', command, input09);
    command = sprintf('%s %s', command, input10);
    command = sprintf('%s %s', command, input11);
    command = sprintf('%s %s', command, input12);
    command = sprintf('%s %s', command, input13);
    command = sprintf('%s %s', command, input14);
    command = sprintf('%s %s', command, input15);

    [status, result] = system(command);
    
    if status
      disp(result);
    end
  end
end