function [changed, mask] = tmaskremove(startTime, stopTime)
  changed = false;
  mask = [];
  
  if startTime < stopTime
    mask = tmask();
    
    if ~isempty(mask)
      start = mask(:, 1);
      stop = mask(:, 2);
      
      k = length(start) + 1;
      
      for ii = 1 : length(start)
        if startTime <= start(ii)
          k = ii;
          break;
        end
      end
      
      if k > 1
        if startTime < stop(k - 1)
          if stopTime < stop(k - 1)
            for ii = length(start) : -1 : k
              start(ii + 1) = start(ii);
              stop(ii + 1) = stop(ii);
            end
            
            start(k) = stopTime;
            stop(k) = stop(k - 1);
          end
          
          stop(k - 1) = startTime;
          changed = true;
        end
      end
      
      while k <= length(start)
        if stopTime <= start(k)
          break;
        elseif stopTime < stop(k)
          start(k) = stopTime;
          changed = true;
          
          break;
        else
          start(k) = [];
          stop(k) = [];
          changed = true;
        end
      end
    end
  end
  
  if changed
    mask = zeros(length(start), 2);
    
    mask(:, 1) = start;
    mask(:, 2) = stop;
    
    channels = local();
    
    if ~isempty(channels)
      [mtdir, ~] = mkdir('.mt');
      
      if mtdir
        fp = fopen(sprintf('%s%s%s', '.mt', filesep, 'tmask'), 'wt');
        
        if fp ~= -1
          [n, ~] = size(mask);
          
          for ii = 1 : n
            fprintf(fp, '%d\t%d\n', mask(ii, 1), mask(ii, 2));
          end
          
          fclose(fp);
        end
      end
    end
    
    broadcast();
  end
end

function broadcast()
  f = get(groot, 'Children');
  
  for ii = 1 : length(f)
    if ~isfield(f(ii).UserData, 'id') || ~isfield(f(ii).UserData, 'folder')
      continue;
    end
    
    if ~strcmp(f(ii).UserData.id, 'atsplot')
      continue;
    end
    
    if ~strcmp(f(ii).UserData.folder, pwd())
      continue;
    end
    
    e.Modifier = {};
    e.Key = 'r';
    
    f(ii).KeyPressFcn(f(ii), e);
  end
end