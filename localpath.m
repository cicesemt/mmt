function localpath()
  localInfo = localinfo();
  
  if ~isempty(localInfo)
    printLocalPath(localInfo);
  end
end

function printLocalPath(localInfo)
  [parentPath, bandName] = fileparts(localInfo.folder);
  [parentPath, siteName] = fileparts(parentPath);
  [~, surveyName] = fileparts(parentPath);
  
  upAndRight = 9492;
  horizontal = 9472;
  
  fprintf('%s\n', surveyName);
  fprintf('%c%c %s\n', upAndRight, horizontal, siteName);
  fprintf('   %c%c %s\n', upAndRight, horizontal, bandName);
end