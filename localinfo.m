function [varargout] = localinfo()
  if nargout > 0
    varargout{1} = [];
  end
  
  files = local();
  
  if ~isempty(files)
    maskinfo = tmaskinfo();
    
    name          = strings(1, length(files));
    bytes         = zeros(1, length(files));
    channelNumber = zeros(1, length(files));
    channelType   = strings(1, length(files));
    
    name(1)          = files(1).name;
    bytes(1)         = files(1).bytes;
    channelNumber(1) = files(1).channelNumber;
    channelType(1)   = files(1).channelType;
    
    pivot = 1;
    
    for ii = 2 : length(files)
      if files(pivot).numberOfSamples > files(ii).numberOfSamples
        pivot = ii;
      end
      
      name(ii)          = files(ii).name;
      bytes(ii)         = files(ii).bytes;
      channelNumber(ii) = files(ii).channelNumber;
      channelType(ii)   = files(ii).channelType;
    end
    
    file = files(pivot);
    
    info.name              = name;
    info.folder            = file.folder;
    info.bytes             = bytes;
    info.aduSerialNumber   = file.aduSerialNumber;
    info.xmlVersion        = file.xmlVersion;
    info.channelNumber     = channelNumber;
    info.runNumber         = file.runNumber;
    info.channelType       = channelType;
    info.boardType         = file.boardType;
    info.samplingRate      = file.samplingRate;
    info.measurementUnit   = file.measurementUnit;
    info.numberOfChannels  = length(files);
    info.numberOfSamples   = file.numberOfSamples;
    info.frequency         = file.frequency;
    
    if ~isempty(maskinfo)
      info.samplesAfterMask = file.numberOfSamples - (maskinfo.totalSeconds * file.frequency);
    end
    
    info.startTime         = file.startTime;
    info.stopTime          = file.stopTime;
    info.days              = file.days;
    info.hours             = file.hours;
    info.minutes           = file.minutes;
    info.seconds           = file.seconds;
    info.totalSeconds      = file.totalSeconds;
    
    if nargout > 0
      varargout{1} = info;
    else
      fprintf('name:                ');
      
      for ii = 1 : length(info.name)
        if ii > 1
          fprintf(' ');
        end
        
        fprintf('%s', info.name(ii));
      end
      
      fprintf('\n');
      fprintf('folder:              %s\n', info.folder);
      fprintf('bytes:               ');
      
      for ii = 1 : length(info.bytes)
        if ii > 1
          fprintf(' ');
        end
        
        fprintf('%d', info.bytes(ii));
      end
      
      fprintf('\n');
      fprintf('adu serial number:   %d\n', info.aduSerialNumber);
      fprintf('xml version:         %d\n', info.xmlVersion);
      fprintf('channel number:      ');
      
      for ii = 1 : length(info.channelNumber)
        if ii > 1
          fprintf(' ');
        end
        
        fprintf('%d', info.channelNumber(ii));
      end
      
      fprintf('\n');
      fprintf('run number:          %d\n', info.runNumber);
      fprintf('channel type:        ');
      
      for ii = 1 : length(info.channelType)
        if ii > 1
          fprintf(' ');
        end
        
        fprintf('%s', info.channelType(ii));
      end
      
      fprintf('\n');
      fprintf('board type:          %s\n', info.boardType);
      fprintf('sampling rate:       %d\n', info.samplingRate);
      fprintf('measurement unit:    %s\n', info.measurementUnit);
      fprintf('number of channels:  %d\n', info.numberOfChannels);
      fprintf('number of samples:   %d\n', info.numberOfSamples);
      fprintf('frequency:           %f\n', info.frequency);
      
      if ~isempty(maskinfo)
        fprintf('samples after mask:  %d\n', info.samplesAfterMask);
      end
      
      fprintf('start time:          %s (%d)\n', datestr(datetime(info.startTime, 'convertfrom', 'posixtime'), 'mmmm dd yyyy HH:MM:SS'), info.startTime);
      fprintf('stop time:           %s (%d)\n', datestr(datetime(info.stopTime, 'convertfrom', 'posixtime'), 'mmmm dd yyyy HH:MM:SS'), info.stopTime);
      fprintf('time length:         ');
      
      if info.days == 1
        fprintf('1 day ');
      else
        fprintf('%d days ', info.days);
      end
      
      if info.hours == 1
        fprintf('1 hour ');
      else
        fprintf('%d hours ', info.hours);
      end
      
      if info.minutes == 1
        fprintf('1 minute ');
      else
        fprintf('%d minutes ', info.minutes);
      end
      
      if info.seconds == 1
        fprintf('1 second\n');
      else
        fprintf('%d seconds\n', info.seconds);
      end
      
      fprintf('total seconds:       %d\n', info.totalSeconds);
    end
  end
end