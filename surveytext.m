function [varargout] = surveytext(varargin)
  currentSurvey = [];
  path = [];
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-path')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          path = varargin{k};
          varargin(:, k) = [];
        end
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSurvey)
    if isempty(path)
      path = pwd();
    end
    
    currentSurvey = survey(path);
  end
  
  if ~isempty(currentSurvey)
    if nargout > 0
      varargout{1} = info(currentSurvey, varargin{:});
    else
      info(currentSurvey, varargin{:});
    end
  end
end