function [varargout] = bandtext(varargin)
  currentBand = [];
  path = [];
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentBand = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-path')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          path = varargin{k};
          varargin(:, k) = [];
        end
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentBand)
    if isempty(path)
      path = pwd();
    end
    
    currentBand = band(path);
  end
  
  if ~isempty(currentBand)
    if nargout > 0
      varargout{1} = info(currentBand, varargin{:});
    else
      info(currentBand, varargin{:});
    end
  end
end