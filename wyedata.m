function [ex, ey, hx, hy] = wyedata(varargin)
  ex = [];
  ey = [];
  hx = [];
  hy = [];
  
  files = [];
  offset = 0;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        files = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-offset')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          num = str2double(varargin{k});
          varargin(:, k) = [];
          
          if ~isnan(num) && num >= 0
            offset = num;
          end
        end
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(files)
    files = wye(varargin{:});
  end
  
  if length(files) == 1
    fp = fopen(sprintf('%s%s%s', files.folder, filesep, files.name), 'r');
    
    if fp ~= -1
      fseek(fp, offset, 'cof');
      
      [header, headerLength] = fread(fp, 7, 'int16');
      
      if headerLength == 7
        numberOfSamples = header(6);
        numberOfChannels = header(7);
        
        dataLength = numberOfChannels * numberOfSamples;
        data = fread(fp, dataLength, 'int16');
        
        if length(data) == dataLength
          hy = data(1 : numberOfChannels : dataLength);
          ex = data(2 : numberOfChannels : dataLength);
          hx = data(3 : numberOfChannels : dataLength);
          ey = data(4 : numberOfChannels : dataLength);
        end
      end
    end
  end
end