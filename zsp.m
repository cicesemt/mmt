function [s, p] = zsp(zxx, zxy, zyx, zyy)
  a = zxy.value + zyx.value;
  b = zyy.value - zxx.value;
  c = zxy.value - zyx.value;
  d = zxx.value + zyy.value;
  
  theta1 = atan(b ./ a);
  theta2 = atan(d ./ c);
  
  z1 = ((a ./ cos(theta1)) + (c ./ cos(theta2))) * 0.5;
  z2 = ((a ./ cos(theta1)) - (c ./ cos(theta2))) * 0.5;
  
  theta = atan((z1 + z2) ./ (z2 - z1));
  
  s.value  = (z2 - z1) ./ (cos(theta) * 2);
  s.period = (zxx.period + zxy.period + zyx.period + zyy.period) / 4;
  p.value  = (z1 .* z2 .* cos(theta) * 2) ./ (z2 - z1);
  p.period = (zxx.period + zxy.period + zyx.period + zyy.period) / 4;
end