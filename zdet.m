function [value, period, error] = zdet(zxx, zxy, zyx, zyy)
  value  = sqrt(zxx.value .* zyy.value - zxy.value .* zyx.value);
  period = (zxx.period + zxy.period + zyx.period + zyy.period) / 4;
  error  = sqrt((abs(zyy.value) .* zxx.error).^2 + (abs(zyx.value) .* zxy.error).^2 + (abs(zxy.value) .* zyx.error).^2 + (abs(zxx.value) .* zyy.error).^2) ./ (2 * abs(value));
end