function [changed, mask] = pmaskclear(path)
  if isempty(path)
    path = pwd();
  end
  
  mask = pmask('-path', path);
  changed = false;
  
  if ~isempty(mask)
    mask = [];
    changed = true;
  end
  
  if changed
    mtd = mtdir(path);
    
    if ~isempty(mtd)
      fp = fopen(sprintf('%s%s%s', mtd, filesep, 'pmask'), 'wt');
      
      if fp ~= -1
        fclose(fp);
      end
    end
    
    broadcast();
  end
end

function broadcast()
  c = get(groot, 'CurrentFigure');
  f = get(groot, 'Children');
  
  for ii = 1 : length(f)
    if ~isfield(f(ii).UserData, 'id')
      continue;
    end
    
    if strcmp(f(ii).UserData.id, 'mmt-azi-plot') || strcmp(f(ii).UserData.id, 'mmt-coh-plot') || strcmp(f(ii).UserData.id, 'mmt-imp-plot') || strcmp(f(ii).UserData.id, 'mmt-res-plot') || strcmp(f(ii).UserData.id, 'mmt-tip-plot')
      e.Modifier = {};
      e.Key = 'r';

      f(ii).KeyPressFcn(f(ii), e);
    end
  end
  
  if ~isempty(c)
    figure(c);
  end
end