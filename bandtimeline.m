function bandtimeline(varargin)
  currentBand = [];
  
  startTime = [];
  stopTime = [];
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentBand = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          startTime = varargin{k};
          varargin(:, k) = [];
          
          if isnan(str2double(startTime))
            startTime = string2posixtime(startTime);
          else
            startTime = fix(str2double(startTime));
          end
        end
      elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          stopTime = varargin{k};
          varargin(:, k) = [];
          
          if isnan(str2double(stopTime))
            stopTime = string2posixtime(stopTime);
          else
            stopTime = fix(str2double(stopTime));
          end
        end
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentBand)
    currentBand = band(varargin{:});
  end
  
  if ~isempty(currentBand)
    [parentFolder, ~] = fileparts(currentBand.folder);
    [~, siteName] = fileparts(parentFolder);
    
    name = currentBand.name;
    
    if isempty(startTime)
      startTime = currentBand.startTime;
    end
    
    if isempty(stopTime)
      stopTime = currentBand.stopTime;
    end
    
    if startTime < stopTime
      step = (stopTime - startTime) / 4;
      
      t0 = startTime;
      t1 = round(startTime + step);
      t2 = round(startTime + step + step);
      t3 = round(stopTime - step);
      t4 = stopTime;
      
      start = datetime(t0, 'convertfrom', 'posixtime');
      stop = datetime(t4, 'convertfrom', 'posixtime');
      
      startLabel = sprintf('%s', datestr(start, 'yyyy/mm/dd HH:MM:SS'));
      stopLabel = sprintf('%s', datestr(stop, 'yyyy/mm/dd HH:MM:SS'));
      
      if start.Year < stop.Year
        pattern = 'yy/mm/dd HH:MM:SS';
      elseif start.Month < stop.Month
        pattern = 'mm/dd HH:MM:SS';
      elseif start.Day < stop.Day
        pattern = 'dd HH:MM:SS';
      elseif start.Hour < stop.Hour
        pattern = 'HH:MM:SS';
      elseif start.Minute < stop.Minute
        pattern = 'MM:SS';
      else
        pattern = 'SS';
      end
      
      label0 = sprintf('%s', datestr(datetime(t0, 'convertfrom', 'posixtime'), pattern));
      label1 = sprintf('%s', datestr(datetime(t1, 'convertfrom', 'posixtime'), pattern));
      label2 = sprintf('%s', datestr(datetime(t2, 'convertfrom', 'posixtime'), pattern));
      label3 = sprintf('%s', datestr(datetime(t3, 'convertfrom', 'posixtime'), pattern));
      label4 = sprintf('%s', datestr(datetime(t4, 'convertfrom', 'posixtime'), pattern));
      
      f = figure();
      a = axes();
      
      f.set('NumberTitle', 'off');
      f.set('ToolBar', 'none');
      f.set('MenuBar', 'none');
      f.set('Name', siteName);
      
      a.FontSize = 10;
      a.FontWeight = 'normal';
      
      a.Title.String = 'Timeline';
      a.Title.FontSize = 14;
      a.Title.FontWeight = 'bold';
      
      a.XLabel.String = {'', sprintf('%s to %s', startLabel, stopLabel), ''};
      a.XLabel.FontSize = 12;
      a.XLabel.FontWeight = 'bold';
      
      a.XLim = [startTime stopTime];
      a.XTick = startTime : step : stopTime;
      a.XTickLabel = {label0 label1 label2 label3 label4};
      a.XGrid = 'on';
      
      a.YLabel.String = 'Band';
      a.YLabel.FontSize = 12;
      a.YLabel.FontWeight = 'bold';
      
      a.YLim = [0 2];
      a.YTick = 1;
      a.YTickLabel = {name};
      a.YGrid = 'on';
      
      a.Box = 'on';
      hold(a, 'on');
      
      fposition = get(f, 'Position');
      fheight = fposition(4);
      
      aposition = get(a, 'Position');
      aheight = aposition(4) * fheight;
      
      lheight = round(aheight / 3);
      
      x = [currentBand.startTime currentBand.stopTime];
      y = [1 1];
      
      plot(x, y, 'Color', [0 0.4470 0.7410], 'LineStyle', '-', 'LineWidth', lheight);
    end
  end
end