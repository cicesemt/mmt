function [varargout] = cwd()
  if nargout > 0
    varargout{1} = pwd();
  else
    fprintf('%s\n', pwd());
  end
end