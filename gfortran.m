function [varargout] = gfortran(varargin)
  if nargout > 0
    varargout = cell(1, nargout);

    for ii = 1 : nargout
      varargout{ii} = [];
    end
  end

  flag.help = false;
  flag.version = false;

  if nargin > 0
    k = 1;

    while k <= length(varargin)
      if strcmpi(varargin{k}, '-help')
        varargin(:, k) = [];
        flag.help = true;
      elseif strcmpi(varargin{k}, '-version')
        varargin(:, k) = [];
        flag.version = true;
      else
        if length(varargin{k}) > 1 && varargin{k}(1) == '-'
          showerror(sprintf('unknown option ''%s''', varargin{k}));
          return;
        end

        k = k + 1;
      end
    end
  end

  if ~isempty(varargin)
    if flag.help || flag.version
      showerror('wrong argument usage');
      return;
    end
  elseif flag.help && flag.version
    showerror('wrong argument usage');
    return;
  end

  if flag.help
    help();
  elseif flag.version
    version = '';

    [c, v] = system(sprintf('%s -dumpversion', gfortran()));

    if c == 0
      version = sprintf('GFortran %s', deblank(v));
    end

    if nargout > 0
      varargout{1} = version;
    elseif ~isempty(version)
      fprintf('%s\n', version);
    end
  else
    name = 'gfortran';
    value = '';

    dname = mfilename('fullpath');
    dname = dname(1 : length(dname) - length(mfilename()));

    if length(dname) > 1 && strcmp(dname(length(dname)), filesep)
      dname = dname(1 : length(dname) - 1);
    end

    dname = sprintf('%s%s.mt', dname, filesep);
    fname = sprintf('%s%s%s', dname, filesep, name);

    value = loadValue(fname, value);

    if ~isempty(varargin)
      if length(varargin) > 1
        showerror('too many arguments');
        return;
      elseif ~ischar(varargin{1}) && ~isstring(varargin{1})
        showerror('invalid argument value');
        return;
      else
        value = saveValue(dname, fname, varargin{1});

        if nargout > 0
          varargout{1} = value;
        end
      end
    else
      if nargout > 0
        varargout{1} = value;
      elseif ~isempty(value)
        fprintf('%s\n', value);
      end
    end
  end
end

function value = saveValue(dname, fname, value)
  [mtdir, ~] = mkdir(dname);

  if mtdir
    fp = fopen(fname, 'wt');

    if fp ~= -1
      fprintf(fp, '%s\n', value);
      fclose(fp);
    end
  end
end

function value = loadValue(fname, value)
  fp = fopen(fname, 'rt');

  if fp ~= -1
    v = fgetl(fp);

    if v ~= -1
      value = v;
    end

    fclose(fp);
  end
end

function help()
  syntax{1} = '{gfortran} [-help] | [-version] | [<path>]';
  
  description{1} = 'Shows or sets the GFortran path. It also allows you to check the compiler version. All arguments are optional and mutually exclusive.';
  description{2} = 'The {-help} argument shows this help. The {-version} argument returns the version of the GFortran compiler. The {<path>} argument sets the path to the compiler, must be absolute and must include the name of the executable file.';
  description{3} = 'Running the command with no arguments displays the GFortran compiler path.';
  
  examples{1} = 'Sets the GFortran compiler path to /usr/local/bin/gfortran.';
  examples{2} = '>> gfortran /usr/local/bin/gfortran';
  examples{3} = 'Shows the path of the GFortran compiler.';
  examples{4} = '>> gfortran';
  examples{5} = 'Shows the version of the GFortran compiler.';
  examples{6} = '>> gfortran -version';
  
  seealso{1} = 'mmt';
  
  showhelp(syntax, description, examples, seealso);
end