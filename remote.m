function [varargout] = remote(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  findCommand     = false;
  selectCommand   = false;
  deselectCommand = false;
  overlapCommand  = false;
  infoCommand     = false;
  pathCommand     = false;
  plotCommand     = false;
  editCommand     = false;
  exportCommand   = false;
  clearCommand    = false;
  decimateCommand = false;
  
  removeTrend = false;
  ignoreMask  = false;
  startTime   = [];
  stopTime    = [];
  timeLength  = [];
  hertz       = [];
  
  if nargin > 0
    if strcmpi(varargin{1}, 'find')
      varargin(:, 1) = [];
      findCommand = true;
    elseif strcmpi(varargin{1}, 'select')
      varargin(:, 1) = [];
      selectCommand = true;
    elseif strcmpi(varargin{1}, 'deselect')
      varargin(:, 1) = [];
      deselectCommand = true;
    elseif strcmpi(varargin{1}, 'overlap')
      varargin(:, 1) = [];
      overlapCommand = true;
    elseif strcmpi(varargin{1}, 'info')
      varargin(:, 1) = [];
      infoCommand = true;
    elseif strcmpi(varargin{1}, 'path')
      varargin(:, 1) = [];
      pathCommand = true;
    elseif strcmpi(varargin{1}, 'plot')
      varargin(:, 1) = [];
      plotCommand = true;
    elseif strcmpi(varargin{1}, 'edit')
      varargin(:, 1) = [];
      editCommand = true;
    elseif strcmpi(varargin{1}, 'export')
      varargin(:, 1) = [];
      exportCommand = true;
    elseif strcmpi(varargin{1}, 'clear')
      varargin(:, 1) = [];
      clearCommand = true;
    elseif strcmpi(varargin{1}, 'decimate')
      varargin(:, 1) = [];
      decimateCommand = true;
    end
    
    if plotCommand || editCommand || exportCommand
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-detrend')
          varargin(:, k) = [];
          removeTrend = true;
        elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            startTime = varargin{k};
            varargin(:, k) = [];
            
            if ischar(startTime) || isstring(startTime)
              if isnan(str2double(startTime))
                startTime = string2posixtime(startTime);
              else
                startTime = fix(str2double(startTime));
              end
            else
              startTime = [];
            end
          end
        elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            stopTime = varargin{k};
            varargin(:, k) = [];
            
            if ischar(stopTime) || isstring(stopTime)
              if isnan(str2double(stopTime))
                stopTime = string2posixtime(stopTime);
              else
                stopTime = fix(str2double(stopTime));
              end
            else
              stopTime = [];
            end
          end
        elseif strcmpi(varargin{k}, '-length') || strcmpi(varargin{k}, '-timeLength')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            timeLength = varargin{k};
            varargin(:, k) = [];
            
            if ischar(timeLength) || isstring(timeLength)
              if ~isnan(str2double(timeLength))
                timeLength = fix(str2double(timeLength));
                
                if timeLength <= 0
                  timeLength = [];
                end
              else
                timeLength = [];
              end
            else
              timeLength = [];
            end
          end
        else
          k = k + 1;
        end
      end
    end
    
    if exportCommand
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-nomask')
          varargin(:, k) = [];
          ignoreMask = true;
        else
          k = k + 1;
        end
      end
    end
    
    if decimateCommand
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-h') || strcmpi(varargin{k}, '-hertz')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            hertz = varargin{k};
            varargin(:, k) = [];
            
            if ischar(hertz)
              hertz = str2double(hertz);
            end
            
            if isnan(hertz)
              hertz = [];
            end
          end
        elseif strcmpi(varargin{k}, '-s') || strcmpi(varargin{k}, '-seconds')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            hertz = varargin{k};
            varargin(:, k) = [];
            
            if ischar(hertz)
              hertz = str2double(hertz);
            end
            
            if isnan(hertz)
              hertz = [];
            else
              hertz = 1 / hertz;
            end
          end
        else
          k = k + 1;
        end
      end
    end
  end
  
  if ~isempty(varargin)
    path = varargin{1};
  else
    path = '';
  end
  
  if findCommand
    if ~isempty(path)
      if nargout > 0
        varargout{1} = remotefind(path);
      else
        remotefind(path);
      end
    else
      if nargout > 0
        varargout{1} = remotefind('../..');
      else
        remotefind('../..');
      end
    end
  elseif selectCommand
    if ~isempty(path)
      remoteselect(path);
    end
  elseif deselectCommand
    remotedeselect();
  elseif overlapCommand
    remoteoverlap(varargin{:});
  elseif infoCommand
    if nargout > 0
      varargout{1} = remoteinfo();
    else
      remoteinfo();
    end
  elseif pathCommand
    remotepath();
  elseif plotCommand
    if isempty(startTime) && isempty(stopTime)
      if ~isempty(timeLength)
        info = remoteinfo();
        
        if ~isempty(info)
          startTime = info.startTime;
          stopTime = info.stopTime;
          
          if stopTime > startTime
            if stopTime - startTime > timeLength
              stopTime = startTime + timeLength;
            end
          end
        end
      end
    end
    
    remoteplot(removeTrend, startTime, stopTime);
  elseif editCommand
    remoteedit(removeTrend, startTime, stopTime);
  elseif exportCommand
    remoteexport(removeTrend, ignoreMask, startTime, stopTime);
  elseif clearCommand
    fprintf('deprecated!\n');
    %remoteclear();
  elseif decimateCommand
    fprintf('not implemented yet!\n');
    %remotedecimate(hertz);
  else
    fp = fopen(sprintf('%s%s%s', '.mt', filesep, 'remote'), 'rt');
    
    if fp ~= -1
      hxpath = fgetl(fp);
      
      if ischar(hxpath)
        hypath = fgetl(fp);
        
        if ischar(hypath)
          hx = atsfile(hxpath);
          hy = atsfile(hypath);
          
          if ~isempty(hx) && ~isempty(hy)
            if nargout > 0
              varargout{1} = [varargout{1}; hx];
              varargout{1} = [varargout{1}; hy];
            else
              fprintf('%s\n', hx.name);
              fprintf('%s\n', hy.name);
            end
          end
        end
      end
      
      fclose(fp);
    end
  end
end