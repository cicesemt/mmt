function showhelp(syntax, description, examples, seealso)
  cws = matlab.desktop.commandwindow.size;

  min_width = 50;
  max_width = 80;
  inc_width = 10;

  width = min_width;

  while width + inc_width <= cws(1) && width + inc_width <= max_width
    width = width + inc_width;
  end

  if ~isempty(syntax)
    cprintf('*#404040', 'Syntax\n');

    for ii = 1 : length(syntax)
      printHighlight(syntax{ii}, '#808080', '*#808080');
    end

    fprintf('\n');
  end

  if ~isempty(description)
    cprintf('*#404040', 'Description\n');

    for ii = 1 : length(description)
      printParagraph(description{ii}, '#202020', '#808080', width);
      fprintf('\n');
    end
  end

  if ~isempty(examples)
    cprintf('*#404040', 'Examples\n');

    for ii = 1 : 2 : length(examples)
      printParagraph(examples{ii}, '#202020', '#808080', width);
      cprintf('#808080', '%s\n', examples{ii + 1});
      fprintf('\n');
    end
  end

  if ~isempty(seealso)
    cprintf('*#404040', 'See also\n');

    for ii = 1 : length(seealso)
      cprintf('#808080', '%s\n', seealso{ii});
    end

    fprintf('\n');
  end
end

function printParagraph(paragraph, color, highlight, width)
  if ~contains(paragraph, ' ')
    printHighlight(paragraph, color, highlight);
  else
    k = strfind(paragraph, ' ');
    line = '';
    index = 1;

    for jj = 1 : length(k)
      if isempty(line)
        line = sprintf('%s', paragraph(index : k(jj)));
      elseif length(line) + length(paragraph(index : k(jj))) > width + 1
        printHighlight(line(1 : length(line) - 1), color, highlight);
        line = sprintf('%s', paragraph(index : k(jj)));
      else
        line = sprintf('%s%s', line, paragraph(index : k(jj)));
      end

      index = k(jj) + 1;
    end

    if index <= length(paragraph)
      if length(line) + length(paragraph(index : length(paragraph))) > width
        printHighlight(line(1 : length(line) - 1), color, highlight);
        line = sprintf('%s', paragraph(index : length(paragraph)));
      else
        line = sprintf('%s%s', line, paragraph(index : length(paragraph)));
      end
    end

    if ~isempty(line)
      printHighlight(line, color, highlight);
    end
  end
end

function printHighlight(line, color, highlight)
  if contains(line, '{')
    if contains(line, '}')
      k1 = strfind(line, '{');
      k2 = strfind(line, '}');

      if k1(length(k1)) > k2(length(k2))
        line = sprintf('%s}', line);
      end
    else
      line = sprintf('%s}', line);
    end
  end

  if contains(line, '}')
    if contains(line, '{')
      k1 = strfind(line, '{');
      k2 = strfind(line, '}');

      if k1(1) > k2(1)
        line = sprintf('{%s', line);
      end
    else
      line = sprintf('{%s', line);
    end
  end

  if contains(line, '{') && contains(line, '}')
    k1 = strfind(line, '{');
    k2 = strfind(line, '}');

    if length(k1) == length(k2)
      flag = true;

      for ii = 1 : length(k1)
        if k1(ii) > k2(ii)
          flag = false;
          break;
        elseif ii < length(k1)
          if k2(ii) > k1(ii + 1)
            flag = false;
            break;
          end
        end
      end

      if flag
        index = 1;

        for ii = 1 : length(k1)
          if index < k1(ii)
            cprintf(color, '%s', line(index : k1(ii) - 1));
          end

          if k1(ii) + 1 <= k2(ii) - 1
            if highlight(1) == '*'
              cprintf(highlight, '%s\b', line(k1(ii) + 1 : k2(ii) - 1));
            else
              cprintf(highlight, '%s', line(k1(ii) + 1 : k2(ii) - 1));
            end
          end

          index = k2(ii) + 1;
        end

        if index <= length(line)
          cprintf(color, '%s', line(index : length(line)));
        end

        fprintf('\n');
      else
        cprintf(color, '%s\n', line);
      end
    else
      cprintf(color, '%s\n', line);
    end
  else
    cprintf(color, '%s\n', line);
  end
end