function birrpclear(all)
  files = local();
  
  if ~isempty(files)
    filename = ofil();
    
    fn = cell(0);
    
    fn{length(fn) + 1} = sprintf('%s.1n.1c2', filename);
    fn{length(fn) + 1} = sprintf('%s.1n.2c2', filename);
    fn{length(fn) + 1} = sprintf('%s.1n.3c2', filename);
    fn{length(fn) + 1} = sprintf('%s.2n.1c2', filename);
    fn{length(fn) + 1} = sprintf('%s.2n.2c2', filename);
    fn{length(fn) + 1} = sprintf('%s.2n.3c2', filename);
    fn{length(fn) + 1} = sprintf('%s.3n.1c2', filename);
    fn{length(fn) + 1} = sprintf('%s.3n.2c2', filename);
    fn{length(fn) + 1} = sprintf('%s.3n.3c2', filename);
    
    if all
      fn{length(fn) + 1} = sprintf('%s.1r.1c2', filename);
      fn{length(fn) + 1} = sprintf('%s.1r.2c2', filename);
      fn{length(fn) + 1} = sprintf('%s.1r.3c2', filename);
      fn{length(fn) + 1} = sprintf('%s.2r.1c2', filename);
      fn{length(fn) + 1} = sprintf('%s.2r.2c2', filename);
      fn{length(fn) + 1} = sprintf('%s.2r.3c2', filename);
      fn{length(fn) + 1} = sprintf('%s.3r.1c2', filename);
      fn{length(fn) + 1} = sprintf('%s.3r.2c2', filename);
      fn{length(fn) + 1} = sprintf('%s.3r.3c2', filename);
    end
    
    fn{length(fn) + 1} = sprintf('%s.1n1.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.1n2.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.1n3.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.2n1.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.2n2.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.2n3.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.3n1.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.3n2.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.3n3.tf', filename);
    
    fn{length(fn) + 1} = sprintf('%s.1r1.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.1r2.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.1r3.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.2r1.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.2r2.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.2r3.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.3r1.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.3r2.tf', filename);
    fn{length(fn) + 1} = sprintf('%s.3r3.tf', filename);
    
    fn{length(fn) + 1} = sprintf('%s.1n1.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.1n2.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.1n3.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.2n1.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.2n2.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.2n3.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.3n1.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.3n2.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.3n3.rf', filename);
    
    fn{length(fn) + 1} = sprintf('%s.1r1.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.1r2.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.1r3.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.2r1.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.2r2.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.2r3.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.3r1.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.3r2.rf', filename);
    fn{length(fn) + 1} = sprintf('%s.3r3.rf', filename);
    
    fn{length(fn) + 1} = sprintf('%s.1n1.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.1n2.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.1n3.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.2n1.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.2n2.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.2n3.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.3n1.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.3n2.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.3n3.rp', filename);
    
    fn{length(fn) + 1} = sprintf('%s.1r1.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.1r2.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.1r3.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.2r1.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.2r2.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.2r3.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.3r1.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.3r2.rp', filename);
    fn{length(fn) + 1} = sprintf('%s.3r3.rp', filename);
    
    fn{length(fn) + 1} = sprintf('%s.r.cov', filename);
    
    if all
      fn{length(fn) + 1} = sprintf('%s.diag', filename);
      fn{length(fn) + 1} = sprintf('%s.j', filename);
    end
    
    for ii = 1 : length(fn)
      fp = fopen(fn{ii}, 'r');
      
      if fp ~= -1
        fclose(fp);
        delete(fn{ii});
      end
    end
  end
end