function edi(varargin)
  sites = cell(0);
  bands = cell(0);
  
  out = [];
  min = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-site')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          sites{length(sites) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          bands{length(bands) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-out')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          out = varargin{k};
          varargin(:, k) = [];
          
          if length(out) <= length('.edi') || ~strcmpi(out(length(out) - length('.edi') + 1 : length(out)), '.edi')
            out = sprintf('%s.edi', out);
          end
        end
      elseif strcmpi(varargin{k}, '-min')
        varargin(:, k) = [];
        min = true;
      else
        k = k + 1;
      end
    end
  end
  
  path = {pwd()};
  
  jfiles = jselect(path, sites, bands);
  c2files = c2select(path, sites, bands, true, true, false, false, true, false);
  
  if ~isempty(jfiles)
    zxx.value = []; zxx.period = []; zxx.error = [];
    zxy.value = []; zxy.period = []; zxy.error = [];
    zyx.value = []; zyx.period = []; zyx.error = [];
    zyy.value = []; zyy.period = []; zyy.error = [];
    
    tzx.value = []; tzx.period = []; tzx.error = [];
    tzy.value = []; tzy.period = []; tzy.error = [];
    
    rxx.value = []; rxx.period = []; rxx.upper = []; rxx.lower = [];
    rxy.value = []; rxy.period = []; rxy.upper = []; rxy.lower = [];
    ryx.value = []; ryx.period = []; ryx.upper = []; ryx.lower = [];
    ryy.value = []; ryy.period = []; ryy.upper = []; ryy.lower = [];
    
    pxx.value = []; pxx.period = []; pxx.upper = []; pxx.lower = [];
    pxy.value = []; pxy.period = []; pxy.upper = []; pxy.lower = [];
    pyx.value = []; pyx.period = []; pyx.upper = []; pyx.lower = [];
    pyy.value = []; pyy.period = []; pyy.upper = []; pyy.lower = [];
    
    cex.value = []; cex.period = []; cex.upper = []; cex.lower = [];
    cey.value = []; cey.period = []; cey.upper = []; cey.lower = [];
    chz.value = []; chz.period = []; chz.upper = []; chz.lower = [];
    
    ACQDATE = [];
    ENDDATE = [];
    MAXCHAN = [];
    EXCHAN  = [];
    EYCHAN  = [];
    HXCHAN  = [];
    HYCHAN  = [];
    HZCHAN  = [];
    RXCHAN  = [];
    RYCHAN  = [];
    
    for ii = 1 : length(jfiles)
      jfile = jfiles{ii};
      
      m = pmask('-path', jfile.folder);
      b = band(jfile.folder);
      
      [zxx.value, zxx.period, zxx.error] = append3(zxx.value, zxx.period, zxx.error, jfile.zxx.value, jfile.zxx.period, jfile.zxx.error, m);
      [zxy.value, zxy.period, zxy.error] = append3(zxy.value, zxy.period, zxy.error, jfile.zxy.value, jfile.zxy.period, jfile.zxy.error, m);
      [zyx.value, zyx.period, zyx.error] = append3(zyx.value, zyx.period, zyx.error, jfile.zyx.value, jfile.zyx.period, jfile.zyx.error, m);
      [zyy.value, zyy.period, zyy.error] = append3(zyy.value, zyy.period, zyy.error, jfile.zyy.value, jfile.zyy.period, jfile.zyy.error, m);
      
      [tzx.value, tzx.period, tzx.error] = append3(tzx.value, tzx.period, tzx.error, jfile.tzx.value, jfile.tzx.period, jfile.tzx.error, m);
      [tzy.value, tzy.period, tzy.error] = append3(tzy.value, tzy.period, tzy.error, jfile.tzy.value, jfile.tzy.period, jfile.tzy.error, m);
      
      [rxx.value, rxx.period, rxx.upper, rxx.lower] = append4(rxx.value, rxx.period, rxx.upper, rxx.lower, jfile.rxx.value, jfile.rxx.period, jfile.rxx.upper, jfile.rxx.lower, m);
      [rxy.value, rxy.period, rxy.upper, rxy.lower] = append4(rxy.value, rxy.period, rxy.upper, rxy.lower, jfile.rxy.value, jfile.rxy.period, jfile.rxy.upper, jfile.rxy.lower, m);
      [ryx.value, ryx.period, ryx.upper, ryx.lower] = append4(ryx.value, ryx.period, ryx.upper, ryx.lower, jfile.ryx.value, jfile.ryx.period, jfile.ryx.upper, jfile.ryx.lower, m);
      [ryy.value, ryy.period, ryy.upper, ryy.lower] = append4(ryy.value, ryy.period, ryy.upper, ryy.lower, jfile.ryy.value, jfile.ryy.period, jfile.ryy.upper, jfile.ryy.lower, m);
      
      [pxx.value, pxx.period, pxx.upper, pxx.lower] = append4(pxx.value, pxx.period, pxx.upper, pxx.lower, jfile.pxx.value, jfile.pxx.period, jfile.pxx.upper, jfile.pxx.lower, m);
      [pxy.value, pxy.period, pxy.upper, pxy.lower] = append4(pxy.value, pxy.period, pxy.upper, pxy.lower, jfile.pxy.value, jfile.pxy.period, jfile.pxy.upper, jfile.pxy.lower, m);
      [pyx.value, pyx.period, pyx.upper, pyx.lower] = append4(pyx.value, pyx.period, pyx.upper, pyx.lower, jfile.pyx.value, jfile.pyx.period, jfile.pyx.upper, jfile.pyx.lower, m);
      [pyy.value, pyy.period, pyy.upper, pyy.lower] = append4(pyy.value, pyy.period, pyy.upper, pyy.lower, jfile.pyy.value, jfile.pyy.period, jfile.pyy.upper, jfile.pyy.lower, m);
      
      if isempty(ACQDATE) || ACQDATE > b.startTime
        ACQDATE = b.startTime;
      end
      
      if isempty(ENDDATE) || ENDDATE < b.stopTime
        ENDDATE = b.stopTime;
      end
      
%       if ~isempty(b.channels.ex) && ~isempty(b.channels.ey) && ~isempty(b.channels.hx) && ~isempty(b.channels.hy)
%         maxchan = 4;
%         
%         if ~isempty(b.channels.hz)
%           maxchan = maxchan + 1;
%         end
%         
%         if ~isempty(b.channels.rx) && ~isempty(b.channels.ry)
%           maxchan = maxchan + 2;
%         end
%         
%         if isempty(MAXCHAN)
%           MAXCHAN = maxchan;
%         elseif MAXCHAN ~= maxchan
%           maxchanWarning();
%           
%           if MAXCHAN < maxchan
%             MAXCHAN = maxchan;
%           end
%         end
%       end
      
      if ~isempty(b.channels)
        maxchan = length(b.channels);
        
        if isempty(MAXCHAN)
          MAXCHAN = maxchan;
        elseif MAXCHAN ~= maxchan
          maxchanWarning();
          
          if MAXCHAN < maxchan
            MAXCHAN = maxchan;
          end
        end
      end
      
%       if ~isempty(b.channels.ex) && ~isempty(b.channels.ey) && ~isempty(b.channels.hx) && ~isempty(b.channels.hy)
%         if isempty(EXCHAN)
%           EXCHAN = b.channels.ex;
%         else
%           chanWarning('EX', EXCHAN, b.channels.ex);
%         end
%         
%         if isempty(EYCHAN)
%           EYCHAN = b.channels.ey;
%         else
%           chanWarning('EY', EYCHAN, b.channels.ey);
%         end
%         
%         if isempty(HXCHAN)
%           HXCHAN = b.channels.hx;
%         else
%           chanWarning('HX', HXCHAN, b.channels.hx);
%         end
%         
%         if isempty(HYCHAN)
%           HYCHAN = b.channels.hy;
%         else
%           chanWarning('HY', HYCHAN, b.channels.hy);
%         end
%         
%         if ~isempty(b.channels.hz)
%           if isempty(HZCHAN)
%             HZCHAN = b.channels.hz;
%           else
%             chanWarning('HZ', HZCHAN, b.channels.hz);
%           end
%         end
%         
%         if ~isempty(b.channels.rx) && ~isempty(b.channels.ry)
%           if isempty(RXCHAN)
%             RXCHAN = b.channels.rx;
%           else
%             chanWarning('EX', RXCHAN, b.channels.rx);
%           end
%           
%           if isempty(RYCHAN)
%             RYCHAN = b.channels.ry;
%           else
%             chanWarning('EY', RYCHAN, b.channels.ry);
%           end
%         end
%       end
      
      if length(b.channels) >= 4
        if isempty(EXCHAN)
          EXCHAN = b.channels(1);
        else
          chanWarning('EX', EXCHAN, b.channels(1));
        end
        
        if isempty(EYCHAN)
          EYCHAN = b.channels(2);
        else
          chanWarning('EY', EYCHAN, b.channels(2));
        end
        
        if isempty(HXCHAN)
          HXCHAN = b.channels(3);
        else
          chanWarning('HX', HXCHAN, b.channels(3));
        end
        
        if isempty(HYCHAN)
          HYCHAN = b.channels(4);
        else
          chanWarning('HY', HYCHAN, b.channels(4));
        end
        
        if length(b.channels) >= 5
          if isempty(HZCHAN)
            HZCHAN = b.channels(5);
          else
            chanWarning('HZ', HZCHAN, b.channels(5));
          end
        end
      end
    end
    
    if ~isempty(c2files)
      for ii = 1 : length(c2files)
        c2file = c2files{ii};
        
        m = pmask('-path', c2file.folder);
        b = band(c2file.folder);
        
        ignored = true;
        
        if c2file.stage == 2
          if c2file.output == 1
            [cex.value, cex.period, cex.upper, cex.lower] = append4(cex.value, cex.period, cex.upper, cex.lower, c2file.coh.value, c2file.coh.period, c2file.coh.upper, c2file.coh.lower, m);
            ignored = false;
          elseif c2file.output == 2
            [cey.value, cey.period, cey.upper, cey.lower] = append4(cey.value, cey.period, cey.upper, cey.lower, c2file.coh.value, c2file.coh.period, c2file.coh.upper, c2file.coh.lower, m);
            ignored = false;
          elseif c2file.output == 3
            [chz.value, chz.period, chz.upper, chz.lower] = append4(chz.value, chz.period, chz.upper, chz.lower, c2file.coh.value, c2file.coh.period, c2file.coh.upper, c2file.coh.lower, m);
            ignored = false;
          end
        end
        
        if ~ignored
          if isempty(ACQDATE) || ACQDATE > b.startTime
            ACQDATE = b.startTime;
          end
          
          if isempty(ENDDATE) || ENDDATE < b.stopTime
            ENDDATE = b.stopTime;
          end
          
%           if ~isempty(b.channels.ex) && ~isempty(b.channels.ey) && ~isempty(b.channels.hx) && ~isempty(b.channels.hy)
%             maxchan = 4;
%             
%             if ~isempty(b.channels.hz)
%               maxchan = maxchan + 1;
%             end
%             
%             if ~isempty(b.channels.rx) && ~isempty(b.channels.ry)
%               maxchan = maxchan + 2;
%             end
%             
%             if isempty(MAXCHAN)
%               MAXCHAN = maxchan;
%             elseif MAXCHAN ~= maxchan
%               maxchanWarning();
%               
%               if MAXCHAN < maxchan
%                 MAXCHAN = maxchan;
%               end
%             end
%           end
          
          if ~isempty(b.channels)
            maxchan = length(b.channels);
            
            if isempty(MAXCHAN)
              MAXCHAN = maxchan;
            elseif MAXCHAN ~= maxchan
              maxchanWarning();
              
              if MAXCHAN < maxchan
                MAXCHAN = maxchan;
              end
            end
          end
          
%           if ~isempty(b.channels.ex) && ~isempty(b.channels.ey) && ~isempty(b.channels.hx) && ~isempty(b.channels.hy)
%             if isempty(EXCHAN)
%               EXCHAN = b.channels.ex;
%             else
%               chanWarning('EX', EXCHAN, b.channels.ex);
%             end
%             
%             if isempty(EYCHAN)
%               EYCHAN = b.channels.ey;
%             else
%               chanWarning('EY', EYCHAN, b.channels.ey);
%             end
%             
%             if isempty(HXCHAN)
%               HXCHAN = b.channels.hx;
%             else
%               chanWarning('HX', HXCHAN, b.channels.hx);
%             end
%             
%             if isempty(HYCHAN)
%               HYCHAN = b.channels.hy;
%             else
%               chanWarning('HY', HYCHAN, b.channels.hy);
%             end
%             
%             if ~isempty(b.channels.hz)
%               if isempty(HZCHAN)
%                 HZCHAN = b.channels.hz;
%               else
%                 chanWarning('HZ', HZCHAN, b.channels.hz);
%               end
%             end
%             
%             if ~isempty(b.channels.rx) && ~isempty(b.channels.ry)
%               if isempty(RXCHAN)
%                 RXCHAN = b.channels.rx;
%               else
%                 chanWarning('EX', RXCHAN, b.channels.rx);
%               end
%               
%               if isempty(RYCHAN)
%                 RYCHAN = b.channels.ry;
%               else
%                 chanWarning('EY', RYCHAN, b.channels.ry);
%               end
%             end
%           end

          if length(b.channels) >= 4
            if isempty(EXCHAN)
              EXCHAN = b.channels(1);
            else
              chanWarning('EX', EXCHAN, b.channels(1));
            end
            
            if isempty(EYCHAN)
              EYCHAN = b.channels(2);
            else
              chanWarning('EY', EYCHAN, b.channels(2));
            end
            
            if isempty(HXCHAN)
              HXCHAN = b.channels(3);
            else
              chanWarning('HX', HXCHAN, b.channels(3));
            end
            
            if isempty(HYCHAN)
              HYCHAN = b.channels(4);
            else
              chanWarning('HY', HYCHAN, b.channels(4));
            end
            
            if length(b.channels) >= 5
              if isempty(HZCHAN)
                HZCHAN = b.channels(5);
              else
                chanWarning('HZ', HZCHAN, b.channels(5));
              end
            end
          end
        end
      end
    end
    
    if ~isempty(ACQDATE)
      ACQDATE = sprintf('%s', datestr(datetime(ACQDATE, 'convertfrom', 'posixtime'), 'mm/dd/yyyy'));
    end
    
    if ~isempty(ENDDATE)
      ENDDATE = sprintf('%s', datestr(datetime(ENDDATE, 'convertfrom', 'posixtime'), 'mm/dd/yyyy'));
    end
    
    [period, index] = sort(zxx.period);
    
    [zxx.value, zxx.period, zxx.error] = sort3(zxx.value, zxx.period, zxx.error, index);
    [zxy.value, zxy.period, zxy.error] = sort3(zxy.value, zxy.period, zxy.error, index);
    [zyx.value, zyx.period, zyx.error] = sort3(zyx.value, zyx.period, zyx.error, index);
    [zyy.value, zyy.period, zyy.error] = sort3(zyy.value, zyy.period, zyy.error, index);
    
    [tzx.value, tzx.period, tzx.error] = sort3(tzx.value, tzx.period, tzx.error, index);
    [tzy.value, tzy.period, tzy.error] = sort3(tzy.value, tzy.period, tzy.error, index);
    
    [rxx.value, rxx.period, rxx.upper, rxx.lower] = sort4(rxx.value, rxx.period, rxx.upper, rxx.lower, index);
    [rxy.value, rxy.period, rxy.upper, rxy.lower] = sort4(rxy.value, rxy.period, rxy.upper, rxy.lower, index);
    [ryx.value, ryx.period, ryx.upper, ryx.lower] = sort4(ryx.value, ryx.period, ryx.upper, ryx.lower, index);
    [ryy.value, ryy.period, ryy.upper, ryy.lower] = sort4(ryy.value, ryy.period, ryy.upper, ryy.lower, index);
    
    [pxx.value, pxx.period, pxx.upper, pxx.lower] = sort4(pxx.value, pxx.period, pxx.upper, pxx.lower, index);
    [pxy.value, pxy.period, pxy.upper, pxy.lower] = sort4(pxy.value, pxy.period, pxy.upper, pxy.lower, index);
    [pyx.value, pyx.period, pyx.upper, pyx.lower] = sort4(pyx.value, pyx.period, pyx.upper, pyx.lower, index);
    [pyy.value, pyy.period, pyy.upper, pyy.lower] = sort4(pyy.value, pyy.period, pyy.upper, pyy.lower, index);
    
    if ~isempty(cex.period)
      [cex.value, cex.period, cex.upper, cex.lower] = sort4(cex.value, cex.period, cex.upper, cex.lower, index);
    end
    
    if ~isempty(cey.period)
      [cey.value, cey.period, cey.upper, cey.lower] = sort4(cey.value, cey.period, cey.upper, cey.lower, index);
    end
    
    if ~isempty(chz.period)
      [chz.value, chz.period, chz.upper, chz.lower] = sort4(chz.value, chz.period, chz.upper, chz.lower, index);
    end
    
    frequency = zeros(1, length(period));
    rotation = zeros(1, length(period));
    
    for ii = 1 : length(frequency)
      frequency(ii) = 1.0 / period(ii);
    end
    
    DATAID = [];
    SECTID = [];
    
    if ~isempty(out)
      DATAID = out(1 : length(out) - length('.edi'));
      SECTID = out(1 : length(out) - length('.edi'));
    end
    
    string = writeHEAD(DATAID, ACQDATE, ENDDATE, min);
    string = writeLine(string);
    string = writeINFO(string);
    string = writeLine(string);
    string = writeDEFINEMEAS(string, MAXCHAN, 0, MAXCHAN, min);
    string = writeLine(string);
    
    if MAXCHAN >= 4
      string = writeComment(string, '****DEFINE MEASUREMENTS FOR MT SITE****');
      string = writeEMEAS(string, 1, EXCHAN, min);
      string = writeEMEAS(string, 2, EYCHAN, min);
      string = writeHMEAS(string, 3, HXCHAN, min);
      string = writeHMEAS(string, 4, HYCHAN, min);
      
      if MAXCHAN == 5 || MAXCHAN == 7
        string = writeHMEAS(string, 5, HZCHAN, min);
      end
      
      if MAXCHAN == 6 || MAXCHAN == 7
        string = writeLine(string);
        string = writeComment(string, '****DEFINE MEASUREMENTS FOR REMOTE REFERENCE****');
        string = writeHMEAS(string, 6, RXCHAN, min);
        string = writeHMEAS(string, 7, RYCHAN, min);
      end
      
      string = writeLine(string);
    end
    
    string = writeMTSECT(string, SECTID, length(frequency), MAXCHAN, min);
    string = writeLine(string);
    string = writeComment(string, '****FREQUENCIES****');
    string = writeFREQ(string, frequency);
    string = writeLine(string);
    string = writeComment(string, '****IMPEDANCE ROTATION ANGLES****');
    string = writeZROT(string, rotation);
    string = writeLine(string);
    string = writeComment(string, '****IMPEDANCES****');
    string = writeZXX(string, zxx.value, zxx.error);
    string = writeZXY(string, zxy.value, zxy.error);
    string = writeZYX(string, zyx.value, zyx.error);
    string = writeZYY(string, zyy.value, zyy.error);
    string = writeLine(string);
    string = writeComment(string, '****TIPPER****');
    string = writeTX(string, tzx.value, tzx.error);
    string = writeTY(string, tzy.value, tzy.error);
    string = writeLine(string);
    string = writeComment(string, '****APPARENT RESISTIVITIES AND PHASES****');
    string = writeRHOXX(string, rxx.value, (rxx.upper - rxx.lower) ./ 2.0);
    string = writeRHOXY(string, rxy.value, (rxy.upper - rxy.lower) ./ 2.0);
    string = writeRHOYX(string, ryx.value, (ryx.upper - ryx.lower) ./ 2.0);
    string = writeRHOYY(string, ryy.value, (ryy.upper - ryy.lower) ./ 2.0);
    string = writePHIXX(string, pxx.value, (pxx.upper - pxx.lower) ./ 2.0);
    string = writePHIXY(string, pxy.value, (pxy.upper - pxy.lower) ./ 2.0);
    string = writePHIYX(string, pyx.value, (pyx.upper - pyx.lower) ./ 2.0);
    string = writePHIYY(string, pyy.value, (pyy.upper - pyy.lower) ./ 2.0);
    string = writeLine(string);
    string = writeComment(string, '****PREDICTED COHERENCIES****');
    string = writeCOHEX(string, cex.value, cex.upper, cex.lower);
    string = writeCOHEY(string, cey.value, cey.upper, cey.lower);
    string = writeCOHHZ(string, chz.value, chz.upper, chz.lower);
    string = writeLine(string);
    string = writeEND(string);
    
    if ~isempty(out)
      fp = fopen(out, 'w');
      
      if fp ~= -1
        fprintf(fp, string);
        fclose(fp);
      end
    else
      fprintf(string);
    end
  end
end

function [value, period, error] = append3(v1, p1, e1, v2, p2, e2, mask)
  value = v1;
  period = p1;
  error = e1;
  
  filter = true(1, length(p2));
  
  for jj = 1 : length(filter)
    for kk = 1 : length(mask)
      if jj == mask(kk)
        filter(jj) = false;
        break;
      end
    end
  end
  
  for jj = 1 : length(filter)
    if filter(jj)
      v2 = v2(filter);
      p2 = p2(filter);
      e2 = e2(filter);
      
      for kk = 1 : length(p2)
        value(length(value) + 1) = v2(kk);
        period(length(period) + 1) = p2(kk);
        error(length(error) + 1) = e2(kk);
      end
      
      break;
    end
  end
end

function [value, period, upper, lower] = append4(v1, p1, u1, l1, v2, p2, u2, l2, mask)
  value = v1;
  period = p1;
  upper = u1;
  lower = l1;
  
  filter = true(1, length(p2));
  
  for jj = 1 : length(filter)
    for kk = 1 : length(mask)
      if jj == mask(kk)
        filter(jj) = false;
        break;
      end
    end
  end
  
  for jj = 1 : length(filter)
    if filter(jj)
      v2 = v2(filter);
      p2 = p2(filter);
      u2 = u2(filter);
      l2 = l2(filter);
      
      for kk = 1 : length(p2)
        value(length(value) + 1) = v2(kk);
        period(length(period) + 1) = p2(kk);
        upper(length(upper) + 1) = u2(kk);
        lower(length(lower) + 1) = l2(kk);
      end
      
      break;
    end
  end
end

function [value, period, error] = sort3(v, p, e, index)
  value = zeros(1, length(v));
  period = zeros(1, length(p));
  error = zeros(1, length(e));
  
  for ii = 1 : length(index)
    value(ii) = v(index(ii));
    period(ii) = p(index(ii));
    error(ii) = e(index(ii));
  end
end

function [value, period, upper, lower] = sort4(v, p, u, l, index)
  value = zeros(1, length(v));
  period = zeros(1, length(p));
  upper = zeros(1, length(u));
  lower = zeros(1, length(l));
  
  for ii = 1 : length(index)
    value(ii) = v(index(ii));
    period(ii) = p(index(ii));
    upper(ii) = u(index(ii));
    lower(ii) = l(index(ii));
  end
end

function string = writeHEAD(DATAID, ACQDATE, ENDDATE, min)
  ACQBY = acqby();
  FILEBY = fileby();
  
  if isempty(ACQBY)
    ACQBY = input('acqby: ', 's');
    
    if ~isempty(ACQBY)
      acqby(ACQBY);
    end
  end
  
  if isempty(FILEBY)
    FILEBY = input('fileby: ', 's');
    
    if ~isempty(FILEBY)
      fileby(FILEBY);
    end
  end
  
  t = datetime('now');
  
  year = sprintf('%d', t.Year);
  month = sprintf('%d', t.Month);
  day = sprintf('%d', t.Day);
  
  if t.Month < 10
    month = sprintf('0%d', t.Month);
  end
  
  if t.Day < 10
    day = sprintf('0%d', t.Day);
  end
  
  FILEDATE = sprintf('%s/%s/%s', month, day, year);
  
  if ~min
    COUNTRY = country();
    
    if isempty(COUNTRY)
      COUNTRY = input('country: ', 's');
      
      if ~isempty(COUNTRY)
        country(COUNTRY);
      end
    end
    
    STATE = state();
    
    if isempty(STATE)
      STATE = input('state: ', 's');
      
      if ~isempty(STATE)
        state(STATE);
      end
    end
    
    COUNTY = county();
    
    if isempty(COUNTY)
      COUNTY = input('county: ', 's');
      
      if ~isempty(COUNTY)
        county(COUNTY);
      end
    end
    
    PROSPECT = prospect();
    
    if isempty(PROSPECT)
      PROSPECT = input('prospect: ', 's');
      
      if ~isempty(PROSPECT)
        prospect(PROSPECT);
      end
    end
    
    LOC = loc();
    
    if isempty(LOC)
      LOC = input('loc: ', 's');
      
      if ~isempty(LOC)
        loc(LOC);
      end
    end
    
    LAT = lat();
    
    if isempty(LAT)
      LAT = input('lat: ', 's');
      
      if ~isempty(LAT)
        lat(LAT);
      end
    end
    
    LONG = long();
    
    if isempty(LONG)
      LONG = input('long: ', 's');
      
      if ~isempty(LONG)
        long(LONG);
      end
    end
    
    ELEV = elev();
    
    if isempty(ELEV)
      ELEV = input('elev: ', 's');
      
      if ~isempty(ELEV)
        elev(ELEV);
      end
    end
    
    XUTM = xutm();
    
    if isempty(XUTM)
      XUTM = input('xutm: ', 's');
      
      if ~isempty(XUTM)
        xutm(XUTM);
      end
    end
    
    YUTM = yutm();
    
    if isempty(YUTM)
      YUTM = input('yutm: ', 's');
      
      if ~isempty(YUTM)
        yutm(YUTM);
      end
    end
    
    DATUM = datum();
    
    if isempty(DATUM)
      DATUM = input('datum: ', 's');
      
      if ~isempty(DATUM)
        datum(DATUM);
      end
    end
  end
  
  string = sprintf('>HEAD\n');
  
  if min
    string = sprintf('%sDATAID=%s\n', string, DATAID);                % required
    string = sprintf('%sACQBY=%s\n', string, ACQBY);                  % required
    string = sprintf('%sFILEBY=%s\n', string, FILEBY);                % required
    string = sprintf('%sACQDATE=%s\n', string, ACQDATE);              % required
    string = sprintf('%sFILEDATE=%s\n', string, FILEDATE);            % required
    string = sprintf('%sSTDVERS=1.0\n', string);                      % required
    string = sprintf('%sPROGVERS=%s\n', string, mmt('-version')); % required
    string = sprintf('%sPROGDATE=%s\n', string, mmt('-date'));        % required
  else
    string = sprintf('%sDATAID=%s\n', string, DATAID);                % required
    string = sprintf('%sACQBY=%s\n', string, ACQBY);                  % required
    string = sprintf('%sFILEBY=%s\n', string, FILEBY);                % required
    string = sprintf('%sACQDATE=%s\n', string, ACQDATE);              % required
    string = sprintf('%sENDDATE=%s\n', string, ENDDATE);
    string = sprintf('%sFILEDATE=%s\n', string, FILEDATE);            % required
    string = sprintf('%sCOUNTRY=%s\n', string, COUNTRY);
    string = sprintf('%sSTATE=%s\n', string, STATE);
    string = sprintf('%sCOUNTY=%s\n', string, COUNTY);
    string = sprintf('%sPROSPECT=%s\n', string, PROSPECT);
    string = sprintf('%sLOC=%s\n', string, LOC);
    string = sprintf('%sLAT=%s\n', string, LAT);
    string = sprintf('%sLONG=%s\n', string, LONG);
    string = sprintf('%sELEV=%s\n', string, ELEV);
    string = sprintf('%sXUTM=%s\n', string, XUTM);
    string = sprintf('%sYUTM=%s\n', string, YUTM);
    string = sprintf('%sDATUM=%s\n', string, DATUM);
    string = sprintf('%sUNITS=M\n', string);
    string = sprintf('%sSTDVERS=SEG 1.0\n', string);                  % required
    string = sprintf('%sPROGVERS=%s\n', string, mmt('-version')); % required
    string = sprintf('%sPROGDATE=%s\n', string, mmt('-date'));        % required
    string = sprintf('%sMAXSECT=16\n', string);
    string = sprintf('%sBINDATA=\n', string);
    string = sprintf('%sEMPTY=1.0E32\n', string);
  end
end

function string = writeINFO(string)
%   infoBlock = cell(0);
%   maxInfo = 256;
%   
%   if 0 > 1
%     fprintf('info (ends with a blank line):\n');
%     
%     while true
%       infoLine = input('- ', 's');
%       
%       if ~isempty(infoLine)
%         infoBlock{length(infoBlock) + 1} = infoLine;
%       else
%         break;
%       end
%     end
%   end
  
  infoText = info();
  infoBlock = cell(0);
  maxInfo = 256;
  
  if ~isempty(infoText)
    infoBlock = split(infoText, '\n');
  end
  
  if isempty(infoBlock)
    infoBlock{length(infoBlock) + 1} = 'This text section is to contain all important notes concerning data';
    infoBlock{length(infoBlock) + 1} = 'acquisition and processing. Any non-standard practices used must be';
    infoBlock{length(infoBlock) + 1} = 'documented here.';
  end
  
  if length(infoBlock) > maxInfo
    maxInfo = length(infoBlock);
  end
  
  string = sprintf('%s>INFO MAXINFO=%d\n', string, maxInfo);
  string = sprintf('%s----------------------------------------------------------------------\n', string);
  
  for ii = 1 : length(infoBlock)
    string = sprintf('%s%s\n', string, infoBlock{ii});
  end
  
  string = sprintf('%s----------------------------------------------------------------------\n', string);
end

function string = writeDEFINEMEAS(string, MAXCHAN, MAXRUN, MAXMEAS, min)
  string = sprintf('%s>=DEFINEMEAS\n', string);
  
  if min
    string = sprintf('%sMAXCHAN=%d\n', string, MAXCHAN);
    string = sprintf('%sMAXRUN=%d\n', string, MAXRUN);
    string = sprintf('%sMAXMEAS=%d\n', string, MAXMEAS);
  else
    string = sprintf('%sMAXCHAN=%d\n', string, MAXCHAN);
    string = sprintf('%sMAXRUN=%d\n', string, MAXRUN);
    string = sprintf('%sMAXMEAS=%d\n', string, MAXMEAS);
    string = sprintf('%sUNITS=M\n', string);
    string = sprintf('%sREFTYPE=CART\n', string);
    string = sprintf('%sREFLOC=\n', string);
    string = sprintf('%sREFLAT=\n', string);
    string = sprintf('%sREFLONG=\n', string);
    string = sprintf('%sREFELEV=\n', string);
  end
end

function string = writeEMEAS(string, id, ch, min)
  string = sprintf('%s>EMEAS ', string);
  
  if id == 1
    chtype = 'EX';
    acqchan = 'CH0';
  elseif id == 2
    chtype = 'EY';
    acqchan = 'CH1';
  end
  
  if min
    string = sprintf('%sID=%d ', string, id);                 % required 
    string = sprintf('%sCHTYPE=%s ', string, chtype);         % required
    string = sprintf('%sX=%.2f ', string, ch.header.posX1);   % required
    string = sprintf('%sY=%.2f ', string, ch.header.posY1);   % required
    string = sprintf('%sX2=%.2f ', string, ch.header.posX2);  % required
    string = sprintf('%sY2=%.2f\n', string, ch.header.posY2); % required
  else
    string = sprintf('%sID=%d ', string, id);                % required 
    string = sprintf('%sCHTYPE=%s ', string, chtype);        % required
    string = sprintf('%sX=%.2f ', string, ch.header.posX1);  % required
    string = sprintf('%sY=%.2f ', string, ch.header.posY1);  % required
    string = sprintf('%sZ=%.2f ', string, ch.header.posZ1);
    string = sprintf('%sX2=%.2f ', string, ch.header.posX2); % required
    string = sprintf('%sY2=%.2f ', string, ch.header.posY2); % required
    string = sprintf('%sZ2=%.2f\n', string, ch.header.posZ2);
    string = sprintf('%s       ACQCHAN=%s ', string, acqchan);
    string = sprintf('%sFILTER= ', string);
    string = sprintf('%sGAIN= ', string);
    string = sprintf('%sMEASDATE=\n', string);
  end
end

function string = writeHMEAS(string, id, ch, min)
  string = sprintf('%s>HMEAS ', string);
  
  if id == 3 || id == 6
    chtype = 'HX';
    acqchan = 'CH2';
    azm = '0.0';
  elseif id == 4 || id == 7
    chtype = 'HY';
    acqchan = 'CH3';
    azm = '90.0';
  elseif id == 5
    chtype = 'HZ';
    acqchan = 'CH4';
    azm = '0.0';
  end
  
  if min
    string = sprintf('%sID=%d ', string, id);               % required
    string = sprintf('%sCHTYPE=%s ', string, chtype);       % required
    string = sprintf('%sX=%.2f ', string, ch.header.posX1); % required
    string = sprintf('%sY=%.2f ', string, ch.header.posY1); % required
    string = sprintf('%sAZM=%s\n', string, azm);            % required
  else
    string = sprintf('%sID=%d ', string, id);               % required
    string = sprintf('%sCHTYPE=%s ', string, chtype);       % required
    string = sprintf('%sX=%.2f ', string, ch.header.posX1); % required
    string = sprintf('%sY=%.2f ', string, ch.header.posY1); % required
    string = sprintf('%sZ=%.2f ', string, ch.header.posZ1);
    string = sprintf('%sAZM=%s ', string, azm);             % required
    string = sprintf('%sDIP=0\n', string);
    string = sprintf('%s       ACQCHAN=%s ', string, acqchan);
    string = sprintf('%sFILTER= ', string);
    string = sprintf('%sSENSOR= ', string);
    string = sprintf('%sGAIN= ', string);
    string = sprintf('%sMEASDATE=\n', string);
  end
end

function string = writeMTSECT(string, sectid, nfreq, maxchan, min)
  string = sprintf('%s>=MTSECT\n', string);
  
  if min
    string = sprintf('%sNFREQ=%d\n', string, nfreq);   % required
  else
    string = sprintf('%sSECTID=%s\n', string, sectid);
    string = sprintf('%sNFREQ=%d\n', string, nfreq);   % required
    string = sprintf('%sMAXBLKS=64\n', string);
    
    if maxchan >= 4
      string = sprintf('%sEX=1\n', string);
      string = sprintf('%sEY=2\n', string);
      string = sprintf('%sHX=3\n', string);
      string = sprintf('%sHY=4\n', string);
      
      if maxchan == 5 || maxchan == 7
        string = sprintf('%sHZ=5\n', string);
      end
      
      if maxchan == 6 || maxchan == 7
        string = sprintf('%sRX=6\n', string);
        string = sprintf('%sRY=7\n', string);
      end
    end
  end
end

function string = writeFREQ(string, values)
  string = writeBlock(string, 'FREQ', values);
end

function string = writeZROT(string, values)
  string = writeBlock(string, 'ZROT', values);
end

function string = writeZXX(string, values, errors)
  string = writeBlock(string, 'ZXXR', real(values));
  string = writeBlock(string, 'ZXXI', imag(values));
  string = writeBlock(string, 'ZXX.STD', errors);
end

function string = writeZXY(string, values, errors)
  string = writeBlock(string, 'ZXYR', real(values));
  string = writeBlock(string, 'ZXYI', imag(values));
  string = writeBlock(string, 'ZXY.STD', errors);
end

function string = writeZYX(string, values, errors)
  string = writeBlock(string, 'ZYXR', real(values));
  string = writeBlock(string, 'ZYXI', imag(values));
  string = writeBlock(string, 'ZYX.STD', errors);
end

function string = writeZYY(string, values, errors)
  string = writeBlock(string, 'ZYYR', real(values));
  string = writeBlock(string, 'ZYYI', imag(values));
  string = writeBlock(string, 'ZYY.STD', errors);
end

function string = writeTX(string, values, errors)
  string = writeBlock(string, 'TXR', real(values));
  string = writeBlock(string, 'TXI', imag(values));
  string = writeBlock(string, 'TX.STD', errors);
end

function string = writeTY(string, values, errors)
  string = writeBlock(string, 'TYR', real(values));
  string = writeBlock(string, 'TYI', imag(values));
  string = writeBlock(string, 'TY.STD', errors);
end

function string = writeRHOXX(string, values, errors)
  string = writeBlock(string, 'RHOXX', values);
  string = writeBlock(string, 'RHOXX.STD', errors);
end

function string = writeRHOXY(string, values, errors)
  string = writeBlock(string, 'RHOXY', values);
  string = writeBlock(string, 'RHOXY.STD', errors);
end

function string = writeRHOYX(string, values, errors)
  string = writeBlock(string, 'RHOYX', values);
  string = writeBlock(string, 'RHOYX.STD', errors);
end

function string = writeRHOYY(string, values, errors)
  string = writeBlock(string, 'RHOYY', values);
  string = writeBlock(string, 'RHOYY.STD', errors);
end

function string = writePHIXX(string, values, errors)
  string = writeBlock(string, 'PHIXX', values);
  string = writeBlock(string, 'PHIXX.STD', errors);
end

function string = writePHIXY(string, values, errors)
  string = writeBlock(string, 'PHIXY', values);
  string = writeBlock(string, 'PHIXY.STD', errors);
end

function string = writePHIYX(string, values, errors)
  string = writeBlock(string, 'PHIYX', values);
  string = writeBlock(string, 'PHIYX.STD', errors);
end

function string = writePHIYY(string, values, errors)
  string = writeBlock(string, 'PHIYY', values);
  string = writeBlock(string, 'PHIYY.STD', errors);
end

function string = writeCOHEX(string, values, upper, lower)
  string = writeBlock(string, 'COHEX.MAG', values);
  string = writeBlock(string, 'COHEX.MIN', lower);
  string = writeBlock(string, 'COHEX.MAX', upper);
end

function string = writeCOHEY(string, values, upper, lower)
  string = writeBlock(string, 'COHEY.MAG', values);
  string = writeBlock(string, 'COHEY.MIN', lower);
  string = writeBlock(string, 'COHEY.MAX', upper);
end

function string = writeCOHHZ(string, values, upper, lower)
  string = writeBlock(string, 'COHHZ.MAG', values);
  string = writeBlock(string, 'COHHZ.MIN', lower);
  string = writeBlock(string, 'COHHZ.MAX', upper);
end

function string = writeEND(string)
  string = writeBlock(string, 'END', []);
end

function string = writeComment(string, comment)
  string = sprintf('%s>!%s!\n', string, comment);
end

function string = writeBlock(string, block, values)
  if isempty(values)
    string = sprintf('%s>%s\n', string, block);
  else
    string = sprintf('%s>%s // %d\n', string, block, length(values));
    string = writeValues(string, values);
  end
end

function string = writeValues(string, values)
  string = sprintf('%s%s', string, sprintf('%12.5E %12.5E %12.5E %12.5E %12.5E %12.5E\n', values));
  
  if mod(length(values), 6) ~= 0
    string = sprintf('%s\n', string);
  end
end

function string = writeLine(string)
  string = sprintf('%s\n', string);
end

function maxchanWarning()
  fprintf('Warning: MAXCHAN does not match\n');
end

function chanWarning(chtype, ch1, ch2)
  if ch1.header.posX1 ~= ch2.header.posX1
    fprintf(sprintf('Warning: X does not match on %s channel\n', chtype));
  end

  if ch1.header.posY1 ~= ch2.header.posY1
    fprintf(sprintf('Warning: Y does not match on %s channel\n', chtype));
  end

  if ch1.header.posZ1 ~= ch2.header.posZ1
    fprintf(sprintf('Warning: Z does not match on %s channel\n', chtype));
  end

  if ch1.header.posX2 ~= ch2.header.posX2
    fprintf(sprintf('Warning: X2 does not match on %s channel\n', chtype));
  end

  if ch1.header.posY2 ~= ch2.header.posY2
    fprintf(sprintf('Warning: Y2 does not match on %s channel\n', chtype));
  end

  if ch1.header.posZ2 ~= ch2.header.posZ2
    fprintf(sprintf('Warning: Z2 does not match on %s channel\n', chtype));
  end
end