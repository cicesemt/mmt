function measexport(varargin)
  files = meas();
  
  if ~isempty(files)
    info = measinfo();
    
    if ~isempty(info)
      removeTrend = false;
      ignoreMask  = false;
      startTime   = [];
      stopTime    = [];
      
      if nargin > 0
        if nargin == 1
          if islogical(varargin{1})
            if varargin{1}
              removeTrend = true;
            end
          end
        elseif nargin == 2
          if islogical(varargin{1}) && islogical(varargin{2})
            if varargin{1}
              removeTrend = true;
            end
            
            if varargin{2}
              ignoreMask = true;
            end
          elseif isnumeric(varargin{1}) && isnumeric(varargin{2})
            startTime = varargin{1};
            stopTime  = varargin{2};
          end
        elseif nargin == 3
          if islogical(varargin{1}) && isnumeric(varargin{2}) && isnumeric(varargin{3})
            if varargin{1}
              removeTrend = true;
            end
            
            startTime = varargin{2};
            stopTime  = varargin{3};
          end
        else
          if islogical(varargin{1}) && islogical(varargin{2}) && isnumeric(varargin{3}) && isnumeric(varargin{4})
            if varargin{1}
              removeTrend = true;
            end
            
            if varargin{2}
              ignoreMask = true;
            end
            
            startTime = varargin{3};
            stopTime  = varargin{4};
          end
        end
      end
      
      if isempty(startTime)
        startTime = info.startTime;
      end
      
      if isempty(stopTime)
        stopTime = info.stopTime;
      end
      
      for ii = 1 : length(files)
        atsexport(sprintf('%s%s%s', files(ii).folder, filesep, files(ii).name), removeTrend, ignoreMask, startTime, stopTime, info.numberOfChannels, info.samplingRate);
      end
    end
  end
end