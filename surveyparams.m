function [varargout] = surveyparams(varargin)
  currentSurvey = [];
  
  flag.reset = false;
  flag.all = false;
  flag.specific = false;
  flag.acqby = false;
  flag.fileby = false;
  flag.country = false;
  flag.state = false;
  flag.county = false;
  flag.prospect = false;
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-reset')
        varargin(:, k) = [];
        flag.reset = true;
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-acqby')
        varargin(:, k) = [];
        flag.specific = true;
        flag.acqby = true;
      elseif strcmpi(varargin{k}, '-fileby')
        varargin(:, k) = [];
        flag.specific = true;
        flag.fileby = true;
      elseif strcmpi(varargin{k}, '-country')
        varargin(:, k) = [];
        flag.specific = true;
        flag.country = true;
      elseif strcmpi(varargin{k}, '-state')
        varargin(:, k) = [];
        flag.specific = true;
        flag.state = true;
      elseif strcmpi(varargin{k}, '-county')
        varargin(:, k) = [];
        flag.specific = true;
        flag.county = true;
      elseif strcmpi(varargin{k}, '-prospect')
        varargin(:, k) = [];
        flag.specific = true;
        flag.prospect = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.acqby = true;
    flag.fileby = true;
    flag.country = true;
    flag.state = true;
    flag.county = true;
    flag.prospect = true;
  elseif ~flag.specific
    flag.acqby = true;
    flag.fileby = true;
    flag.country = true;
    flag.state = true;
    flag.county = true;
    flag.prospect = true;
  end
  
  if isempty(currentSurvey)
    currentSurvey = survey(varargin{:});
  end
  
  if ~isempty(currentSurvey)
    if flag.reset
      resetParams(currentSurvey, flag);
    end
    
    if nargout > 0
      varargout{1} = buildParams(currentSurvey, flag);
    elseif ~flag.reset
      printParams(currentSurvey, flag);
    end
  end
end

function resetParams(currentSurvey, flag)
  if flag.acqby
    acqby(currentSurvey, '-reset');
  end
  
  if flag.fileby
    fileby(currentSurvey, '-reset');
  end
  
  if flag.country
    country(currentSurvey, '-reset');
  end
  
  if flag.state
    state(currentSurvey, '-reset');
  end
  
  if flag.county
    county(currentSurvey, '-reset');
  end
  
  if flag.prospect
    prospect(currentSurvey, '-reset');
  end
end

function params = buildParams(currentSurvey, flag)
  params = [];
  
  if flag.acqby
    params.acqby = acqby(currentSurvey);
  end
  
  if flag.fileby
    params.fileby = fileby(currentSurvey);
  end
  
  if flag.country
    params.country = country(currentSurvey);
  end
  
  if flag.state
    params.state = state(currentSurvey);
  end
  
  if flag.county
    params.county = county(currentSurvey);
  end
  
  if flag.prospect
    params.prospect = prospect(currentSurvey);
  end
end

function printParams(currentSurvey, flag)
  names = cell(0);
  values = cell(0);
  
  if flag.acqby
    names{length(names) + 1} = 'acqby:';
    values{length(values) + 1} = acqby(currentSurvey);
  end
  
  if flag.fileby
    names{length(names) + 1} = 'fileby:';
    values{length(values) + 1} = fileby(currentSurvey);
  end
  
  if flag.country
    names{length(names) + 1} = 'country:';
    values{length(values) + 1} = country(currentSurvey);
  end
  
  if flag.state
    names{length(names) + 1} = 'state:';
    values{length(values) + 1} = state(currentSurvey);
  end
  
  if flag.county
    names{length(names) + 1} = 'county:';
    values{length(values) + 1} = county(currentSurvey);
  end
  
  if flag.prospect
    names{length(names) + 1} = 'prospect:';
    values{length(values) + 1} = prospect(currentSurvey);
  end
  
  width = 0;
  
  for ii = 1 : length(names)
    if width < length(names{ii})
      width = length(names{ii});
    end
  end
  
  for ii = 1 : length(names)
    if length(names{ii}) < width
      names{ii} = sprintf(sprintf('%%s%%%ds', width - length(names{ii})), names{ii}, ' ');
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s %s\n', names{ii}, values{ii});
  end
end