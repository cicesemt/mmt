function [varargout] = phic(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  name = 'phic';
  defaultValue = 0.0;
  
  value = fparam(name, defaultValue);
  
  if ~isempty(value)
    resetFlag = false;
    errorFlag = false;
    
    if nargin > 0
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-reset')
          varargin(:, k) = [];
          resetFlag = true;
        else
          if length(varargin{k}) > 1 && varargin{k}(1) == '-' && isnan(str2double(varargin{k}))
            cprintf('err', 'unknown option ''%s'' \n', varargin{k});
            errorFlag = true;
            break;
          end
          
          k = k + 1;
        end
      end
    end
    
    if ~errorFlag
      if resetFlag
        if ~isempty(varargin)
          cprintf('err', 'too many arguments\n');
        elseif value ~= defaultValue
          value = fparam(name, value, defaultValue);
        end
      elseif ~isempty(varargin)
        if length(varargin) > 1
          cprintf('err', 'too many arguments\n');
        else
          v = nan;
          
          if ischar(varargin{1}) || isstring(varargin{1})
            v = str2double(varargin{1});
          elseif isnumeric(varargin{1})
            v = varargin{1};
          end
          
          if isnan(v)
            cprintf('err', 'invalid value\n');
          elseif v < 0.0 || v > 360.0
            cprintf('err', 'value out of range\n');
          else
            value = fparam(name, value, v);
          end
        end
      elseif nargout == 0
        fprintf('%.3f\n', value);
      end
    end
    
    if nargout > 0
      varargout{1} = value;
    end
  end
end