function [varargout] = atsdata(filename, varargin)
  if nargout > 0
    varargout = cell(nargout);
    
    for ii = 1 : nargout
      varargout{ii} = [];
    end
  end
  
  file = atsfile(filename);
  
  if ~isempty(file)
    removeTrend = false;
    startTime   = [];
    stopTime    = [];
    
    if nargin > 1
      if nargin == 2
        if islogical(varargin{1})
          if varargin{1}
            removeTrend = true;
          end
        end
      elseif nargin == 3
        if isnumeric(varargin{1}) && isnumeric(varargin{2})
          startTime = varargin{1};
          stopTime  = varargin{2};
        end
      else
        if islogical(varargin{1}) && isnumeric(varargin{2}) && isnumeric(varargin{3})
          if varargin{1}
            removeTrend = true;
          end
          
          startTime = varargin{2};
          stopTime  = varargin{3};
        end
      end
    end
    
    if isempty(startTime)
      startTime = file.startTime;
    end
    
    if isempty(stopTime)
      stopTime = file.stopTime;
    end
    
    if startTime >= file.startTime && stopTime <= file.stopTime
      startTime = file.startTime + round((startTime - file.startTime) * file.frequency) / file.frequency;
      stopTime = file.stopTime + round((stopTime - file.stopTime) * file.frequency) / file.frequency;
      
      fp = fopen(sprintf('%s%s%s', file.folder, filesep, file.name), 'r');
      
      if fp ~= -1
        fseek(fp, 1024, 'cof');
        
        skip  = (startTime - file.startTime) * file.frequency;
        count = (stopTime - startTime) * file.frequency;
        
        if count > 0
          if skip > 0
            fseek(fp, skip * 4, 'cof');
          end
          
          data = fread(fp, count, 'int32');
          data = data * file.header.mvFactor;
          
          if removeTrend
            data = detrend(data);
          end
          
          if nargout > 0
            varargout{1} = data;
            
            if nargout > 1
              varargout{2} = startTime;
              
              if nargout > 2
                varargout{3} = stopTime;
              end
            end
          else
            for ii = 1 : length(data)
              fprintf('%e\n', data(ii));
            end
          end
        end
        
        fclose(fp);
      end
    end
  end
end