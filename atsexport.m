function atsexport(filename, varargin)
  file = atsfile(filename);
  
  if ~isempty(file)
    removeTrend      = false;
    ignoreMask       = false;
    startTime        = [];
    stopTime         = [];
    numberOfChannels = [];
    segmentLength    = [];
    
    if nargin > 1
      if nargin == 2
        if islogical(varargin{1})
          if varargin{1}
            removeTrend = true;
          end
        end
      elseif nargin == 3
        if islogical(varargin{1}) && islogical(varargin{2})
          if varargin{1}
            removeTrend = true;
          end
          
          if varargin{2}
            ignoreMask = true;
          end
        elseif isnumeric(varargin{1}) && isnumeric(varargin{2})
          startTime = varargin{1};
          stopTime  = varargin{2};
        end
      elseif nargin == 4
        if islogical(varargin{1}) && isnumeric(varargin{2}) && isnumeric(varargin{3})
          if varargin{1}
            removeTrend = true;
          end
          
          startTime = varargin{2};
          stopTime  = varargin{3};
        end
      else
        if islogical(varargin{1}) && islogical(varargin{2}) && isnumeric(varargin{3}) && isnumeric(varargin{4})
          if varargin{1}
            removeTrend = true;
          end
          
          if varargin{2}
            ignoreMask = true;
          end
          
          startTime = varargin{3};
          stopTime  = varargin{4};
          
          if nargin > 5
            if isnumeric(varargin{5})
              numberOfChannels = varargin{5};
            end
            
            if nargin > 6
              if isnumeric(varargin{6})
                segmentLength = varargin{6};
              end
            end
          end
        end
      end
    end
    
    if isempty(startTime)
      startTime = file.startTime;
    end
    
    if isempty(stopTime)
      stopTime = file.stopTime;
    end
    
    if isempty(numberOfChannels)
      numberOfChannels = 1;
    end
    
    if isempty(segmentLength)
      segmentLength = file.samplingRate;
    end
    
    [data, startTime, stopTime] = atsdata(sprintf('%s%s%s', file.folder, filesep, file.name), removeTrend, startTime, stopTime);
    
    if ~ignoreMask
      data = enmask(data, file.frequency, startTime, stopTime, tmask());
    end
    
    if ~isempty(data)
      fp = fopen(filnam(file), 'w');
      
      if fp ~= -1
        if segmentLength > length(data)
          segmentLength = length(data);
        end
        
        numberOfSegments = fix(length(data) / segmentLength);
        numberOfSamples  = numberOfSegments * segmentLength;
        
        if numberOfSamples ~= length(data)
          data = data(1 : numberOfSamples);
        end
        
        if strcmpi(file.channelType, 'EX')
          dipoleLength = abs(file.header.posX1) + abs(file.header.posX2);
        elseif strcmpi(file.channelType, 'EY')
          dipoleLength = abs(file.header.posY1) + abs(file.header.posY2);
        else
          dipoleLength = 0;
        end
        
        count = fprintf(fp, 'Number of segments    : %5d \n', numberOfSegments);
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        count = fprintf(fp, 'Original file         : %3s \n', sprintf('%s%s%s', file.folder, filesep, file.name));
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        count = fprintf(fp, 'Number of channels    : %5d \n', numberOfChannels);
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        count = fprintf(fp, 'Segment length        : %5d \n', segmentLength);
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        count = fprintf(fp, 'Sampling frequency    : %8f \n', file.frequency);
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        count = fprintf(fp, 'Gain stage 1          : %5d \n', file.header.gainStage1);
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        count = fprintf(fp, 'Gain stage 2          : %5d \n', file.header.gainStage2);
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        count = fprintf(fp, 'Angle                 : %8f \n', file.header.angle);
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        count = fprintf(fp, 'Dipole length         : %8f \n', dipoleLength);
        
        for n = 1 : (256 - count)
          fprintf(fp, '%c', 0);
        end
        
        fwrite(fp, data, 'float32');
        
        fclose(fp);
      end
    end
  end
end