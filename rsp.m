function [s, p] = rsp(zxx, zxy, zyx, zyy)
  a = zxy.value + zyx.value;
  b = zyy.value - zxx.value;
  c = zxy.value - zyx.value;
  d = zxx.value + zyy.value;
  
  theta1 = atan(b ./ a);
  theta2 = atan(d ./ c);
  
  z1 = ((a ./ cos(theta1)) + (c ./ cos(theta2))) * 0.5;
  z2 = ((a ./ cos(theta1)) - (c ./ cos(theta2))) * 0.5;
  
  theta = atan((z1 + z2) ./ (z2 - z1));

  zs = (z2 - z1) ./ (cos(theta) * 2);
  zp = (z1 .* z2 .* cos(theta) * 2) ./ (z2 - z1);
  
  period = (zxx.period + zxy.period + zyx.period + zyy.period) / 4;
  omega  = 2 * pi ./ period;
  mu     = 4 * pi * 1.0e-7;
  
  s.rho    = (abs(zs).^2) ./ (omega * mu);
  s.phi    = angle(zs) * (180 / pi);
  s.period = period;
  p.rho    = (abs(zp).^2) ./ (omega * mu);
  p.phi    = angle(zp) * (180 / pi);
  p.period = period;
end