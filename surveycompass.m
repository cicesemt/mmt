function surveycompass(varargin)
  currentSurvey = [];
  
  flag.all = false;
  flag.specific = false;
  flag.north = false;
  flag.northeast = false;
  flag.northwest = false;
  flag.east = false;
  flag.west = false;
  flag.southeast = false;
  flag.southwest = false;
  flag.south = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-north')
        varargin(:, k) = [];
        flag.specific = true;
        flag.north = true;
      elseif strcmpi(varargin{k}, '-northeast')
        varargin(:, k) = [];
        flag.specific = true;
        flag.northeast = true;
      elseif strcmpi(varargin{k}, '-northwest')
        varargin(:, k) = [];
        flag.specific = true;
        flag.northwest = true;
      elseif strcmpi(varargin{k}, '-east')
        varargin(:, k) = [];
        flag.specific = true;
        flag.east = true;
      elseif strcmpi(varargin{k}, '-west')
        varargin(:, k) = [];
        flag.specific = true;
        flag.west = true;
      elseif strcmpi(varargin{k}, '-southeast')
        varargin(:, k) = [];
        flag.specific = true;
        flag.southeast = true;
      elseif strcmpi(varargin{k}, '-southwest')
        varargin(:, k) = [];
        flag.specific = true;
        flag.southwest = true;
      elseif strcmpi(varargin{k}, '-south')
        varargin(:, k) = [];
        flag.specific = true;
        flag.south = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.north = true;
    flag.northeast = true;
    flag.northwest = true;
    flag.east = true;
    flag.west = true;
    flag.southeast = true;
    flag.southwest = true;
    flag.south = true;
  elseif ~flag.specific
    flag.north = true;
    flag.northeast = true;
    flag.northwest = true;
    flag.east = true;
    flag.west = true;
    flag.southeast = true;
    flag.southwest = true;
    flag.south = true;
  end
  
  if isempty(currentSurvey)
    currentSurvey = survey(varargin{:});
  end
  
  if ~isempty(currentSurvey)
    points = struct(...
      'name', {}, ...
      'direction', {}, ...
      'distance', {}...
    );
    
    p = points;
    d = zeros(0);
    
    for ii = 1 : length(currentSurvey.sites)
      point.name = currentSurvey.sites(ii).name;
      point.direction = directioncalc(currentSurvey.latitude, currentSurvey.longitude, currentSurvey.sites(ii).latitude, currentSurvey.sites(ii).longitude);
      point.distance = distancecalc(currentSurvey.latitude, currentSurvey.longitude, currentSurvey.sites(ii).latitude, currentSurvey.sites(ii).longitude);
      
      p(length(p) + 1) = point;
      d(length(d) + 1) = point.distance;
    end
    
    [~, index] = sort(d);
    
    for ii = 1 : length(index)
      points(ii) = p(index(ii));
    end
    
    if ~isempty(points)
      printPoints(points, flag);
    end
  end
end

function printPoints(points, flag)
  names = cell(0);
  directions = cell(0);
  distances = cell(0);
  units = cell(0);
  
  for ii = 1 : length(points)
    ignore = false;
    
    if strcmpi(points(ii).direction, 'N') && ~flag.north
      ignore = true;
    elseif strcmpi(points(ii).direction, 'NE') && ~flag.northeast
      ignore = true;
    elseif strcmpi(points(ii).direction, 'NW') && ~flag.northwest
      ignore = true;
    elseif strcmpi(points(ii).direction, 'E') && ~flag.east
      ignore = true;
    elseif strcmpi(points(ii).direction, 'W') && ~flag.west
      ignore = true;
    elseif strcmpi(points(ii).direction, 'SE') && ~flag.southeast
      ignore = true;
    elseif strcmpi(points(ii).direction, 'SW') && ~flag.southwest
      ignore = true;
    elseif strcmpi(points(ii).direction, 'S') && ~flag.south
      ignore = true;
    end
    
    if ~ignore
      names{length(names) + 1} = points(ii).name;
      directions{length(directions) + 1} = points(ii).direction;
      
      if points(ii).distance < 1000
        distances{length(distances) + 1} = sprintf('%.2f', points(ii).distance);
        units{length(units) + 1} = 'm';
      else
        distances{length(distances) + 1} = thousandsep(points(ii).distance / 1000, 5);
        units{length(units) + 1} = 'km';
      end
    end
  end
  
  namesWidth = 0;
  directionsWidth = 0;
  distancesWidth = 0;
  unitsWidth = 0;
  
  for ii = 1 : length(names)
    if namesWidth < length(names{ii})
      namesWidth = length(names{ii});
    end
    
    if directionsWidth < length(directions{ii})
      directionsWidth = length(directions{ii});
    end
    
    if distancesWidth < length(distances{ii})
      distancesWidth = length(distances{ii});
    end
    
    if unitsWidth < length(units{ii})
      unitsWidth = length(units{ii});
    end
  end
  
  for ii = 1 : length(names)
    if length(names{ii}) < namesWidth
      names{ii} = sprintf(sprintf('%%%ds%%s', namesWidth - length(names{ii})), ' ', names{ii});
    end
    
    if length(directions{ii}) < directionsWidth
      directions{ii} = sprintf(sprintf('%%%ds%%s', directionsWidth - length(directions{ii})), ' ', directions{ii});
    end
    
    if length(distances{ii}) < distancesWidth
      distances{ii} = sprintf(sprintf('%%%ds%%s', distancesWidth - length(distances{ii})), ' ', distances{ii});
    end
    
    if length(units{ii}) < unitsWidth
      units{ii} = sprintf(sprintf('%%%ds%%s', unitsWidth - length(units{ii})), ' ', units{ii});
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s %s %s %s\n', names{ii}, directions{ii}, distances{ii}, units{ii});
  end
end