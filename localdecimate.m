function localdecimate(varargin)
  files = local();
  
  if ~isempty(files)
    info = localinfo();
    
    if ~isempty(info)
      hertz = [];
      
      if nargin > 0
        if nargin == 1
          if isnumeric(varargin{1})
            hertz = varargin{1};
          end
        end
      end
      
      if isempty(hertz)
        hertz = info.frequency;
      end
      
      valid_decimation = false;
      
      if hertz >= 1
        hertz = fix(hertz);

        if hertz <= info.frequency && mod(info.frequency, hertz) == 0
          valid_decimation = true;
        end
      elseif hertz > 0
        seconds = fix(1/hertz);
        hertz = 1/seconds;

        if hertz <= info.frequency
          if info.frequency >= 1
            valid_decimation = true;
          elseif mod(seconds, 1/info.frequency) == 0
            valid_decimation = true;
          end
        end
      end
      
      folder = atsfolder(pwd());
      
      if ~isempty(folder) && valid_decimation
        if folder.defaultFormat || folder.longFormat
          year   = datestr(datetime(info.startTime, 'convertfrom','posixtime'), 'yyyy');
          month  = datestr(datetime(info.startTime, 'convertfrom','posixtime'), 'mm');
          day    = datestr(datetime(info.startTime, 'convertfrom','posixtime'), 'dd');
          hour   = datestr(datetime(info.startTime, 'convertfrom','posixtime'), 'HH');
          minute = datestr(datetime(info.startTime, 'convertfrom','posixtime'), 'MM');
          second = datestr(datetime(info.startTime, 'convertfrom','posixtime'), 'SS');
          
          name = sprintf('%s_%s-%s-%s_%s-%s-%s', folder.meas, year, month, day, hour, minute, second);
          
          if folder.longFormat
            if hertz >= 1
              name = sprintf('%s_%d%s', name, hertz, 'H');
            else
              name = sprintf('%s_%d%s', name, 1/hertz, 'S');
            end
          end
        else
          name = folder.name;
        end
        
        parent = folder.parent;
        
        if strcmp(parent(length(parent)), filesep)
          parent = parent(1 : length(parent) - 1);
        end
        
        path = sprintf('%s%s%s', parent, filesep, name);
        pathSuccess = false;
        k = 0;
        
        while true
          if isempty(dir(path))
            [pathSuccess, ~] = mkdir(path);
            break;
          end
          
          k = k + 1;
          
          if k > 1
            path = sprintf('%s%s%s_decimate_%d', parent, filesep, name, k);
          else
            path = sprintf('%s%s%s_decimate', parent, filesep, name);
          end
        end
        
        if pathSuccess
          for ii = 1 : length(files)
            atsdecimate(sprintf('%s%s%s', files(ii).folder, filesep, files(ii).name), hertz, path);
          end
        end
      end
    end
  end  
end