function string = latitude2string(latitude)
  millis  = abs(latitude);
  degrees = fix(millis / (1000*60*60));
  millis  = mod(millis , (1000*60*60));
  minutes = fix(millis / (1000*60));
  millis  = mod(millis , (1000*60));
  seconds = fix(millis / (1000));
  millis  = mod(millis , (1000));
  
  if latitude < 0
    direction = 'S';
  else
    direction = 'N';
  end
  
  if millis < 10
    string = sprintf('%d%c %d'' %d.00%d\" %c', degrees, 176, minutes, seconds, millis, direction);
  elseif millis < 100
    string = sprintf('%d%c %d'' %d.0%d\" %c', degrees, 176, minutes, seconds, millis, direction);
  else
    string = sprintf('%d%c %d'' %d.%d\" %c', degrees, 176, minutes, seconds, millis, direction);
  end
end