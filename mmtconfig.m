function [value] = mmtconfig(key)
  mmt.ats.plot.color          = [0 0.4470 0.7410];
  mmt.ats.plot.maskColor      = [0.85 0.85 0.85];
  mmt.ats.plot.selectionColor = [0.8500 0.3250 0.0980];
  mmt.ats.plot.lineStyle      = '-';
  mmt.ats.plot.lineWidth      = 0.5;
  mmt.ats.plot.marker         = 'none';
  mmt.ats.plot.markerSize     = 6;
  
  if strcmp(key, 'mmt.ats.plot.color')
    value = mmt.ats.plot.color;
  elseif strcmp(key, 'mmt.ats.plot.maskColor')
    value = mmt.ats.plot.maskColor;
  elseif strcmp(key, 'mmt.ats.plot.selectionColor')
    value = mmt.ats.plot.selectionColor;
  elseif strcmp(key, 'mmt.ats.plot.lineStyle')
    value = mmt.ats.plot.lineStyle;
  elseif strcmp(key, 'mmt.ats.plot.lineWidth')
    value = mmt.ats.plot.lineWidth;
  elseif strcmp(key, 'mmt.ats.plot.marker')
    value = mmt.ats.plot.marker;
  elseif strcmp(key, 'mmt.ats.plot.markerSize')
    value = mmt.ats.plot.markerSize;
  end
end