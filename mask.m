function [varargout] = mask(varargin)
  if nargout > 0
    varargout{1} = tmask(varargin{:});
  else
    tmask(varargin{:});
  end
end