function [varargout] = band(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    if strcmpi(varargin{1}, 'info')
      varargin(:, 1) = [];
      bandinfo(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'channels')
      varargin(:, 1) = [];
      bandchannels(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'path')
      varargin(:, 1) = [];
      bandpath(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'match')
      varargin(:, 1) = [];
      bandmatch(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'timeline')
      varargin(:, 1) = [];
      bandtimeline(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'samples')
      varargin(:, 1) = [];
      bandsamples(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'text')
      varargin(:, 1) = [];
      
      if nargout > 0
        varargout{1} = bandtext(varargin{:});
      else
        bandtext(varargin{:});
      end
      
      return;
    end
  end
  
  if ~isempty(varargin)
    path = varargin{1};
  else
    path = pwd();
  end
  
  currentBand = loadBand(path);
  
  if ~isempty(currentBand)
    if nargout > 0
      varargout{1} = currentBand;
    else
      fprintf('%s\n', currentBand.name);
    end
  end
end

function currentBand = loadBand(path)
  currentBand = [];
  
  files = ats(path);
  
  if ~isempty(files)
    for ii = 1 : length(files)
      ex = [];
      ey = [];
      hx = [];
      hy = [];
      hz = [];
      
      pivot = files(ii);
      
      if strcmpi(pivot.channelType, 'Ex')
        ex = pivot;
      elseif strcmpi(pivot.channelType, 'Ey')
        ey = pivot;
      elseif strcmpi(pivot.channelType, 'Hx')
        hx = pivot;
      elseif strcmpi(pivot.channelType, 'Hy')
        hy = pivot;
      elseif strcmpi(pivot.channelType, 'Hz')
        hz = pivot;
      else
        continue;
      end
      
      for jj = 1 : length(files)
        if ii ~= jj
          file = files(jj);
          
          if file.aduSerialNumber == pivot.aduSerialNumber && file.xmlVersion == pivot.xmlVersion && file.runNumber == pivot.runNumber && strcmpi(file.boardType, pivot.boardType) && file.samplingRate == pivot.samplingRate && strcmpi(file.measurementUnit, pivot.measurementUnit)
            if file.channelNumber ~= pivot.channelNumber && ~strcmpi(file.channelType, pivot.channelType)
              if file.startTime == pivot.startTime && strcmpi(file.header.measurementType, pivot.header.measurementType)
                
                distance = distancecalc(pivot.header.latitude, pivot.header.longitude, file.header.latitude, file.header.longitude);
                
                if distance < 5
                  if strcmpi(file.channelType, 'Ex')
                    if isempty(ex)
                      ex = file;
                    end
                  elseif strcmpi(file.channelType, 'Ey')
                    if isempty(ey)
                      ey = file;
                    end
                  elseif strcmpi(file.channelType, 'Hx')
                    if isempty(hx)
                      hx = file;
                    end
                  elseif strcmpi(file.channelType, 'Hy')
                    if isempty(hy)
                      hy = file;
                    end
                  elseif strcmpi(file.channelType, 'Hz')
                    if isempty(hz)
                      hz = file;
                    end
                  end
                end
                
              end
            end
          end
          
        end
      end
      
      if ~isempty(ex) && ~isempty(ey) && ~isempty(hx) && ~isempty(hy)
        currentBand.name = sprintf('%d%s', pivot.samplingRate, pivot.measurementUnit);
        currentBand.folder = pivot.folder;
        currentBand.bytes = ex.bytes + ey.bytes + hx.bytes + hy.bytes;
        currentBand.samplingRate = pivot.samplingRate;
        currentBand.measurementUnit = pivot.measurementUnit;
        currentBand.numberOfSamples = 0;
        currentBand.frequency = pivot.frequency;
        currentBand.latitude = ex.header.latitude + ey.header.latitude + hx.header.latitude + hy.header.latitude;
        currentBand.longitude = ex.header.longitude + ey.header.longitude + hx.header.longitude + hy.header.longitude;
        currentBand.elevation = ex.header.elevation + ey.header.elevation + hx.header.elevation + hy.header.elevation;
        currentBand.startTime = 0;
        currentBand.stopTime = 0;
        currentBand.channels(1) = ex;
        currentBand.channels(2) = ey;
        currentBand.channels(3) = hx;
        currentBand.channels(4) = hy;
        
        smaller = ex;
        
        if smaller.numberOfSamples > ey.numberOfSamples
          smaller = ey;
        end
        
        if smaller.numberOfSamples > hx.numberOfSamples
          smaller = hx;
        end
        
        if smaller.numberOfSamples > hy.numberOfSamples
          smaller = hy;
        end
        
        if ~isempty(hz)
          currentBand.bytes = currentBand.bytes + hz.bytes;
          currentBand.latitude = currentBand.latitude + hz.header.latitude;
          currentBand.longitude = currentBand.longitude + hz.header.longitude;
          currentBand.elevation = currentBand.elevation + hz.header.elevation;
          currentBand.channels(5) = hz;
          
          if smaller.numberOfSamples > hz.numberOfSamples
            smaller = hz;
          end
        end
        
        currentBand.numberOfSamples = smaller.numberOfSamples;
        currentBand.latitude = round(currentBand.latitude / length(currentBand.channels));
        currentBand.longitude = round(currentBand.longitude / length(currentBand.channels));
        currentBand.elevation = round(currentBand.elevation / length(currentBand.channels));
        currentBand.startTime = smaller.startTime;
        currentBand.stopTime = smaller.stopTime;
        
        break;
      end
    end
  end
end