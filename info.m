function [varargout] = info(varargin)
  name = mfilename();
  defaultValue = '';
  
  currentStruct = [];
  path = [];
  rflag = false;
  eflag = false;
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentStruct = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-path')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          path = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-reset')
        varargin(:, k) = [];
        rflag = true;
      elseif strcmpi(varargin{k}, '-edit')
        varargin(:, k) = [];
        eflag = true;
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentStruct)
    if isempty(path)
      path = pwd();
    end
    
    currentStruct = band(path);
    
    if isempty(currentStruct)
      currentStruct = site(path);
      
      if isempty(currentStruct)
        currentStruct = survey(path);
      end
    end
  end
  
  if ~isempty(currentStruct)
    dname = sprintf('%s%s%s', currentStruct.folder, filesep, '.mt');
    fname = sprintf('%s%s%s', dname, filesep, name);
    
    if eflag
      editValue(dname, fname, defaultValue);
    else
      if rflag
        value = saveValue(dname, fname, defaultValue);
      elseif ~isempty(varargin)
        value = saveValue(dname, fname, varargin{1});
      else
        value = loadValue(fname, defaultValue);
        
        if ~isempty(value) && nargout == 0
          fprintf('%s\n', value);
        end
      end
      
      if ~isempty(value) && nargout > 0
        varargout{1} = value;
      end
    end
  end
end

function value = saveValue(dname, fname, value)
  [mtdir, ~] = mkdir(dname);
  
  if mtdir
    fp = fopen(fname, 'wt');
    
    if fp ~= -1
      fprintf(fp, '%s', value);
      fclose(fp);
    end
  end
end

function editValue(dname, fname, defaultValue)
  eflag = false;
  
  [mtdir, ~] = mkdir(dname);
  
  if mtdir
    fp = fopen(fname, 'rt');
    
    if fp ~= -1
      eflag = true;
      fclose(fp);
    else
      fp = fopen(fname, 'wt');
      
      if fp ~= -1
        eflag = true;
        fprintf(fp, '%s', defaultValue);
        fclose(fp);
      end
    end
  end
  
  if eflag
    edit(fname);
  end
end

function value = loadValue(fname, defaultValue)
  value = defaultValue;
  
  fp = fopen(fname, 'rt');
  
  if fp ~= -1
    value = '';
    count = 0;
    
    while true
      v = fgetl(fp);
      
      if v == -1
        break;
      end
      
      if count > 0
        value = sprintf('%s\n', value);
      end
      
      value = sprintf('%s%s', value, v);
      count = count + 1;
    end
    
    fclose(fp);
  end
end