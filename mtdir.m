function [varargout] = mtdir(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    path = varargin{1};
  
    if length(path) > 1 && strcmp(path(length(path)), filesep)
      path = path(1 : length(path) - 1);
    end
  else
    path = pwd();
  end
  
  files = ats(path);
  
  if ~isempty(files)
    for ii = 1 : length(files)
      ex = [];
      ey = [];
      hx = [];
      hy = [];
      hz = [];
      
      pivot = files(ii);
      
      if strcmpi(pivot.channelType, 'EX')
        ex = pivot;
      elseif strcmpi(pivot.channelType, 'EY')
        ey = pivot;
      elseif strcmpi(pivot.channelType, 'HX')
        hx = pivot;
      elseif strcmpi(pivot.channelType, 'HY')
        hy = pivot;
      elseif strcmpi(pivot.channelType, 'HZ')
        hz = pivot;
      else
        continue;
      end
      
      for jj = 1 : length(files)
        if ii ~= jj
          file = files(jj);
          
          if file.aduSerialNumber == pivot.aduSerialNumber && file.xmlVersion == pivot.xmlVersion && file.runNumber == pivot.runNumber && strcmpi(file.boardType, pivot.boardType) && file.samplingRate == pivot.samplingRate && strcmpi(file.measurementUnit, pivot.measurementUnit)
            if file.channelNumber ~= pivot.channelNumber && ~strcmpi(file.channelType, pivot.channelType)
              if strcmpi(file.header.systemType, pivot.header.systemType) && file.startTime == pivot.startTime
                
                distance = sqrt((file.header.latitude - pivot.header.latitude)^2 + (file.header.longitude - pivot.header.longitude)^2 + (file.header.elevation - pivot.header.elevation)^2);
                
                if distance < 20
                  if strcmpi(file.channelType, 'EX')
                    if isempty(ex)
                      ex = file;
                    end
                  elseif strcmpi(file.channelType, 'EY')
                    if isempty(ey)
                      ey = file;
                    end
                  elseif strcmpi(file.channelType, 'HX')
                    if isempty(hx)
                      hx = file;
                    end
                  elseif strcmpi(file.channelType, 'HY')
                    if isempty(hy)
                      hy = file;
                    end
                  elseif strcmpi(file.channelType, 'HZ')
                    if isempty(hz)
                      hz = file;
                    end
                  end
                end
                
              end
            end
          end
          
        end
      end
      
      if ~isempty(ex) && ~isempty(ey) && ~isempty(hx) && ~isempty(hy)
        path = sprintf('%s%s.mt', path, filesep);
        
        [success, ~] = mkdir(path);
        
        if success
          if nargout > 0
            varargout{1} = path;
          else
            fprintf('%s\n', path);
          end
        end
        
        break;
      end
    end
  end
end