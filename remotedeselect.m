function remotedeselect()
  fp = fopen(sprintf('%s%s%s', '.mt', filesep, 'remote'), 'rt');
  
  if fp ~= -1
    fclose(fp);
    
    fp = fopen(sprintf('%s%s%s', '.mt', filesep, 'remote'), 'wt');
    
    if fp ~= -1
      fclose(fp);
    end
  end
end