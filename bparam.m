function value = bparam(name, defaultValue, varargin)
  value = [];
  
  globalParam = false;
  
  if strcmpi(name, 'npcsm')
    globalParam = true;
  elseif strcmpi(name, 'nptsm')
    globalParam = true;
  elseif strcmpi(name, 'nptssm')
    globalParam = true;
  elseif strcmpi(name, 'noutm')
    globalParam = true;
  elseif strcmpi(name, 'ninpm')
    globalParam = true;
  elseif strcmpi(name, 'nrefm')
    globalParam = true;
  elseif strcmpi(name, 'nrsitem')
    globalParam = true;
  elseif strcmpi(name, 'nsectm')
    globalParam = true;
  elseif strcmpi(name, 'nblk')
    globalParam = true;
  elseif strcmpi(name, 'nfsm')
    globalParam = true;
  elseif strcmpi(name, 'nfm')
    globalParam = true;
  elseif strcmpi(name, 'gfortran')
    globalParam = true;
  end
  
  dname = [];
  fname = [];
  
  if globalParam
    dname = mfilename('fullpath');
    dname = dname(1 : length(dname) - length(mfilename()));
    
    if length(dname) > 1 && strcmp(dname(length(dname)), filesep)
      dname = dname(1 : length(dname) - 1);
    end
    
    dname = sprintf('%s%s.mt', dname, filesep);
    fname = sprintf('%s%s%s', dname, filesep, name);
  else
    files = local();
    
    if ~isempty(files)
      dname = '.mt';
      fname = sprintf('%s%s%s', dname, filesep, name);
    end
  end
  
  if ~isempty(fname)
    value = defaultValue;
    
    if nargin > 2
      if ischar(varargin{1}) || isstring(varargin{1})
        if strcmpi(varargin{1}, 'true') || strcmpi(varargin{1}, 'on')
          value = true;
        elseif strcmpi(varargin{1}, 'false') || strcmpi(varargin{1}, 'off')
          value = false;
        else
          n = str2double(varargin{1});
          
          if ~isnan(n)
            if n ~= 0
              value = true;
            else
              value = false;
            end
          end
        end
      elseif isnumeric(varargin{1})
        if varargin{1} ~= 0
          value = true;
        else
          value = false;
        end
      elseif islogical(varargin{1})
        value = varargin{1};
      end
      
      [mtdir, ~] = mkdir(dname);
      
      if mtdir
        fp = fopen(fname, 'wt');
        
        if fp ~= -1
          if value
            fprintf(fp, 'true\n');
          else
            fprintf(fp, 'false\n');
          end
          
          fclose(fp);
        end
      end
    else
      fp = fopen(fname, 'rt');
      
      if fp ~= -1
        v = fgetl(fp);
        
        if ischar(v)
          if strcmpi(v, 'true') || strcmpi(v, 'on')
            value = true;
          elseif strcmpi(v, 'false') || strcmpi(v, 'off')
            value = false;
          end
        end
        
        fclose(fp);
      end
    end
  end
end