function [azimuth, skew] = zas(zxx, zxy, zyx, zyy)
  a = zxy.value + zyx.value;
  b = zyy.value - zxx.value;
  c = zxy.value - zyx.value;
  d = zxx.value + zyy.value;
  
  theta1 = atan(b ./ a);
  theta2 = atan(d ./ c);
  
  delta   = theta1 - theta2;
  average = (theta1 + theta2) / 2;
  
  azimuth.delta   = real(delta) * (180 / pi);
  azimuth.average = real(average) * (180 / pi);
  azimuth.period  = (zxx.period + zxy.period + zyx.period + zyy.period) / 4;
  
  skew.delta   = imag(delta);
  skew.average = imag(average);
  skew.period  = (zxx.period + zxy.period + zyx.period + zyy.period) / 4;
end