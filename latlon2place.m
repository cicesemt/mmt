function [country, state, county, prospect] = latlon2place(latitude, longitude)
  countries = [
     62100000, -319500000;
     55800000, -324900000;
     82800000, -367200000;
    143136000, -354600000;
  ];
  
  countryNames = {
    'Belize'
    'Guatemala'
    'México'
    'United States'
  };
  
  d = zeros(1, length(countries));
  
  for ii = 1 : length(countries)
    d(ii) = distancecalc(latitude, longitude, countries(ii, 1), countries(ii, 2));
  end
  
  m = 1;
  
  for ii = 2 : length(d)
    if d(m) > d(ii)
      m = ii;
    end
  end
  
  country = countryNames{m};
  state = [];
  county = [];
  prospect = [];
  
  if m == 3
    states = [
       79200000, -368520001;
      108000000, -414000000;
       92999999, -402599999;
       67799999, -324900000;
       59400000, -333000000;
      103500000, -382500000;
       69419999, -356879999;
       98399999, -367200000;
       68879999, -374040000;
       89399999, -377399999;
       75600000, -363600000;
       63600001, -360000000;
       73800000, -356400000;
       73199999, -373200001;
       69720001, -358800001;
       69000001, -366599999;
       67500000, -356640001;
       78599999, -377399999;
       92099999, -359100000;
       61200000, -347400000;
       67500000, -352500001;
       75060000, -359460000;
       70800001, -317700000;
       81000000, -361800000;
       90000000, -387000000;
      106800001, -397800000;
       64800000, -333600001;
       86400000, -355500000;
       69900001, -353400001;
       69599999, -348000001;
       74700000, -319799999;
       83700000, -370800000;
    ];
    
    stateNames = {
      'Aguascalientes'
      'Baja California'
      'Baja California Sur'
      'Campeche'
      'Chiapas'
      'Chihuahua'
      'Ciudad de México'
      'Coahuila'
      'Colima'
      'Durango'
      'Guanajuato'
      'Guerrero'
      'Hidalgo'
      'Jalisco'
      'México'
      'Michoacán'
      'Morelos'
      'Nayarit'
      'Nuevo León'
      'Oaxaca'
      'Puebla'
      'Querétaro'
      'Quintana Roo'
      'San Luis Potosí'
      'Sinaloa'
      'Sonora'
      'Tabasco'
      'Tamaulipas'
      'Tlaxcala'
      'Veracruz'
      'Yucatán'
      'Zacatecas'
    };
    
    d = zeros(1, length(states));
    
    for ii = 1 : length(states)
      d(ii) = distancecalc(latitude, longitude, states(ii, 1), states(ii, 2));
    end
    
    m = 1;
    
    for ii = 2 : length(d)
      if d(m) > d(ii)
        m = ii;
      end
    end
    
    state = stateNames{m};
    
    if m == 2
      counties = [
        117018000, -414531000;
        117007999, -421098998;
        116435999, -421314001;
        117181001, -419875999;
        114720001, -419820001;
        110059999, -417404999;
      ];
      
      countyNames = {
        'Mexicali'
        'Tijuana'
        'Playas de Rosarito'
        'Tecate'
        'Ensenada'
        'San Quintín'
      };
      
      d = zeros(1, length(counties));
      
      for ii = 1 : length(counties)
        d(ii) = distancecalc(latitude, longitude, counties(ii, 1), counties(ii, 2));
      end
      
      m = 1;
      
      for ii = 2 : length(d)
        if d(m) > d(ii)
          m = ii;
        end
      end
      
      county = countyNames{m};
      
      if m == 5
        prospects = [
          115534001, -420667999;
          115471102, -419860102;
          115552001, -419640998;
          114882001, -418576000;
          114815002, -420093000;
          114720098, -419747000;
          115081999, -419937998;
          114194999, -419658001;
          113591002, -419069999;
          112594000, -418956800;
          112770000, -418492102;
          113054000, -416630902;
          111845002, -418353001;
          109273000, -412709800;
          101458001, -414784001;
        ];
        
        prospectNames = {
          'La Misión'
          'El Porvenir'
          'Francisco Zarco'
          'Real del Castillo'
          'El Sauzal de Rodríguez'
          'Ensenada'
          'San Antonio de las Minas'
          'Maneadero'
          'Santo Tomás'
          'Eréndira'
          'San Vicente'
          'Valle de la Trinidad'
          'Punta Colonet'
          'Puertecitos'
          'Isla de Cedros'
        };
      
        d = zeros(1, length(prospects));
        
        for ii = 1 : length(prospects)
          d(ii) = distancecalc(latitude, longitude, prospects(ii, 1), prospects(ii, 2));
        end
        
        m = 1;
        
        for ii = 2 : length(d)
          if d(m) > d(ii)
            m = ii;
          end
        end
        
        prospect = prospectNames{m};
      end
    end
  end
end