function wyesegments(varargin)
  files = [];
  
  flag.all = false;
  flag.specific = false;
  flag.decimationExponent = false;
  flag.blockNumber = false;
  flag.segmentNumber = false;
  flag.settings = false;
  flag.xlength = false;
  flag.ylength = false;
  flag.numberOfSamples = false;
  flag.numberOfChannels = false;
  flag.filter.blocks = zeros(0);
  flag.filter.segments = zeros(0);
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        files = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-decimationExponent')
        varargin(:, k) = [];
        flag.specific = true;
        flag.decimationExponent = true;
      elseif strcmpi(varargin{k}, '-blockNumber')
        varargin(:, k) = [];
        flag.specific = true;
        flag.blockNumber = true;
      elseif strcmpi(varargin{k}, '-segmentNumber')
        varargin(:, k) = [];
        flag.specific = true;
        flag.segmentNumber = true;
      elseif strcmpi(varargin{k}, '-settings')
        varargin(:, k) = [];
        flag.specific = true;
        flag.settings = true;
      elseif strcmpi(varargin{k}, '-xlength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.xlength = true;
      elseif strcmpi(varargin{k}, '-ylength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.ylength = true;
      elseif strcmpi(varargin{k}, '-numberOfSamples')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfSamples = true;
      elseif strcmpi(varargin{k}, '-numberOfChannels')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfChannels = true;
      elseif strcmpi(varargin{k}, '-block')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          blockNumber = str2double(varargin{k});
          varargin(:, k) = [];
          
          if ~isnan(blockNumber)
            flag.filter.blocks(length(flag.filter.blocks) + 1) = blockNumber;
          end
        end
      elseif strcmpi(varargin{k}, '-segment')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          segmentNumber = str2double(varargin{k});
          varargin(:, k) = [];
          
          if ~isnan(segmentNumber)
            flag.filter.segments(length(flag.filter.segments) + 1) = segmentNumber;
          end
        end
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.decimationExponent = true;
    flag.blockNumber = true;
    flag.segmentNumber = true;
    flag.settings = true;
    flag.xlength = true;
    flag.ylength = true;
    flag.numberOfSamples = true;
    flag.numberOfChannels = true;
  elseif ~flag.specific
    flag.decimationExponent = true;
    flag.blockNumber = true;
    flag.segmentNumber = true;
    flag.settings = true;
    flag.xlength = true;
    flag.ylength = true;
    flag.numberOfSamples = true;
    flag.numberOfChannels = true;
  end
  
  if isempty(files)
    files = wye(varargin{:});
  end
  
  if ~isempty(files)
    for ii = 1 : length(files)
      printSegments(files(ii), flag);
    end
  end
end

function printSegments(file, flag)
  name = cell(0);
  value = cell(0);
  
  for ii = 1 : file.numberOfBlocks
    if ~isempty(flag.filter.blocks)
      ignore = true;
      
      for jj = 1 : length(flag.filter.blocks)
        if flag.filter.blocks(jj) == file.blocks(ii).header.blockNumber
          ignore = false;
          break;
        end
      end
      
      if ignore
        continue;
      end
    end
    
    for jj = 1 : file.blocks(ii).numberOfSegments
      if ~isempty(flag.filter.segments)
        ignore = true;
        
        for kk = 1 : length(flag.filter.segments)
          if flag.filter.segments(kk) == file.blocks(ii).segments(jj).header.segmentNumber
            ignore = false;
            break;
          end
        end
        
        if ignore
          continue;
        end
      end
      
      if flag.decimationExponent
        name{length(name) + 1} = 'decimation exponent:';
        value{length(value) + 1} = sprintf('%d', file.blocks(ii).segments(jj).header.decimationExponent);
      end
      
      if flag.blockNumber
        name{length(name) + 1} = 'block number:';
        value{length(value) + 1} = sprintf('%d', file.blocks(ii).header.blockNumber);
      end
      
      if flag.segmentNumber
        name{length(name) + 1} = 'segment number:';
        value{length(value) + 1} = sprintf('%d', file.blocks(ii).segments(jj).header.segmentNumber);
      end
      
      if flag.settings
        name{length(name) + 1} = 'settings:';
        value{length(value) + 1} = sprintf('%d', file.blocks(ii).segments(jj).header.settings);
      end
      
      if flag.xlength
        name{length(name) + 1} = 'xlength:';
        value{length(value) + 1} = sprintf('%d', file.blocks(ii).segments(jj).header.xlength);
      end
      
      if flag.ylength
        name{length(name) + 1} = 'ylength:';
        value{length(value) + 1} = sprintf('%d', file.blocks(ii).segments(jj).header.ylength);
      end
      
      if flag.numberOfSamples
        name{length(name) + 1} = 'number of samples:';
        value{length(value) + 1} = sprintf('%d', file.blocks(ii).segments(jj).header.numberOfSamples);
      end
      
      if flag.numberOfChannels
        name{length(name) + 1} = 'number of channels:';
        value{length(value) + 1} = sprintf('%d', file.blocks(ii).segments(jj).header.numberOfChannels);
      end
    end
  end
  
  width = 0;
  
  for ii = 1 : length(name)
    if width < length(name{ii})
      width = length(name{ii});
    end
  end
  
  for ii = 1 : length(name)
    if length(name{ii}) < width
      name{ii} = sprintf(sprintf('%%s%%%ds', width - length(name{ii})), name{ii}, ' ');
    end
  end
  
  for ii = 1 : length(name)
    fprintf('%s %s\n', name{ii}, value{ii});
  end
end