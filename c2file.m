function c2file = c2file(filename)
  c2file = [];
  
  file = dir(filename);
  
  if length(file) == 1 && length(file.name) > 7 && ~file.isdir
    filetype = file.name(length(file.name) - 6 : length(file.name));
    
    input     = 0;
    output    = 0;
    nonrobust = 0;
    bounded   = 0;
    stage     = 0;
    
    if strcmpi(filetype, '.1n.1c2')
      input     = 1;
      nonrobust = 1;
      stage     = 1;
    elseif strcmpi(filetype, '.1r.1c2')
      input   = 1;
      bounded = 1;
      stage   = 1;
    elseif strcmpi(filetype, '.2n.1c2')
      input     = 2;
      nonrobust = 1;
      stage     = 1;
    elseif strcmpi(filetype, '.2r.1c2')
      input   = 2;
      bounded = 1;
      stage   = 1;
    elseif strcmpi(filetype, '.1n.2c2')
      output    = 1;
      nonrobust = 1;
      stage     = 2;
    elseif strcmpi(filetype, '.1r.2c2')
      output  = 1;
      bounded = 1;
      stage   = 2;
    elseif strcmpi(filetype, '.2n.2c2')
      output    = 2;
      nonrobust = 1;
      stage     = 2;
    elseif strcmpi(filetype, '.2r.2c2')
      output  = 2;
      bounded = 1;
      stage   = 2;
    elseif strcmpi(filetype, '.3n.2c2')
      output    = 3;
      nonrobust = 1;
      stage     = 2;
    elseif strcmpi(filetype, '.3r.2c2')
      output  = 3;
      bounded = 1;
      stage   = 2;
    end
    
    if (input || output) && (nonrobust || bounded) && stage
      j = jfile(sprintf('%s%s%s.j', file.folder, filesep, file.name(1 : length(file.name) - 7)));
      
      if ~isempty(j)
        fp = fopen(sprintf('%s%s%s', file.folder, filesep, file.name), 'r');
        
        if fp ~= -1
          coh.value  = zeros(0);
          coh.period = zeros(0);
          coh.upper  = zeros(0);
          coh.lower  = zeros(0);
          
          while ~feof(fp)
            l = deblank(sprintf('%s', fgetl(fp)));
            
            if ~isempty(l)
              values = sscanf(l, '%f');
              
              if (nonrobust && length(values) == 9) || (bounded && length(values) == 10)
                coh.value(length(coh.value) + 1)   = values(3);
                coh.period(length(coh.period) + 1) = values(1);
                coh.upper(length(coh.upper) + 1)   = values(7);
                coh.lower(length(coh.lower) + 1)   = values(6);
              end
            end
          end
          
          if ~isempty(coh.value)
            coh.value  = flip(coh.value);
            coh.period = flip(coh.period);
            coh.upper  = flip(coh.upper);
            coh.lower  = flip(coh.lower);
            
            c2file.name      = file.name;
            c2file.folder    = file.folder;
            c2file.bytes     = file.bytes;
            c2file.site      = j.site;
            c2file.band      = j.band;
            c2file.input     = input;
            c2file.output    = output;
            c2file.nonrobust = nonrobust;
            c2file.bounded   = bounded;
            c2file.stage     = stage;
            c2file.coh       = coh;
            
            c2file.samplingRate = j.samplingRate;
            c2file.measurementUnit = j.measurementUnit;
            c2file.frequencyBand = j.frequencyBand;
          end
          
          fclose(fp);
        end
      end
    end
  end
end