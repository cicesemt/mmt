function showerror(error)
  if ~isempty(error)
    cprintf('#B51700', '%s\n', error);
  end
end