function paramsverify(globalFlag, surveyFlag, siteFlag)
  if globalFlag
    verifyGlobal();
  elseif surveyFlag
    if ~isempty(survey())
      verifySurvey(survey());
    end
  elseif siteFlag
    if ~isempty(site())
      verifySite(site());
    end
  else
    if ~isempty(local())
      verifyLocal();
    elseif ~isempty(site())
      verifySite(site());
    elseif ~isempty(survey())
      verifySurvey(survey());
    end
  end
end

function verifyGlobal()
  if nrefm() < ninpm()
    cprintf('err', '[mistake] nrefm must be greater than or equals to ninpm\n');
  end
end

function verifySurvey(survey)
  if isempty(acqby(survey))
    cprintf([1 0.5 0], '[warning] acqby is empty\n');
  end

  if isempty(fileby(survey))
    cprintf([1 0.5 0], '[warning] fileby is empty\n');
  end
end

function verifySite(site)
  if isempty(loc(site))
    cprintf([1 0.5 0], '[warning] loc is empty\n');
  end
end

function verifyLocal()
  if nout() > noutm()
    cprintf('err', '[mistake] nout must be less than noutm\n');
  end

  if ninp() > ninpm()
    cprintf('err', '[mistake] ninp must be less than ninpm\n');
  end

  nrrFlag = false;

  if ilev()
    if nref() < ninp()
      cprintf('err', '[mistake] nref must be at least as large as ninp\n');
    end

    if nref() > nrefm()
      cprintf('err', '[mistake] nref must be less than nrefm\n');
    end

    if nref() > 3
      if nrr() ~= 1
        cprintf([1 0.5 0], '[warning] nrr must be set to 1\n');
        nrrFlag = true;
      end
    elseif nref() == 3 && ninp() < 3
      if nrr() ~= 1
        cprintf([1 0.5 0], '[warning] nrr must be set to 1\n');
        nrrFlag = true;
      end
    end
  end

  if ilev() && nrr()
    nrrFlag = true;
  end

  offs = 0.57 - (0.07 * tbw());

  if ~deltat()
    cprintf([1 0.5 0], '[warning] deltat must be set to 1\n');
  end

  if mod(nfft(), 2) ~= 0
    cprintf([1 0.5 0], '[warning] nfft must be even\n');
  elseif nfft() / (nsctinc()^(nsctmax() - 1)) < 2
    cprintf([1 0.5 0], '[warning] nfft/(nsctinc^(nsctmax-1)) is too small\n');
  end

  if tbw() >= 2.0 && tbw() <= 3.0
    if nfinc() < 2
      cprintf([1 0.5 0], '[warning] nfinc must be at least 2\n');
    end
  elseif tbw() > 3.0
    if nfinc < 3
      cprintf([1 0.5 0], '[warning] nfinc must be at least 3\n');
    end
  end

  if (nptsm() * (nsctinc()^(nsctmax() - 1))) / (nfft() * offs) > nsectm()
    cprintf('err', '[mistake] number of sections exceeds dimension of nsectm\n');
  end

  if nfft() > nptssm()
    cprintf('err', '[mistake] nfft exceeds dimension of nptssm\n');
  end

  if nsctmax() * nfsect() > nfm()
    cprintf('err', '[mistake] number of periods exceeds dimension of nfm\n');
  end

  if nfsect() > nfsm()
    cprintf('err', '[mistake] periods per section exceeds dimension of nfsm\n');
  end

  if ilev()
    if (nrrFlag && c2threshb()) || c2threshe() || (nout() == 3 && ~nz() && c2threshe1())
      if ~perlo()
        cprintf('err', '[mistake] perlo must be non zero\n');
      end

      if ~perhi()
        cprintf('err', '[mistake] perhi must be non zero\n');
      end
    end
  end

  c2crit = 1 - 0.05^(2 / (2 * (nblk() - ninp())));

  if nrrFlag
    if c2threshb() && c2threshb() < c2crit
      cprintf([1 0.5 0], '[warning] zero coherence at 95%% significance of %f is larger than specified value of c2threshb for 1st stage\n', c2crit);
    end
  end

  if c2threshe() && c2threshe() < c2crit
    cprintf([1 0.5 0], '[warning] zero coherence at 95%% significance of %f is larger than specified value of c2threshe for 2nd stage\n', c2crit);
  end

  if nout() == 3 && ~nz() && c2threshe1() && c2threshe1() < c2crit
    cprintf([1 0.5 0], '[warning] zero coherence at 95%% significance of %f is larger than specified value of c2threshe1 for third output variable\n', c2crit);
  end
end