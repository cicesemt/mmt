function [varargout] = site(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    if strcmpi(varargin{1}, 'info')
      varargin(:, 1) = [];
      siteinfo(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'bands')
      varargin(:, 1) = [];
      sitebands(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'channels')
      varargin(:, 1) = [];
      sitechannels(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'path')
      varargin(:, 1) = [];
      sitepath(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'match')
      varargin(:, 1) = [];
      sitematch(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'compass')
      varargin(:, 1) = [];
      sitecompass(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'timeline')
      varargin(:, 1) = [];
      sitetimeline(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'samples')
      varargin(:, 1) = [];
      sitesamples(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'elevation')
      varargin(:, 1) = [];
      siteelevation(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'map')
      varargin(:, 1) = [];
      sitemap(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'params')
      varargin(:, 1) = [];
      
      if nargout > 0
        varargout{1} = siteparams(varargin{:});
      else
        siteparams(varargin{:});
      end
      
      return;
    end
    
    if strcmpi(varargin{1}, 'text')
      varargin(:, 1) = [];
      
      if nargout > 0
        varargout{1} = sitetext(varargin{:});
      else
        sitetext(varargin{:});
      end
      
      return;
    end
  end
  
  if ~isempty(varargin)
    path = varargin{1};
  else
    path = pwd();
  end
  
  currentSite = loadSite(path);
  
  if ~isempty(currentSite)
    if nargout > 0
      varargout{1} = currentSite;
    else
      fprintf('%s\n', currentSite.name);
    end
  end
end

function currentSite = loadSite(path)
  currentSite = [];
  
  files = dir(path);
  
  if ~isempty(files)
    currentPath = [];
    
    bands = struct(...
      'name', {}, ...
      'folder', {}, ...
      'bytes', {}, ...
      'samplingRate', {}, ...
      'measurementUnit', {}, ...
      'numberOfSamples', {}, ...
      'frequency', {}, ...
      'latitude', {}, ...
      'longitude', {}, ...
      'elevation', {}, ...
      'startTime', {}, ...
      'stopTime', {}, ...
      'channels', {}...
    );
    
    startings = zeros(0);
    
    for ii = 1 : length(files)
      file = files(ii);
      
      if strcmp(file.name, '.')
        currentPath = file.folder;
      elseif ~strcmp(file.name, '..') && file.isdir
        currentBand = band(sprintf('%s%s%s', file.folder, filesep, file.name));
        
        if ~isempty(currentBand) && strcmp(currentBand.folder, sprintf('%s%s%s', file.folder, filesep, file.name))
          bands(length(bands) + 1) = currentBand;
          startings(length(startings) + 1) = currentBand.startTime;
        end
      end
    end
    
    if ~isempty(bands)
      [path, ~] = fileparts(bands(1).folder);
      [~, name] = fileparts(path);
      
      currentSite.name = name;
      currentSite.folder = path;
      currentSite.bytes = bands(1).bytes;
      currentSite.numberOfSamples = bands(1).numberOfSamples;
      currentSite.latitude = bands(1).latitude;
      currentSite.longitude = bands(1).longitude;
      currentSite.elevation = bands(1).elevation;
      currentSite.startTime = bands(1).startTime;
      currentSite.stopTime = bands(1).stopTime;
      currentSite.bands = struct(...
        'name', {}, ...
        'folder', {}, ...
        'bytes', {}, ...
        'samplingRate', {}, ...
        'measurementUnit', {}, ...
        'numberOfSamples', {}, ...
        'frequency', {}, ...
        'latitude', {}, ...
        'longitude', {}, ...
        'elevation', {}, ...
        'startTime', {}, ...
        'stopTime', {}, ...
        'channels', {}...
      );
      currentSite.currentBand = [];
      
      for ii = 2 : length(bands)
        currentSite.bytes = currentSite.bytes + bands(ii).bytes;
        currentSite.numberOfSamples = currentSite.numberOfSamples + bands(ii).numberOfSamples;
        currentSite.latitude = currentSite.latitude + bands(ii).latitude;
        currentSite.longitude = currentSite.longitude + bands(ii).longitude;
        currentSite.elevation = currentSite.elevation + bands(ii).elevation;
        
        if currentSite.startTime > bands(ii).startTime
          currentSite.startTime = bands(ii).startTime;
        end
        
        if currentSite.stopTime < bands(ii).stopTime
          currentSite.stopTime = bands(ii).stopTime;
        end
      end
      
      currentSite.latitude = round(currentSite.latitude / length(bands));
      currentSite.longitude = round(currentSite.longitude / length(bands));
      currentSite.elevation = round(currentSite.elevation / length(bands));
      
      [~, index] = sort(startings);
      
      for ii = 1 : length(index)
        currentSite.bands(ii) = bands(index(ii));
        
        if strcmp(currentSite.bands(ii).folder, pwd())
          currentSite.currentBand = currentSite.bands(ii);
        end
      end
      
      while true
        swap = false;
        
        for ii = 2 : length(currentSite.bands)
          if currentSite.bands(ii - 1).frequency > currentSite.bands(ii).frequency
            swp = currentSite.bands(ii - 1);
            currentSite.bands(ii - 1) = currentSite.bands(ii);
            currentSite.bands(ii) = swp;
            
            swap = true;
          end
        end
        
        if ~swap
          break;
        end
      end
    elseif ~isempty(currentPath)
      currentBand = band(currentPath);
      
      if ~isempty(currentBand)
        [parentPath, ~] = fileparts(currentBand.folder);
        currentSite = site(parentPath);
      end
    end
  end
end