function remoteoverlap(varargin)
  startTime = [];
  stopTime = [];
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          startTime = varargin{k};
          varargin(:, k) = [];
          
          if isnan(str2double(startTime))
            startTime = string2posixtime(startTime);
          else
            startTime = fix(str2double(startTime));
          end
        end
      elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          stopTime = varargin{k};
          varargin(:, k) = [];
          
          if isnan(str2double(stopTime))
            stopTime = string2posixtime(stopTime);
          else
            stopTime = fix(str2double(stopTime));
          end
        end
      else
        k = k + 1;
      end
    end
  end
  
  localInfo = localinfo();
  
  if ~isempty(localInfo)
    remoteInfo = remoteinfo();
    
    if ~isempty(remoteInfo)
      if isempty(startTime)
        startTime = min(localInfo.startTime, remoteInfo.startTime);
      end
      
      if isempty(stopTime)
        stopTime = max(localInfo.stopTime, remoteInfo.stopTime);
      end
      
      if startTime < stopTime
        time1 = max(localInfo.startTime, remoteInfo.startTime);
        time2 = min(localInfo.stopTime, remoteInfo.stopTime);
        
        start = datetime(time1, 'convertfrom', 'posixtime');
        stop = datetime(time2, 'convertfrom', 'posixtime');
        
        startLabel = sprintf('%s', datestr(start, 'yyyy/mm/dd HH:MM:SS'));
        stopLabel = sprintf('%s', datestr(stop, 'yyyy/mm/dd HH:MM:SS'));
        
        if start.Year < stop.Year
          pattern = 'yy/mm/dd HH:MM:SS';
        elseif start.Month < stop.Month
          pattern = 'mm/dd HH:MM:SS';
        elseif start.Day < stop.Day
          pattern = 'dd HH:MM:SS';
        elseif start.Hour < stop.Hour
          pattern = 'HH:MM:SS';
        elseif start.Minute < stop.Minute
          pattern = 'MM:SS';
        else
          pattern = 'SS';
        end
        
        label1 = sprintf('%s', datestr(datetime(time1, 'convertfrom', 'posixtime'), pattern));
        label2 = sprintf('%s', datestr(datetime(time2, 'convertfrom', 'posixtime'), pattern));
        
        f = figure();
        a = axes();
        
        f.set('NumberTitle', 'off');
        f.set('ToolBar', 'none');
        f.set('MenuBar', 'none');
        f.set('Name', 'Overlap');
        
        a.FontSize = 10;
        a.FontWeight = 'normal';
        
        a.Title.String = sprintf('%d%%', round(rate(localInfo.startTime, localInfo.stopTime, remoteInfo.startTime, remoteInfo.stopTime)));
        a.Title.FontSize = 14;
        a.Title.FontWeight = 'bold';
        
        a.XLabel.String = {'', sprintf('%s to %s', startLabel, stopLabel), ''};
        a.XLabel.FontSize = 12;
        a.XLabel.FontWeight = 'bold';
        
        a.XLim = [startTime stopTime];
        a.XTick = [time1 time2];
        
        if (time2 - time1) / (stopTime - startTime) > 0.2
          a.XTickLabel = {label1 label2};
        else
          a.XTickLabel = {'' ''};
        end
        
        a.XGrid = 'on';
        
        a.YLim = [0 3];
        a.YTick = [1 2];
        a.YTickLabel = {'remote', 'local'};
        a.YGrid = 'on';
        
        a.Box = 'on';
        hold(a, 'on');
        
        fposition = get(f, 'Position');
        fheight = fposition(4);
        
        aposition = get(a, 'Position');
        aheight = aposition(4) * fheight;
        
        lheight = round(aheight / (1.1 * 2 + 1));
        
        if localInfo.startTime < time1
          plot([localInfo.startTime time1], [2 2], 'Color', [0 0.4470 0.7410 0.25], 'LineStyle', '-', 'LineWidth', lheight);
        end
        
        plot([time1 time2], [2 2], 'Color', [0 0.4470 0.7410], 'LineStyle', '-', 'LineWidth', lheight);
        
        if localInfo.stopTime > time2
          plot([time2 localInfo.stopTime], [2 2], 'Color', [0 0.4470 0.7410 0.25], 'LineStyle', '-', 'LineWidth', lheight);
        end
        
        if remoteInfo.startTime < time1
          plot([remoteInfo.startTime time1], [1 1], 'Color', [0 0.4470 0.7410 0.25], 'LineStyle', '-', 'LineWidth', lheight);
        end
        
        plot([time1 time2], [1 1], 'Color', [0 0.4470 0.7410], 'LineStyle', '-', 'LineWidth', lheight);
        
        if remoteInfo.stopTime > time2
          plot([time2 remoteInfo.stopTime], [1 1], 'Color', [0 0.4470 0.7410 0.25], 'LineStyle', '-', 'LineWidth', lheight);
        end
      end
    end
  end
end

function rate = rate(startTime1, stopTime1, startTime2, stopTime2)
  rate = 0;
  
  if startTime1 < stopTime2
    if stopTime1 > startTime2
      if startTime1 > startTime2
        startTime2 = startTime1;
      end
      
      if stopTime1 < stopTime2
        stopTime2 = stopTime1;
      end
      
      rate = ((stopTime2 - startTime2) / (stopTime1 - startTime1)) * 100;
    end
  end
end