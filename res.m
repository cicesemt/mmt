function res(varargin)
  sites = cell(0);
  bands = cell(0);
  
  xxflag = false;
  xyflag = false;
  yxflag = false;
  yyflag = false;
  ssflag = false;
  ppflag = false;
  ddflag = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-site')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          sites{length(sites) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          bands{length(bands) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-xx')
        xxflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-xy')
        xyflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-yx')
        yxflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-yy')
        yyflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-s')
        ssflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-p')
        ppflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-det')
        ddflag = true;
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if ~isempty(varargin)
    args = varargin;
  else
    args = {pwd()};
  end
  
  if ~xxflag && ~xyflag && ~yxflag && ~yyflag && ~ssflag && ~ppflag && ~ddflag
    xyflag = true;
    yxflag = true;
  end
  
  jfiles = jselect(args, sites, bands);
  
  if ~isempty(jfiles)
    ymax = 10^4;
    ymin = 10^0;
    
    for ii = 1 : length(jfiles)
      jfile = jfiles{ii};
      
      if xxflag
        ymax = max(ymax, max(jfile.rxx.value));
        ymin = min(ymin, min(jfile.rxx.value));
      end
      
      if xyflag
        ymax = max(ymax, max(jfile.rxy.value));
        ymin = min(ymin, min(jfile.rxy.value));
      end
      
      if yxflag
        ymax = max(ymax, max(jfile.ryx.value));
        ymin = min(ymin, min(jfile.ryx.value));
      end
      
      if yyflag
        ymax = max(ymax, max(jfile.ryy.value));
        ymin = min(ymin, min(jfile.ryy.value));
      end
      
      if ssflag || ppflag || ddflag
        [zxx, zxy, zyx, zyy] = zohms(jfile);
        
        if ssflag || ppflag
          [s, p] = rsp(zxx, zxy, zyx, zyy);
          
          if ssflag
            ymax = max(ymax, max(s.rho));
            ymin = min(ymin, min(s.rho));
          end
          
          if ppflag
            ymax = max(ymax, max(p.rho));
            ymin = min(ymin, min(p.rho));
          end
        end
        
        if ddflag
          [r] = rdet(zxx, zxy, zyx, zyy);
          
          ymax = max(ymax, max(r.value));
          ymin = min(ymin, min(r.value));
        end
      end
    end
    
    k = 4;
    
    while true
      if ymax <= 10^k
        ymax = 10^k;
        break;
      end
      
      k = k + 1;
    end
    
    k = 0;
    
    while true
      if ymin >= 10^k
        ymin = 10^k;
        break;
      end
      
      k = k - 1;
    end
    
    f = figure;
    f.set('NumberTitle', 'off');
    f.set('MenuBar', 'figure');
    f.set('ToolBar', 'figure');
    
    f.UserData.id = 'mmt-res-plot';
    f.UserData.freezed = false;
    
    f.UserData.RhoPlot            = [];
    f.UserData.RhoSeries          = {};
    f.UserData.RhoMaskSeries      = {};
    f.UserData.RhoRepaintSeries   = {};
    f.UserData.RhoSelectionSeries = {};
    
    f.UserData.PhiPlot            = [];
    f.UserData.PhiSeries          = {};
    f.UserData.PhiMaskSeries      = {};
    f.UserData.PhiRepaintSeries   = {};
    f.UserData.PhiSelectionSeries = {};
    
    set(f, 'KeyPressFcn', @keypress);
    
    for ii = 1 : length(jfiles)
      jfile = jfiles{ii};
      
      if ii == 1
        f.set('Name', sprintf('%s[%s]', jfile.site, jfile.frequencyBand));
      else
        f.set('Name', sprintf('%s, %s', f.get('Name'), sprintf('%s[%s]', jfile.site, jfile.frequencyBand)));
      end
      
      if xxflag
        showXX(f, jfile, ymin);
      end
      
      if xyflag
        showXY(f, jfile, ymin);
      end
      
      if yxflag
        showYX(f, jfile, ymin);
      end
      
      if yyflag
        showYY(f, jfile, ymin);
      end
      
      if ssflag || ppflag || ddflag
        [zxx, zxy, zyx, zyy] = zohms(jfile);
        
        if ssflag || ppflag
          [s, p] = rsp(zxx, zxy, zyx, zyy);
          
          if ssflag
            showSS(f, jfile, s);
          end
          
          if ppflag
            showPP(f, jfile, p);
          end
        end
        
        if ddflag
          [r, p] = rdet(zxx, zxy, zyx, zyy);
          
          showDD(f, jfile, r, p, ymin);
        end
      end
    end
    
    rhoplot = subplot(2, 1, 1);
    title('Resistivity');
    ylim([ymin, ymax]);
    ylabel('Rho_{ap} [Ohm-m]');
    
    legends = cell(0);
    
    if xxflag
      legends{length(legends) + 1} = '\rho_{XX}';
    end
    
    if xyflag
      legends{length(legends) + 1} = '\rho_{XY}';
    end
    
    if yxflag
      legends{length(legends) + 1} = '\rho_{YX}';
    end
    
    if yyflag
      legends{length(legends) + 1} = '\rho_{YY}';
    end
    
    if ssflag
      legends{length(legends) + 1} = '\rho_{S}';
    end
    
    if ppflag
      legends{length(legends) + 1} = '\rho_{P}';
    end
    
    if ddflag
      legends{length(legends) + 1} = '\rho_{det}';
    end
    
    legend(gca, legends);
    grid on;
    
    phiplot = subplot(2, 1, 2);
    ylim([-180, 180]);
    yticks(-180 : 45 : 180);
    ylabel('Phase [degrees]');
    xlabel('Period [seconds]');
    
    legends = cell(0);
    
    if xxflag
      legends{length(legends) + 1} = '\phi_{XX}';
    end
    
    if xyflag
      legends{length(legends) + 1} = '\phi_{XY}';
    end
    
    if yxflag
      legends{length(legends) + 1} = '\phi_{YX}';
    end
    
    if yyflag
      legends{length(legends) + 1} = '\phi_{YY}';
    end
    
    if ssflag
      legends{length(legends) + 1} = '\phi_{S}';
    end
    
    if ppflag
      legends{length(legends) + 1} = '\phi_{P}';
    end
    
    if ddflag
      legends{length(legends) + 1} = '\phi_{det}';
    end
    
    legend(gca, legends);
    grid on;
    
    posr = get(rhoplot, 'position');
    posp = get(phiplot, 'position');
    
    posr(4) = posr(4) + (posp(4) * 0.25);
    posr(2) = posr(2) - (posp(4) * 0.25);
    posp(4) = posp(4) - (posp(4) * 0.25);
    
    set(rhoplot, 'position', posr);
    set(phiplot, 'position', posp);
    
    f.UserData.RhoPlot = rhoplot;
    f.UserData.PhiPlot = phiplot;
    
    f.UserData.RhoPlot.UserData.AxesType = 'rho';
    f.UserData.PhiPlot.UserData.AxesType = 'phi';
    
    enmask(f);
    repaint(f);
  end
end

function showXX(figure, jfile, ymin)
  value  = jfile.rxx.value;
  period = jfile.rxx.period;
  upper  = jfile.rxx.upper;
  lower  = jfile.rxx.lower;
  
  axes = subplot(2, 1, 1);
  series = loglog(period, value, 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = lower;
  ye(2, :) = upper;
  
  for ii = 1 : length(ye(1, :))
    if ye(1, ii) <= 0
      ye(1, ii) = ymin;
    end
  end
  
  eseries = loglog(xe, ye, 'Color', 'b', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'rxx';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'rxx';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.RhoSeries{length(figure.UserData.RhoSeries) + 1} = series;
  
  value  = jfile.pxx.value;
  period = jfile.pxx.period;
  upper  = jfile.pxx.upper;
  lower  = jfile.pxx.lower;
  
  axes = subplot(2, 1, 2);
  series = semilogx(period, value, 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = lower;
  ye(2, :) = upper;
  
  eseries = semilogx(xe, ye, 'Color', 'b', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'pxx';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'pxx';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.PhiSeries{length(figure.UserData.PhiSeries) + 1} = series;
end

function showXY(figure, jfile, ymin)
  value  = jfile.rxy.value;
  period = jfile.rxy.period;
  upper  = jfile.rxy.upper;
  lower  = jfile.rxy.lower;
  
  axes = subplot(2, 1, 1);
  series = loglog(period, value, 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = lower;
  ye(2, :) = upper;
  
  for ii = 1 : length(ye(1, :))
    if ye(1, ii) <= 0
      ye(1, ii) = ymin;
    end
  end
  
  eseries = loglog(xe, ye, 'Color', 'b', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'rxy';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'rxy';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.RhoSeries{length(figure.UserData.RhoSeries) + 1} = series;
  
  value  = jfile.pxy.value;
  period = jfile.pxy.period;
  upper  = jfile.pxy.upper;
  lower  = jfile.pxy.lower;
  
  axes = subplot(2, 1, 2);
  series = semilogx(period, value, 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = lower;
  ye(2, :) = upper;
  
  eseries = semilogx(xe, ye, 'Color', 'b', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'pxy';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'pxy';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.PhiSeries{length(figure.UserData.PhiSeries) + 1} = series;
end

function showYX(figure, jfile, ymin)
  value  = jfile.ryx.value;
  period = jfile.ryx.period;
  upper  = jfile.ryx.upper;
  lower  = jfile.ryx.lower;
  
  axes = subplot(2, 1, 1);
  series = loglog(period, value, 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = lower;
  ye(2, :) = upper;
  
  for ii = 1 : length(ye(1, :))
    if ye(1, ii) <= 0
      ye(1, ii) = ymin;
    end
  end
  
  eseries = loglog(xe, ye, 'Color', 'r', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'ryx';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'ryx';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.RhoSeries{length(figure.UserData.RhoSeries) + 1} = series;
  
  value  = jfile.pyx.value;
  period = jfile.pyx.period;
  upper  = jfile.pyx.upper;
  lower  = jfile.pyx.lower;
  
  axes = subplot(2, 1, 2);
  series = semilogx(period, value, 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = lower;
  ye(2, :) = upper;
  
  eseries = semilogx(xe, ye, 'Color', 'r', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'pyx';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'pyx';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.PhiSeries{length(figure.UserData.PhiSeries) + 1} = series;
end

function showYY(figure, jfile, ymin)
  value  = jfile.ryy.value;
  period = jfile.ryy.period;
  upper  = jfile.ryy.upper;
  lower  = jfile.ryy.lower;
  
  axes = subplot(2, 1, 1);
  series = loglog(period, value, 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = lower;
  ye(2, :) = upper;
  
  for ii = 1 : length(ye(1, :))
    if ye(1, ii) <= 0
      ye(1, ii) = ymin;
    end
  end
  
  eseries = loglog(xe, ye, 'Color', 'r', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'ryy';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'ryy';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.RhoSeries{length(figure.UserData.RhoSeries) + 1} = series;
  
  value  = jfile.pyy.value;
  period = jfile.pyy.period;
  upper  = jfile.pyy.upper;
  lower  = jfile.pyy.lower;
  
  axes = subplot(2, 1, 2);
  series = semilogx(period, value, 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = lower;
  ye(2, :) = upper;
  
  eseries = semilogx(xe, ye, 'Color', 'r', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'pyy';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'pyy';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.PhiSeries{length(figure.UserData.PhiSeries) + 1} = series;
end

function showSS(figure, jfile, s)
  axes = subplot(2, 1, 1);
  series = loglog(s.period, s.rho, 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;

  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'rs';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = [];
  
  figure.UserData.RhoSeries{length(figure.UserData.RhoSeries) + 1} = series;

  axes = subplot(2, 1, 2);
  series = semilogx(s.period, s.phi, 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'ps';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = [];
  
  figure.UserData.PhiSeries{length(figure.UserData.PhiSeries) + 1} = series;
end

function showPP(figure, jfile, p)
  axes = subplot(2, 1, 1);
  series = loglog(p.period, p.rho, 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'rp';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = [];
  
  figure.UserData.RhoSeries{length(figure.UserData.RhoSeries) + 1} = series;
  
  axes = subplot(2, 1, 2);
  series = semilogx(p.period, p.phi, 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'pp';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = [];
  
  figure.UserData.PhiSeries{length(figure.UserData.PhiSeries) + 1} = series;
end

function showDD(figure, jfile, r, p, ymin)
  axes = subplot(2, 1, 1);
  series = loglog(r.period, r.value, 'Color', 'k', 'LineStyle', 'none', 'Marker', 'o');
  hold on;

  xe = zeros(2, length(r.period));
  xe(1, :) = r.period;
  xe(2, :) = r.period;
  ye = zeros(2, length(r.period));
  ye(1, :) = r.lower;
  ye(2, :) = r.upper;

  for ii = 1 : length(ye(1, :))
    if ye(1, ii) <= 0
      ye(1, ii) = ymin;
    end
  end

  eseries = loglog(xe, ye, 'Color', 'k', 'LineStyle', '-', 'HandleVisibility', 'off');

  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'rd';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'rd';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.RhoSeries{length(figure.UserData.RhoSeries) + 1} = series;
  
  axes = subplot(2, 1, 2);
  series = semilogx(p.period, p.value, 'Color', 'k', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(p.period));
  xe(1, :) = p.period;
  xe(2, :) = p.period;
  ye = zeros(2, length(p.period));
  ye(1, :) = p.lower;
  ye(2, :) = p.upper;
  
  eseries = semilogx(xe, ye, 'Color', 'k', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'pd';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'pd';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.PhiSeries{length(figure.UserData.PhiSeries) + 1} = series;
end

function enmask(figure)
  rhoMaskSeries = figure.UserData.RhoMaskSeries;
  phiMaskSeries = figure.UserData.PhiMaskSeries;
  
  figure.UserData.RhoMaskSeries = {};
  figure.UserData.PhiMaskSeries = {};
  
  axes(figure.UserData.RhoPlot);
  
  for ii = 1 : length(figure.UserData.RhoSeries)
    series = figure.UserData.RhoSeries{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = false(1, length(series.XData));
    flag = false;
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = true;
          flag = true;
          break;
        end
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      mdata = loglog(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      mdata.UserData.SeriesType  = 'mdata';
      mdata.UserData.SeriesName  = series.UserData.SeriesName;
      mdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      mdata.UserData.SeriesError = [];
      
      if ~isempty(series.UserData.SeriesError)
        for jj = 1 : length(filter)
          if filter(jj)
            errorSeries = series.UserData.SeriesError(jj);
            
            XData     = errorSeries.XData;
            YData     = errorSeries.YData;
            LineStyle = errorSeries.LineStyle;
            Marker    = errorSeries.Marker;
            
            merror = loglog(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
            
            merror.UserData.SeriesType = 'merror';
            merror.UserData.SeriesName = errorSeries.UserData.SeriesName;
            merror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
            merror.UserData.SeriesData = mdata;
            merror.UserData.XData      = errorSeries.UserData.XData;
            merror.UserData.YData      = errorSeries.UserData.YData;
            
            set(merror, 'ButtonDownFcn', @seriesmousedown);
            
            mdata.UserData.SeriesError(length(mdata.UserData.SeriesError) + 1) = merror;
          end
        end
      end
      
      set(mdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.RhoMaskSeries{length(figure.UserData.RhoMaskSeries) + 1} = mdata;
    end
  end
  
  axes(figure.UserData.PhiPlot);
  
  for ii = 1 : length(figure.UserData.PhiSeries)
    series = figure.UserData.PhiSeries{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = false(1, length(series.XData));
    flag = false;
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = true;
          flag = true;
          break;
        end
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      mdata = semilogx(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      mdata.UserData.SeriesType  = 'mdata';
      mdata.UserData.SeriesName  = series.UserData.SeriesName;
      mdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      mdata.UserData.SeriesError = [];
      
      if ~isempty(series.UserData.SeriesError)
        for jj = 1 : length(filter)
          if filter(jj)
            errorSeries = series.UserData.SeriesError(jj);
            
            XData     = errorSeries.XData;
            YData     = errorSeries.YData;
            LineStyle = errorSeries.LineStyle;
            Marker    = errorSeries.Marker;
            
            merror = semilogx(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
            
            merror.UserData.SeriesType = 'merror';
            merror.UserData.SeriesName = errorSeries.UserData.SeriesName;
            merror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
            merror.UserData.SeriesData = mdata;
            merror.UserData.XData      = errorSeries.UserData.XData;
            merror.UserData.YData      = errorSeries.UserData.YData;
            
            set(merror, 'ButtonDownFcn', @seriesmousedown);
            
            mdata.UserData.SeriesError(length(mdata.UserData.SeriesError) + 1) = merror;
          end
        end
      end
      
      set(mdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.PhiMaskSeries{length(figure.UserData.PhiMaskSeries) + 1} = mdata;
    end
  end
  
  for ii = 1 : length(rhoMaskSeries)
    for jj = 1 : length(rhoMaskSeries{ii}.UserData.SeriesError)
      delete(rhoMaskSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(rhoMaskSeries{ii});
  end
  
  for ii = 1 : length(phiMaskSeries)
    for jj = 1 : length(phiMaskSeries{ii}.UserData.SeriesError)
      delete(phiMaskSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(phiMaskSeries{ii});
  end
end

function repaint(figure)
  rhoRepaintSeries = figure.UserData.RhoRepaintSeries;
  phiRepaintSeries = figure.UserData.PhiRepaintSeries;
  
  figure.UserData.RhoRepaintSeries = {};
  figure.UserData.PhiRepaintSeries = {};
  
  axes(figure.UserData.RhoPlot);
  
  for ii = 1 : length(figure.UserData.RhoSeries)
    series = figure.UserData.RhoSeries{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = true(1, length(series.XData));
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = false;
          break;
        end
      end
    end
    
    flag = false;
    
    for jj = 1 : length(filter)
      if filter(jj)
        flag = true;
        break;
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      Color     = series.Color;
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      rdata = loglog(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      rdata.UserData.SeriesType  = 'rdata';
      rdata.UserData.SeriesName  = series.UserData.SeriesName;
      rdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      rdata.UserData.SeriesError = [];
      
      if ~isempty(series.UserData.SeriesError)
        for jj = 1 : length(filter)
          if filter(jj)
            errorSeries = series.UserData.SeriesError(jj);
            
            XData     = errorSeries.XData;
            YData     = errorSeries.YData;
            Color     = errorSeries.Color;
            LineStyle = errorSeries.LineStyle;
            Marker    = errorSeries.Marker;

            rerror = loglog(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
            
            rerror.UserData.SeriesType = 'rerror';
            rerror.UserData.SeriesName = errorSeries.UserData.SeriesName;
            rerror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
            rerror.UserData.SeriesData = rdata;
            rerror.UserData.XData      = errorSeries.UserData.XData;
            rerror.UserData.YData      = errorSeries.UserData.YData;
            
            set(rerror, 'ButtonDownFcn', @seriesmousedown);
            
            rdata.UserData.SeriesError(length(rdata.UserData.SeriesError) + 1) = rerror;
          end
        end
      end
      
      set(rdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.RhoRepaintSeries{length(figure.UserData.RhoRepaintSeries) + 1} = rdata;
    end
  end
  
  axes(figure.UserData.PhiPlot);
  
  for ii = 1 : length(figure.UserData.PhiSeries)
    series = figure.UserData.PhiSeries{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = true(1, length(series.XData));
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = false;
          break;
        end
      end
    end
    
    flag = false;
    
    for jj = 1 : length(filter)
      if filter(jj)
        flag = true;
        break;
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      Color     = series.Color;
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      rdata = semilogx(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      rdata.UserData.SeriesType  = 'rdata';
      rdata.UserData.SeriesName  = series.UserData.SeriesName;
      rdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      rdata.UserData.SeriesError = [];
      
      if ~isempty(series.UserData.SeriesError)
        for jj = 1 : length(filter)
          if filter(jj)
            errorSeries = series.UserData.SeriesError(jj);
            
            XData     = errorSeries.XData;
            YData     = errorSeries.YData;
            Color     = errorSeries.Color;
            LineStyle = errorSeries.LineStyle;
            Marker    = errorSeries.Marker;
            
            rerror = semilogx(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
            
            rerror.UserData.SeriesType = 'rerror';
            rerror.UserData.SeriesName = errorSeries.UserData.SeriesName;
            rerror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
            rerror.UserData.SeriesData = rdata;
            rerror.UserData.XData      = errorSeries.UserData.XData;
            rerror.UserData.YData      = errorSeries.UserData.YData;
            
            set(rerror, 'ButtonDownFcn', @seriesmousedown);
            
            rdata.UserData.SeriesError(length(rdata.UserData.SeriesError) + 1) = rerror;
          end
        end
      end
      
      set(rdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.PhiRepaintSeries{length(figure.UserData.PhiRepaintSeries) + 1} = rdata;
    end
  end
  
  for ii = 1 : length(rhoRepaintSeries)
    for jj = 1 : length(rhoRepaintSeries{ii}.UserData.SeriesError)
      delete(rhoRepaintSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(rhoRepaintSeries{ii});
  end
  
  for ii = 1 : length(phiRepaintSeries)
    for jj = 1 : length(phiRepaintSeries{ii}.UserData.SeriesError)
      delete(phiRepaintSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(phiRepaintSeries{ii});
  end
end

function rhoSelect(figure, x1, y1, x2, y2)
  rhoSelectionSeries = figure.UserData.RhoSelectionSeries;
  phiSelectionSeries = figure.UserData.PhiSelectionSeries;
  
  figure.UserData.RhoSelectionSeries = {};
  figure.UserData.PhiSelectionSeries = {};
  
  axes(figure.UserData.RhoPlot);
  
  seriesFilter = false(1, length(figure.UserData.RhoSeries));
  dataFilter = cell(1, length(figure.UserData.RhoSeries));
  dataIndex = cell(1, length(figure.UserData.RhoSeries));
  
  for ii = 1 : length(figure.UserData.RhoSeries)
    series = figure.UserData.RhoSeries{ii};
    
    dataFilter{ii} = false(1, length(series.XData));
    dataIndex{ii} = zeros(0);
    
    for jj = 1 : length(series.XData)
      if series.XData(jj) >= x1 && series.XData(jj) <= x2 && series.YData(jj) >= y1 && series.YData(jj) <= y2
        seriesFilter(ii) = true;
        dataFilter{ii}(jj) = true;
        dataIndex{ii}(length(dataIndex{ii}) + 1) = jj;
      end
    end
    
    if seriesFilter(ii)
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = loglog(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesName  = series.UserData.SeriesName;
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = loglog(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesName = errorSeries.UserData.SeriesName;
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.RhoSelectionSeries{length(figure.UserData.RhoSelectionSeries) + 1} = sdata;
    end
  end
  
  axes(figure.UserData.PhiPlot);
  
  for ii = 1 : length(figure.UserData.PhiSeries)
    if seriesFilter(ii)
      series = figure.UserData.PhiSeries{ii};
      
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesName  = series.UserData.SeriesName;
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesName = errorSeries.UserData.SeriesName;
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.PhiSelectionSeries{length(figure.UserData.PhiSelectionSeries) + 1} = sdata;
    end
  end
  
  for ii = 1 : length(rhoSelectionSeries)
    for jj = 1 : length(rhoSelectionSeries{ii}.UserData.SeriesError)
      delete(rhoSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(rhoSelectionSeries{ii});
  end
  
  for ii = 1 : length(phiSelectionSeries)
    for jj = 1 : length(phiSelectionSeries{ii}.UserData.SeriesError)
      delete(phiSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(phiSelectionSeries{ii});
  end
end

function phiSelect(figure, x1, y1, x2, y2)
  rhoSelectionSeries = figure.UserData.RhoSelectionSeries;
  phiSelectionSeries = figure.UserData.PhiSelectionSeries;
  
  figure.UserData.RhoSelectionSeries = {};
  figure.UserData.PhiSelectionSeries = {};
  
  axes(figure.UserData.PhiPlot);
  
  seriesFilter = false(1, length(figure.UserData.PhiSeries));
  dataFilter = cell(1, length(figure.UserData.PhiSeries));
  dataIndex = cell(1, length(figure.UserData.PhiSeries));
  
  for ii = 1 : length(figure.UserData.PhiSeries)
    series = figure.UserData.PhiSeries{ii};
    
    dataFilter{ii} = false(1, length(series.XData));
    dataIndex{ii} = zeros(0);
    
    for jj = 1 : length(series.XData)
      if series.XData(jj) >= x1 && series.XData(jj) <= x2 && series.YData(jj) >= y1 && series.YData(jj) <= y2
        seriesFilter(ii) = true;
        dataFilter{ii}(jj) = true;
        dataIndex{ii}(length(dataIndex{ii}) + 1) = jj;
      end
    end
    
    if seriesFilter(ii)
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesName  = series.UserData.SeriesName;
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesName = errorSeries.UserData.SeriesName;
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.PhiSelectionSeries{length(figure.UserData.PhiSelectionSeries) + 1} = sdata;
    end
  end
  
  axes(figure.UserData.RhoPlot);
  
  for ii = 1 : length(figure.UserData.RhoSeries)
    if seriesFilter(ii)
      series = figure.UserData.RhoSeries{ii};
      
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = loglog(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesName  = series.UserData.SeriesName;
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = loglog(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesName = errorSeries.UserData.SeriesName;
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.RhoSelectionSeries{length(figure.UserData.RhoSelectionSeries) + 1} = sdata;
    end
  end
  
  for ii = 1 : length(rhoSelectionSeries)
    for jj = 1 : length(rhoSelectionSeries{ii}.UserData.SeriesError)
      delete(rhoSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(rhoSelectionSeries{ii});
  end
  
  for ii = 1 : length(phiSelectionSeries)
    for jj = 1 : length(phiSelectionSeries{ii}.UserData.SeriesError)
      delete(phiSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(phiSelectionSeries{ii});
  end
end

function deselect(figure)
  for ii = 1 : length(figure.UserData.RhoSelectionSeries)
    for jj = 1 : length(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesError)
      delete(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(figure.UserData.RhoSelectionSeries{ii});
  end
  
  for ii = 1 : length(figure.UserData.PhiSelectionSeries)
    for jj = 1 : length(figure.UserData.PhiSelectionSeries{ii}.UserData.SeriesError)
      delete(figure.UserData.PhiSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(figure.UserData.PhiSelectionSeries{ii});
  end
  
  figure.UserData.RhoSelectionSeries = {};
  figure.UserData.PhiSelectionSeries = {};
end

function keypress(figure, event)
  shift = false;
  
  for ii = 1 : length(event.Modifier)
    if strcmp(event.Modifier{ii}, 'shift')
      shift = true;
    end
  end
  
  if strcmp(event.Key, 'r') && ~figure.UserData.freezed
    enmask(figure);
    repaint(figure);
    deselect(figure);
  elseif strcmp(event.Key, 'd')
    figure.UserData.freezed = true;
    flag = false;
    
    for ii = 1 : length(figure.UserData.RhoSelectionSeries)
      for jj = 1 : length(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesIndex)
        [changed, ~] = pmaskadd(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesIndex(jj));
        
        if changed
          flag = true;
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'u')
    figure.UserData.freezed = true;
    flag = false;
    
    for ii = 1 : length(figure.UserData.RhoSelectionSeries)
      for jj = 1 : length(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesIndex)
        [changed, ~] = pmaskremove(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesIndex(jj));
        
        if changed
          flag = true;
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'delete') || strcmp(event.Key, 'backspace')
    figure.UserData.freezed = true;
    flag = false;
    
    if shift
      for ii = 1 : length(figure.UserData.RhoSelectionSeries)
        for jj = 1 : length(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesIndex)
          [changed, ~] = pmaskremove(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesIndex(jj));
          
          if changed
            flag = true;
          end
        end
      end
    else
      for ii = 1 : length(figure.UserData.RhoSelectionSeries)
        for jj = 1 : length(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesIndex)
          [changed, ~] = pmaskadd(figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.RhoSelectionSeries{ii}.UserData.SeriesIndex(jj));
          
          if changed
            flag = true;
          end
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'escape')
    deselect(figure);
  end
end

function seriesmousedown(series, ~)
  mousedown(series.Parent, series);
end

function axesmousedown(axes, ~)
  mousedown(axes, []);
end

function mousedown(axes, ~)
  figure = axes.Parent;
  
  if strcmp(figure.SelectionType, 'normal')
    currentPoint = axes.CurrentPoint;
    
    x1 = currentPoint(1, 1);
    y1 = currentPoint(1, 2);
    
    if x1 >= axes.XLim(1) && x1 <= axes.XLim(2)
      if y1 >= axes.YLim(1) && y1 <= axes.YLim(2)
        top = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        left = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        right = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        bottom = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        
        figure.Pointer = 'crosshair';
        figure.WindowButtonMotionFcn = @mousemotion;
        figure.WindowButtonUpFcn = @mouseup;
      end
    end
  end
  
  function mousemotion(~, ~)
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = currentPoint(1, 2);
    
    if x2 < axes.XLim(1)
      x2 = axes.XLim(1);
    elseif x2 > axes.XLim(2)
      x2 = axes.XLim(2);
    end
    
    if y2 < axes.YLim(1)
      y2 = axes.YLim(1);
    elseif y2 > axes.YLim(2)
      y2 = axes.YLim(2);
    end
    
    top.XData = [x1, x2];
    top.YData = [y1, y1];
    
    left.XData = [x1, x1];
    left.YData = [y1, y2];
    
    right.XData = [x2, x2];
    right.YData = [y1, y2];
    
    bottom.XData = [x1, x2];
    bottom.YData = [y2, y2];
    
    drawnow();
  end
  
  function mouseup(~, ~)
    figure.Pointer = 'arrow';
    figure.WindowButtonMotionFcn = [];
    figure.WindowButtonUpFcn = [];
    
    delete(top);
    delete(left);
    delete(right);
    delete(bottom);
    
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = currentPoint(1, 2);
    
    if x1 ~= x2 && y1 ~= y2
      if x1 > x2
        x  = x1;
        x1 = x2;
        x2 = x;
      end
      
      if y1 > y2
        y  = y1;
        y1 = y2;
        y2 = y;
      end
      
      if x1 < axes.XLim(1)
        x1 = axes.XLim(1);
      end
      
      if x2 > axes.XLim(2)
        x2 = axes.XLim(2);
      end
      
      if y1 < axes.YLim(1)
        y1 = axes.YLim(1);
      end
      
      if y2 > axes.YLim(2)
        y2 = axes.YLim(2);
      end
      
      if strcmp(axes.UserData.AxesType, 'rho')
        rhoSelect(figure, x1, y1, x2, y2);
      elseif strcmp(axes.UserData.AxesType, 'phi')
        phiSelect(figure, x1, y1, x2, y2);
      end
    else
      deselect(figure);
    end
  end
end