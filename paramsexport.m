function paramsexport()
  lfiles = local();
  
  if ~isempty(lfiles)
    ex = lfiles(1);
    ey = lfiles(2);
    hx = lfiles(3);
    hy = lfiles(4);
    
    if length(lfiles) > 4
      hz = lfiles(5);
    else
      hz = [];
    end
    
    rfiles = remote();
    
    if ~isempty(rfiles)
      rx = rfiles(1);
      ry = rfiles(2);
    else
      rx = hx;
      ry = hy;
    end
    
    fp = fopen('parameters.birrp', 'wt');
    
    if fp ~= -1
      if ilev()
        fprintf(fp, 'ilev                    T\n');
      else
        fprintf(fp, 'ilev                    F\n');
      end
      
      fprintf(fp, 'path                    ./\n');
      fprintf(fp, 'nlev                    %d\n', nlev());
      fprintf(fp, 'nout                    %d\n', nout());
      fprintf(fp, 'ninp                    %d\n', ninp());
      
      nrrFlag = false;
      
      if ilev()
        fprintf(fp, 'nref                    %d\n', nref());
        
        if nref() > 3
          nrrFlag = true;
          
          fprintf(fp, 'nr3                     %d\n', nr3());
          fprintf(fp, 'nr2                     %d\n', nr2());
        elseif nref() == 3 && ninp() < 3
          nrrFlag = true;
        end
      end
      
      if ilev()
        if nrr()
          nrrFlag = true;
        end
        
        fprintf(fp, 'nrr                     %d\n', nrr());
      end
      
      fprintf(fp, 'tbw                     %.7f\n', tbw());
      fprintf(fp, 'deltat                  %.8f\n', deltat());
      
      fprintf(fp, 'nfft nsctinc nsctmax    %d,%d,%d\n', nfft(), nsctinc(), nsctmax());
      fprintf(fp, 'nf1 nfinc nfsect        %d,%d,%d\n', nf1(), nfinc(), nfsect());
      fprintf(fp, 'frm                     y\n');
      
      if ilev()
        fprintf(fp, 'mfft                    %d\n', mfft());
      end
      
      if ilev()
        fprintf(fp, 'uin ainlin ainuin       %.4f,%.4f,%.4f\n', uin(), ainlin(), ainuin());
      else
        fprintf(fp, 'uin ainuin              %.4f,%.4f\n', uin(), ainuin());
      end
      
      if nrrFlag
        fprintf(fp, 'c2threshb               %.3f\n', c2threshb());
      end
      
      fprintf(fp, 'c2threshe               %.3f\n', c2threshe());
      
      if nout() == 3
        fprintf(fp, 'nz                      %d\n', nz());
        
        if ~nz()
          fprintf(fp, 'c2threshe1              %.3f\n', c2threshe1());
        end
      end
      
      if ilev()
        if (nrrFlag && c2threshb()) || c2threshe() || (nout() == 3 && ~nz() && c2threshe1())
          fprintf(fp, 'perlo perhi             %.4f,%.4f\n', perlo(), perhi());
        end
      end
      
      fprintf(fp, 'ofil                    %s\n', ofil());
      fprintf(fp, 'nlev                    %d\n', nlev());
      
      if ilev()
        fprintf(fp, 'nprej                   %d\n', nprej());
      end
      
      fprintf(fp, 'npcs                    %d\n', npcs());
      fprintf(fp, 'nar                     %d\n', nar());
      fprintf(fp, 'imode                   %d\n', imode());
      fprintf(fp, 'jmode                   %d\n', jmode());
      
      if nout() >= 1
        fprintf(fp, 'filnam                  %s\n', filnam(ex));
        
        if nout() >= 2
          fprintf(fp, 'filnam                  %s\n', filnam(ey));
          
          if nout() >= 3
            fprintf(fp, 'filnam                  %s\n', filnam(hz));
          end
        end
      end
      
      if ninp() >= 1
        fprintf(fp, 'filnam                  %s\n', filnam(hx));
        
        if ninp() >= 2
          fprintf(fp, 'filnam                  %s\n', filnam(hy));
        end
      end
      
      if nref() >= 1
        fprintf(fp, 'filnam                  %s\n', filnam(rx));
        
        if nref() >= 2
          fprintf(fp, 'filnam                  %s\n', filnam(ry));
        end
      end
      
      if nout() >= 2
        fprintf(fp, 'theta1a theta2a phia    %.3f,%.3f,%.3f\n', theta1a(), theta2a(), phia());
      end
      
      fprintf(fp, 'theta1b theta2b phib    %.3f,%.3f,%.3f\n', theta1b(), theta2b(), phib());
      fprintf(fp, 'theta1c theta2c phic    %.3f,%.3f,%.3f\n', theta1c(), theta2c(), phic());
      
      if nr2() == 2
        fprintf(fp, 'theta1d theta2d phid    %.3f,%.3f,%.3f\n', theta1d(), theta2d(), phid());
      end
      
      fclose(fp);
    end
  end
end