function atsplot(filename, varargin)
  file = atsfile(filename);
  
  if ~isempty(file)
    removeTrend = false;
    startTime   = [];
    stopTime    = [];
    current     = 1;
    total       = 1;
    
    if nargin > 1
      if nargin == 2
        if islogical(varargin{1})
          if varargin{1}
            removeTrend = true;
          end
        end
      elseif nargin == 3
        if isnumeric(varargin{1})
          startTime = varargin{1};
        end
        
        if isnumeric(varargin{2})
          stopTime = varargin{2};
        end
      else
        if islogical(varargin{1})
          if varargin{1}
            removeTrend = true;
          end
        end
        
        if isnumeric(varargin{2})
          startTime = varargin{2};
        end
        
        if isnumeric(varargin{3})
          stopTime = varargin{3};
        end
        
        if nargin > 5
          if isnumeric(varargin{4}) && isnumeric(varargin{5})
            if varargin{4} > 0 && varargin{5} > 0 && varargin{4} <= varargin{5}
              current = varargin{4};
              total   = varargin{5};
            end
          end
        end
      end
    end
    
    if isempty(startTime)
      startTime = file.startTime;
    end
    
    if isempty(stopTime)
      stopTime = file.stopTime;
    end
    
    [data, startTime, stopTime] = atsdata(sprintf('%s%s%s', file.folder, filesep, file.name), removeTrend, startTime, stopTime);
    
    if ~isempty(data)
      if current == 1
        f = figure();
        f.set('NumberTitle', 'off');
        f.set('ToolBar', 'none');
        f.set('MenuBar', 'none');
        f.set('Name', sprintf('%d%s%s%d%s', file.aduSerialNumber, upper(file.channelType), upper(file.boardType), file.samplingRate, upper(file.measurementUnit)));
        
        f.UserData.id                 = 'atsplot';
        f.UserData.folder             = file.folder;
        f.UserData.startTime          = file.startTime;
        f.UserData.stopTime           = file.stopTime;
        f.UserData.timeWidth          = f.UserData.stopTime - f.UserData.startTime;
        f.UserData.frequency          = file.frequency;
        f.UserData.viewport.startTime = startTime;
        f.UserData.viewport.stopTime  = stopTime;
        f.UserData.viewport.timeWidth = f.UserData.viewport.stopTime - f.UserData.viewport.startTime;
        f.UserData.zoom.factor        = 0.1;
        f.UserData.zoom.level         = zeros(1, total);
        f.UserData.selection          = [];
        
        f.UserData.color          = mmtconfig('mmt.ats.plot.color');
        f.UserData.maskColor      = mmtconfig('mmt.ats.plot.maskColor');
        f.UserData.selectionColor = mmtconfig('mmt.ats.plot.selectionColor');
        f.UserData.lineStyle      = mmtconfig('mmt.ats.plot.lineStyle');
        f.UserData.lineWidth      = mmtconfig('mmt.ats.plot.lineWidth');
        f.UserData.marker         = mmtconfig('mmt.ats.plot.marker');
        f.UserData.markerSize     = mmtconfig('mmt.ats.plot.markerSize');
        
        set(f, 'KeyPressFcn', @KeyPressFcn);
        set(f, 'WindowScrollWheelFcn', @WindowScrollWheelFcn);
        
        show(f, file, data, removeTrend, current, total);
      else
        f = gcf();
        
        if startTime == f.UserData.viewport.startTime && stopTime == f.UserData.viewport.stopTime
          f.set('Name', sprintf('%s, %s', f.get('Name'), sprintf('%d%s%s%d%s', file.aduSerialNumber, upper(file.channelType), upper(file.boardType), file.samplingRate, upper(file.measurementUnit))));
          
          if file.startTime > f.UserData.startTime
            f.UserData.startTime = file.startTime;
            f.UserData.timeWidth = f.UserData.stopTime - f.UserData.startTime;
          end
          
          if file.stopTime < f.UserData.stopTime
            f.UserData.stopTime  = file.stopTime;
            f.UserData.timeWidth = f.UserData.stopTime - f.UserData.startTime;
          end
          
          show(f, file, data, removeTrend, current, total);
        end
      end
    end
  end
end

function show(f, file, data, removeTrend, current, total)
  figure(f);
  axes = subplot(total, 1, current);
  cla(axes, 'reset');
  
  period = 1 / file.frequency;
  xdata = f.UserData.viewport.startTime : period : f.UserData.viewport.stopTime - period;
  series = plot(xdata, data, 'Color', f.UserData.color, 'LineStyle', f.UserData.lineStyle, 'LineWidth', f.UserData.lineWidth, 'Marker', f.UserData.marker, 'MarkerSize', f.UserData.markerSize);
  
  axes.YLabel.String     = file.channelType;
  axes.YLabel.FontSize   = 12;
  axes.YLabel.FontWeight = 'bold';
  
  axes.XLim       = [xdata(1) xdata(length(xdata))];
  axes.XTick      = xdata(1) : (xdata(length(xdata)) - xdata(1)) / 4 : xdata(length(xdata));
  axes.XTickLabel = {};
  
  y1 = axes.YLim(1);
  y2 = axes.YLim(2);
  
  if f.UserData.zoom.level(current) > 0
    for ii = 1 : f.UserData.zoom.level(current)
      yinc = (y2 - y1) * (f.UserData.zoom.factor / 2);
      
      y1 = y1 - yinc;
      y2 = y2 + yinc;
    end
  elseif f.UserData.zoom.level(current) < 0
    for ii = 1 : abs(f.UserData.zoom.level(current)) 
      yinc = ((y2 - y1) / (1 + f.UserData.zoom.factor)) * (f.UserData.zoom.factor / 2);
      
      y1 = y1 + yinc;
      y2 = y2 - yinc;
    end
  end
  
  axes.YLim(1) = y1;
  axes.YLim(2) = y2;
  
  axes.YTick      = axes.YLim(1) : (axes.YLim(2) - axes.YLim(1)) / 4 : axes.YLim(2);
  axes.YTickLabel = {};
  
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(axes, 'ButtonDownFcn', @axesmousedown);
  
  if current == 1
    totalSamples = f.UserData.timeWidth * file.frequency;
    
    if length(data) ~= totalSamples
      axes.Title.String = sprintf('%d samples (%d total)', length(data), totalSamples);
    else
      axes.Title.String = sprintf('%d samples', length(data));
    end
    
    axes.Title.FontSize   = 14;
    axes.Title.FontWeight = 'bold';
  end
  
  if current == total
    start = datestr(datetime(f.UserData.viewport.startTime, 'convertfrom', 'posixtime'), 'mmm dd, yyyy HH:MM:SS');
    stop = datestr(datetime(f.UserData.viewport.stopTime, 'convertfrom', 'posixtime'), 'mmm dd, yyyy HH:MM:SS');
    
    axes.XLabel.String = sprintf('%s to %s', start, stop);
    axes.XLabel.FontSize = 12;
    axes.XLabel.FontWeight = 'bold';
    
    i1 = (f.UserData.viewport.startTime - f.UserData.startTime) * f.UserData.frequency;
    i5 = (f.UserData.viewport.stopTime - f.UserData.startTime) * f.UserData.frequency;
    
    i2 = i1 + (i5 - i1) / 4;
    i3 = i2 + (i5 - i1) / 4;
    i4 = i3 + (i5 - i1) / 4;
    
    axes.XTickLabel = {sprintf('%d',i1) sprintf('%d',i2) sprintf('%d',i3) sprintf('%d',i4) sprintf('%d',i5)};
  end
  
  hold(axes, 'on');
  
  mask(f, file, data);
  
  axes.UserData.index       = current;
  axes.UserData.file        = file;
  axes.UserData.removeTrend = removeTrend;
  
  f.UserData.axes{current} = axes;
end

function mask(figure, file, data)
  tm = tmask();
  
  if ~isempty(tm)
    start = tm(:, 1);
    stop  = tm(:, 2);
    
    k = length(start) + 1;
    
    for ii = 1 : length(start)
      if figure.UserData.viewport.startTime <= start(ii)
        k = ii;
        break;
      end
    end
    
    if k > 1
      if figure.UserData.viewport.startTime < stop(k - 1)
        k = k - 1;
      end
    end
    
    for ii = k : length(start)
      if start(ii) >= figure.UserData.viewport.stopTime
        break;
      end
      
      startTime = max(start(ii), figure.UserData.viewport.startTime);
      stopTime  = min(stop(ii), figure.UserData.viewport.stopTime);
      
      k1 = (startTime - figure.UserData.viewport.startTime) * file.frequency;
      k2 = (stopTime - figure.UserData.viewport.startTime) * file.frequency;
      
      m = false(figure.UserData.viewport.timeWidth * file.frequency, 1);
      m(k1 + 1 : k2, 1) = true;
      
      period = 1 / file.frequency;
      xdata = startTime : period : stopTime - period;
      series = plot(xdata, data(m), 'Color', figure.UserData.maskColor, 'LineStyle', figure.UserData.lineStyle, 'LineWidth', figure.UserData.lineWidth, 'Marker', figure.UserData.marker, 'MarkerSize', figure.UserData.markerSize);
      
      set(series, 'ButtonDownFcn', @seriesmousedown);
    end
  end
end

function refresh(figure)
  for ii = 1 : length(figure.UserData.axes)
    axes = figure.UserData.axes{ii};
    
    file        = axes.UserData.file;
    removeTrend = axes.UserData.removeTrend;
    startTime   = figure.UserData.viewport.startTime;
    stopTime    = figure.UserData.viewport.stopTime;
    
    data = atsdata(sprintf('%s%s%s', file.folder, filesep, file.name), removeTrend, startTime, stopTime);
    show(figure, file, data, removeTrend, ii, length(figure.UserData.axes));
  end
  
  if ~isempty(figure.UserData.selection)
    select(figure, figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
  end
end

function select(figure, startTime, stopTime)
  if startTime < figure.UserData.viewport.stopTime && stopTime > figure.UserData.viewport.startTime
    if startTime < figure.UserData.viewport.startTime
      start = figure.UserData.viewport.startTime;
    else
      start = startTime;
    end
    
    if stopTime > figure.UserData.viewport.stopTime
      stop = figure.UserData.viewport.stopTime;
    else
      stop = stopTime;
    end
    
    for ii = 1 : length(figure.UserData.axes)
      axes = figure.UserData.axes{ii};
      
      file        = axes.UserData.file;
      removeTrend = axes.UserData.removeTrend;
      
      data = atsdata(sprintf('%s%s%s', file.folder, filesep, file.name), removeTrend, start, stop);
      
      if ~isempty(data)
        subplot(length(figure.UserData.axes), 1, ii);
        
        xdata = start : 1 / file.frequency : stop - (1 / file.frequency);
        series = plot(xdata, data, 'Color', figure.UserData.selectionColor, 'LineStyle', figure.UserData.lineStyle, 'LineWidth', figure.UserData.lineWidth, 'Marker', figure.UserData.marker, 'MarkerSize', figure.UserData.markerSize);
        
        set(series, 'ButtonDownFcn', @seriesmousedown);
        
        axes.UserData.selection = series;
      end
    end
  end
  
  if ~isempty(figure.UserData.axes)
    timeWidth = stopTime - startTime;
    
    if timeWidth == 1
      figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String = {sprintf('Selected: 1 second'), figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String{2}};
    else
      figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String = {sprintf('Selected: %d seconds', timeWidth), figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String{2}};
    end
  end
  
  figure.UserData.selection.startTime = startTime;
  figure.UserData.selection.stopTime  = stopTime;
  figure.UserData.selection.timeWidth = stopTime - startTime;
end

function deselect(figure)
  for ii = 1 : length(figure.UserData.axes)
    axes = figure.UserData.axes{ii};
    
    if isfield(axes.UserData, 'selection')
      series = axes.UserData.selection;
      
      if ~isempty(series)
        axes.UserData.selection = [];
        delete(series);
      end
    end
  end
  
  if ~isempty(figure.UserData.axes)
    figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String = {'', figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String{2}};
  end
  
  figure.UserData.selection = [];
end

function KeyPressFcn(figure, event)
  alt = false;
  shift = false;
  
  for ii = 1 : length(event.Modifier)
    if strcmp(event.Modifier{ii}, 'alt')
      alt = true;
    elseif strcmp(event.Modifier{ii}, 'shift')
      shift = true;
    end
  end
  
  if strcmp(event.Key, 'leftarrow')
    if figure.UserData.viewport.startTime > figure.UserData.startTime
      if alt
        figure.UserData.viewport.startTime = figure.UserData.viewport.startTime - 1;
        figure.UserData.viewport.stopTime  = figure.UserData.viewport.stopTime - 1;
      else
        if figure.UserData.viewport.startTime - figure.UserData.viewport.timeWidth >= figure.UserData.startTime
          figure.UserData.viewport.startTime = figure.UserData.viewport.startTime - figure.UserData.viewport.timeWidth;
          figure.UserData.viewport.stopTime  = figure.UserData.viewport.stopTime - figure.UserData.viewport.timeWidth;
        else
          figure.UserData.viewport.startTime = figure.UserData.startTime;
          figure.UserData.viewport.stopTime  = figure.UserData.startTime + figure.UserData.viewport.timeWidth;
        end
      end
      
      refresh(figure);
    end
  elseif strcmp(event.Key, 'rightarrow')
    if figure.UserData.viewport.stopTime < figure.UserData.stopTime
      if alt
        figure.UserData.viewport.startTime = figure.UserData.viewport.startTime + 1;
        figure.UserData.viewport.stopTime  = figure.UserData.viewport.stopTime + 1;
      else
        if figure.UserData.viewport.stopTime + figure.UserData.viewport.timeWidth <= figure.UserData.stopTime
          figure.UserData.viewport.startTime = figure.UserData.viewport.startTime + figure.UserData.viewport.timeWidth;
          figure.UserData.viewport.stopTime  = figure.UserData.viewport.stopTime + figure.UserData.viewport.timeWidth;
        else
          figure.UserData.viewport.startTime = figure.UserData.stopTime - figure.UserData.viewport.timeWidth;
          figure.UserData.viewport.stopTime  = figure.UserData.stopTime;
        end
      end
      
      refresh(figure);
    end
  elseif strcmp(event.Key, 'uparrow')
    if figure.UserData.viewport.startTime > figure.UserData.startTime
      figure.UserData.viewport.startTime = figure.UserData.startTime;
      figure.UserData.viewport.stopTime  = figure.UserData.startTime + figure.UserData.viewport.timeWidth;
      
      refresh(figure);
    end
  elseif strcmp(event.Key, 'downarrow')
    if figure.UserData.viewport.stopTime < figure.UserData.stopTime
      figure.UserData.viewport.startTime = figure.UserData.stopTime - figure.UserData.viewport.timeWidth;
      figure.UserData.viewport.stopTime  = figure.UserData.stopTime;
      
      refresh(figure);
    end
  elseif strcmp(event.Key, 'r')
    refresh(figure);
  elseif strcmp(event.Key, 'd')
    if ~isempty(figure.UserData.selection)
      tmaskadd(figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
      deselect(figure);
    end
  elseif strcmp(event.Key, 'u')
    if ~isempty(figure.UserData.selection)
      tmaskremove(figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
      deselect(figure);
    end
  elseif strcmp(event.Key, 'delete') || strcmp(event.Key, 'backspace')
    if ~isempty(figure.UserData.selection)
      if shift
        tmaskremove(figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
      else
        tmaskadd(figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
      end
      
      deselect(figure);
    end
  elseif strcmp(event.Key, 'escape')
    deselect(figure);
  end
end

function WindowScrollWheelFcn(figure, event)
  if event.VerticalScrollCount ~= 0
    axes = [];
    
    mousePoint = get(figure, 'CurrentPoint');
    figurePosition = get(figure, 'Position');
    
    mouseX = mousePoint(1);
    mouseY = mousePoint(2);
    
    figureWidth = figurePosition(3);
    figureHeight = figurePosition(4);
    
    for ii = 1 : length(figure.UserData.axes)
      position = get(figure.UserData.axes{ii}, 'Position');
      
      left = position(1) * figureWidth;
      bottom = position(2) * figureHeight;
      right = (position(1) + position(3)) * figureWidth;
      top = (position(2) + position(4)) * figureHeight;
      
      if mouseX > left && mouseX < right
        if mouseY > bottom && mouseY < top
          axes = figure.UserData.axes{ii};
          break;
        end
      end
    end
    
    if ~isempty(axes)
      if event.VerticalScrollCount < 0
        zoomIn(figure, axes);
      elseif event.VerticalScrollCount > 0
        zoomOut(figure, axes);
      end
    else
      if event.VerticalScrollCount < 0
        for ii = 1 : length(figure.UserData.axes)
          zoomIn(figure, figure.UserData.axes{ii});
        end
      elseif event.VerticalScrollCount > 0
        for ii = 1 : length(figure.UserData.axes)
          zoomOut(figure, figure.UserData.axes{ii});
        end
      end
    end
  end
end

function zoomIn(figure, axes)
  inc = ((axes.YLim(2) - axes.YLim(1)) / (1 + figure.UserData.zoom.factor)) * (figure.UserData.zoom.factor / 2);

  axes.YLim(1) = axes.YLim(1) + inc;
  axes.YLim(2) = axes.YLim(2) - inc;

  axes.YTick = axes.YLim(1) : (axes.YLim(2) - axes.YLim(1)) / 4 : axes.YLim(2);

  figure.UserData.zoom.level(axes.UserData.index) = figure.UserData.zoom.level(axes.UserData.index) - 1;
end

function zoomOut(figure, axes)
  inc = (axes.YLim(2) - axes.YLim(1)) * (figure.UserData.zoom.factor / 2);

  axes.YLim(1) = axes.YLim(1) - inc;
  axes.YLim(2) = axes.YLim(2) + inc;

  axes.YTick = axes.YLim(1) : (axes.YLim(2) - axes.YLim(1)) / 4 : axes.YLim(2);

  figure.UserData.zoom.level(axes.UserData.index) = figure.UserData.zoom.level(axes.UserData.index) + 1;
end

function seriesmousedown(series, ~)
  mousedown(series.Parent, series);
end

function axesmousedown(axes, ~)
  mousedown(axes, []);
end

function mousedown(axes, series)
  figure = axes.Parent;
  
  if strcmp(figure.SelectionType, 'normal')
    currentPoint = axes.CurrentPoint;
    
    x1 = currentPoint(1, 1);
    y1 = axes.YLim(1); %currentPoint(1, 2);
    
    if x1 >= figure.UserData.viewport.startTime && x1 <= figure.UserData.viewport.stopTime
      if y1 >= axes.YLim(1) && y1 <= axes.YLim(2)
        top = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--');
        left = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--');
        right = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--');
        bottom = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--');
        
        figure.Pointer = 'crosshair';
        figure.WindowButtonMotionFcn = @mousemotion;
        figure.WindowButtonUpFcn = @mouseup;
      end
    end
  end
  
  function mousemotion(~, ~)
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = axes.YLim(2); %currentPoint(1, 2);
    
    if x2 < figure.UserData.viewport.startTime
      x2 = figure.UserData.viewport.startTime;
    elseif x2 > figure.UserData.viewport.stopTime
      x2 = figure.UserData.viewport.stopTime;
    end
    
    if y2 < axes.YLim(1)
      y2 = axes.YLim(1);
    elseif y2 > axes.YLim(2)
      y2 = axes.YLim(2);
    end
    
    top.XData = [x1, x2];
    top.YData = [y1, y1];
    
    left.XData = [x1, x1];
    left.YData = [y1, y2];
    
    right.XData = [x2, x2];
    right.YData = [y1, y2];
    
    bottom.XData = [x1, x2];
    bottom.YData = [y2, y2];
    
    drawnow();
  end
  
  function mouseup(~, ~)
    figure.Pointer = 'arrow';
    figure.WindowButtonMotionFcn = [];
    figure.WindowButtonUpFcn = [];
    
    delete(top);
    delete(left);
    delete(right);
    delete(bottom);
    
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = currentPoint(1, 2);
    
    if ~isempty(series) || (x1 ~= x2 && y1 ~= y2)
      deselect(figure);
      
      if x1 > x2
        x  = x1;
        x1 = x2;
        x2 = x;
      end
      
      %x1 = floor(x1);
      %x2 = ceil(x2);
      
      if x1 < figure.UserData.viewport.startTime
        x1 = figure.UserData.viewport.startTime;
      end
      
      if x2 > figure.UserData.viewport.stopTime
        x2 = figure.UserData.viewport.stopTime;
      end
      
      x1 = figure.UserData.startTime + ceil((x1 - figure.UserData.startTime) * figure.UserData.frequency) / figure.UserData.frequency;
      x2 = figure.UserData.stopTime + ceil((x2 - figure.UserData.stopTime) * figure.UserData.frequency) / figure.UserData.frequency;
      
      select(figure, x1, x2);
    end
  end
end