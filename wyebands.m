function wyebands(varargin)
  files = [];
  
  flag.all = false;
  flag.specific = false;
  flag.name = false;
  flag.samplingFrequency = false;
  flag.samplingSeconds = false;
  flag.decimationExponent = false;
  flag.numberOfSamples = false;
  flag.timeLength = false;
  flag.filter = cell(0);
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        files = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-name')
        varargin(:, k) = [];
        flag.specific = true;
        flag.name = true;
      elseif strcmpi(varargin{k}, '-samplingFrequency')
        varargin(:, k) = [];
        flag.specific = true;
        flag.samplingFrequency = true;
      elseif strcmpi(varargin{k}, '-samplingSeconds')
        varargin(:, k) = [];
        flag.specific = true;
        flag.samplingSeconds = true;
      elseif strcmpi(varargin{k}, '-decimationExponent')
        varargin(:, k) = [];
        flag.specific = true;
        flag.decimationExponent = true;
      elseif strcmpi(varargin{k}, '-numberOfSamples')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfSamples = true;
      elseif strcmpi(varargin{k}, '-timeLength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.timeLength = true;
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          flag.filter{length(flag.filter) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.name = true;
    flag.samplingFrequency = true;
    flag.samplingSeconds = true;
    flag.decimationExponent = true;
    flag.numberOfSamples = true;
    flag.timeLength = true;
  elseif ~flag.specific
    flag.name = true;
    flag.samplingFrequency = true;
    flag.samplingSeconds = true;
    flag.decimationExponent = true;
    flag.numberOfSamples = true;
    flag.timeLength = true;
  end
  
  if isempty(files)
    files = wye(varargin{:});
  end
  
  if ~isempty(files)
    for ii = 1 : length(files)
      printBands(files(ii), flag);
    end
  end
end

function printBands(file, flag)
  name = cell(0);
  value = cell(0);
  
  for ii = 1 : file.numberOfBands
    if ~isempty(flag.filter)
      ignore = true;
      
      for jj = 1 : length(flag.filter)
        if strcmpi(flag.filter{jj}, file.bands(ii).name)
          ignore = false;
          break;
        end
      end
      
      if ignore
        continue;
      end
    end
    
    if flag.name
      name{length(name) + 1} = 'name:';
      value{length(value) + 1} = sprintf('%s', file.bands(ii).name);
    end
    
    if flag.samplingFrequency
      name{length(name) + 1} = 'sampling frequency:';
      value{length(value) + 1} = sprintf('%d', file.bands(ii).samplingFrequency);
    end
    
    if flag.samplingSeconds
      name{length(name) + 1} = 'sampling seconds:';
      value{length(value) + 1} = sprintf('%d', file.bands(ii).samplingSeconds);
    end
    
    if flag.decimationExponent
      name{length(name) + 1} = 'decimation exponent:';
      value{length(value) + 1} = sprintf('%d', file.bands(ii).decimationExponent);
    end
    
    if flag.numberOfSamples
      name{length(name) + 1} = 'number of samples:';
      value{length(value) + 1} = sprintf('%d', file.bands(ii).numberOfSamples);
    end
    
    if flag.timeLength
      name{length(name) + 1} = 'time length:';
      value{length(value) + 1} = sprintf('%s', timeLength(file.bands(ii).timeLength.days, file.bands(ii).timeLength.hours, file.bands(ii).timeLength.minutes, file.bands(ii).timeLength.seconds));
    end
  end
  
  width = 0;
  
  for ii = 1 : length(name)
    if width < length(name{ii})
      width = length(name{ii});
    end
  end
  
  for ii = 1 : length(name)
    if length(name{ii}) < width
      name{ii} = sprintf(sprintf('%%s%%%ds', width - length(name{ii})), name{ii}, ' ');
    end
  end
  
  for ii = 1 : length(name)
    fprintf('%s %s\n', name{ii}, value{ii});
  end
end

function timeLength = timeLength(days, hours, minutes, seconds)
  timeLength = '';
  
  if days > 0
    if days == 1
      timeLength = '1 day ';
    else
      timeLength = sprintf('%d days ', days);
    end
  end
  
  if hours == 1
    timeLength = sprintf('%s1 hour ', timeLength);
  else
    timeLength = sprintf('%s%d hours ', timeLength, hours);
  end
  
  if minutes == 1
    timeLength = sprintf('%s1 minute ', timeLength);
  else
    timeLength = sprintf('%s%d minutes ', timeLength, minutes);
  end
  
  if seconds == 1
    timeLength = sprintf('%s1 second', timeLength);
  else
    timeLength = sprintf('%s%d seconds', timeLength, seconds);
  end
end