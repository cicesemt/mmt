function params(varargin)
  resetCommand = false;
  verifyCommand = false;

  globalFlag = false;
  surveyFlag = false;
  siteFlag = false;
  errorFlag = false;

  if nargin > 0
    if strcmpi(varargin{1}, 'reset')
      varargin(:, 1) = [];
      resetCommand = true;
    elseif strcmpi(varargin{1}, 'verify')
      varargin(:, 1) = [];
      verifyCommand = true;
    end

    k = 1;

    while k <= length(varargin)
      if strcmpi(varargin{k}, '-global')
        varargin(:, k) = [];
        globalFlag = true;
      elseif strcmpi(varargin{k}, '-survey')
        varargin(:, k) = [];
        surveyFlag = true;
      elseif strcmpi(varargin{k}, '-site')
        varargin(:, k) = [];
        siteFlag = true;
      else
        if length(varargin{k}) > 1 && varargin{k}(1) == '-' && isnan(str2double(varargin{k}))
          cprintf('err', 'unknown option ''%s'' \n', varargin{k});
          errorFlag = true;
          break;
        end

        k = k + 1;
      end
    end
  end

  if ~errorFlag
    if ~isempty(varargin)
      cprintf('err', 'too many arguments\n');
    elseif resetCommand
      paramsreset(globalFlag, surveyFlag, siteFlag);
    elseif verifyCommand
      paramsverify(globalFlag, surveyFlag, siteFlag);
    else
      if globalFlag
        showGlobal();
      elseif surveyFlag
        if ~isempty(survey())
          showSurvey(survey());
        end
      elseif siteFlag
        if ~isempty(site())
          showSite(site());
        end
      else
        if ~isempty(local())
          showLocal();
        elseif ~isempty(site())
          showSite(site());
        elseif ~isempty(survey())
          showSurvey(survey());
        end
      end
    end
  end
end

function showGlobal()
  fprintf('npcsm: %d\n', npcsm());
  fprintf('nptsm: %d\n', nptsm());
  fprintf('nptssm: %d\n', nptssm());
  fprintf('noutm: %d\n', noutm());
  fprintf('ninpm: %d\n', ninpm());
  fprintf('nrefm: %d\n', nrefm());
  fprintf('nrsitem: %d\n', nrsitem());
  fprintf('nsectm: %d\n', nsectm());
  fprintf('nblk: %d\n', nblk());
  fprintf('nfsm: %d\n', nfsm());
  fprintf('nfm: %d\n', nfm());
end

function showSurvey(survey)
  fprintf('acqby: %s\n', acqby(survey));
  fprintf('fileby: %s\n', fileby(survey));
  fprintf('country: %s\n', country(survey));
  fprintf('state: %s\n', state(survey));
  fprintf('county: %s\n', county(survey));
  fprintf('prospect: %s\n', prospect(survey));
end

function showSite(site)
  fprintf('loc: %s\n', loc(site));
  fprintf('lat: %s\n', lat(site));
  fprintf('long: %s\n', long(site));
  fprintf('elev: %s\n', elev(site));
  fprintf('xutm: %s\n', xutm(site));
  fprintf('yutm: %s\n', yutm(site));
  fprintf('datum: %s\n', datum(site));
end

function showLocal()
  fprintf('ilev: %d\n', ilev());
  fprintf('nout: %d\n', nout());
  fprintf('ninp: %d\n', ninp());

  if ilev()
    fprintf('nref: %d\n', nref());

    if nref() > 3
      fprintf('nr3 nr2: %d %d\n', nr3(), nr2());
    end
  end

  if ilev()
    fprintf('nrr: %d\n', nrr());
  end

  fprintf('tbw: %.7f\n', tbw());
  fprintf('deltat: %.8f\n', deltat());

  if ilev()
    fprintf('nfft nsctinc nsctmax: %d %d %d\n', nfft(), nsctinc(), nsctmax());
  else
    fprintf('nfft nsctmax: %d %d\n', nfft(), nsctmax());
  end

  if ilev()
    fprintf('nf1 nfinc nfsect: %d %d %d\n', nf1(), nfinc(), nfsect());
  end

  if ilev()
    fprintf('mfft: %d\n', mfft());
  end

  if ilev()
    fprintf('uin ainlin ainuin: %.4f %.4f %.4f\n', uin(), ainlin(), ainuin());
  else
    fprintf('uin ainuin: %.4f %.4f\n', uin(), ainuin());
  end

  if ilev()
    if nrr() == 1
      fprintf('c2threshb: %.3f\n', c2threshb());
    end
  end

  fprintf('c2threshe: %.3f\n', c2threshe());

  if nout() == 3
    fprintf('nz: %d\n', nz());

    if nz() == 0
      fprintf('c2threshe1: %.3f\n', c2threshe1());
    end
  end

  if ilev()
    if c2threshb() ~= 0 || c2threshe() ~= 0 || c2threshe1() ~= 0
      fprintf('perlo perhi: %.4f %.4f\n', perlo(), perhi());
    end
  end

  fprintf('ofil: %s\n', ofil());
  fprintf('nlev: %d\n', nlev());

  if ilev()
    fprintf('nprej: %d\n', nprej());
  end

  fprintf('npcs: %d\n', npcs());
  fprintf('nar: %d\n', nar());
  fprintf('imode: %d\n', imode());
  fprintf('jmode: %d\n', jmode());

  if nout() >= 2
    fprintf('theta1a theta2a phia: %.3f %.3f %.3f\n', theta1a(), theta2a(), phia());
  end

  fprintf('theta1b theta2b phib: %.3f %.3f %.3f\n', theta1b(), theta2b(), phib());
  fprintf('theta1c theta2c phic: %.3f %.3f %.3f\n', theta1c(), theta2c(), phic());

  if nr2() == 2
    fprintf('theta1d theta2d phid: %.3f %.3f %.3f\n', theta1d(), theta2d(), phid());
  end
end