function surveyelevation(varargin)
  currentSurvey = survey(varargin{:});
  
  if ~isempty(currentSurvey)
    name = currentSurvey.name;
    sites = currentSurvey.sites;
    siteCount = length(sites);
    siteNames = cell(1, siteCount);
    elevation = 0;
    
    for ii = 1 : siteCount
      siteNames{ii} = sites(ii).name;
      
      if elevation < sites(ii).elevation
        elevation = sites(ii).elevation;
      end
    end
    
    ymin = 0;
    yuni = {'Centimeters' 'Centimeters' 'Meters' 'Meters' 'Meters' 'Kilometres'};
    ydiv = {1 1 100 100 100 100000};
    yind = 1;
    ymax = elevation;
    
    for ii = 1 : 6
      if elevation < 10^ii
        yind = ii;
        
        for jj = 10^(ii-1) : 10^(ii-1) : 10^ii
          if elevation < jj
            ymax = jj;
            
            break;
          end
        end
        
        break;
      end
    end
    
    step = (ymax - ymin) / 4;
    
    y0 = ymin;
    y1 = ymin + step;
    y2 = ymin + step + step;
    y3 = ymax - step;
    y4 = ymax;
    
    label = yuni{yind};
    
    if mod(ymax/ydiv{yind}, 4) == 0
      digits = 0;
    elseif mod(ymax/ydiv{yind}, 2) == 0
      digits = 1;
    else
      digits = 2;
    end
    
    label0 = sprintf(sprintf('%%.%df', digits), y0/ydiv{yind});
    label1 = sprintf(sprintf('%%.%df', digits), y1/ydiv{yind});
    label2 = sprintf(sprintf('%%.%df', digits), y2/ydiv{yind});
    label3 = sprintf(sprintf('%%.%df', digits), y3/ydiv{yind});
    label4 = sprintf(sprintf('%%.%df', digits), y4/ydiv{yind});
    
    f = figure();
    a = axes();
    
    f.set('NumberTitle', 'off');
    f.set('ToolBar', 'none');
    f.set('MenuBar', 'none');
    f.set('Name', name);
    
    a.FontSize = 10;
    a.FontWeight = 'normal';
    
    a.Title.String = 'Elevation';
    a.Title.FontSize = 14;
    a.Title.FontWeight = 'bold';
    
    a.XLabel.String = 'Sites';
    a.XLabel.FontSize = 12;
    a.XLabel.FontWeight = 'bold';
    
    a.XLim = [0 siteCount + 1];
    a.XTick = 1 : siteCount;
    a.XTickLabel = siteNames;
    
    a.YLabel.String = label;
    a.YLabel.FontSize = 12;
    a.YLabel.FontWeight = 'bold';
    
    a.YLim = [ymin ymax];
    a.YTick = ymin : step : ymax;
    a.YTickLabel = {label0 label1 label2 label3 label4};
    a.YGrid = 'on';
    
    a.Box = 'on';
    hold(a, 'on');
    
    fposition = get(f, 'Position');
    fwidth = fposition(3);
    
    aposition = get(a, 'Position');
    awidth = aposition(3) * fwidth;
    
    if siteCount > 1
      lwidth = round(awidth / (1.1 * siteCount + 1));
    else
      lwidth = round(awidth / (2 * siteCount + 1));
    end
    
    for ii = 1 : siteCount
      x = [ii ii];
      y = [0 sites(ii).elevation];
      
      plot(x, y, 'Color', [0 0.4470 0.7410], 'LineStyle', '-', 'LineWidth', lwidth);
    end
  end
end