function paramsreset(globalFlag, surveyFlag, siteFlag)
  if globalFlag
    resetGlobal();
  elseif surveyFlag
    if ~isempty(survey())
      resetSurvey(survey());
    end
  elseif siteFlag
    if ~isempty(site())
      resetSite(site());
    end
  else
    if ~isempty(local())
      resetLocal();
    elseif ~isempty(site())
      resetSite(site());
    elseif ~isempty(survey())
      resetSurvey(survey());
    end
  end
end

function resetGlobal()
  npcsm('-reset');
  nptsm('-reset');
  nptssm('-reset');
  noutm('-reset');
  ninpm('-reset');
  nrefm('-reset');
  nrsitem('-reset');
  nsectm('-reset');
  nblk('-reset');
  nfsm('-reset');
  nfm('-reset');
end

function resetSurvey(survey)
  acqby(survey, '-reset');
  fileby(survey, '-reset');
  country(survey, '-reset');
  state(survey, '-reset');
  county(survey, '-reset');
  prospect(survey, '-reset');
end

function resetSite(site)
  loc(site, '-reset');
  lat(site, '-reset');
  long(site, '-reset');
  elev(site, '-reset');
  xutm(site, '-reset');
  yutm(site, '-reset');
  datum(site, '-reset');
end

function resetLocal()
  ilev('-reset');
  nout('-reset');
  ninp('-reset');
  nref('-reset');
  nr3('-reset');
  nr2('-reset');
  nrr('-reset');
  tbw('-reset');
  deltat('-reset');
  nfft('-reset');
  nsctinc('-reset');
  nsctmax('-reset');
  nf1('-reset');
  nfinc('-reset');
  nfsect('-reset');
  mfft('-reset');
  uin('-reset');
  ainlin('-reset');
  ainuin('-reset');
  c2threshb('-reset');
  c2threshe('-reset');
  nz('-reset');
  c2threshe1('-reset');
  perlo('-reset');
  perhi('-reset');
  ofil('-reset');
  nlev('-reset');
  nprej('-reset');
  npcs('-reset');
  nar('-reset');
  imode('-reset');
  jmode('-reset');
  theta1a('-reset');
  theta1b('-reset');
  theta1c('-reset');
  theta1d('-reset');
  theta2a('-reset');
  theta2b('-reset');
  theta2c('-reset');
  theta2d('-reset');
  phia('-reset');
  phib('-reset');
  phic('-reset');
  phid('-reset');
end