function string = elevation2string(elevation)
  string = sprintf('%.2f m', elevation / 100);
end