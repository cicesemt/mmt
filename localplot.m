function localplot(varargin)
  files = local();
  
  if ~isempty(files)
    info = localinfo();
    
    if ~isempty(info)
      removeTrend = false;
      startTime   = [];
      stopTime    = [];
      
      if nargin > 0
        if nargin == 1
          if islogical(varargin{1})
            if varargin{1}
              removeTrend = true;
            end
          end
        elseif nargin == 2
          if isnumeric(varargin{1})
            startTime = varargin{1};
          end
          
          if isnumeric(varargin{2})
            stopTime = varargin{2};
          end
        else
          if islogical(varargin{1})
            if varargin{1}
              removeTrend = true;
            end
          end
          
          if isnumeric(varargin{2})
            startTime = varargin{2};
          end
          
          if isnumeric(varargin{3})
            stopTime = varargin{3};
          end
        end
      end
      
      if isempty(startTime)
        startTime = info.startTime;
      end
      
      if isempty(stopTime)
        stopTime = info.stopTime;
      end
      
      for ii = 1 : length(files)
        atsplot(sprintf('%s%s%s', files(ii).folder, filesep, files(ii).name), removeTrend, startTime, stopTime, ii, length(files));
      end
    end
  end
end