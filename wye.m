function [varargout] = wye(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    if strcmpi(varargin{1}, 'header')
      varargin(:, 1) = [];
      wyeheader(varargin{:});
      return;
    elseif strcmpi(varargin{1}, 'info')
      varargin(:, 1) = [];
      wyeinfo(varargin{:});
      return;
    elseif strcmpi(varargin{1}, 'bands')
      varargin(:, 1) = [];
      wyebands(varargin{:});
      return;
    elseif strcmpi(varargin{1}, 'blocks')
      varargin(:, 1) = [];
      wyeblocks(varargin{:});
      return;
    elseif strcmpi(varargin{1}, 'segments')
      varargin(:, 1) = [];
      wyesegments(varargin{:});
      return;
    elseif strcmpi(varargin{1}, 'data')
    elseif strcmpi(varargin{1}, 'plot')
    end
  end
  
  if ~isempty(varargin)
    args = varargin;
  else
    args = {pwd()};
  end
  
  wyefiles = cell(0);
  
  for ii = 1 : length(args)
    files = dir(args{ii});
    
    if ~isempty(files)
      for jj = 1 : length(files)
        file = wyefile(sprintf('%s%s%s', files(jj).folder, filesep, files(jj).name));
        
        if ~isempty(file)
          duplicated = false;
          
          for kk = 1 : length(wyefiles)
            if strcmpi(sprintf('%s%s%s', wyefiles{kk}.folder, filesep, wyefiles{kk}.name), sprintf('%s%s%s', file.folder, filesep, file.name))
              duplicated = true;
              break;
            end
          end
          
          if ~duplicated
            wyefiles{length(wyefiles) + 1} = file;
          end
        end
      end
    end
  end
  
  if ~isempty(wyefiles)
    if nargout > 0
      for ii = 1 : length(wyefiles)
        varargout{1} = [varargout{1} wyefiles{ii}];
      end
    else
      for ii = 1 : length(wyefiles)
        fprintf('%s\n', wyefiles{ii}.name);
      end
    end
  end
end