function changed = tmaskclear()
  changed = false;
  
  mask = tmask();
  
  if ~isempty(mask)
    fp = fopen(sprintf('%s%s%s', '.mt', filesep, 'tmask'), 'wt');
    
    if fp ~= -1
      changed = true;
      fclose(fp);
    end
  end
  
  if changed
    broadcast();
  end
end

function broadcast()
  f = get(groot, 'Children');
  
  for ii = 1 : length(f)
    if ~isfield(f(ii).UserData, 'id') || ~isfield(f(ii).UserData, 'folder')
      continue;
    end
    
    if ~strcmp(f(ii).UserData.id, 'atsplot')
      continue;
    end
    
    if ~strcmp(f(ii).UserData.folder, pwd())
      continue;
    end
    
    e.Modifier = {};
    e.Key = 'r';
    
    f(ii).KeyPressFcn(f(ii), e);
  end
end