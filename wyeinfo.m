function wyeinfo(varargin)
  files = [];
  
  flag.all = false;
  flag.specific = false;
  flag.name = false;
  flag.folder = false;
  flag.bytes = false;
  flag.soundingName = false;
  flag.soundingNumber = false;
  flag.samplingSeconds = false;
  flag.numberOfSamples = false;
  flag.timeLength = false;
  flag.numberOfBands = false;
  flag.numberOfBlocks = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        files = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-name')
        varargin(:, k) = [];
        flag.specific = true;
        flag.name = true;
      elseif strcmpi(varargin{k}, '-folder')
        varargin(:, k) = [];
        flag.specific = true;
        flag.folder = true;
      elseif strcmpi(varargin{k}, '-bytes')
        varargin(:, k) = [];
        flag.specific = true;
        flag.bytes = true;
      elseif strcmpi(varargin{k}, '-soundingName')
        varargin(:, k) = [];
        flag.specific = true;
        flag.soundingName = true;
      elseif strcmpi(varargin{k}, '-soundingNumber')
        varargin(:, k) = [];
        flag.specific = true;
        flag.soundingNumber = true;
      elseif strcmpi(varargin{k}, '-samplingSeconds')
        varargin(:, k) = [];
        flag.specific = true;
        flag.samplingSeconds = true;
      elseif strcmpi(varargin{k}, '-numberOfSamples')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfSamples = true;
      elseif strcmpi(varargin{k}, '-timeLength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.timeLength = true;
      elseif strcmpi(varargin{k}, '-numberOfBands')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfBands = true;
      elseif strcmpi(varargin{k}, '-numberOfBlocks')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfBlocks = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.name = true;
    flag.folder = true;
    flag.bytes = true;
    flag.soundingName = true;
    flag.soundingNumber = true;
    flag.samplingSeconds = true;
    flag.numberOfSamples = true;
    flag.timeLength = true;
    flag.numberOfBands = true;
    flag.numberOfBlocks = true;
  elseif ~flag.specific
    flag.name = true;
    flag.folder = true;
    flag.bytes = true;
    flag.soundingName = true;
    flag.soundingNumber = true;
    flag.samplingSeconds = true;
    flag.numberOfSamples = true;
    flag.timeLength = true;
    flag.numberOfBands = true;
    flag.numberOfBlocks = true;
  end
  
  if isempty(files)
    files = wye(varargin{:});
  end
  
  if ~isempty(files)
    for ii = 1 : length(files)
      printInfo(files(ii), flag);
    end
  end
end

function printInfo(file, flag)
  name = cell(0);
  value = cell(0);
  
  if flag.name
    name{length(name) + 1} = 'name:';
    value{length(value) + 1} = sprintf('%s', file.name);
  end

  if flag.folder
    name{length(name) + 1} = 'folder:';
    value{length(value) + 1} = sprintf('%s', file.folder);
  end

  if flag.bytes
    name{length(name) + 1} = 'bytes:';
    value{length(value) + 1} = sprintf('%d', file.bytes);
  end

  if flag.soundingName
    name{length(name) + 1} = 'sounding name:';
    value{length(value) + 1} = sprintf('%s', file.soundingName);
  end

  if flag.soundingNumber
    name{length(name) + 1} = 'sounding number:';
    value{length(value) + 1} = sprintf('%d', file.soundingNumber);
  end

  if flag.samplingSeconds
    name{length(name) + 1} = 'sampling seconds:';
    value{length(value) + 1} = sprintf('%d', file.samplingSeconds);
  end

  if flag.numberOfSamples
    name{length(name) + 1} = 'number of samples:';
    value{length(value) + 1} = sprintf('%d', file.numberOfSamples);
  end
  
  if flag.timeLength
    name{length(name) + 1} = 'time length:';
    value{length(value) + 1} = sprintf('%s', timeLength(file.timeLength.days, file.timeLength.hours, file.timeLength.minutes, file.timeLength.seconds));
  end
  
  if flag.numberOfBands
    name{length(name) + 1} = 'number of bands:';
    value{length(value) + 1} = sprintf('%d', file.numberOfBands);
  end
  
  if flag.numberOfBlocks
    name{length(name) + 1} = 'number of blocks:';
    value{length(value) + 1} = sprintf('%d', file.numberOfBlocks);
  end
  
  width = 0;
  
  for ii = 1 : length(name)
    if width < length(name{ii})
      width = length(name{ii});
    end
  end
  
  for ii = 1 : length(name)
    if length(name{ii}) < width
      name{ii} = sprintf(sprintf('%%s%%%ds', width - length(name{ii})), name{ii}, ' ');
    end
  end
  
  for ii = 1 : length(name)
    fprintf('%s %s\n', name{ii}, value{ii});
  end
end

function timeLength = timeLength(days, hours, minutes, seconds)
  timeLength = '';
  
  if days > 0
    if days == 1
      timeLength = '1 day ';
    else
      timeLength = sprintf('%d days ', days);
    end
  end
  
  if hours == 1
    timeLength = sprintf('%s1 hour ', timeLength);
  else
    timeLength = sprintf('%s%d hours ', timeLength, hours);
  end
  
  if minutes == 1
    timeLength = sprintf('%s1 minute ', timeLength);
  else
    timeLength = sprintf('%s%d minutes ', timeLength, minutes);
  end
  
  if seconds == 1
    timeLength = sprintf('%s1 second', timeLength);
  else
    timeLength = sprintf('%s%d seconds', timeLength, seconds);
  end
end