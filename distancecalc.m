function distance = distancecalc(lat1, lon1, lat2, lon2)
  distance = 0;
  
  if lat1 ~= lat2 || lon1 ~= lon2
    lat1 = lat1 / (1000*60*60);
    lat2 = lat2 / (1000*60*60);
    
    corr = 21384.685755 * abs((lat1 + lat2) / 180);
    r = 6378137 - corr;
    
    lat1 = lat1 * (pi / 180);
    lat2 = lat2 * (pi / 180);
    lon1 = (lon1 / (1000*60*60)) * (pi / 180);
    lon2 = (lon2 / (1000*60*60)) * (pi / 180);
    
    a = sin((lat2 - lat1) / 2)^2;
    b = cos(lat1);
    c = cos(lat2);
    d = sin((lon2 - lon1) / 2)^2;
    
    h = a + b * c * d;
    
    if h >= 0 && h <= 1
      distance = 2 * r * asin(h^0.5);
    end
  end
end