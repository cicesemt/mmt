function [varargout] = measinfo()
  if nargout > 0
    varargout{1} = [];
  end
  
  files = meas();
  
  if ~isempty(files)
    binfo = localinfo();
    rinfo = remoteinfo();
    
    if ~isempty(binfo)
      maskinfo = tmaskinfo();
      
      samplingRate = binfo.samplingRate;
      measurementUnit   = binfo.measurementUnit;
      
      name          = strings(1, length(files));
      bytes         = zeros(1, length(files));
      channelNumber = zeros(1, length(files));
      channelType   = strings(1, length(files));
      
      for ii = 1 : length(files)
        name(ii)          = files(ii).name;
        bytes(ii)         = files(ii).bytes;
        channelNumber(ii) = files(ii).channelNumber;
        channelType(ii)   = files(ii).channelType;
      end
      
      if ~isempty(rinfo) && samplingRate == rinfo.samplingRate && measurementUnit == rinfo.measurementUnit
        folder          = strings(1, 2);
        aduSerialNumber = zeros(1, 2);
        xmlVersion      = zeros(1, 2);
        runNumber       = zeros(1, 2);
        boardType       = strings(1, 2);
        
        folder(1) = binfo.folder;
        folder(2) = rinfo.folder;
        
        aduSerialNumber(1) = binfo.aduSerialNumber;
        aduSerialNumber(2) = rinfo.aduSerialNumber;
        
        xmlVersion(1) = binfo.xmlVersion;
        xmlVersion(2) = rinfo.xmlVersion;
        
        runNumber(1) = binfo.runNumber;
        runNumber(2) = rinfo.runNumber;
        
        boardType(1) = binfo.boardType;
        boardType(2) = rinfo.boardType;
        
        startTime = max(binfo.startTime, rinfo.startTime);
        stopTime  = min(binfo.stopTime, rinfo.stopTime);
      else
        folder          = binfo.folder;
        aduSerialNumber = binfo.aduSerialNumber;
        xmlVersion      = binfo.xmlVersion;
        runNumber       = binfo.runNumber;
        boardType       = binfo.boardType;
        
        startTime = binfo.startTime;
        stopTime  = binfo.stopTime;
      end
      
      frequency = binfo.frequency;
      totalSeconds = stopTime - startTime;
      numberOfSamples = totalSeconds * frequency;
      
      seconds = totalSeconds;
      days    = fix(seconds/86400);
      seconds = mod(seconds,86400);
      hours   = fix(seconds/3600);
      seconds = mod(seconds,3600);
      minutes = fix(seconds/60);
      seconds = mod(seconds,60);
      
      info.name              = name;
      info.folder            = folder;
      info.bytes             = bytes;
      info.aduSerialNumber   = aduSerialNumber;
      info.xmlVersion        = xmlVersion;
      info.channelNumber     = channelNumber;
      info.runNumber         = runNumber;
      info.channelType       = channelType;
      info.boardType         = boardType;
      info.samplingRate      = samplingRate;
      info.measurementUnit   = measurementUnit;
      info.numberOfChannels  = length(files);
      info.numberOfSamples   = numberOfSamples;
      info.frequency         = frequency;
      
      if ~isempty(maskinfo)
        info.samplesAfterMask = numberOfSamples - (maskinfo.totalSeconds * frequency);
      end
      
      info.startTime         = startTime;
      info.stopTime          = stopTime;
      info.days              = days;
      info.hours             = hours;
      info.minutes           = minutes;
      info.seconds           = seconds;
      info.totalSeconds      = totalSeconds;
      
      if nargout > 0
        varargout{1} = info;
      else
        fprintf('name:                ');
        
        for ii = 1 : length(info.name)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%s', info.name(ii));
        end
        
        fprintf('\n');
        fprintf('folder:              ');
        
        for ii = 1 : length(info.folder)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%s', info.folder(ii));
        end
        
        fprintf('\n');
        fprintf('bytes:               ');
        
        for ii = 1 : length(info.bytes)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%d', info.bytes(ii));
        end
        
        fprintf('\n');
        fprintf('adu serial number:   ');
        
        for ii = 1 : length(info.aduSerialNumber)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%d', info.aduSerialNumber(ii));
        end
        
        fprintf('\n');
        fprintf('xml version:         ');
        
        for ii = 1 : length(info.xmlVersion)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%d', info.xmlVersion(ii));
        end
        
        fprintf('\n');
        fprintf('channel number:      ');
        
        for ii = 1 : length(info.channelNumber)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%d', info.channelNumber(ii));
        end
        
        fprintf('\n');
        fprintf('run number:          ');
        
        for ii = 1 : length(info.runNumber)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%d', info.runNumber(ii));
        end
        
        fprintf('\n');
        fprintf('channel type:        ');
        
        for ii = 1 : length(info.channelType)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%s', info.channelType(ii));
        end
        
        fprintf('\n');
        fprintf('board type:          ');
        
        for ii = 1 : length(info.boardType)
          if ii > 1
            fprintf(' ');
          end
          
          fprintf('%s', info.boardType(ii));
        end
        
        fprintf('\n');
        fprintf('sampling rate:       %d\n', info.samplingRate);
        fprintf('measurement unit:    %s\n', info.measurementUnit);
        fprintf('number of channels:  %d\n', info.numberOfChannels);
        fprintf('number of samples:   %d\n', info.numberOfSamples);
        fprintf('frequency:           %f\n', info.frequency);
        
        if ~isempty(maskinfo)
          fprintf('samples after mask:  %d\n', info.samplesAfterMask);
        end
        
        fprintf('start time:          %s (%d)\n', datestr(datetime(info.startTime, 'convertfrom', 'posixtime'), 'mmmm dd yyyy HH:MM:SS'), info.startTime);
        fprintf('stop time:           %s (%d)\n', datestr(datetime(info.stopTime, 'convertfrom', 'posixtime'), 'mmmm dd yyyy HH:MM:SS'), info.stopTime);
        fprintf('time length:         ');
        
        if info.days == 1
          fprintf('1 day ');
        else
          fprintf('%d days ', info.days);
        end
        
        if info.hours == 1
          fprintf('1 hour ');
        else
          fprintf('%d hours ', info.hours);
        end
        
        if info.minutes == 1
          fprintf('1 minute ');
        else
          fprintf('%d minutes ', info.minutes);
        end
        
        if info.seconds == 1
          fprintf('1 second\n');
        else
          fprintf('%d seconds\n', info.seconds);
        end
        
        fprintf('total seconds:       %d\n', info.totalSeconds);
      end
    end
  end
end