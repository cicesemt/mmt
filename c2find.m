function [varargout] = c2find(path)
  if nargout > 0
    varargout{1} = [];
  end
  
  if length(path) > 1
    if path(length(path)) == filesep
      path = path(1 : length(path) - 1);
    end
  end
  
  files = dir(path);
  
  if ~isempty(files)
    for ii = 1 : length(files)
      file = files(ii);
      
      if ~strcmp(file.name, '.') && ~strcmp(file.name, '..') && file.isdir
        if nargout > 0
          varargout{1} = [varargout{1}; c2find(sprintf('%s%s%s', path, filesep, file.name))];
        else
          c2find(sprintf('%s%s%s', path, filesep, file.name));
        end
      end
    end
    
    for ii = 1 : length(files)
      file = files(ii);
      
      if ~strcmp(file.name, '.') && ~strcmp(file.name, '..') && ~file.isdir
        f = c2file(sprintf('%s%s%s', path, filesep, file.name));
        
        if ~isempty(f)
          if nargout > 0
            varargout{1} = [varargout{1}; f];
          else
            fprintf('%s%s%s\n', path, filesep, file.name);
          end
        end
      end
    end
  end
end