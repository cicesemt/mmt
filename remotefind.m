function [varargout] = remotefind(path)
  if nargout > 0
    varargout{1} = [];
  end
  
  info = localinfo();
  
  if ~isempty(info)
    if length(path) > 1
      if path(length(path)) == filesep
        path = path(1 : length(path) - 1);
      end
    end
    
    files = dir(path);
    
    if ~isempty(files)
      for ii = 1 : length(files)
        file = files(ii);
        
        if ~strcmp(file.name, '.') && ~strcmp(file.name, '..') && file.isdir
          if nargout > 0
            varargout{1} = [varargout{1}; remotefind(sprintf('%s%s%s', path, filesep, file.name))];
          else
            remotefind(sprintf('%s%s%s', path, filesep, file.name));
          end
        end
      end
      
      ats_files = cell(1, length(files));
      ats_count = 0;
      
      for ii = 1 : length(files)
        file = files(ii);
        
        if ~strcmp(file.name, '.') && ~strcmp(file.name, '..') && ~file.isdir
          ats_file = atsfile(sprintf('%s%s%s', path, filesep, file.name));
          
          if ~isempty(ats_file)
            if ats_file.aduSerialNumber ~= info.aduSerialNumber && ats_file.samplingRate == info.samplingRate && ats_file.measurementUnit == info.measurementUnit
              ats_count = ats_count + 1;
              ats_files{ats_count} = ats_file;
            end
          end
        end
      end
      
      if ats_count > 0
        for ii = 1 : ats_count
          hx = [];
          hy = [];
          
          pivot = ats_files{ii};
          
          if strcmpi(pivot.channelType, 'HX')
            hx = pivot;
          elseif strcmpi(pivot.channelType, 'HY')
            hy = pivot;
          else
            continue;
          end
          
          for jj = 1 : ats_count
            if ii ~= jj
              file = ats_files{jj};
              
              if file.aduSerialNumber == pivot.aduSerialNumber && file.xmlVersion == pivot.xmlVersion && file.runNumber == pivot.runNumber && strcmpi(file.boardType, pivot.boardType) && file.samplingRate == pivot.samplingRate && strcmpi(file.measurementUnit, pivot.measurementUnit)
                if file.channelNumber ~= pivot.channelNumber && ~strcmpi(file.channelType, pivot.channelType)
                  if strcmpi(file.header.systemType, pivot.header.systemType) && file.startTime == pivot.startTime
                    
                    distance = sqrt((file.header.latitude - pivot.header.latitude)^2 + (file.header.longitude - pivot.header.longitude)^2 + (file.header.elevation - pivot.header.elevation)^2);
                    
                    if distance < 20
                      if strcmpi(file.channelType, 'HX')
                        if isempty(hx)
                          hx = file;
                        end
                      elseif strcmpi(file.channelType, 'HY')
                        if isempty(hy)
                          hy = file;
                        end
                      end
                    end
                    
                  end
                end
              end
              
            end
          end
          
          if ~isempty(hx) && ~isempty(hy)
            startTime = max(hx.startTime, hy.startTime);
            stopTime  = min(hx.stopTime, hy.stopTime);
            
            if startTime < info.stopTime && info.startTime < stopTime
              find.folder = path;
              find.startTime = max(startTime, info.startTime);
              find.stopTime = min(stopTime, info.stopTime);
              find.timeSpan = rate(info.startTime, info.stopTime, startTime, stopTime);
              find.days = 0;
              find.hours = 0;
              find.minutes = 0;
              find.seconds = 0;
              find.totalSeconds = find.stopTime - find.startTime;
              
              find.seconds = find.totalSeconds;
              find.days = fix(find.seconds/86400);
              find.seconds = mod(find.seconds,86400);
              find.hours = fix(find.seconds/3600);
              find.seconds = mod(find.seconds,3600);
              find.minutes = fix(find.seconds/60);
              find.seconds = mod(find.seconds,60);
              
              if nargout > 0
                varargout{1} = [find; varargout{1}];
              else
                fprintf('%s (overlap: %d%%)\n', find.folder, round(find.timeSpan));
              end
              
              break;
            end
          end
          
        end
      end
    end
  end
end

function rate = rate(startTime1, stopTime1, startTime2, stopTime2)
  rate = 0;
  
  if startTime1 < stopTime2
    if stopTime1 > startTime2
      if startTime1 > startTime2
        startTime2 = startTime1;
      end
      
      if stopTime1 < stopTime2
        stopTime2 = stopTime1;
      end
      
      rate = ((stopTime2 - startTime2) / (stopTime1 - startTime1)) * 100;
    end
  end
end