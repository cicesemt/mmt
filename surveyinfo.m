function surveyinfo(varargin)
  currentSurvey = [];
  
  flag.raw = false;
  flag.all = false;
  flag.specific = false;
  flag.name = false;
  flag.folder = false;
  flag.bytes = false;
  flag.numberOfSamples = false;
  flag.latitude = false;
  flag.longitude = false;
  flag.elevation = false;
  flag.startTime = false;
  flag.stopTime = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-raw')
        varargin(:, k) = [];
        flag.raw = true;
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-name')
        varargin(:, k) = [];
        flag.specific = true;
        flag.name = true;
      elseif strcmpi(varargin{k}, '-folder')
        varargin(:, k) = [];
        flag.specific = true;
        flag.folder = true;
      elseif strcmpi(varargin{k}, '-bytes')
        varargin(:, k) = [];
        flag.specific = true;
        flag.bytes = true;
      elseif strcmpi(varargin{k}, '-numberOfSamples')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfSamples = true;
      elseif strcmpi(varargin{k}, '-latitude')
        varargin(:, k) = [];
        flag.specific = true;
        flag.latitude = true;
      elseif strcmpi(varargin{k}, '-longitude')
        varargin(:, k) = [];
        flag.specific = true;
        flag.longitude = true;
      elseif strcmpi(varargin{k}, '-elevation')
        varargin(:, k) = [];
        flag.specific = true;
        flag.elevation = true;
      elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.startTime = true;
      elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.stopTime = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.name = true;
    flag.folder = true;
    flag.bytes = true;
    flag.numberOfSamples = true;
    flag.latitude = true;
    flag.longitude = true;
    flag.elevation = true;
    flag.startTime = true;
    flag.stopTime = true;
  elseif ~flag.specific
    flag.name = true;
    flag.numberOfSamples = true;
    flag.latitude = true;
    flag.longitude = true;
    flag.elevation = true;
    flag.startTime = true;
    flag.stopTime = true;
  end
  
  if isempty(currentSurvey)
    currentSurvey = survey(varargin{:});
  end
  
  if ~isempty(currentSurvey)
    printInfo(currentSurvey, flag);
  end
end

function printInfo(currentSurvey, flag)
  names = cell(0);
  values = cell(0);
  
  if flag.name
    names{length(names) + 1} = 'name:';
    values{length(values) + 1} = currentSurvey.name;
  end
  
  if flag.folder
    names{length(names) + 1} = 'folder:';
    values{length(values) + 1} = currentSurvey.folder;
  end
  
  if flag.bytes
    names{length(names) + 1} = 'bytes:';
    
    if flag.raw
      values{length(values) + 1} = sprintf('%d', currentSurvey.bytes);
    else
      values{length(values) + 1} = thousandsep(currentSurvey.bytes, 0);
    end
  end
  
  if flag.numberOfSamples
    names{length(names) + 1} = 'number of samples:';
    
    if flag.raw
      values{length(values) + 1} = sprintf('%d', currentSurvey.numberOfSamples);
    else
      values{length(values) + 1} = thousandsep(currentSurvey.numberOfSamples, 0);
    end
  end
  
  if flag.latitude
    names{length(names) + 1} = 'latitude:';
    
    if flag.raw
      values{length(values) + 1} = sprintf('%d', currentSurvey.latitude);
    else
      values{length(values) + 1} = latitude2string(currentSurvey.latitude);
    end
  end
  
  if flag.longitude
    names{length(names) + 1} = 'longitude:';
    
    if flag.raw
      values{length(values) + 1} = sprintf('%d', currentSurvey.longitude);
    else
      values{length(values) + 1} = longitude2string(currentSurvey.longitude);
    end
  end
  
  if flag.elevation
    names{length(names) + 1} = 'elevation:';
    
    if flag.raw
      values{length(values) + 1} = sprintf('%d', currentSurvey.elevation);
    else
      values{length(values) + 1} = elevation2string(currentSurvey.elevation);
    end
  end
  
  if flag.startTime
    names{length(names) + 1} = 'start time:';
    
    if flag.raw
      values{length(values) + 1} = sprintf('%d', currentSurvey.startTime);
    else
      values{length(values) + 1} = datetime2string(posix2datetime(currentSurvey.startTime));
    end
  end
  
  if flag.stopTime
    names{length(names) + 1} = 'stop time:';
    
    if flag.raw
      values{length(values) + 1} = sprintf('%d', currentSurvey.stopTime);
    else
      values{length(values) + 1} = datetime2string(posix2datetime(currentSurvey.stopTime));
    end
  end
  
  width = 0;
  
  for ii = 1 : length(names)
    if width < length(names{ii})
      width = length(names{ii});
    end
  end
  
  for ii = 1 : length(names)
    if length(names{ii}) < width
      names{ii} = sprintf(sprintf('%%s%%%ds', width - length(names{ii})), names{ii}, ' ');
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s %s\n', names{ii}, values{ii});
  end
end