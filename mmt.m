function [varargout] = mmt(varargin)
  year = '2024';
  month = '09';
  day = '03';

  if nargout > 0
    varargout = cell(1, nargout);

    for ii = 1 : nargout
      varargout{ii} = [];
    end
  end

  flag.help = false;
  flag.version = false;
  flag.mversion = false;
  flag.gversion = false;
  flag.date = false;

  if nargin > 0
    k = 1;

    while k <= length(varargin)
      if strcmpi(varargin{k}, '-help')
        varargin(:, k) = [];
        flag.help = true;
      elseif strcmpi(varargin{k}, '-version')
        varargin(:, k) = [];
        flag.version = true;
      elseif strcmpi(varargin{k}, '-mversion')
        varargin(:, k) = [];
        flag.mversion = true;
      elseif strcmpi(varargin{k}, '-gversion')
        varargin(:, k) = [];
        flag.gversion = true;
      elseif strcmpi(varargin{k}, '-date')
        varargin(:, k) = [];
        flag.date = true;
      else
        if length(varargin{k}) > 1 && varargin{k}(1) == '-'
          showerror('unknown option ''%s''', varargin{k});
          return;
        end

        k = k + 1;
      end
    end
  end

  if ~isempty(varargin)
    showerror('wrong argument usage');
    return;
  elseif flag.help && (flag.version || flag.mversion || flag.gversion || flag.date)
    showerror('wrong argument usage');
    return;
  end

  if flag.help
    help();
  else
    mmt.version = sprintf('MMT %s.%s.%s', year, month, day);
    mmt.mversion = sprintf('Matlab %s', version());
    mmt.map = '';
    mmt.signal = '';
    mmt.gversion = '';
    mmt.date = sprintf('%s/%s/%s', year, month, day);

    map = ver('map');

    if ~isempty(map)
      mmt.map = sprintf('%s %s %s', map.Name, map.Version, map.Release);
    end

    signal = ver('signal');

    if ~isempty(signal)
      mmt.signal = sprintf('%s %s %s', signal.Name, signal.Version, signal.Release);
    end

    [c, v] = system(sprintf('%s -dumpversion', gfortran()));

    if c == 0
      mmt.gversion = sprintf('GFortran %s', deblank(v));
    end

    if nargout > 0
      if flag.version || flag.mversion || flag.gversion || flag.date
        k = 1;

        if flag.version
          varargout{k} = mmt.version;
          k = k + 1;
        end

        if flag.mversion
          varargout{k} = mmt.mversion;
          k = k + 1;

          if ~isempty(mmt.map)
            varargout{k} = mmt.map;
            k = k + 1;
          end

          if ~isempty(mmt.signal)
            varargout{k} = mmt.signal;
            k = k + 1;
          end
        end

        if flag.gversion && ~isempty(mmt.gversion)
          varargout{k} = mmt.gversion;
          k = k + 1;
        end

        if flag.date
          varargout{k} = mmt.date;
        end
      else
        varargout{1} = mmt.version;
        varargout{2} = mmt.mversion;

        if ~isempty(mmt.map)
          varargout{3} = mmt.map;

          if ~isempty(mmt.signal)
            varargout{4} = mmt.signal;
            
            if ~isempty(mmt.gversion)
              varargout{5} = mmt.gversion;
            end
          elseif ~isempty(mmt.gversion)
            varargout{4} = mmt.gversion;
          end
        elseif ~isempty(mmt.signal)
          varargout{3} = mmt.signal;

          if ~isempty(mmt.gversion)
            varargout{4} = mmt.gversion;
          end
        elseif ~isempty(mmt.gversion)
          varargout{3} = mmt.gversion;
        end
      end
    else
      if flag.version || flag.mversion || flag.gversion || flag.date
        if flag.version
          fprintf('%s\n', mmt.version);
        end

        if flag.mversion
          fprintf('%s\n', mmt.mversion);

          if ~isempty(mmt.map)
            fprintf('%s\n', mmt.map);
          end

          if ~isempty(mmt.signal)
            fprintf('%s\n', mmt.signal);
          end
        end

        if flag.gversion && ~isempty(mmt.gversion)
          fprintf('%s\n', mmt.gversion);
        end

        if flag.date
          fprintf('%s\n', mmt.date);
        end
      else
        fprintf('%s\n', mmt.version);
        fprintf('%s\n', mmt.mversion);

        if ~isempty(mmt.map)
          fprintf('%s\n', mmt.map);
        end

        if ~isempty(mmt.signal)
          fprintf('%s\n', mmt.signal);
        end

        if ~isempty(mmt.gversion)
          fprintf('%s\n', mmt.gversion);
        end
      end
    end
  end
end

function help()
  syntax{1} = '{mmt} [-help] | ([-version] [-mversion] [-gversion])';

  description{1} = 'Shows the MMT version, the Matlab version, the Mapping Toolbox version, and the GFortran compiler version. All arguments are optional.';
  description{2} = 'The {-help} argument shows this help. The {-version} argument displays only the version of MMT. The {-mversion} argument displays the version of Matlab and the version of Mapping Toolbox. The {-gversion} argument displays only the version of the GFortran compiler.';

  examples{1} = 'Shows the version of MMT, Matlab, Mapping Toolbox and GFortran.';
  examples{2} = '>> mmt';
  examples{3} = 'Shows the version of MMT.';
  examples{4} = '>> mmt -version';
  examples{5} = 'Shows the version of GFortran.';
  examples{6} = '>> mmt -gversion';

  seealso{1} = 'gfortran';

  showhelp(syntax, description, examples, seealso);
end