function paramsclear()
  if ~isempty(local())
    fn = 'parameters.birrp';
    fp = fopen(fn, 'r');
    
    if fp ~= -1
      fclose(fp);
      delete(fn);
    end
  end
end