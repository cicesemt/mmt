function [varargout] = ats(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  infoCommand     = false;
  dataCommand     = false;
  plotCommand     = false;
  editCommand     = false;
  exportCommand   = false;
  clearCommand    = false;
  extractCommand  = false;
  decimateCommand = false;
  
  removeTrend = false;
  ignoreMask  = false;
  startTime   = [];
  stopTime    = [];
  timeLength  = [];
  hertz       = [];
  
  flag.ex = false;
  flag.ey = false;
  flag.hx = false;
  flag.hy = false;
  flag.hz = false;
  
  if nargin > 0
    if strcmpi(varargin{1}, 'header')
      varargin(:, 1) = [];
      
      if nargout > 0
        varargout{1} = atsheader(varargin{:});
      else
        atsheader(varargin{:});
      end
      
      return;
    elseif strcmpi(varargin{1}, 'info')
      varargin(:, 1) = [];
      infoCommand = true;
    elseif strcmpi(varargin{1}, 'data')
      varargin(:, 1) = [];
      dataCommand = true;
    elseif strcmpi(varargin{1}, 'plot')
      varargin(:, 1) = [];
      plotCommand = true;
    elseif strcmpi(varargin{1}, 'edit')
      varargin(:, 1) = [];
      editCommand = true;
    elseif strcmpi(varargin{1}, 'export')
      varargin(:, 1) = [];
      exportCommand = true;
    elseif strcmpi(varargin{1}, 'clear')
      varargin(:, 1) = [];
      clearCommand = true;
    elseif strcmpi(varargin{1}, 'extract')
      varargin(:, 1) = [];
      extractCommand = true;
    elseif strcmpi(varargin{1}, 'decimate')
      varargin(:, 1) = [];
      decimateCommand = true;
    end
    
    if dataCommand || plotCommand || editCommand || exportCommand
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-detrend')
          varargin(:, k) = [];
          removeTrend = true;
        elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            startTime = varargin{k};
            varargin(:, k) = [];
            
            if ischar(startTime) || isstring(startTime)
              if isnan(str2double(startTime))
                startTime = string2posixtime(startTime);
              else
                startTime = fix(str2double(startTime));
              end
            else
              startTime = [];
            end
          end
        elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            stopTime = varargin{k};
            varargin(:, k) = [];
            
            if ischar(stopTime) || isstring(stopTime)
              if isnan(str2double(stopTime))
                stopTime = string2posixtime(stopTime);
              else
                stopTime = fix(str2double(stopTime));
              end
            else
              stopTime = [];
            end
          end
        elseif strcmpi(varargin{k}, '-length') || strcmpi(varargin{k}, '-timeLength')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            timeLength = varargin{k};
            varargin(:, k) = [];
            
            if ischar(timeLength) || isstring(timeLength)
              if ~isnan(str2double(timeLength))
                timeLength = fix(str2double(timeLength));
                
                if timeLength <= 0
                  timeLength = [];
                end
              else
                timeLength = [];
              end
            else
              timeLength = [];
            end
          end
        else
          k = k + 1;
        end
      end
    end
    
    if exportCommand
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-nomask')
          varargin(:, k) = [];
          ignoreMask = true;
        else
          k = k + 1;
        end
      end
    end
    
    if extractCommand
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            startTime = varargin{k};
            varargin(:, k) = [];
            
            if ischar(startTime) || isstring(startTime)
              if isnan(str2double(startTime))
                startTime = string2posixtime(startTime);
              else
                startTime = fix(str2double(startTime));
              end
            else
              startTime = [];
            end
          end
        elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            stopTime = varargin{k};
            varargin(:, k) = [];
            
            if ischar(stopTime) || isstring(stopTime)
              if isnan(str2double(stopTime))
                stopTime = string2posixtime(stopTime);
              else
                stopTime = fix(str2double(stopTime));
              end
            else
              stopTime = [];
            end
          end
        elseif strcmpi(varargin{k}, '-length') || strcmpi(varargin{k}, '-timeLength')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            timeLength = varargin{k};
            varargin(:, k) = [];
            
            if ischar(timeLength) || isstring(timeLength)
              if ~isnan(str2double(timeLength))
                timeLength = fix(str2double(timeLength));
                
                if timeLength <= 0
                  timeLength = [];
                end
              else
                timeLength = [];
              end
            else
              timeLength = [];
            end
          end
        else
          k = k + 1;
        end
      end
    end
    
    if decimateCommand
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-h') || strcmpi(varargin{k}, '-hertz')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            hertz = varargin{k};
            varargin(:, k) = [];
            
            if ischar(hertz)
              hertz = str2double(hertz);
            end
            
            if isnan(hertz)
              hertz = [];
            end
          end
        elseif strcmpi(varargin{k}, '-s') || strcmpi(varargin{k}, '-seconds')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            hertz = varargin{k};
            varargin(:, k) = [];
            
            if ischar(hertz)
              hertz = str2double(hertz);
            end
            
            if isnan(hertz)
              hertz = [];
            else
              hertz = 1 / hertz;
            end
          end
        else
          k = k + 1;
        end
      end
    end
    
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-ex')
        varargin(:, k) = [];
        flag.ex = true;
      elseif strcmpi(varargin{k}, '-ey')
        varargin(:, k) = [];
        flag.ey = true;
      elseif strcmpi(varargin{k}, '-hx')
        varargin(:, k) = [];
        flag.hx = true;
      elseif strcmpi(varargin{k}, '-hy')
        varargin(:, k) = [];
        flag.hy = true;
      elseif strcmpi(varargin{k}, '-hz')
        varargin(:, k) = [];
        flag.hz = true;
      else
        k = k + 1;
      end
    end
  end
  
  if ~flag.ex && ~flag.ey && ~flag.hx && ~flag.hy && ~flag.hz
    flag.ex = true;
    flag.ey = true;
    flag.hx = true;
    flag.hy = true;
    flag.hz = true;
  end
  
  if ~isempty(varargin)
    args = varargin;
  else
    args = {pwd()};
  end
  
  atsfiles = cell(0);
  
  for ii = 1 : length(args)
    files = dir(args{ii});
    
    if ~isempty(files)
      for jj = 1 : length(files)
        file = atsfile(sprintf('%s%s%s', files(jj).folder, filesep, files(jj).name));
        
        if ~isempty(file)
          if strcmp(file.channelType, 'Ex')
            if ~flag.ex
              continue;
            end
          elseif strcmp(file.channelType, 'Ey')
            if ~flag.ey
              continue;
            end
          elseif strcmp(file.channelType, 'Hx')
            if ~flag.hx
              continue;
            end
          elseif strcmp(file.channelType, 'Hy')
            if ~flag.hy
              continue;
            end
          elseif strcmp(file.channelType, 'Hz')
            if ~flag.hz
              continue;
            end
          end
          
          duplicated = false;
          
          for kk = 1 : length(atsfiles)
            if strcmpi(sprintf('%s%s%s', atsfiles{kk}.folder, filesep, atsfiles{kk}.name), sprintf('%s%s%s', file.folder, filesep, file.name))
              duplicated = true;
              break;
            end
          end
          
          if ~duplicated
            atsfiles{length(atsfiles) + 1} = file;
          end
        end
      end
    end
  end
  
  if ~isempty(atsfiles)
    if infoCommand
      if nargout > 0
        for ii = 1 : length(atsfiles)
          varargout{1} = [varargout{1} atsinfo(sprintf('%s%s%s', atsfiles{ii}.folder, filesep, atsfiles{ii}.name))];
        end
      else
        for ii = 1 : length(atsfiles)
          atsinfo(sprintf('%s%s%s', atsfiles{ii}.folder, filesep, atsfiles{ii}.name));
        end
      end
    elseif dataCommand || plotCommand || editCommand || exportCommand
      if isempty(startTime) && isempty(stopTime)
        if ~isempty(timeLength)
          startTime = atsfiles{1}.startTime;

          for ii = 2 : length(atsfiles)
            if atsfiles{ii}.startTime > startTime
              startTime = atsfiles{ii}.startTime;
            end
          end
          
          stopTime = atsfiles{1}.stopTime;

          for ii = 2 : length(atsfiles)
            if atsfiles{ii}.stopTime < stopTime
              stopTime = atsfiles{ii}.stopTime;
            end
          end
          
          if stopTime > startTime
            if stopTime - startTime > timeLength
              stopTime = startTime + timeLength;
            end
          end
        end
      end
      
      if isempty(startTime)
        startTime = atsfiles{1}.startTime;
        
        for ii = 2 : length(atsfiles)
          if atsfiles{ii}.startTime > startTime
            startTime = atsfiles{ii}.startTime;
          end
        end
      end
      
      if isempty(stopTime)
        stopTime = atsfiles{1}.stopTime;
        
        for ii = 2 : length(atsfiles)
          if atsfiles{ii}.stopTime < stopTime
            stopTime = atsfiles{ii}.stopTime;
          end
        end
      end
      
      if stopTime > startTime
        files = cell(0);
        
        for ii = 1 : length(atsfiles)
          if startTime >= atsfiles{ii}.startTime && stopTime <= atsfiles{ii}.stopTime
            files{length(files) + 1} = atsfiles{ii};
          end
        end
        
        if dataCommand
          if nargout > 0
            for ii = 1 : length(files)
              varargout{1} = [varargout{1} atsdata(sprintf('%s%s%s', files{ii}.folder, filesep, files{ii}.name), removeTrend, startTime, stopTime)];
            end
          else
            for ii = 1 : length(files)
              if ii > 1
                fprintf('\n');
              end
              
              atsdata(sprintf('%s%s%s', files{ii}.folder, filesep, files{ii}.name), removeTrend, startTime, stopTime);
            end
          end
        elseif plotCommand
          for ii = 1 : length(files)
            atsplot(sprintf('%s%s%s', files{ii}.folder, filesep, files{ii}.name), removeTrend, startTime, stopTime, ii, length(files));
          end
        elseif editCommand
          for ii = 1 : length(files)
            %atsedit(sprintf('%s%s%s', files{ii}.folder, filesep, files{ii}.name), removeTrend, startTime, stopTime, ii, length(files));
          end
        elseif exportCommand
          for ii = 1 : length(files)
            atsexport(sprintf('%s%s%s', files{ii}.folder, filesep, files{ii}.name), removeTrend, ignoreMask, startTime, stopTime);
          end
        end
      end
    elseif clearCommand
      fprintf('deprecated!\n');
      %for ii = 1 : length(atsfiles)
      %  atsclear(sprintf('%s%s%s', atsfiles{ii}.folder, filesep, atsfiles{ii}.name));
      %end
    elseif extractCommand
      %
      % TODO: extract with timeLength
      %
      for ii = 1 : length(atsfiles)
        if isempty(startTime) && isempty(stopTime)
          atsextract(sprintf('%s%s%s', atsfiles{ii}.folder, filesep, atsfiles{ii}.name), atsfiles{ii}.startTime, atsfiles{ii}.stopTime);
        elseif isempty(startTime) && ~isempty(stopTime)
          atsextract(sprintf('%s%s%s', atsfiles{ii}.folder, filesep, atsfiles{ii}.name), atsfiles{ii}.startTime, stopTime);
        elseif ~isempty(startTime) && isempty(stopTime)
          atsextract(sprintf('%s%s%s', atsfiles{ii}.folder, filesep, atsfiles{ii}.name), startTime, atsfiles{ii}.stopTime);
        else
          atsextract(sprintf('%s%s%s', atsfiles{ii}.folder, filesep, atsfiles{ii}.name), startTime, stopTime);
        end
      end
    elseif decimateCommand
      for ii = 1 : length(atsfiles)
        atsdecimate(sprintf('%s%s%s', atsfiles{ii}.folder, filesep, atsfiles{ii}.name), hertz);
      end
    else
      if nargout > 0
        for ii = 1 : length(atsfiles)
          varargout{1} = [varargout{1} atsfiles{ii}];
        end
      else
        for ii = 1 : length(atsfiles)
          fprintf('%s\n', atsfiles{ii}.name);
        end
      end
    end
  end
end