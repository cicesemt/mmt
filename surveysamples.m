function surveysamples(varargin)
  currentSurvey = survey(varargin{:});
  
  if ~isempty(currentSurvey)
    name = currentSurvey.name;
    sites = currentSurvey.sites;
    siteCount = length(sites);
    siteNames = cell(1, siteCount);
    numberOfSamples = 0;
    
    for ii = 1 : siteCount
      siteNames{ii} = sites(ii).name;
      
      if numberOfSamples < sites(ii).numberOfSamples
        numberOfSamples = sites(ii).numberOfSamples;
      end
    end
    
    ymin = 0;
    yuni = {'' 'K' 'M' 'G' 'T'};
    yind = 0;
    ydiv = 1024^yind;
    yind = 1;
    ymax = 1024^yind;
    
    for ii = 1 : 5
      if numberOfSamples <= ymax
        for jj = 10*(ii-1)+1 : 10*ii
          ymax = 2^jj;
          
          if numberOfSamples <= ymax
            break;
          end
        end
        
        break;
      end
      
      ydiv = 1024^yind;
      yind = yind + 1;
      ymax = 1024^yind;
    end
    
    step = (ymax - ymin) / 4;
    
    y0 = ymin;
    y1 = ymin + step;
    y2 = ymin + step + step;
    y3 = ymax - step;
    y4 = ymax;
    
    if y4/ydiv >= 4
      digits = 0;
    elseif y4/ydiv >= 2
      digits = 1;
    else
      digits = 2;
    end
    
    if strcmp(yuni{yind}, 'T')
      label = 'Trillions of samples';
    elseif strcmp(yuni{yind}, 'G')
      label = 'Billions of samples';
    elseif strcmp(yuni{yind}, 'M')
      label = 'Millions of samples';
    elseif strcmp(yuni{yind}, 'K')
      label = 'Thousands of samples';
    else
      label = 'Samples';
    end
    
    label0 = sprintf(sprintf('%%.%df', digits), y0/ydiv);
    label1 = sprintf(sprintf('%%.%df', digits), y1/ydiv);
    label2 = sprintf(sprintf('%%.%df', digits), y2/ydiv);
    label3 = sprintf(sprintf('%%.%df', digits), y3/ydiv);
    label4 = sprintf(sprintf('%%.%df', digits), y4/ydiv);
    
    f = figure();
    a = axes();
    
    f.set('NumberTitle', 'off');
    f.set('ToolBar', 'none');
    f.set('MenuBar', 'none');
    f.set('Name', name);
    
    a.FontSize = 10;
    a.FontWeight = 'normal';
    
    a.Title.String = 'Number of samples';
    a.Title.FontSize = 14;
    a.Title.FontWeight = 'bold';
    
    a.XLabel.String = 'Sites';
    a.XLabel.FontSize = 12;
    a.XLabel.FontWeight = 'bold';
    
    a.XLim = [0 siteCount + 1];
    a.XTick = 1 : siteCount;
    a.XTickLabel = siteNames;
    
    a.YLabel.String = label;
    a.YLabel.FontSize = 12;
    a.YLabel.FontWeight = 'bold';
    
    a.YLim = [ymin ymax];
    a.YTick = ymin : step : ymax;
    a.YTickLabel = {label0 label1 label2 label3 label4};
    a.YGrid = 'on';
    
    a.Box = 'on';
    hold(a, 'on');
    
    fposition = get(f, 'Position');
    fwidth = fposition(3);
    
    aposition = get(a, 'Position');
    awidth = aposition(3) * fwidth;
    
    if siteCount > 1
      lwidth = round(awidth / (1.1 * siteCount + 1));
    else
      lwidth = round(awidth / (2 * siteCount + 1));
    end
    
    for ii = 1 : siteCount
      x = [ii ii];
      y = [0 sites(ii).numberOfSamples];
      
      plot(x, y, 'Color', [0 0.4470 0.7410], 'LineStyle', '-', 'LineWidth', lwidth);
    end
  end
end