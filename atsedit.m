function atsedit(filename, varargin)
%   file = atsfile(filename);
%   
%   if ~isempty(file)
%     removeTrend = false;
%     startTime   = [];
%     stopTime    = [];
%     current     = 1;
%     total       = 1;
%     
%     if nargin > 1
%       if nargin == 2
%         if islogical(varargin{1})
%           if varargin{1}
%             removeTrend = true;
%           end
%         end
%       elseif nargin == 3
%         if isnumeric(varargin{1})
%           startTime = varargin{1};
%         end
%         
%         if isnumeric(varargin{2})
%           stopTime = varargin{2};
%         end
%       else
%         if islogical(varargin{1})
%           if varargin{1}
%             removeTrend = true;
%           end
%         end
%         
%         if isnumeric(varargin{2})
%           startTime = varargin{2};
%         end
%         
%         if isnumeric(varargin{3})
%           stopTime = varargin{3};
%         end
%         
%         if nargin > 5
%           if isnumeric(varargin{4}) && isnumeric(varargin{5})
%             if varargin{4} > 0 && varargin{5} > 0 && varargin{4} <= varargin{5}
%               current = varargin{4};
%               total   = varargin{5};
%             end
%           end
%         end
%       end
%     end
%     
%     if isempty(startTime)
%       startTime = file.startTime;
%     end
%     
%     if isempty(stopTime)
%       stopTime = file.stopTime;
%     end
%     
%     data = atsdata(sprintf('%s%s%s', file.folder, filesep, file.name), removeTrend, startTime, stopTime);
%     
%     if ~isempty(data)
%       if current == 1
%         f = figure();
%         f.set('NumberTitle', 'off');
%         f.set('MenuBar', 'none');
%         f.set('Name', sprintf('%d%s%s%d%s', file.aduSerialNumber, upper(file.channelType), upper(file.boardType), file.samplingFrequency, upper(file.measurementUnit)));
%         
%         f.UserData.id                  = 'atsedit';
%         f.UserData.folder              = file.folder;
%         f.UserData.startTime           = file.startTime;
%         f.UserData.stopTime            = file.stopTime;
%         f.UserData.timeWidth           = f.UserData.stopTime - f.UserData.startTime;
%         f.UserData.viewport.startTime  = startTime;
%         f.UserData.viewport.stopTime   = stopTime;
%         f.UserData.viewport.timeWidth  = f.UserData.viewport.stopTime - f.UserData.viewport.startTime;
%         f.UserData.selection           = [];
%         
%         set(f, 'KeyPressFcn', @KeyPressFcn);
%         
%         show(f, file, data, removeTrend, current, total);
%       else
%         f = gcf();
%         
%         if startTime == f.UserData.viewport.startTime && stopTime == f.UserData.viewport.stopTime
%           f.set('Name', sprintf('%s, %s', f.get('Name'), sprintf('%d%s%s%d%s', file.aduSerialNumber, upper(file.channelType), upper(file.boardType), file.samplingFrequency, upper(file.measurementUnit))));
%           
%           if file.startTime > f.UserData.startTime
%             f.UserData.startTime = file.startTime;
%             f.UserData.timeWidth = f.UserData.stopTime - f.UserData.startTime;
%           end
%           
%           if file.stopTime < f.UserData.stopTime
%             f.UserData.stopTime  = file.stopTime;
%             f.UserData.timeWidth = f.UserData.stopTime - f.UserData.startTime;
%           end
%           
%           show(f, file, data, removeTrend, current, total);
%         end
%       end
%     end
%   end
end

function show(f, file, data, removeTrend, current, total)
  figure(f);
  axes = subplot(total, 1, current);
  cla(axes, 'reset');
  
  period = 1 / file.samplingFrequency;
  xdata = f.UserData.viewport.startTime : period : f.UserData.viewport.stopTime - period;
  series = plot(xdata, data, 'Color', [0 0.4470 0.7410], 'LineStyle', '-');
  
  axes.YLabel.String     = file.channelType;
  axes.YLabel.FontSize   = 12;
  axes.YLabel.FontWeight = 'bold';
  
  axes.XLim       = [xdata(1) xdata(length(xdata))];
  axes.XTick      = xdata(1) : (xdata(length(xdata)) - xdata(1)) / 4 : xdata(length(xdata));
  axes.XTickLabel = {};
  
  axes.YTick      = axes.YLim(1) : (axes.YLim(2) - axes.YLim(1)) / 4 : axes.YLim(2);
  axes.YTickLabel = {};
  
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(axes, 'ButtonDownFcn', @axesmousedown);
  
  if current == 1
    totalSamples = f.UserData.timeWidth * file.samplingFrequency;
    
    if length(data) ~= totalSamples
      axes.Title.String = sprintf('%d samples (%d total)', length(data), totalSamples);
    else
      axes.Title.String = sprintf('%d samples', length(data));
    end
    
    axes.Title.FontSize   = 14;
    axes.Title.FontWeight = 'bold';
  end
  
  if current == total
    start = datestr(datetime(f.UserData.viewport.startTime, 'convertfrom', 'posixtime'), 'mmm dd, yyyy HH:MM:SS');
    stop  = datestr(datetime(f.UserData.viewport.stopTime, 'convertfrom', 'posixtime'), 'mmm dd, yyyy HH:MM:SS');
    
    axes.XLabel.String     = {'', sprintf('%s to %s', start, stop)};
    axes.XLabel.FontSize   = 12;
    axes.XLabel.FontWeight = 'bold';
  end
  
  hold(axes, 'on');
  
  mask(f, file, data);
  
  axes.UserData.file        = file;
  axes.UserData.removeTrend = removeTrend;
  
  f.UserData.axes{current} = axes;
end

function mask(figure, file, data)
  tm = tmask();
  
  if ~isempty(tm)
    start = tm(:, 1);
    stop  = tm(:, 2);
    
    k = length(start) + 1;
    
    for ii = 1 : length(start)
      if figure.UserData.viewport.startTime <= start(ii)
        k = ii;
        break;
      end
    end
    
    if k > 1
      if figure.UserData.viewport.startTime < stop(k - 1)
        k = k - 1;
      end
    end
    
    for ii = k : length(start)
      if start(ii) >= figure.UserData.viewport.stopTime
        break;
      end
      
      startTime = max(start(ii), figure.UserData.viewport.startTime);
      stopTime  = min(stop(ii), figure.UserData.viewport.stopTime);
      
      k1 = (startTime - figure.UserData.viewport.startTime) * file.samplingFrequency;
      k2 = (stopTime - figure.UserData.viewport.startTime) * file.samplingFrequency;
      
      m = false(figure.UserData.viewport.timeWidth * file.samplingFrequency, 1);
      m(k1 + 1 : k2, 1) = true;
      
      period = 1 / file.samplingFrequency;
      xdata = startTime : period : stopTime - period;
      series = plot(xdata, data(m), 'Color', [0.85 0.85 0.85], 'LineStyle', '-');
      
      set(series, 'ButtonDownFcn', @seriesmousedown);
    end
  end
end

function refresh(figure)
  for ii = 1 : length(figure.UserData.axes)
    axes = figure.UserData.axes{ii};
    
    file        = axes.UserData.file;
    removeTrend = axes.UserData.removeTrend;
    startTime   = figure.UserData.viewport.startTime;
    stopTime    = figure.UserData.viewport.stopTime;
    
    data = atsdata(sprintf('%s%s%s', file.folder, filesep, file.name), removeTrend, startTime, stopTime);
    show(figure, file, data, removeTrend, ii, length(figure.UserData.axes));
  end
  
  if ~isempty(figure.UserData.selection)
    select(figure, figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
  end
end

function select(figure, startTime, stopTime)
  if startTime < figure.UserData.viewport.stopTime && stopTime > figure.UserData.viewport.startTime
    if startTime < figure.UserData.viewport.startTime
      start = figure.UserData.viewport.startTime;
    else
      start = startTime;
    end
    
    if stopTime > figure.UserData.viewport.stopTime
      stop = figure.UserData.viewport.stopTime;
    else
      stop = stopTime;
    end
    
    for ii = 1 : length(figure.UserData.axes)
      axes = figure.UserData.axes{ii};
      
      file        = axes.UserData.file;
      removeTrend = axes.UserData.removeTrend;
      
      data = atsdata(sprintf('%s%s%s', file.folder, filesep, file.name), removeTrend, start, stop);
      
      if ~isempty(data)
        subplot(length(figure.UserData.axes), 1, ii);
        
        xdata = start : 1 / file.samplingFrequency : stop - (1 / file.samplingFrequency);
        series = plot(xdata, data, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', '-');
        
        set(series, 'ButtonDownFcn', @seriesmousedown);
        
        axes.UserData.selection = series;
      end
    end
  end
  
  if ~isempty(figure.UserData.axes)
    timeWidth = stopTime - startTime;
    
    if timeWidth == 1
      figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String = {sprintf('Selected: 1 second'), figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String{2}};
    else
      figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String = {sprintf('Selected: %d seconds', timeWidth), figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String{2}};
    end
  end
  
  figure.UserData.selection.startTime = startTime;
  figure.UserData.selection.stopTime  = stopTime;
  figure.UserData.selection.timeWidth = stopTime - startTime;
end

function deselect(figure)
  for ii = 1 : length(figure.UserData.axes)
    axes = figure.UserData.axes{ii};
    
    if isfield(axes.UserData, 'selection')
      series = axes.UserData.selection;
      
      if ~isempty(series)
        axes.UserData.selection = [];
        delete(series);
      end
    end
  end
  
  if ~isempty(figure.UserData.axes)
    figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String = {'', figure.UserData.axes{length(figure.UserData.axes)}.XLabel.String{2}};
  end
  
  figure.UserData.selection = [];
end

function KeyPressFcn(figure, event)
  alt   = false;
  shift = false;
  
  for ii = 1 : length(event.Modifier)
    if strcmp(event.Modifier{ii}, 'alt')
      alt = true;
    elseif strcmp(event.Modifier{ii}, 'shift')
      shift = true;
    end
  end
  
  if strcmp(event.Key, 'leftarrow')
    if figure.UserData.viewport.startTime > figure.UserData.startTime
      if alt
        figure.UserData.viewport.startTime = figure.UserData.viewport.startTime - 1;
        figure.UserData.viewport.stopTime  = figure.UserData.viewport.stopTime - 1;
      else
        if figure.UserData.viewport.startTime - figure.UserData.viewport.timeWidth >= figure.UserData.startTime
          figure.UserData.viewport.startTime = figure.UserData.viewport.startTime - figure.UserData.viewport.timeWidth;
          figure.UserData.viewport.stopTime  = figure.UserData.viewport.stopTime - figure.UserData.viewport.timeWidth;
        else
          figure.UserData.viewport.startTime = figure.UserData.startTime;
          figure.UserData.viewport.stopTime  = figure.UserData.startTime + figure.UserData.viewport.timeWidth;
        end
      end
      
      refresh(figure);
    end
  elseif strcmp(event.Key, 'rightarrow')
    if figure.UserData.viewport.stopTime < figure.UserData.stopTime
      if alt
        figure.UserData.viewport.startTime = figure.UserData.viewport.startTime + 1;
        figure.UserData.viewport.stopTime  = figure.UserData.viewport.stopTime + 1;
      else
        if figure.UserData.viewport.stopTime + figure.UserData.viewport.timeWidth <= figure.UserData.stopTime
          figure.UserData.viewport.startTime = figure.UserData.viewport.startTime + figure.UserData.viewport.timeWidth;
          figure.UserData.viewport.stopTime  = figure.UserData.viewport.stopTime + figure.UserData.viewport.timeWidth;
        else
          figure.UserData.viewport.startTime = figure.UserData.stopTime - figure.UserData.viewport.timeWidth;
          figure.UserData.viewport.stopTime  = figure.UserData.stopTime;
        end
      end
      
      refresh(figure);
    end
  elseif strcmp(event.Key, 'uparrow')
    if figure.UserData.viewport.startTime > figure.UserData.startTime
      figure.UserData.viewport.startTime = figure.UserData.startTime;
      figure.UserData.viewport.stopTime  = figure.UserData.startTime + figure.UserData.viewport.timeWidth;
      
      refresh(figure);
    end
  elseif strcmp(event.Key, 'downarrow')
    if figure.UserData.viewport.stopTime < figure.UserData.stopTime
      figure.UserData.viewport.startTime = figure.UserData.stopTime - figure.UserData.viewport.timeWidth;
      figure.UserData.viewport.stopTime  = figure.UserData.stopTime;
      
      refresh(figure);
    end
  elseif strcmp(event.Key, 'r')
    refresh(figure);
  elseif strcmp(event.Key, 'd')
    if ~isempty(figure.UserData.selection)
      tmaskadd(figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
      deselect(figure);
    end
  elseif strcmp(event.Key, 'u')
    if ~isempty(figure.UserData.selection)
      tmaskremove(figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
      deselect(figure);
    end
  elseif strcmp(event.Key, 'delete') || strcmp(event.Key, 'backspace')
    if ~isempty(figure.UserData.selection)
      if shift
        tmaskremove(figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
      else
        tmaskadd(figure.UserData.selection.startTime, figure.UserData.selection.stopTime);
      end
      
      deselect(figure);
    end
  elseif strcmp(event.Key, 'escape')
    deselect(figure);
  end
end

function seriesmousedown(series, ~)
  mousedown(series.Parent, series);
end

function axesmousedown(axes, ~)
  mousedown(axes, []);
end

function mousedown(axes, series)
  figure = axes.Parent;
  
  if strcmp(figure.SelectionType, 'normal')
    currentPoint = axes.CurrentPoint;
    
    x1 = currentPoint(1, 1);
    y1 = axes.YLim(1); %currentPoint(1, 2);
    
    if x1 >= figure.UserData.viewport.startTime && x1 <= figure.UserData.viewport.stopTime
      if y1 >= axes.YLim(1) && y1 <= axes.YLim(2)
        top = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--');
        left = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--');
        right = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--');
        bottom = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--');
        
        figure.Pointer = 'crosshair';
        figure.WindowButtonMotionFcn = @mousemotion;
        figure.WindowButtonUpFcn = @mouseup;
      end
    end
  end
  
  function mousemotion(~, ~)
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = axes.YLim(2); %currentPoint(1, 2);
    
    if x2 < figure.UserData.viewport.startTime
      x2 = figure.UserData.viewport.startTime;
    elseif x2 > figure.UserData.viewport.stopTime
      x2 = figure.UserData.viewport.stopTime;
    end
    
    if y2 < axes.YLim(1)
      y2 = axes.YLim(1);
    elseif y2 > axes.YLim(2)
      y2 = axes.YLim(2);
    end
    
    top.XData = [x1, x2];
    top.YData = [y1, y1];
    
    left.XData = [x1, x1];
    left.YData = [y1, y2];
    
    right.XData = [x2, x2];
    right.YData = [y1, y2];
    
    bottom.XData = [x1, x2];
    bottom.YData = [y2, y2];
    
    drawnow();
  end
  
  function mouseup(~, ~)
    figure.Pointer = 'arrow';
    figure.WindowButtonMotionFcn = [];
    figure.WindowButtonUpFcn = [];
    
    delete(top);
    delete(left);
    delete(right);
    delete(bottom);
    
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = currentPoint(1, 2);
    
    if ~isempty(series) || (x1 ~= x2 && y1 ~= y2)
      deselect(figure);
      
      if x1 > x2
        x  = x1;
        x1 = x2;
        x2 = x;
      end
      
      x1 = floor(x1);
      x2 = ceil(x2);
      
      if x1 < figure.UserData.viewport.startTime
        x1 = figure.UserData.viewport.startTime;
      end
      
      if x2 > figure.UserData.viewport.stopTime
        x2 = figure.UserData.viewport.stopTime;
      end
      
      select(figure, x1, x2);
    end
  end
end