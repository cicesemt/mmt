function jfiles = jselect(args, sites, bands)
  jfiles = cell(0);
  
  for ii = 1 : length(args)
    files = jfind(args{ii});
    
    if ~isempty(files)
      for jj = 1 : length(files)
        duplicated = false;
        
        for kk = 1 : length(jfiles)
          if strcmpi(sprintf('%s%s%s', files(jj).folder, filesep, files(jj).name), sprintf('%s%s%s', jfiles{kk}.folder, filesep, jfiles{kk}.name))
            duplicated = true;
            break;
          end
        end
        
        if ~duplicated
          siteflag = true;
          bandflag = true;
          
          if ~isempty(sites)
            siteflag = false;
            
            for kk = 1 : length(sites)
              if strcmpi(files(jj).site, sites{kk})
                siteflag = true;
                break;
              end
            end
          end
          
          if ~isempty(bands)
            bandflag = false;
            
            for kk = 1 : length(bands)
              if strcmpi(files(jj).frequencyBand, bands{kk})
                bandflag = true;
                break;
              end
            end
          end
          
          if siteflag && bandflag
            jfiles{length(jfiles) + 1} = files(jj);
          end
        end
      end
    end
  end
  
end