function tip(varargin)
  sites = cell(0);
  bands = cell(0);
  
  zxflag = false;
  zyflag = false;
  rwflag = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-site')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          sites{length(sites) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          bands{length(bands) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-zx')
        zxflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-zy')
        zyflag = true;
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-raw')
        rwflag = true;
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if ~isempty(varargin)
    args = varargin;
  else
    args = {pwd()};
  end
  
  if ~zxflag && ~zyflag
    zxflag = true;
    zyflag = true;
  end
  
  jfiles = jselect(args, sites, bands);
  
  if ~isempty(jfiles)
    f = figure;
    f.set('NumberTitle', 'off');
    f.set('MenuBar', 'figure');
    f.set('ToolBar', 'figure');
    
    f.UserData.id = 'mmt-tip-plot';
    f.UserData.freezed = false;
    
    f.UserData.TopPlot            = [];
    f.UserData.TopSeries          = {};
    f.UserData.TopMaskSeries      = {};
    f.UserData.TopRepaintSeries   = {};
    f.UserData.TopSelectionSeries = {};
    
    f.UserData.BottomPlot            = [];
    f.UserData.BottomSeries          = {};
    f.UserData.BottomMaskSeries      = {};
    f.UserData.BottomRepaintSeries   = {};
    f.UserData.BottomSelectionSeries = {};
    
    set(f, 'KeyPressFcn', @keypress);
    
    for ii = 1 : length(jfiles)
      jfile = jfiles{ii};
      
      if ii == 1
        f.set('Name', sprintf('%s[%s]', jfile.site, jfile.frequencyBand));
      else
        f.set('Name', sprintf('%s, %s', f.get('Name'), sprintf('%s[%s]', jfile.site, jfile.frequencyBand)));
      end
      
      if rwflag
        if zxflag
          showRawZX(f, jfile);
        end
        
        if zyflag
          showRawZY(f, jfile);
        end
      else
        if zxflag
          showZX(f, jfile);
        end
        
        if zyflag
          showZY(f, jfile);
        end
      end
    end
    
    legends = cell(0);
    
    if zxflag
      legends{length(legends) + 1} = 'T_{ZX}';
    end
    
    if zyflag
      legends{length(legends) + 1} = 'T_{ZY}';
    end
    
    plot1 = subplot(2, 1, 1);
    title('Tipper');
    
    if rwflag
      ylabel('Real');
    else
      ylabel('Magnitude [Ohm]');
    end
    
    legend(gca, legends);
    grid on;
    
    plot2 = subplot(2, 1, 2);
    xlabel('Period [seconds]');
    
    if rwflag
      ylabel('Imaginary');
    else
      ylabel('Phase [degrees]');
      ylim([-180, 180]);
      yticks(-180 : 45 : 180);
    end
    
    legend(gca, legends);
    grid on;
    
    if ~rwflag
      pos1 = get(plot1, 'position');
      pos2 = get(plot2, 'position');
      
      pos1(4) = pos1(4) + (pos2(4) * 0.25);
      pos1(2) = pos1(2) - (pos2(4) * 0.25);
      pos2(4) = pos2(4) - (pos2(4) * 0.25);
      
      set(plot1, 'position', pos1);
      set(plot2, 'position', pos2);
    end
    
    f.UserData.TopPlot = plot1;
    f.UserData.BottomPlot = plot2;
    
    f.UserData.TopPlot.UserData.AxesType = 'top';
    f.UserData.BottomPlot.UserData.AxesType = 'bottom';
    
    enmask(f);
    repaint(f);
  end
end

function showZX(figure, jfile)
  value  = conj(jfile.tzx.value);
  period = jfile.tzx.period;
  
  magnitude = abs(value);
  phase     = angle(value) * (180 / pi);
  
  axes = subplot(2, 1, 1);
  series = semilogx(period, magnitude, 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'mzx';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = [];
  
  figure.UserData.TopSeries{length(figure.UserData.TopSeries) + 1} = series;
  
  axes = subplot(2, 1, 2);
  series = semilogx(period, phase, 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'pzx';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = [];
  
  figure.UserData.BottomSeries{length(figure.UserData.BottomSeries) + 1} = series;
end

function showZY(figure, jfile)
  value  = conj(jfile.tzy.value);
  period = jfile.tzy.period;
  
  magnitude = abs(value);
  phase     = angle(value) * (180 / pi);
  
  axes = subplot(2, 1, 1);
  series = semilogx(period, magnitude, 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'mzy';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = [];
  
  figure.UserData.TopSeries{length(figure.UserData.TopSeries) + 1} = series;
  
  axes = subplot(2, 1, 2);
  series = semilogx(period, phase, 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'pzy';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = [];
  
  figure.UserData.BottomSeries{length(figure.UserData.BottomSeries) + 1} = series;
end

function showRawZX(figure, jfile)
  value  = jfile.tzx.value;
  period = jfile.tzx.period;
  error  = jfile.tzx.error;
  
  axes = subplot(2, 1, 1);
  series = semilogx(period, real(value), 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = real(value) - error;
  ye(2, :) = real(value) + error;
  
  eseries = semilogx(xe, ye, 'Color', 'b', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'rezx';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'rezx';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.TopSeries{length(figure.UserData.TopSeries) + 1} = series;
  
  axes = subplot(2, 1, 2);
  series = semilogx(period, imag(value), 'Color', 'b', 'LineStyle', 'none', 'Marker', 'o');
  hold on;
  
  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = imag(value) - error;
  ye(2, :) = imag(value) + error;
  
  eseries = semilogx(xe, ye, 'Color', 'b', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'imzx';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'imzx';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.BottomSeries{length(figure.UserData.BottomSeries) + 1} = series;
end

function showRawZY(figure, jfile)
  value  = jfile.tzy.value;
  period = jfile.tzy.period;
  error  = jfile.tzy.error;

  axes = subplot(2, 1, 1);
  series = semilogx(period, real(value), 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;

  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = real(value) - error;
  ye(2, :) = real(value) + error;

  eseries = semilogx(xe, ye, 'Color', 'r', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'rezy';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'rezy';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.TopSeries{length(figure.UserData.TopSeries) + 1} = series;

  axes = subplot(2, 1, 2);
  series = semilogx(period, imag(value), 'Color', 'r', 'LineStyle', 'none', 'Marker', 'o');
  hold on;

  xe = zeros(2, length(period));
  xe(1, :) = period;
  xe(2, :) = period;
  ye = zeros(2, length(period));
  ye(1, :) = imag(value) - error;
  ye(2, :) = imag(value) + error;

  eseries = semilogx(xe, ye, 'Color', 'r', 'LineStyle', '-', 'HandleVisibility', 'off');
  
  set(axes, 'ButtonDownFcn', @axesmousedown);
  set(series, 'ButtonDownFcn', @seriesmousedown);
  set(eseries, 'ButtonDownFcn', @seriesmousedown);
  
  series.UserData.SeriesType  = 'data';
  series.UserData.SeriesName  = 'imzy';
  series.UserData.SeriesFile  = jfile;
  series.UserData.SeriesError = eseries;
  
  for ii = 1 : length(eseries)
    eseries(ii).UserData.SeriesType = 'error';
    eseries(ii).UserData.SeriesName = 'imzy';
    eseries(ii).UserData.SeriesFile = jfile;
    eseries(ii).UserData.SeriesData = series;
    eseries(ii).UserData.XData      = eseries(ii).XData(1);
    eseries(ii).UserData.YData      = series.YData(ii);
  end
  
  figure.UserData.BottomSeries{length(figure.UserData.BottomSeries) + 1} = series;
end

function enmask(figure)
  topMaskSeries = figure.UserData.TopMaskSeries;
  bottomMaskSeries = figure.UserData.BottomMaskSeries;
  
  figure.UserData.TopMaskSeries = {};
  figure.UserData.BottomMaskSeries = {};
  
  axes(figure.UserData.TopPlot);
  
  for ii = 1 : length(figure.UserData.TopSeries)
    series = figure.UserData.TopSeries{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = false(1, length(series.XData));
    flag = false;
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = true;
          flag = true;
          break;
        end
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      mdata = semilogx(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      mdata.UserData.SeriesType  = 'mdata';
      mdata.UserData.SeriesName  = series.UserData.SeriesName;
      mdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      mdata.UserData.SeriesError = [];
      
      if ~isempty(series.UserData.SeriesError)
        for jj = 1 : length(filter)
          if filter(jj)
            errorSeries = series.UserData.SeriesError(jj);
            
            XData     = errorSeries.XData;
            YData     = errorSeries.YData;
            LineStyle = errorSeries.LineStyle;
            Marker    = errorSeries.Marker;
            
            merror = semilogx(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
            
            merror.UserData.SeriesType = 'merror';
            merror.UserData.SeriesName = errorSeries.UserData.SeriesName;
            merror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
            merror.UserData.SeriesData = mdata;
            merror.UserData.XData      = errorSeries.UserData.XData;
            merror.UserData.YData      = errorSeries.UserData.YData;
            
            set(merror, 'ButtonDownFcn', @seriesmousedown);
            
            mdata.UserData.SeriesError(length(mdata.UserData.SeriesError) + 1) = merror;
          end
        end
      end
      
      set(mdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.TopMaskSeries{length(figure.UserData.TopMaskSeries) + 1} = mdata;
    end
  end
  
  axes(figure.UserData.BottomPlot);
  
  for ii = 1 : length(figure.UserData.BottomSeries)
    series = figure.UserData.BottomSeries{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = false(1, length(series.XData));
    flag = false;
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = true;
          flag = true;
          break;
        end
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      mdata = semilogx(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      mdata.UserData.SeriesType  = 'mdata';
      mdata.UserData.SeriesName  = series.UserData.SeriesName;
      mdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      mdata.UserData.SeriesError = [];
      
      if ~isempty(series.UserData.SeriesError)
        for jj = 1 : length(filter)
          if filter(jj)
            errorSeries = series.UserData.SeriesError(jj);
            
            XData     = errorSeries.XData;
            YData     = errorSeries.YData;
            LineStyle = errorSeries.LineStyle;
            Marker    = errorSeries.Marker;
            
            merror = semilogx(XData, YData, 'Color', [0.75 0.75 0.75], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
            
            merror.UserData.SeriesType = 'merror';
            merror.UserData.SeriesName = errorSeries.UserData.SeriesName;
            merror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
            merror.UserData.SeriesData = mdata;
            merror.UserData.XData      = errorSeries.UserData.XData;
            merror.UserData.YData      = errorSeries.UserData.YData;
            
            set(merror, 'ButtonDownFcn', @seriesmousedown);
            
            mdata.UserData.SeriesError(length(mdata.UserData.SeriesError) + 1) = merror;
          end
        end
      end
      
      set(mdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.BottomMaskSeries{length(figure.UserData.BottomMaskSeries) + 1} = mdata;
    end
  end
  
  for ii = 1 : length(topMaskSeries)
    for jj = 1 : length(topMaskSeries{ii}.UserData.SeriesError)
      delete(topMaskSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(topMaskSeries{ii});
  end
  
  for ii = 1 : length(bottomMaskSeries)
    for jj = 1 : length(bottomMaskSeries{ii}.UserData.SeriesError)
      delete(bottomMaskSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(bottomMaskSeries{ii});
  end
end

function repaint(figure)
  topRepaintSeries = figure.UserData.TopRepaintSeries;
  bottomRepaintSeries = figure.UserData.BottomRepaintSeries;
  
  figure.UserData.TopRepaintSeries = {};
  figure.UserData.BottomRepaintSeries = {};
  
  axes(figure.UserData.TopPlot);
  
  for ii = 1 : length(figure.UserData.TopSeries)
    series = figure.UserData.TopSeries{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = true(1, length(series.XData));
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = false;
          break;
        end
      end
    end
    
    flag = false;
    
    for jj = 1 : length(filter)
      if filter(jj)
        flag = true;
        break;
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      Color     = series.Color;
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      rdata = semilogx(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      rdata.UserData.SeriesType  = 'rdata';
      rdata.UserData.SeriesName  = series.UserData.SeriesName;
      rdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      rdata.UserData.SeriesError = [];
      
      if ~isempty(series.UserData.SeriesError)
        for jj = 1 : length(filter)
          if filter(jj)
            errorSeries = series.UserData.SeriesError(jj);
            
            XData     = errorSeries.XData;
            YData     = errorSeries.YData;
            Color     = errorSeries.Color;
            LineStyle = errorSeries.LineStyle;
            Marker    = errorSeries.Marker;

            rerror = semilogx(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
            
            rerror.UserData.SeriesType = 'rerror';
            rerror.UserData.SeriesName = errorSeries.UserData.SeriesName;
            rerror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
            rerror.UserData.SeriesData = rdata;
            rerror.UserData.XData      = errorSeries.UserData.XData;
            rerror.UserData.YData      = errorSeries.UserData.YData;
            
            set(rerror, 'ButtonDownFcn', @seriesmousedown);
            
            rdata.UserData.SeriesError(length(rdata.UserData.SeriesError) + 1) = rerror;
          end
        end
      end
      
      set(rdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.TopRepaintSeries{length(figure.UserData.TopRepaintSeries) + 1} = rdata;
    end
  end
  
  axes(figure.UserData.BottomPlot);
  
  for ii = 1 : length(figure.UserData.BottomSeries)
    series = figure.UserData.BottomSeries{ii};
    
    mask = pmask('-path', series.UserData.SeriesFile.folder);
    
    filter = true(1, length(series.XData));
    
    for jj = 1 : length(series.XData)
      for kk = 1 : length(mask)
        if jj == mask(kk)
          filter(jj) = false;
          break;
        end
      end
    end
    
    flag = false;
    
    for jj = 1 : length(filter)
      if filter(jj)
        flag = true;
        break;
      end
    end
    
    if(flag)
      XData     = series.XData(filter);
      YData     = series.YData(filter);
      Color     = series.Color;
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      rdata = semilogx(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      rdata.UserData.SeriesType  = 'rdata';
      rdata.UserData.SeriesName  = series.UserData.SeriesName;
      rdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      rdata.UserData.SeriesError = [];
      
      if ~isempty(series.UserData.SeriesError)
        for jj = 1 : length(filter)
          if filter(jj)
            errorSeries = series.UserData.SeriesError(jj);
            
            XData     = errorSeries.XData;
            YData     = errorSeries.YData;
            Color     = errorSeries.Color;
            LineStyle = errorSeries.LineStyle;
            Marker    = errorSeries.Marker;
            
            rerror = semilogx(XData, YData, 'Color', Color, 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
            
            rerror.UserData.SeriesType = 'rerror';
            rerror.UserData.SeriesName = errorSeries.UserData.SeriesName;
            rerror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
            rerror.UserData.SeriesData = rdata;
            rerror.UserData.XData      = errorSeries.UserData.XData;
            rerror.UserData.YData      = errorSeries.UserData.YData;
            
            set(rerror, 'ButtonDownFcn', @seriesmousedown);
            
            rdata.UserData.SeriesError(length(rdata.UserData.SeriesError) + 1) = rerror;
          end
        end
      end
      
      set(rdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.BottomRepaintSeries{length(figure.UserData.BottomRepaintSeries) + 1} = rdata;
    end
  end
  
  for ii = 1 : length(topRepaintSeries)
    for jj = 1 : length(topRepaintSeries{ii}.UserData.SeriesError)
      delete(topRepaintSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(topRepaintSeries{ii});
  end
  
  for ii = 1 : length(bottomRepaintSeries)
    for jj = 1 : length(bottomRepaintSeries{ii}.UserData.SeriesError)
      delete(bottomRepaintSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(bottomRepaintSeries{ii});
  end
end

function topSelect(figure, x1, y1, x2, y2)
  topSelectionSeries = figure.UserData.TopSelectionSeries;
  bottomSelectionSeries = figure.UserData.BottomSelectionSeries;
  
  figure.UserData.TopSelectionSeries = {};
  figure.UserData.BottomSelectionSeries = {};
  
  axes(figure.UserData.TopPlot);
  
  seriesFilter = false(1, length(figure.UserData.TopSeries));
  dataFilter = cell(1, length(figure.UserData.TopSeries));
  dataIndex = cell(1, length(figure.UserData.TopSeries));
  
  for ii = 1 : length(figure.UserData.TopSeries)
    series = figure.UserData.TopSeries{ii};
    
    dataFilter{ii} = false(1, length(series.XData));
    dataIndex{ii} = zeros(0);
    
    for jj = 1 : length(series.XData)
      if series.XData(jj) >= x1 && series.XData(jj) <= x2 && series.YData(jj) >= y1 && series.YData(jj) <= y2
        seriesFilter(ii) = true;
        dataFilter{ii}(jj) = true;
        dataIndex{ii}(length(dataIndex{ii}) + 1) = jj;
      end
    end
    
    if seriesFilter(ii)
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesName  = series.UserData.SeriesName;
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesName = errorSeries.UserData.SeriesName;
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.TopSelectionSeries{length(figure.UserData.TopSelectionSeries) + 1} = sdata;
    end
  end
  
  axes(figure.UserData.BottomPlot);
  
  for ii = 1 : length(figure.UserData.BottomSeries)
    if seriesFilter(ii)
      series = figure.UserData.BottomSeries{ii};
      
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesName  = series.UserData.SeriesName;
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesName = errorSeries.UserData.SeriesName;
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.BottomSelectionSeries{length(figure.UserData.BottomSelectionSeries) + 1} = sdata;
    end
  end
  
  for ii = 1 : length(topSelectionSeries)
    for jj = 1 : length(topSelectionSeries{ii}.UserData.SeriesError)
      delete(topSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(topSelectionSeries{ii});
  end
  
  for ii = 1 : length(bottomSelectionSeries)
    for jj = 1 : length(bottomSelectionSeries{ii}.UserData.SeriesError)
      delete(bottomSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(bottomSelectionSeries{ii});
  end
end

function bottomSelect(figure, x1, y1, x2, y2)
  topSelectionSeries = figure.UserData.TopSelectionSeries;
  bottomSelectionSeries = figure.UserData.BottomSelectionSeries;
  
  figure.UserData.TopSelectionSeries = {};
  figure.UserData.BottomSelectionSeries = {};
  
  axes(figure.UserData.BottomPlot);
  
  seriesFilter = false(1, length(figure.UserData.BottomSeries));
  dataFilter = cell(1, length(figure.UserData.BottomSeries));
  dataIndex = cell(1, length(figure.UserData.BottomSeries));
  
  for ii = 1 : length(figure.UserData.BottomSeries)
    series = figure.UserData.BottomSeries{ii};
    
    dataFilter{ii} = false(1, length(series.XData));
    dataIndex{ii} = zeros(0);
    
    for jj = 1 : length(series.XData)
      if series.XData(jj) >= x1 && series.XData(jj) <= x2 && series.YData(jj) >= y1 && series.YData(jj) <= y2
        seriesFilter(ii) = true;
        dataFilter{ii}(jj) = true;
        dataIndex{ii}(length(dataIndex{ii}) + 1) = jj;
      end
    end
    
    if seriesFilter(ii)
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesName  = series.UserData.SeriesName;
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesName = errorSeries.UserData.SeriesName;
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.BottomSelectionSeries{length(figure.UserData.BottomSelectionSeries) + 1} = sdata;
    end
  end
  
  axes(figure.UserData.TopPlot);
  
  for ii = 1 : length(figure.UserData.TopSeries)
    if seriesFilter(ii)
      series = figure.UserData.TopSeries{ii};
      
      XData     = series.XData(dataFilter{ii});
      YData     = series.YData(dataFilter{ii});
      LineStyle = series.LineStyle;
      Marker    = series.Marker;
      
      sdata = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
      
      sdata.UserData.SeriesType  = 'sdata';
      sdata.UserData.SeriesName  = series.UserData.SeriesName;
      sdata.UserData.SeriesFile  = series.UserData.SeriesFile;
      sdata.UserData.SeriesError = [];
      sdata.UserData.SeriesIndex = dataIndex{ii};
      
      for jj = 1 : length(series.UserData.SeriesError)
        if dataFilter{ii}(jj)
          errorSeries = series.UserData.SeriesError(jj);
          
          XData     = errorSeries.XData;
          YData     = errorSeries.YData;
          LineStyle = errorSeries.LineStyle;
          Marker    = errorSeries.Marker;
          
          serror = semilogx(XData, YData, 'Color', [0.8500 0.3250 0.0980], 'LineStyle', LineStyle, 'Marker', Marker, 'HandleVisibility', 'off');
          
          serror.UserData.SeriesType = 'serror';
          serror.UserData.SeriesName = errorSeries.UserData.SeriesName;
          serror.UserData.SeriesFile = errorSeries.UserData.SeriesFile;
          serror.UserData.SeriesData = sdata;
          serror.UserData.XData      = errorSeries.UserData.XData;
          serror.UserData.YData      = errorSeries.UserData.YData;
          
          set(serror, 'ButtonDownFcn', @seriesmousedown);
          
          sdata.UserData.SeriesError(length(sdata.UserData.SeriesError) + 1) = serror;
        end
      end
      
      set(sdata, 'ButtonDownFcn', @seriesmousedown);
      
      figure.UserData.TopSelectionSeries{length(figure.UserData.TopSelectionSeries) + 1} = sdata;
    end
  end
  
  for ii = 1 : length(topSelectionSeries)
    for jj = 1 : length(topSelectionSeries{ii}.UserData.SeriesError)
      delete(topSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(topSelectionSeries{ii});
  end
  
  for ii = 1 : length(bottomSelectionSeries)
    for jj = 1 : length(bottomSelectionSeries{ii}.UserData.SeriesError)
      delete(bottomSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(bottomSelectionSeries{ii});
  end
end

function deselect(figure)
  for ii = 1 : length(figure.UserData.TopSelectionSeries)
    for jj = 1 : length(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesError)
      delete(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(figure.UserData.TopSelectionSeries{ii});
  end
  
  for ii = 1 : length(figure.UserData.BottomSelectionSeries)
    for jj = 1 : length(figure.UserData.BottomSelectionSeries{ii}.UserData.SeriesError)
      delete(figure.UserData.BottomSelectionSeries{ii}.UserData.SeriesError(jj));
    end
    
    delete(figure.UserData.BottomSelectionSeries{ii});
  end
  
  figure.UserData.TopSelectionSeries = {};
  figure.UserData.BottomSelectionSeries = {};
end

function keypress(figure, event)
  shift = false;
  
  for ii = 1 : length(event.Modifier)
    if strcmp(event.Modifier{ii}, 'shift')
      shift = true;
    end
  end
  
  if strcmp(event.Key, 'r') && ~figure.UserData.freezed
    enmask(figure);
    repaint(figure);
    deselect(figure);
  elseif strcmp(event.Key, 'd')
    figure.UserData.freezed = true;
    flag = false;
    
    for ii = 1 : length(figure.UserData.TopSelectionSeries)
      for jj = 1 : length(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesIndex)
        [changed, ~] = pmaskadd(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.TopSelectionSeries{ii}.UserData.SeriesIndex(jj));
        
        if changed
          flag = true;
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'u')
    figure.UserData.freezed = true;
    flag = false;
    
    for ii = 1 : length(figure.UserData.TopSelectionSeries)
      for jj = 1 : length(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesIndex)
        [changed, ~] = pmaskremove(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.TopSelectionSeries{ii}.UserData.SeriesIndex(jj));
        
        if changed
          flag = true;
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'delete') || strcmp(event.Key, 'backspace')
    figure.UserData.freezed = true;
    flag = false;
    
    if shift
      for ii = 1 : length(figure.UserData.TopSelectionSeries)
        for jj = 1 : length(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesIndex)
          [changed, ~] = pmaskremove(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.TopSelectionSeries{ii}.UserData.SeriesIndex(jj));
          
          if changed
            flag = true;
          end
        end
      end
    else
      for ii = 1 : length(figure.UserData.TopSelectionSeries)
        for jj = 1 : length(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesIndex)
          [changed, ~] = pmaskadd(figure.UserData.TopSelectionSeries{ii}.UserData.SeriesFile.folder, figure.UserData.TopSelectionSeries{ii}.UserData.SeriesIndex(jj));
          
          if changed
            flag = true;
          end
        end
      end
    end
    
    if flag
      enmask(figure);
      repaint(figure);
    end
    
    deselect(figure);
    figure.UserData.freezed = false;
  elseif strcmp(event.Key, 'escape')
    deselect(figure);
  end
end

function seriesmousedown(series, ~)
  mousedown(series.Parent, series);
end

function axesmousedown(axes, ~)
  mousedown(axes, []);
end

function mousedown(axes, ~)
  figure = axes.Parent;
  
  if strcmp(figure.SelectionType, 'normal')
    currentPoint = axes.CurrentPoint;
    
    x1 = currentPoint(1, 1);
    y1 = currentPoint(1, 2);
    
    if x1 >= axes.XLim(1) && x1 <= axes.XLim(2)
      if y1 >= axes.YLim(1) && y1 <= axes.YLim(2)
        top = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        left = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        right = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        bottom = line('XData', x1, 'YData', y1, 'Color', 'k', 'LineStyle', '--', 'HandleVisibility', 'off');
        
        figure.Pointer = 'crosshair';
        figure.WindowButtonMotionFcn = @mousemotion;
        figure.WindowButtonUpFcn = @mouseup;
      end
    end
  end
  
  function mousemotion(~, ~)
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = currentPoint(1, 2);
    
    if x2 < axes.XLim(1)
      x2 = axes.XLim(1);
    elseif x2 > axes.XLim(2)
      x2 = axes.XLim(2);
    end
    
    if y2 < axes.YLim(1)
      y2 = axes.YLim(1);
    elseif y2 > axes.YLim(2)
      y2 = axes.YLim(2);
    end
    
    top.XData = [x1, x2];
    top.YData = [y1, y1];
    
    left.XData = [x1, x1];
    left.YData = [y1, y2];
    
    right.XData = [x2, x2];
    right.YData = [y1, y2];
    
    bottom.XData = [x1, x2];
    bottom.YData = [y2, y2];
    
    drawnow();
  end
  
  function mouseup(~, ~)
    figure.Pointer = 'arrow';
    figure.WindowButtonMotionFcn = [];
    figure.WindowButtonUpFcn = [];
    
    delete(top);
    delete(left);
    delete(right);
    delete(bottom);
    
    currentPoint = axes.CurrentPoint;
    
    x2 = currentPoint(1, 1);
    y2 = currentPoint(1, 2);
    
    if x1 ~= x2 && y1 ~= y2
      if x1 > x2
        x  = x1;
        x1 = x2;
        x2 = x;
      end
      
      if y1 > y2
        y  = y1;
        y1 = y2;
        y2 = y;
      end
      
      if x1 < axes.XLim(1)
        x1 = axes.XLim(1);
      end
      
      if x2 > axes.XLim(2)
        x2 = axes.XLim(2);
      end
      
      if y1 < axes.YLim(1)
        y1 = axes.YLim(1);
      end
      
      if y2 > axes.YLim(2)
        y2 = axes.YLim(2);
      end
      
      if strcmp(axes.UserData.AxesType, 'top')
        topSelect(figure, x1, y1, x2, y2);
      elseif strcmp(axes.UserData.AxesType, 'bottom')
        bottomSelect(figure, x1, y1, x2, y2);
      end
    else
      deselect(figure);
    end
  end
end