function atsextract(filename, varargin)
  file = atsfile(filename);
  
  if ~isempty(file)
    startTime  = [];
    stopTime   = [];
    outputPath = [];
    
    if nargin > 1
      if nargin == 2
        if ischar(varargin{1})
          outputPath = varargin{1};
        end
      elseif nargin == 3
        if isnumeric(varargin{1}) && isnumeric(varargin{2})
          startTime = varargin{1};
          stopTime  = varargin{2};
        end
      elseif nargin == 4
        if isnumeric(varargin{1}) && isnumeric(varargin{2}) && ischar(varargin{3})
          startTime  = varargin{1};
          stopTime   = varargin{2};
          outputPath = varargin{3};
        end
      end
    end
    
    if isempty(startTime)
      startTime = file.startTime;
    end
    
    if isempty(stopTime)
      stopTime = file.stopTime;
    end
    
    if isempty(outputPath)
      outputPath = pwd();
    end
    
    [data, startTime, ~] = atsdata(sprintf('%s%s%s', file.folder, filesep, file.name), false, startTime, stopTime);
    
    if ~isempty(data)
      data = data / file.header.mvFactor;
      
      if file.aduSerialNumber < 10
        aduSerialNumber = sprintf('00%d', file.aduSerialNumber);
      elseif file.aduSerialNumber < 100
        aduSerialNumber = sprintf('0%d', file.aduSerialNumber);
      else
        aduSerialNumber = sprintf('%d', file.aduSerialNumber);
      end
      
      if file.xmlVersion < 10
        xmlVersion = sprintf('0%d', file.xmlVersion);
      else
        xmlVersion = sprintf('%d', file.xmlVersion);
      end
      
      if file.channelNumber < 10
        channelNumber = sprintf('0%d', file.channelNumber);
      else
        channelNumber = sprintf('%d', file.channelNumber);
      end
      
      if file.runNumber < 10
        runNumber = sprintf('00%d', file.runNumber);
      elseif file.runNumber < 100
        runNumber = sprintf('0%d', file.runNumber);
      else
        runNumber = sprintf('%d', file.runNumber);
      end
      
      if strcmpi(file.boardType, 'LF')
        boardType = 'L';
      elseif strcmpi(file.boardType, 'HF')
        boardType = 'H';
      else
        boardType = 'M';
      end
      
      fname = sprintf('%s%s%s_V%s_C%s_R%s_T%s_B%s_%d%s.ats', outputPath, filesep, aduSerialNumber, xmlVersion, channelNumber, runNumber, file.channelType, boardType, file.samplingRate, file.measurementUnit);
      
      k = 0;
      
      while true
        fp = fopen(fname, 'r');
        
        if fp == -1
          break;
        end
        
        fclose(fp);
        
        k = k + 1;
        
        if k > 1
          fname = sprintf('%s%s%s_V%s_C%s_R%s_T%s_B%s_%d%s_extract_%d.ats', outputPath, filesep, aduSerialNumber, xmlVersion, channelNumber, runNumber, file.channelType, boardType, file.samplingRate, file.measurementUnit, k);
        else
          fname = sprintf('%s%s%s_V%s_C%s_R%s_T%s_B%s_%d%s_extract.ats', outputPath, filesep, aduSerialNumber, xmlVersion, channelNumber, runNumber, file.channelType, boardType, file.samplingRate, file.measurementUnit);
        end
      end
      
      fp = fopen(fname, 'w');
       
      if fp ~= -1
        channelType       = file.header.channelType;
        sensorType        = file.header.sensorType;
        latLongType       = file.header.latLongType;
        gpsStat           = file.header.gpsStat;
        systemType        = file.header.systemType;
        surveyHeaderName  = file.header.surveyHeaderName;
        measurementType   = file.header.measurementType;
        selfTestResult    = file.header.selfTestResult;
        reserved5         = file.header.reserved5;
        lfFilters         = file.header.lfFilters;
        aduCalFilename    = file.header.aduCalFilename;
        sensorCalFilename = file.header.sensorCalFilename;
        hfFilters         = file.header.hfFilters;
        boardType         = file.header.boardType;
        client            = file.header.client;
        contractor        = file.header.contractor;
        area              = file.header.area;
        surveyID          = file.header.surveyID;
        operator          = file.header.operator;
        reserved          = file.header.reserved;
        xmlHeader         = file.header.xmlHeader;
        comments          = sprintf('%s\nweather: %s\n', file.header.comments, file.header.weather);
        
        while length(channelType) < 2
          channelType = sprintf('%s\0', channelType);
        end
        
        while length(sensorType) < 6
          sensorType = sprintf('%s\0', sensorType);
        end
        
        while length(latLongType) < 1
          latLongType = sprintf('%s\0', latLongType);
        end
        
        while length(gpsStat) < 1
          gpsStat = sprintf('%s\0', gpsStat);
        end
        
        while length(systemType) < 12
          systemType = sprintf('%s\0', systemType);
        end
        
        while length(surveyHeaderName) < 12
          surveyHeaderName = sprintf('%s\0', surveyHeaderName);
        end
        
        while length(measurementType) < 4
          measurementType = sprintf('%s\0', measurementType);
        end
        
        while length(selfTestResult) < 2
          selfTestResult = sprintf('%s\0', selfTestResult);
        end
        
        while length(reserved5) < 2
          reserved5 = sprintf('%s\0', reserved5);
        end
        
        while length(lfFilters) < 8
          lfFilters = sprintf('%s\0', lfFilters);
        end
        
        while length(aduCalFilename) < 12
          aduCalFilename = sprintf('%s\0', aduCalFilename);
        end
        
        while length(sensorCalFilename) < 12
          sensorCalFilename = sprintf('%s\0', sensorCalFilename);
        end
        
        while length(hfFilters) < 8
          hfFilters = sprintf('%s\0', hfFilters);
        end
        
        while length(boardType) < 4
          boardType = sprintf('%s\0', boardType);
        end
        
        while length(client) < 16
          client = sprintf('%s\0', client);
        end
        
        while length(contractor) < 16
          contractor = sprintf('%s\0', contractor);
        end
        
        while length(area) < 16
          area = sprintf('%s\0', area);
        end
        
        while length(surveyID) < 16
         surveyID  = sprintf('%s\0', surveyID);
        end
        
        while length(operator) < 16
          operator = sprintf('%s\0', operator);
        end
        
        while length(reserved) < 112
          reserved = sprintf('%s\0', reserved);
        end
        
        while length(xmlHeader) < 64
          xmlHeader = sprintf('%s\0', xmlHeader);
        end
        
        while length(comments) < 512
          comments = sprintf('%s\0', comments);
        end
        
        numberOfSamples   = length(data);
        samplingRate = file.samplingRate;
        
        fwrite(fp, file.header.length,             'int16');
        fwrite(fp, file.header.version,            'int16');
        fwrite(fp, numberOfSamples,                'int32');
        fwrite(fp, samplingRate,                   'float32');
        fwrite(fp, startTime,                      'int32');
        fwrite(fp, file.header.mvFactor,           'float64');
        fwrite(fp, file.header.gmtOffset,          'int32');
        fwrite(fp, file.header.originalFrequency,  'float32');
        fwrite(fp, file.header.aduSerialNumber,    'int16');
        fwrite(fp, file.header.adcSerialNumber,    'int16');
        fwrite(fp, file.header.channelNumber,      'int8');
        fwrite(fp, file.header.chopper,            'int8');
        fwrite(fp, channelType,                    'int8');
        fwrite(fp, sensorType,                     'int8');
        fwrite(fp, file.header.sensorSerialNumber, 'int16');
        fwrite(fp, file.header.posX1,              'float32');
        fwrite(fp, file.header.posY1,              'float32');
        fwrite(fp, file.header.posZ1,              'float32');
        fwrite(fp, file.header.posX2,              'float32');
        fwrite(fp, file.header.posY2,              'float32');
        fwrite(fp, file.header.posZ2,              'float32');
        fwrite(fp, file.header.dipoleLength,       'float32');
        fwrite(fp, file.header.angle,              'float32');
        fwrite(fp, file.header.probeRes,           'float32');
        fwrite(fp, file.header.dcOffset,           'float32');
        fwrite(fp, file.header.gainStage1,         'float32');
        fwrite(fp, file.header.gainStage2,         'float32');
        fwrite(fp, file.header.latitude,           'int32');
        fwrite(fp, file.header.longitude,          'int32');
        fwrite(fp, file.header.elevation,          'int32');
        fwrite(fp, latLongType,                    'int8');
        fwrite(fp, file.header.addCoordType,       'int8');
        fwrite(fp, file.header.gaussRefMeridian,   'int16');
        fwrite(fp, file.header.hochwert,           'float64');
        fwrite(fp, file.header.rechtswert,         'float64');
        fwrite(fp, gpsStat,                        'int8');
        fwrite(fp, file.header.gpsAccuracy,        'int8');
        fwrite(fp, file.header.utcOffset,          'int16');
        fwrite(fp, systemType,                     'int8');
        fwrite(fp, surveyHeaderName,               'int8');
        fwrite(fp, measurementType,                'int8');
        fwrite(fp, file.header.dcOffsetCorrValue,  'float64');
        fwrite(fp, file.header.dcOffsetCorrOn,     'int8');
        fwrite(fp, file.header.inputDivOn,         'int8');
        fwrite(fp, file.header.notUsedVar,         'int16');
        fwrite(fp, selfTestResult,                 'int8');
        fwrite(fp, reserved5,                      'int8');
        fwrite(fp, file.header.calFreqs,           'int16');
        fwrite(fp, file.header.calEntryLength,     'int16');
        fwrite(fp, file.header.calVersion,         'int16');
        fwrite(fp, file.header.calStartAddress,    'int16');
        fwrite(fp, lfFilters,                      'int8');
        fwrite(fp, aduCalFilename,                 'int8');
        fwrite(fp, file.header.aduCalTime,         'int32');
        fwrite(fp, sensorCalFilename,              'int8');
        fwrite(fp, file.header.sensorCalTime,      'int32');
        fwrite(fp, file.header.powerlineFreq1,     'float32');
        fwrite(fp, file.header.powerlineFreq2,     'float32');
        fwrite(fp, hfFilters,                      'int8');
        fwrite(fp, file.header.originalMvFactor,   'float64');
        fwrite(fp, file.header.unsedVar,           'int32');
        fwrite(fp, boardType,                      'int8');
        fwrite(fp, client,                         'int8');
        fwrite(fp, contractor,                     'int8');
        fwrite(fp, area,                           'int8');
        fwrite(fp, surveyID,                       'int8');
        fwrite(fp, operator,                       'int8');
        fwrite(fp, reserved,                       'int8');
        fwrite(fp, xmlHeader,                      'int8');
        fwrite(fp, comments,                       'int8');
        
        fwrite(fp, data, 'int32');
        
        fclose(fp);
      end
    end
  end
end