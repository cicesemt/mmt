function bandinfo(varargin)
  currentBand = [];
  
  flag.all = false;
  flag.specific = false;
  flag.name = false;
  flag.folder = false;
  flag.bytes = false;
  flag.samplingRate = false;
  flag.measurementUnit = false;
  flag.numberOfSamples = false;
  flag.frequency = false;
  flag.latitude = false;
  flag.longitude = false;
  flag.elevation = false;
  flag.startTime = false;
  flag.stopTime = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentBand = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-name')
        varargin(:, k) = [];
        flag.specific = true;
        flag.name = true;
      elseif strcmpi(varargin{k}, '-folder')
        varargin(:, k) = [];
        flag.specific = true;
        flag.folder = true;
      elseif strcmpi(varargin{k}, '-bytes')
        varargin(:, k) = [];
        flag.specific = true;
        flag.bytes = true;
      elseif strcmpi(varargin{k}, '-samplingRate')
        varargin(:, k) = [];
        flag.specific = true;
        flag.samplingRate = true;
      elseif strcmpi(varargin{k}, '-measurementUnit')
        varargin(:, k) = [];
        flag.specific = true;
        flag.measurementUnit = true;
      elseif strcmpi(varargin{k}, '-numberOfSamples')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfSamples = true;
      elseif strcmpi(varargin{k}, '-frequency')
        varargin(:, k) = [];
        flag.specific = true;
        flag.frequency = true;
      elseif strcmpi(varargin{k}, '-latitude')
        varargin(:, k) = [];
        flag.specific = true;
        flag.latitude = true;
      elseif strcmpi(varargin{k}, '-longitude')
        varargin(:, k) = [];
        flag.specific = true;
        flag.longitude = true;
      elseif strcmpi(varargin{k}, '-elevation')
        varargin(:, k) = [];
        flag.specific = true;
        flag.elevation = true;
      elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.startTime = true;
      elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.stopTime = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.name = true;
    flag.folder = true;
    flag.bytes = true;
    flag.samplingRate = true;
    flag.measurementUnit = true;
    flag.numberOfSamples = true;
    flag.frequency = true;
    flag.latitude = true;
    flag.longitude = true;
    flag.elevation = true;
    flag.startTime = true;
    flag.stopTime = true;
  elseif ~flag.specific
    flag.name = true;
    flag.numberOfSamples = true;
    flag.latitude = true;
    flag.longitude = true;
    flag.elevation = true;
    flag.startTime = true;
    flag.stopTime = true;
  end
  
  if isempty(currentBand)
    currentBand = band(varargin{:});
  end
  
  if ~isempty(currentBand)
    printInfo(currentBand, flag);
  end
end

function printInfo(currentBand, flag)
  names = cell(0);
  values = cell(0);
  raw = cell(0);
  
  if flag.name
    names{length(names) + 1} = 'name:';
    values{length(values) + 1} = currentBand.name;
    raw{length(raw) + 1} = '';
  end
  
  if flag.folder
    names{length(names) + 1} = 'folder:';
    values{length(values) + 1} = currentBand.folder;
    raw{length(raw) + 1} = '';
  end
  
  if flag.bytes
    names{length(names) + 1} = 'bytes:';
    
    if currentBand.bytes < 1000
      values{length(values) + 1} = sprintf('%d', currentBand.bytes);
      raw{length(raw) + 1} = '';
    else
      values{length(values) + 1} = thousandsep(currentBand.bytes, 0);
      raw{length(raw) + 1} = sprintf('%d', currentBand.bytes);
    end
  end
  
  if flag.samplingRate
    names{length(names) + 1} = 'sampling rate:';
    
    if currentBand.samplingRate < 1000
      values{length(values) + 1} = sprintf('%d', currentBand.samplingRate);
      raw{length(raw) + 1} = '';
    else
      values{length(values) + 1} = thousandsep(currentBand.samplingRate, 0);
      raw{length(raw) + 1} = sprintf('%d', currentBand.samplingRate);
    end
  end
  
  if flag.measurementUnit
    names{length(names) + 1} = 'measurement unit:';
    values{length(values) + 1} = currentBand.measurementUnit;
    raw{length(raw) + 1} = '';
  end
  
  if flag.numberOfSamples
    names{length(names) + 1} = 'number of samples:';
    
    if currentBand.numberOfSamples < 1000
      values{length(values) + 1} = sprintf('%d', currentBand.numberOfSamples);
      raw{length(raw) + 1} = '';
    else
      values{length(values) + 1} = thousandsep(currentBand.numberOfSamples, 0);
      raw{length(raw) + 1} = sprintf('%d', currentBand.numberOfSamples);
    end
  end
  
  if flag.frequency
    names{length(names) + 1} = 'frequency:';
    
    values{length(values) + 1} = sprintf('%f', currentBand.frequency);
    raw{length(raw) + 1} = '';
  end
  
  if flag.latitude
    names{length(names) + 1} = 'latitude:';
    values{length(values) + 1} = latitude2string(currentBand.latitude);
    raw{length(raw) + 1} = sprintf('%d', currentBand.latitude);
  end
  
  if flag.longitude
    names{length(names) + 1} = 'longitude:';
    values{length(values) + 1} = longitude2string(currentBand.longitude);
    raw{length(raw) + 1} = sprintf('%d', currentBand.longitude);
  end
  
  if flag.elevation
    names{length(names) + 1} = 'elevation:';
    values{length(values) + 1} = elevation2string(currentBand.elevation);
    raw{length(raw) + 1} = sprintf('%d', currentBand.elevation);
  end
  
  if flag.startTime
    names{length(names) + 1} = 'start time:';
    values{length(values) + 1} = datetime2string(posix2datetime(currentBand.startTime));
    raw{length(raw) + 1} = sprintf('%d', currentBand.startTime);
  end
  
  if flag.stopTime
    names{length(names) + 1} = 'stop time:';
    values{length(values) + 1} = datetime2string(posix2datetime(currentBand.stopTime));
    raw{length(raw) + 1} = sprintf('%d', currentBand.stopTime);
  end
  
  namesWidth = 0;
  valuesWidth = 0;
  rawWidth = 0;
  
  for ii = 1 : length(names)
    if namesWidth < length(names{ii})
      namesWidth = length(names{ii});
    end
    
    if ~isempty(raw{ii})
      if valuesWidth < length(values{ii})
        valuesWidth = length(values{ii});
      end
      
      if rawWidth < length(raw{ii})
        rawWidth = length(raw{ii});
      end
    end
  end
  
  for ii = 1 : length(names)
    if length(names{ii}) < namesWidth
      names{ii} = sprintf(sprintf('%%s%%%ds', namesWidth - length(names{ii})), names{ii}, ' ');
    end
    
    if ~isempty(raw{ii})
      if length(values{ii}) < valuesWidth
        values{ii} = sprintf(sprintf('%%s%%%ds', valuesWidth - length(values{ii})), values{ii}, ' ');
      end
      
      if length(raw{ii}) < rawWidth
        raw{ii} = sprintf(sprintf('%%%ds%%s', rawWidth - length(raw{ii})), ' ', raw{ii});
      end
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s ', names{ii});
    
    if ~isempty(values{ii})
      fprintf('%s', values{ii});
    end
    
    if ~isempty(raw{ii})
      fprintf(' [%s]', raw{ii});
    end
    
    fprintf('\n');
  end
end