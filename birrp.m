function birrp(varargin)
  compileCommand = false;
  
  if nargin > 0
    if strcmpi(varargin{1}, 'compile')
      varargin(:, 1) = [];
      compileCommand = true;
    end
  end
  
  if compileCommand
    if ~isempty(varargin)
      cprintf('err', 'too many arguments\n');
    else
      birrpcompile();
    end
  else
    files = meas();
    
    if ~isempty(files)
      removeTrend = false;
      ignoreMask = false;
      startTime = [];
      stopTime = [];
      
      k = 1;
      
      while k <= length(varargin)
        if strcmpi(varargin{k}, '-detrend')
          varargin(:, k) = [];
          removeTrend = true;
        elseif strcmpi(varargin{k}, '-nomask')
          varargin(:, k) = [];
          ignoreMask = true;
        elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            startTime = varargin{k};
            varargin(:, k) = [];
            
            if ischar(startTime)
              startTime = str2double(startTime);
            end
          end
        elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
          varargin(:, k) = [];
          
          if k <= length(varargin)
            stopTime = varargin{k};
            varargin(:, k) = [];
            
            if ischar(stopTime)
              stopTime = str2double(stopTime);
            end
          end
        else
          k = k + 1;
        end
      end
      
      if isempty(varargin)
        exportFiles = false;
        exportParams = false;
        
        for ii = 1 : length(files)
          fp = fopen(filnam(files(ii)), 'r');
          
          if fp ~= -1
            fclose(fp);
          else
            exportFiles = true;
            break;
          end
        end
        
        fp = fopen('parameters.birrp', 'r');
        
        if fp ~= -1
          fclose(fp);
        else
          exportParams = true;
        end
        
        if exportFiles
          measexport(removeTrend, ignoreMask, startTime, stopTime);
        end
        
        if exportParams
          paramsexport();
        end
        
        mmtpath = mfilename('fullpath');
        mmtpath = mmtpath(1 : length(mmtpath) - length(mfilename()));
        
        if length(mmtpath) > 1 && strcmp(mmtpath(length(mmtpath)), filesep)
          mmtpath = mmtpath(1 : length(mmtpath) - 1);
        end
        
        homepath = sprintf('%s%s%s%s%s', mmtpath, filesep, 'birrp');
        command = sprintf('%s%s%s', homepath, filesep, 'birrp');
        
        birrpclear(true);
        
        paramswriter(sprintf('%s.params', ofil()));
        
        [~, result] = system(command);
        
        disp(result);
        
        if exportFiles
          measclear();
        end
        
        if exportParams
          paramsclear();
        end
        
        birrpclear(false);
      end
    end
  end
end