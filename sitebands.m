function sitebands(varargin)
  currentSite = [];
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSite = varargin{k};
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSite)
    currentSite = site(varargin{:});
  end
  
  if ~isempty(currentSite)
    printBands(currentSite);
  end
end

function printBands(currentSite)
  names = cell(0);
  
  for ii = 1 : length(currentSite.bands)
    ignore = false;
    
    for jj = 1 : length(names)
      if strcmpi(names{jj}, currentSite.bands(ii).name)
        ignore = true;
        break;
      end
    end
    
    if ~ignore
      names{length(names) + 1} = currentSite.bands(ii).name;
    end
  end
  
  width = 0;
  
  for ii = 1 : length(names)
    if width < length(names{ii})
      width = length(names{ii});
    end
  end
  
  for ii = 1 : length(names)
    if length(names{ii}) < width
      names{ii} = sprintf(sprintf('%%%ds%%s', width - length(names{ii})), ' ', names{ii});
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s\n', names{ii});
  end
end