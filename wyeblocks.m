function wyeblocks(varargin)
  files = [];
  
  flag.all = false;
  flag.specific = false;
  flag.decimationExponent = false;
  flag.blockNumber = false;
  flag.settings = false;
  flag.xlength = false;
  flag.ylength = false;
  flag.numberOfSamples = false;
  flag.numberOfChannels = false;
  flag.filter = zeros(0);
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        files = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-decimationExponent')
        varargin(:, k) = [];
        flag.specific = true;
        flag.decimationExponent = true;
      elseif strcmpi(varargin{k}, '-blockNumber')
        varargin(:, k) = [];
        flag.specific = true;
        flag.blockNumber = true;
      elseif strcmpi(varargin{k}, '-settings')
        varargin(:, k) = [];
        flag.specific = true;
        flag.settings = true;
      elseif strcmpi(varargin{k}, '-xlength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.xlength = true;
      elseif strcmpi(varargin{k}, '-ylength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.ylength = true;
      elseif strcmpi(varargin{k}, '-numberOfSamples')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfSamples = true;
      elseif strcmpi(varargin{k}, '-numberOfChannels')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfChannels = true;
      elseif strcmpi(varargin{k}, '-block')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          blockNumber = str2double(varargin{k});
          varargin(:, k) = [];
          
          if ~isnan(blockNumber)
            flag.filter(length(flag.filter) + 1) = blockNumber;
          end
        end
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.decimationExponent = true;
    flag.blockNumber = true;
    flag.settings = true;
    flag.xlength = true;
    flag.ylength = true;
    flag.numberOfSamples = true;
    flag.numberOfChannels = true;
  elseif ~flag.specific
    flag.decimationExponent = true;
    flag.blockNumber = true;
    flag.settings = true;
    flag.xlength = true;
    flag.ylength = true;
    flag.numberOfSamples = true;
    flag.numberOfChannels = true;
  end
  
  if isempty(files)
    files = wye(varargin{:});
  end
  
  if ~isempty(files)
    for ii = 1 : length(files)
      printBlocks(files(ii), flag);
    end
  end
end

function printBlocks(file, flag)
  name = cell(0);
  value = cell(0);
  
  for ii = 1 : file.numberOfBlocks
    if ~isempty(flag.filter)
      ignore = true;
      
      for jj = 1 : length(flag.filter)
        if flag.filter(jj) == file.blocks(ii).header.blockNumber
          ignore = false;
          break;
        end
      end
      
      if ignore
        continue;
      end
    end
    
    if flag.decimationExponent
      name{length(name) + 1} = 'decimation exponent:';
      value{length(value) + 1} = sprintf('%d', file.blocks(ii).header.decimationExponent);
    end
    
    if flag.blockNumber
      name{length(name) + 1} = 'block number:';
      value{length(value) + 1} = sprintf('%d', file.blocks(ii).header.blockNumber);
    end
    
    if flag.settings
      name{length(name) + 1} = 'settings:';
      value{length(value) + 1} = sprintf('%d', file.blocks(ii).header.settings);
    end
    
    if true
      name{length(name) + 1} = 'filter type:';
      value{length(value) + 1} = filterType(file.blocks(ii).header.settings);
    end
    
    if true
      name{length(name) + 1} = 'electric gain:';
      value{length(value) + 1} = electricGain(file.blocks(ii).header.settings);
    end
    
    if true
      name{length(name) + 1} = 'magnetic gain:';
      value{length(value) + 1} = magneticGain(file.blocks(ii).header.settings);
    end
    
    if flag.xlength
      name{length(name) + 1} = 'xlength:';
      value{length(value) + 1} = sprintf('%d', file.blocks(ii).header.xlength);
    end
    
    if flag.ylength
      name{length(name) + 1} = 'ylength:';
      value{length(value) + 1} = sprintf('%d', file.blocks(ii).header.ylength);
    end
    
    if flag.numberOfSamples
      name{length(name) + 1} = 'number of samples:';
      value{length(value) + 1} = sprintf('%d', file.blocks(ii).header.numberOfSamples);
    end
    
    if flag.numberOfChannels
      name{length(name) + 1} = 'number of channels:';
      value{length(value) + 1} = sprintf('%d', file.blocks(ii).header.numberOfChannels);
    end
  end
  
  width = 0;
  
  for ii = 1 : length(name)
    if width < length(name{ii})
      width = length(name{ii});
    end
  end
  
  for ii = 1 : length(name)
    if length(name{ii}) < width
      name{ii} = sprintf(sprintf('%%s%%%ds', width - length(name{ii})), name{ii}, ' ');
    end
  end
  
  for ii = 1 : length(name)
    fprintf('%s %s\n', name{ii}, value{ii});
  end
end

function ft = filterType(settings)
  ft = 'unknown';
  
  a = 496; % XXX 0000 11111 0000
  b = bitand(settings, a);
  
  b1 = 352; % XXX 0000 10110 0000
  b4 = 192; % XXX 0000 01100 0000
  b7 = 128; % XXX 0000 01000 0000
  
  if b == b1
    ft = 'low-pass';
  elseif b == b4 
    ft = 'mid-pass';
  elseif b == b7 
    ft = 'high-pass';
  end
end

function eg = electricGain(settings)
  eg = 'unknown';
  
  a = 15; % XXX 0000 00000 1111
  b = bitand(settings, a);
  
  if b == 8
    eg = '-1';
  elseif b == 0
    eg = '1';
  elseif b == 1
    eg = '2';
  elseif b == 2
    eg = '4';
  elseif b == 3
    eg = '8';
  elseif b == 4
    eg = '10';
  elseif b == 5
    eg = '20';
  elseif b == 6
    eg = '40';
  elseif b == 7
    eg = '80';
  end
end

function mg = magneticGain(settings)
  mg = 'unknown';
  
  a = 7680; % XXX 1111 00000 0000
  b = bitand(settings, a);
  
  if b == 4096
    mg = '-1';
  elseif b == 0
    mg = '1';
  elseif b == 512
    mg = '2';
  elseif b == 1024
    mg = '4';
  elseif b == 1536
    mg = '8';
  elseif b == 2048
    mg = '10';
  elseif b == 2560
    mg = '20';
  elseif b == 3072
    mg = '40';
  elseif b == 3584
    mg = '80';
  end
end

% Filter settings
% XXX 0000 00000 0000
% 
% Filter type
% XXX 0000 10110 0000 = 160 hex = 352 --> LOW_PASS  (B1)
% XXX 0000 01100 0000 =  C0 hex = 192 --> MID_PASS  (B4)
% XXX 0000 01000 0000 =  80 hex = 128 --> HIGH_PASS (B7)
% 
% Electric gain
% XXX 0000 00000 1000 = 8 hex =  8 --> -1
% XXX 0000 00000 0000 = 0 hex =  0 -->  1
% XXX 0000 00000 0001 = 1 hex =  1 -->  2
% XXX 0000 00000 0010 = 2 hex =  2 -->  4
% XXX 0000 00000 0011 = 3 hex =  3 -->  8
% XXX 0000 00000 0100 = 4 hex =  4 --> 10
% XXX 0000 00000 0101 = 5 hex =  5 --> 20
% XXX 0000 00000 0110 = 6 hex =  6 --> 40
% XXX 0000 00000 0111 = 7 hex =  7 --> 80
% 
% Magnetic gain
% XXX 1000 00000 0000 = 1000 hex = 4096 --> -1
% XXX 0000 00000 0000 =    0 hex =    0 -->  1
% XXX 0001 00000 0000 =  200 hex =  512 -->  2
% XXX 0010 00000 0000 =  400 hex = 1024 -->  4
% XXX 0011 00000 0000 =  600 hex = 1536 -->  8
% XXX 0100 00000 0000 =  800 hex = 2048 --> 10
% XXX 0101 00000 0000 =  A00 hex = 2560 --> 20
% XXX 0110 00000 0000 =  C00 hex = 3072 --> 40
% XXX 0111 00000 0000 =  E00 hex = 3584 --> 80
