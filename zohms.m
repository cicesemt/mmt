function [zxx, zxy, zyx, zyy] = zohms(jfile)
  factor = 4 * pi * 10^-4;
  
  zxx.value  = factor * conj(jfile.zxx.value);
  zxx.period = jfile.zxx.period;
  zxx.error  = factor * jfile.zxx.error;
  
  zxy.value  = factor * conj(jfile.zxy.value);
  zxy.period = jfile.zxy.period;
  zxy.error  = factor * jfile.zxy.error;
  
  zyx.value  = factor * conj(jfile.zyx.value);
  zyx.period = jfile.zyx.period;
  zyx.error  = factor * jfile.zyx.error;
  
  zyy.value  = factor * conj(jfile.zyy.value);
  zyy.period = jfile.zyy.period;
  zyy.error  = factor * jfile.zyy.error;
end