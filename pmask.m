function [varargout] = pmask(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  addCommand    = false;
  removeCommand = false;
  clearCommand  = false;
  
  path = [];
  indexes = zeros(0);
  
  if nargin > 0
    if strcmpi(varargin{1}, 'add')
      varargin(:, 1) = [];
      addCommand = true;
    elseif strcmpi(varargin{1}, 'remove')
      varargin(:, 1) = [];
      removeCommand = true;
    elseif strcmpi(varargin{1}, 'clear')
      varargin(:, 1) = [];
      clearCommand = true;
    end
    
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-path')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          path = varargin{k};
          varargin(:, k) = [];
        end
      else
        k = k + 1;
      end
    end
    
    if addCommand || removeCommand
      while length(varargin) >= 1
        index = varargin{1};
        varargin(:, 1) = [];
        
        if ischar(index)
          index = str2double(index);
        end
        
        if ~isnan(index)
          indexes(length(indexes) + 1) = index;
        end
      end
    end
  end
  
  if isempty(path)
    path = pwd();
  elseif strcmp(path(length(path)), filesep)
    path = path(1 : length(path) - 1);
  end
  
  if isempty(varargin)
    if addCommand
      if ~isempty(indexes)
        for ii = 1 : length(indexes)
          [~, mask] = pmaskadd(path, indexes(ii));
        end
        
        if nargout > 0
          varargout{1} = mask;
        end
      end
    elseif removeCommand
      if ~isempty(indexes)
        for ii = 1 : length(indexes)
          [~, mask] = pmaskremove(path, indexes(ii));
        end
        
        if nargout > 0
          varargout{1} = mask;
        end
      end
    elseif clearCommand
      pmaskclear(path);
    elseif isempty(indexes)
      fp = fopen(sprintf('%s%s%s%s%s', path, filesep, '.mt', filesep, 'pmask'), 'rt');
      
      if fp ~= -1
        while ~feof(fp)
          l = fgetl(fp);
          
          if ~isnumeric(l)
            value = sscanf(l, '%d');
            
            if ~isempty(value)
              indexes(length(indexes) + 1) = value;
            end
          end
        end
        
        if ~isempty(indexes)
          if nargout > 0
            varargout{1} = indexes;
          else
            for ii = 1 : length(indexes)
              fprintf('%d\n', indexes(ii));
            end
          end
        end
        
        fclose(fp);
      end
    end
  end
end