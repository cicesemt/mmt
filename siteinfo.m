function siteinfo(varargin)
  currentSite = [];
  
  flag.all = false;
  flag.specific = false;
  flag.name = false;
  flag.folder = false;
  flag.bytes = false;
  flag.numberOfSamples = false;
  flag.latitude = false;
  flag.longitude = false;
  flag.elevation = false;
  flag.startTime = false;
  flag.stopTime = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSite = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-name')
        varargin(:, k) = [];
        flag.specific = true;
        flag.name = true;
      elseif strcmpi(varargin{k}, '-folder')
        varargin(:, k) = [];
        flag.specific = true;
        flag.folder = true;
      elseif strcmpi(varargin{k}, '-bytes')
        varargin(:, k) = [];
        flag.specific = true;
        flag.bytes = true;
      elseif strcmpi(varargin{k}, '-numberOfSamples')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfSamples = true;
      elseif strcmpi(varargin{k}, '-latitude')
        varargin(:, k) = [];
        flag.specific = true;
        flag.latitude = true;
      elseif strcmpi(varargin{k}, '-longitude')
        varargin(:, k) = [];
        flag.specific = true;
        flag.longitude = true;
      elseif strcmpi(varargin{k}, '-elevation')
        varargin(:, k) = [];
        flag.specific = true;
        flag.elevation = true;
      elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.startTime = true;
      elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.stopTime = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.name = true;
    flag.folder = true;
    flag.bytes = true;
    flag.numberOfSamples = true;
    flag.latitude = true;
    flag.longitude = true;
    flag.elevation = true;
    flag.startTime = true;
    flag.stopTime = true;
  elseif ~flag.specific
    flag.name = true;
    flag.numberOfSamples = true;
    flag.latitude = true;
    flag.longitude = true;
    flag.elevation = true;
    flag.startTime = true;
    flag.stopTime = true;
  end
  
  if isempty(currentSite)
    currentSite = site(varargin{:});
  end
  
  if ~isempty(currentSite)
    printInfo(currentSite, flag);
  end
end

function printInfo(currentSite, flag)
  names = cell(0);
  values = cell(0);
  raw = cell(0);
  
  if flag.name
    names{length(names) + 1} = 'name:';
    values{length(values) + 1} = currentSite.name;
    raw{length(raw) + 1} = '';
  end
  
  if flag.folder
    names{length(names) + 1} = 'folder:';
    values{length(values) + 1} = currentSite.folder;
    raw{length(raw) + 1} = '';
  end
  
  if flag.bytes
    names{length(names) + 1} = 'bytes:';
    
    if currentSite.bytes < 1000
      values{length(values) + 1} = sprintf('%d', currentSite.bytes);
      raw{length(raw) + 1} = '';
    else
      values{length(values) + 1} = thousandsep(currentSite.bytes, 0);
      raw{length(raw) + 1} = sprintf('%d', currentSite.bytes);
    end
  end
  
  if flag.numberOfSamples
    names{length(names) + 1} = 'number of samples:';
    
    if currentSite.numberOfSamples < 1000
      values{length(values) + 1} = sprintf('%d', currentSite.numberOfSamples);
      raw{length(raw) + 1} = '';
    else
      values{length(values) + 1} = thousandsep(currentSite.numberOfSamples, 0);
      raw{length(raw) + 1} = sprintf('%d', currentSite.numberOfSamples);
    end
  end
  
  if flag.latitude
    names{length(names) + 1} = 'latitude:';
    values{length(values) + 1} = latitude2string(currentSite.latitude);
    raw{length(raw) + 1} = sprintf('%d', currentSite.latitude);
  end
  
  if flag.longitude
    names{length(names) + 1} = 'longitude:';
    values{length(values) + 1} = longitude2string(currentSite.longitude);
    raw{length(raw) + 1} = sprintf('%d', currentSite.longitude);
  end
  
  if flag.elevation
    names{length(names) + 1} = 'elevation:';
    values{length(values) + 1} = elevation2string(currentSite.elevation);
    raw{length(raw) + 1} = sprintf('%d', currentSite.elevation);
  end
  
  if flag.startTime
    names{length(names) + 1} = 'start time:';
    values{length(values) + 1} = datetime2string(posix2datetime(currentSite.startTime));
    raw{length(raw) + 1} = sprintf('%d', currentSite.startTime);
  end
  
  if flag.stopTime
    names{length(names) + 1} = 'stop time:';
    values{length(values) + 1} = datetime2string(posix2datetime(currentSite.stopTime));
    raw{length(raw) + 1} = sprintf('%d', currentSite.stopTime);
  end
  
  namesWidth = 0;
  valuesWidth = 0;
  rawWidth = 0;
  
  for ii = 1 : length(names)
    if namesWidth < length(names{ii})
      namesWidth = length(names{ii});
    end
    
    if ~isempty(raw{ii})
      if valuesWidth < length(values{ii})
        valuesWidth = length(values{ii});
      end
      
      if rawWidth < length(raw{ii})
        rawWidth = length(raw{ii});
      end
    end
  end
  
  for ii = 1 : length(names)
    if length(names{ii}) < namesWidth
      names{ii} = sprintf(sprintf('%%s%%%ds', namesWidth - length(names{ii})), names{ii}, ' ');
    end
    
    if ~isempty(raw{ii})
      if length(values{ii}) < valuesWidth
        values{ii} = sprintf(sprintf('%%s%%%ds', valuesWidth - length(values{ii})), values{ii}, ' ');
      end
      
      if length(raw{ii}) < rawWidth
        raw{ii} = sprintf(sprintf('%%%ds%%s', rawWidth - length(raw{ii})), ' ', raw{ii});
      end
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s ', names{ii});
    
    if ~isempty(values{ii})
      fprintf('%s', values{ii});
    end
    
    if ~isempty(raw{ii})
      fprintf(' [%s]', raw{ii});
    end
    
    fprintf('\n');
  end
end