function surveysites(varargin)
  currentSurvey = [];
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSurvey)
    currentSurvey = survey(varargin{:});
  end
  
  if ~isempty(currentSurvey)
    for ii = 1 : length(currentSurvey.sites)
      fprintf('%s\n', currentSurvey.sites(ii).name);
    end
  end
end