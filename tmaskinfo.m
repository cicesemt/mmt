function [varargout] = tmaskinfo(varargin)
  flag.all = false;
  flag.specific = false;
  flag.numberOfZones = false;
  flag.timeSpan = false;
  flag.timeLength = false;
  flag.totalSeconds = false;
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-numberOfZones')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfZones = true;
      elseif strcmpi(varargin{k}, '-timeSpan')
        varargin(:, k) = [];
        flag.specific = true;
        flag.timeSpan = true;
      elseif strcmpi(varargin{k}, '-timeLength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.timeLength = true;
      elseif strcmpi(varargin{k}, '-totalSeconds')
        varargin(:, k) = [];
        flag.specific = true;
        flag.totalSeconds = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.numberOfZones = true;
    flag.timeSpan = true;
    flag.timeLength = true;
    flag.totalSeconds = true;
  elseif ~flag.specific
    flag.numberOfZones = true;
    flag.timeSpan = true;
  end
  
  zones = tmaskzones();
  
  if ~isempty(zones)
    info = buildInfo(zones);
    
    if nargout > 0
      varargout{1} = info;
    else
      printInfo(info, flag);
    end
  end
end

function info = buildInfo(zones)
  info.numberOfZones = length(zones);
  info.timeSpan = 0;
  info.days = 0;
  info.hours = 0;
  info.minutes = 0;
  info.seconds = 0;
  info.totalSeconds = 0;
  
  for ii = 1 : length(zones)
    info.timeSpan = info.timeSpan + zones(ii).timeSpan;
    info.totalSeconds = info.totalSeconds + zones(ii).totalSeconds;
  end
  
  info.seconds = info.totalSeconds;
  info.days = fix(info.seconds / 86400);
  info.seconds = mod(info.seconds, 86400);
  info.hours = fix(info.seconds / 3600);
  info.seconds = mod(info.seconds, 3600);
  info.minutes = fix(info.seconds / 60);
  info.seconds = mod(info.seconds, 60);
end

function printInfo(info, flag)
  name = cell(0);
  value = cell(0);
  raw = cell(0);
  
  if flag.numberOfZones
    name{length(name) + 1} = 'number of zones:';
    value{length(value) + 1} = thousandsep(info.numberOfZones, 0);
    raw{length(raw) + 1} = '';
  end
  
  if flag.timeSpan
    name{length(name) + 1} = 'time span:';
    value{length(value) + 1} = sprintf('%d%%', round(info.timeSpan));
    raw{length(raw) + 1} = '';
  end
  
  if flag.timeLength
    name{length(name) + 1} = 'time length:';
    value{length(value) + 1} = timeLength(info.days, info.hours, info.minutes, info.seconds);
    raw{length(raw) + 1} = '';
  end
  
  if flag.totalSeconds
    name{length(name) + 1} = 'total seconds:';
    value{length(value) + 1} = thousandsep(info.totalSeconds, 0);
    raw{length(raw) + 1} = '';
  end
  
  nameWidth = 0;
  valueWidth = 0;
  rawWidth = 0;
  
  for ii = 1 : length(name)
    if nameWidth < length(name{ii})
      nameWidth = length(name{ii});
    end
    
    if ~isempty(raw{ii})
      if valueWidth < length(value{ii})
        valueWidth = length(value{ii});
      end
      
      if rawWidth < length(raw{ii})
        rawWidth = length(raw{ii});
      end
    end
  end
  
  for ii = 1 : length(name)
    if length(name{ii}) < nameWidth
      name{ii} = sprintf(sprintf('%%s%%%ds', nameWidth - length(name{ii})), name{ii}, ' ');
    end
    
    if ~isempty(raw{ii})
      if length(value{ii}) < valueWidth
        value{ii} = sprintf(sprintf('%%s%%%ds', valueWidth - length(value{ii})), value{ii}, ' ');
      end
      
      if length(raw{ii}) < rawWidth
        raw{ii} = sprintf(sprintf('%%%ds%%s', rawWidth - length(raw{ii})), ' ', raw{ii});
      end
    end
  end
  
  for ii = 1 : length(name)
    fprintf('%s ', name{ii});
    
    if ~isempty(value{ii})
      fprintf('%s', value{ii});
    end
    
    if ~isempty(raw{ii})
      fprintf(' [%s]', raw{ii});
    end
    
    fprintf('\n');
  end
end

function timeLength = timeLength(days, hours, minutes, seconds)
  timeLength = '';
  
  if days > 0
    if days == 1
      timeLength = '1 day ';
    else
      timeLength = sprintf('%d days ', days);
    end
  end
  
  if hours == 1
    timeLength = sprintf('%s1 hour ', timeLength);
  else
    timeLength = sprintf('%s%d hours ', timeLength, hours);
  end
  
  if minutes == 1
    timeLength = sprintf('%s1 minute ', timeLength);
  else
    timeLength = sprintf('%s%d minutes ', timeLength, minutes);
  end
  
  if seconds == 1
    timeLength = sprintf('%s1 second', timeLength);
  else
    timeLength = sprintf('%s%d seconds', timeLength, seconds);
  end
end