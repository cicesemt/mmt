function surveypath(varargin)
  currentSurvey = [];
  
  flag.depth = 2;
  flag.default = false;
  flag.long = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-survey')
        varargin(:, k) = [];
        flag.depth = 0;
      elseif strcmpi(varargin{k}, '-site')
        varargin(:, k) = [];
        flag.depth = 1;
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        flag.depth = 2;
      elseif strcmpi(varargin{k}, '-channel')
        varargin(:, k) = [];
        flag.depth = 3;
      elseif strcmpi(varargin{k}, '-format')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          format = varargin{k};
          varargin(:, k) = [];
          
          if strcmpi(format, 'default')
            flag.default = true;
          elseif strcmpi(format, 'long')
            flag.long = true;
          end
        end
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSurvey)
    currentSurvey = survey(varargin{:});
  end
  
  if ~isempty(currentSurvey)
    if flag.default || flag.long
      formatSurvey(currentSurvey, flag);
    else
      printSurvey(currentSurvey, flag);
    end
  end
end

function formatSurvey(currentSurvey, flag)
  for ii = 1 : length(currentSurvey.sites)
    formatSite(currentSurvey.sites(ii), flag);
  end
end

function formatSite(currentSite , flag)
  for ii = 1 : length(currentSite.bands)
    formatBand(currentSite.bands(ii), flag);
  end
end

function formatBand(currentBand, flag)
  if flag.default
    long2default(currentBand);
  elseif flag.long
    default2long(currentBand);
  end
end

function printSurvey(currentSurvey, flag)
  if flag.depth >= 0
    [~, name] = fileparts(currentSurvey.folder);
    
    fprintf('%s\n', name);
    
    for ii = 1 : length(currentSurvey.sites)
      printSite(currentSurvey.sites(ii), ii == length(currentSurvey.sites), flag);
    end
  end
end

function printSite(currentSite, lastSite, flag)
  if flag.depth >= 1
    [~, name] = fileparts(currentSite.folder);
    
    upAndRight = 9492;
    verticalAndRight = 9500;
    horizontal = 9472;
    
    if lastSite
      right = upAndRight;
    else
      right = verticalAndRight;
    end
    
    fprintf('%c%c %s\n', right, horizontal, name);
    
    for ii = 1 : length(currentSite.bands)
      printBand(currentSite.bands(ii), lastSite, ii == length(currentSite.bands), flag);
    end
  end
end

function printBand(currentBand, lastSite, lastBand, flag)
  if flag.depth >= 2
    [~, name] = fileparts(currentBand.folder);
    
    vertical = 9474;
    upAndRight = 9492;
    verticalAndRight = 9500;
    horizontal = 9472;
    
    if lastSite
      vertical = ' ';
    end
    
    if lastBand
      right = upAndRight;
    else
      right = verticalAndRight;
    end
    
    fprintf('%c  %c%c %s\n', vertical, right, horizontal, name);
    
    for ii = 1 : length(currentBand.channels)
      printChannel(currentBand.channels(ii), lastSite, lastBand, ii == length(currentBand.channels), flag);
    end
  end
end

function printChannel(currentChannel, lastSite, lastBand, lastChannel, flag)
  if flag.depth >= 3
    name = currentChannel.name;
    
    vertical1 = 9474;
    vertical2 = 9474;
    upAndRight = 9492;
    verticalAndRight = 9500;
    horizontal = 9472;
    
    if lastSite
      vertical1 = ' ';
    end
    
    if lastBand
      vertical2 = ' ';
    end
    
    if lastChannel
      right = upAndRight;
    else
      right = verticalAndRight;
    end
    
    fprintf('%c  %c  %c%c %s\n', vertical1, vertical2, right, horizontal, name);
  end
end

function default2long(currentBand)
  [path, name] = fileparts(currentBand.folder);
  
  if length(name) == length('meas_YYYY-MM-DD_HH-MM-SS')
    meas = name(1 : 4);
    
    if strcmpi(meas, 'meas')
      u0 = name( 5 :  5);
      u1 = name(16 : 16);
      
      if strcmp(u0, '_') && strcmp(u1, '_')
        h0 = name(10 : 10);
        h1 = name(13 : 13);
        h2 = name(19 : 19);
        h3 = name(22 : 22);
        
        if strcmp(h0, '-') && strcmp(h1, '-') && strcmp(h2, '-') && strcmp(h3, '-')
          year   = str2double(name( 6 :  9));
          month  = str2double(name(11 : 12));
          day    = str2double(name(14 : 15));
          hour   = str2double(name(17 : 18));
          minute = str2double(name(20 : 21));
          second = str2double(name(23 : 24));
          
          if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute) && ~isnan(second)
            year   = name( 6 :  9);
            month  = name(11 : 12);
            day    = name(14 : 15);
            hour   = name(17 : 18);
            minute = name(20 : 21);
            second = name(23 : 24);
            
            source = currentBand.folder;
            target = sprintf('%s%s%s_%s-%s-%s_%s-%s-%s_%s', path, filesep, meas, year, month, day, hour, minute, second, currentBand.name);
            
            if strcmp(source, pwd())
              changedirectory = true;
            else
              changedirectory = false;
            end
            
            if movefile(source, target) && changedirectory
              cd(target);
            end
          end
        end
      end
    end
  end
end

function long2default(currentBand)
  [path, name] = fileparts(currentBand.folder);
  
  if length(name) >= length('meas_YYYY-MM-DD_HH-MM-SS_?U')
    meas = name(1 : 4);
    
    if strcmpi(meas, 'meas')
      u0 = name( 5 :  5);
      u1 = name(16 : 16);
      u2 = name(25 : 25);
      
      if strcmp(u0, '_') && strcmp(u1, '_') && strcmp(u2, '_')
        h0 = name(10 : 10);
        h1 = name(13 : 13);
        h2 = name(19 : 19);
        h3 = name(22 : 22);
        
        measurementUnit = name(length(name) : length(name));
        
        if strcmp(h0, '-') && strcmp(h1, '-') && strcmp(h2, '-') && strcmp(h3, '-') && (strcmpi(measurementUnit, 'H') || strcmpi(measurementUnit, 'S'))
          year   = str2double(name( 6 :  9));
          month  = str2double(name(11 : 12));
          day    = str2double(name(14 : 15));
          hour   = str2double(name(17 : 18));
          minute = str2double(name(20 : 21));
          second = str2double(name(23 : 24));
          
          samplingRate = str2double(name(26 : length(name) - 1));
          
          if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute) && ~isnan(second) && ~isnan(samplingRate)
            if samplingRate == currentBand.samplingRate && measurementUnit == currentBand.measurementUnit
              year   = name( 6 :  9);
              month  = name(11 : 12);
              day    = name(14 : 15);
              hour   = name(17 : 18);
              minute = name(20 : 21);
              second = name(23 : 24);
              
              source = currentBand.folder;
              target = sprintf('%s%s%s_%s-%s-%s_%s-%s-%s', path, filesep, meas, year, month, day, hour, minute, second);
              
              if strcmp(source, pwd())
                changedirectory = true;
              else
                changedirectory = false;
              end
              
              if movefile(source, target) && changedirectory
                cd(target);
              end
            end
          end
        end
      end
    end
  end
end