function wyeplot(varargin)
  files = [];
  
  flag.bands = cell(0);
  flag.ex = false;
  flag.ey = false;
  flag.hx = false;
  flag.hy = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        files = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          flag.bands{length(flag.bands) + 1} = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-channel')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          if strcmpi(varargin{k}, 'Ex')
            flag.ex = true;
          elseif strcmpi(varargin{k}, 'Ey')
            flag.ey = true;
          elseif strcmpi(varargin{k}, 'Hx')
            flag.hx = true;
          elseif strcmpi(varargin{k}, 'Hy')
            flag.hy = true;
          end
          
          varargin(:, k) = [];
        end
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(files)
    files = wye(varargin{:});
  end
  
  if ~isempty(files)
    if ~flag.ex && ~flag.ey && ~flag.hx && ~flag.hy
      flag.ex = true;
      flag.ey = true;
      flag.hx = true;
      flag.hy = true;
    end
    
    for ii = 1 : length(files)
      plotFile(files(ii), flag);
    end
  end
end

function plotFile(file, flag)
  bands = cell(0);
  
  for ii = 1 : file.numberOfBlocks
    bandName = sprintf('%dK', 192 / (2^file.blocks(ii).header.decimationExponent));
    
    ignore = false;
    
    if ~isempty(flag.bands)
      ignore = true;
      
      for jj = 1 : length(flag.bands)
        if strcmpi(flag.bands(jj), bandName)
          ignore = false;
          break;
        end
      end
    end
    
    if ~ignore && file.blocks(ii).numberOfSegments > 0
      for jj = 1 : length(bands)
        if strcmp(bands{jj}.name, bandName)
          ignore = true;
          break;
        end
      end
      
      if ~ignore
        for jj = 1 : file.numberOfBands
          if strcmp(file.bands(jj).name, bandName)
            plotBlock(file, jj, ii, 1, flag);
            bands{length(bands) + 1} = file.bands(jj);
            break;
          end
        end
      end
    end
  end
end

function plotBlock(file, bandIndex, blockIndex, segmentIndex, flag)
  [ex, ey, hx, hy] = wyedata(file, '-offset', file.blocks(blockIndex).segments(segmentIndex).offset);
  
  count = 0;
  
  if flag.ex
    if ~isempty(ex)
      count = count + 1;
    else
      flag.ex = false;
    end
  end
  
  if flag.ey
    if ~isempty(ey)
      count = count + 1;
    else
      flag.ey = false;
    end
  end
  
  if flag.hx
    if ~isempty(hx)
      count = count + 1;
    else
      flag.hx = false;
    end
  end
  
  if flag.hy
    if ~isempty(hy)
      count = count + 1;
    else
      flag.hy = false;
    end
  end
  
  if count > 0
    f = figure();
    f.set('NumberTitle', 'off');
    f.set('ToolBar', 'none');
    f.set('MenuBar', 'none');
    f.set('Name', sprintf('%s (%s)', file.name, file.bands(bandIndex).name));
    
    show(f, file, bandIndex, blockIndex, segmentIndex, ex, ey, hx, hy, count, flag);
  end
end

function show(f, file, bandIndex, blockIndex, segmentIndex, ex, ey, hx, hy, total, flag)
  figure(f);
  
  current = 1;
  
  if flag.ex
    axes = subplot(total, 1, current);
    cla(axes, 'reset');
    
    startTime = 0;
    stopTime = file.blocks(blockIndex).segments(segmentIndex).samplingSeconds;
    
    period = 1 / file.bands(bandIndex).samplingFrequency;
    xdata = startTime : period : stopTime - period;
    series = plot(xdata, ex, 'Color', [0 0.4470 0.7410], 'LineStyle', '-');
    
    axes.YLabel.String = 'Ex';
    axes.YLabel.FontSize = 12;
    axes.YLabel.FontWeight = 'bold';
    
    axes.XLim = [xdata(1) xdata(length(xdata))];
    axes.XTick = xdata(1) : (xdata(length(xdata)) - xdata(1)) / 4 : xdata(length(xdata));
    axes.XTickLabel = {};
    
    axes.YTick = axes.YLim(1) : (axes.YLim(2) - axes.YLim(1)) / 4 : axes.YLim(2);
    axes.YTickLabel = {};
    
    current = current + 1;
  end
  
  if flag.ey
    axes = subplot(total, 1, current);
    cla(axes, 'reset');
    
    startTime = 0;
    stopTime = file.blocks(blockIndex).segments(segmentIndex).samplingSeconds;
    
    period = 1 / file.bands(bandIndex).samplingFrequency;
    xdata = startTime : period : stopTime - period;
    series = plot(xdata, ey, 'Color', [0 0.4470 0.7410], 'LineStyle', '-');
    
    axes.YLabel.String = 'Ey';
    axes.YLabel.FontSize = 12;
    axes.YLabel.FontWeight = 'bold';
    
    axes.XLim = [xdata(1) xdata(length(xdata))];
    axes.XTick = xdata(1) : (xdata(length(xdata)) - xdata(1)) / 4 : xdata(length(xdata));
    axes.XTickLabel = {};
    
    axes.YTick = axes.YLim(1) : (axes.YLim(2) - axes.YLim(1)) / 4 : axes.YLim(2);
    axes.YTickLabel = {};
    
    current = current + 1;
  end
  
  if flag.hx
    axes = subplot(total, 1, current);
    cla(axes, 'reset');
    
    startTime = 0;
    stopTime = file.blocks(blockIndex).segments(segmentIndex).samplingSeconds;
    
    period = 1 / file.bands(bandIndex).samplingFrequency;
    xdata = startTime : period : stopTime - period;
    series = plot(xdata, hx, 'Color', [0 0.4470 0.7410], 'LineStyle', '-');
    
    axes.YLabel.String = 'Hx';
    axes.YLabel.FontSize = 12;
    axes.YLabel.FontWeight = 'bold';
    
    axes.XLim = [xdata(1) xdata(length(xdata))];
    axes.XTick = xdata(1) : (xdata(length(xdata)) - xdata(1)) / 4 : xdata(length(xdata));
    axes.XTickLabel = {};
    
    axes.YTick = axes.YLim(1) : (axes.YLim(2) - axes.YLim(1)) / 4 : axes.YLim(2);
    axes.YTickLabel = {};
    
    current = current + 1;
  end
  
  if flag.hy
    axes = subplot(total, 1, current);
    cla(axes, 'reset');
    
    startTime = 0;
    stopTime = file.blocks(blockIndex).segments(segmentIndex).samplingSeconds;
    
    period = 1 / file.bands(bandIndex).samplingFrequency;
    xdata = startTime : period : stopTime - period;
    series = plot(xdata, hy, 'Color', [0 0.4470 0.7410], 'LineStyle', '-');
    
    axes.YLabel.String = 'Hy';
    axes.YLabel.FontSize = 12;
    axes.YLabel.FontWeight = 'bold';
    
    axes.XLim = [xdata(1) xdata(length(xdata))];
    axes.XTick = xdata(1) : (xdata(length(xdata)) - xdata(1)) / 4 : xdata(length(xdata));
    axes.XTickLabel = {};
    
    axes.YTick = axes.YLim(1) : (axes.YLim(2) - axes.YLim(1)) / 4 : axes.YLim(2);
    axes.YTickLabel = {};
    
    current = current + 1;
  end
end