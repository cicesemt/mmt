function [varargout] = loc(varargin)
  name = mfilename();
  defaultValue = '';
  
  currentSite = [];
  path = [];
  reset = false;
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSite = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-path')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          path = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-reset')
        varargin(:, k) = [];
        reset = true;
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSite)
    if isempty(path)
      path = pwd();
    end
    
    currentSite = site(path);
  end
  
  if ~isempty(currentSite)
    dname = sprintf('%s%s%s', currentSite.folder, filesep, '.mt');
    fname = sprintf('%s%s%s', dname, filesep, name);
    
    if reset
      value = saveValue(dname, fname, defaultValue);
    elseif ~isempty(varargin)
      value = saveValue(dname, fname, varargin{1});
    else
      value = loadValue(fname, defaultValue);
      
      if ~isempty(value) && nargout == 0
        fprintf('%s\n', value);
      end
    end
    
    if ~isempty(value) && nargout > 0
      varargout{1} = value;
    end
  end
end

function value = saveValue(dname, fname, value)
  [mtdir, ~] = mkdir(dname);
  
  if mtdir
    fp = fopen(fname, 'wt');
    
    if fp ~= -1
      fprintf(fp, '%s\n', value);
      fclose(fp);
    end
  end
end

function value = loadValue(fname, defaultValue)
  value = defaultValue;
  
  fp = fopen(fname, 'rt');
  
  if fp ~= -1
    v = fgetl(fp);
    
    if v ~= -1
      value = v;
    end
    
    fclose(fp);
  end
end