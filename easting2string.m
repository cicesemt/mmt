function string = easting2string(easting)
  string = '';
  len = length(sprintf('%d', fix(easting)));
  
  if len < 7
    for ii = 1 : 7 - len
      string = sprintf('%s0', string);
    end
  end
  
  string = sprintf('%s%.2f m E', string, easting);
end