function bandmatch(varargin)
  currentBand = [];
  currentSite = [];
  currentSurvey = [];
  
  flag.all = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentBand = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      else
        k = k + 1;
      end
    end
  end
  
  if ~isempty(currentBand)
    currentSite = site(fileparts(currentBand.folder));
    
    if ~isempty(currentSite)
      currentSurvey = survey(fileparts(currentSite.folder));
    end
  elseif ~isempty(varargin)
    currentBand = band(varargin{:});
    
    if ~isempty(currentBand)
      currentSite = site(fileparts(currentBand.folder));
      
      if ~isempty(currentSite)
        currentSurvey = survey(fileparts(currentSite.folder));
      end
    end
  else
    currentSurvey = survey();
    
    if ~isempty(currentSurvey)
      currentSite = currentSurvey.currentSite;
      
      if ~isempty(currentSite)
        currentBand = currentSite.currentBand;
      end
    end
  end
  
  if ~isempty(currentSurvey)
    if ~isempty(currentSite)
      if ~isempty(currentBand)
        match = struct(...
          'name', {}, ...
          'rate', {}...
        );
        
        rates = zeros(0);
        
        for ii = 1 : length(currentSurvey.sites)
          if ~strcmp(currentSite.folder, currentSurvey.sites(ii).folder)
            for jj = 1 : length(currentSurvey.sites(ii).bands)
              
              rate = calcRate(currentBand, currentSurvey.sites(ii).bands(jj));
              
              if rate > 0
                m.name = currentSurvey.sites(ii).name;
                m.rate = round(rate);
                
                match(length(match) + 1) = m;
                rates(length(rates) + 1) = m.rate;
              end
              
            end
          end
        end
        
        if ~isempty(match)
          matches.name = currentBand.name;
          matches.match = struct(...
            'name', {}, ...
            'rate', {}...
          );
          
          [~, index] = sort(rates);
          
          if ~flag.all
            matches.match = match(index(length(index)));
          else
            for ii = length(index) : -1 : 1
              matches.match(length(index) - ii + 1) = match(index(ii));
            end
          end
          
          printMatches(matches);
        end
      end
    end
  end
end

function rate = calcRate(band1, band2)
  rate = 0;
  
  if band1.frequency == band2.frequency
    if band1.startTime < band2.stopTime
      if band1.stopTime > band2.startTime
        start1 = band1.startTime;
        stop1 = band1.stopTime;
        
        if band1.startTime > band2.startTime
          start2 = band1.startTime;
        else
          start2 = band2.startTime;
        end
        
        if band1.stopTime < band2.stopTime
          stop2 = band1.stopTime;
        else
          stop2 = band2.stopTime;
        end
        
        rate = ((stop2 - start2) / (stop1 - start1)) * 100;
      end
    end
  end
end

function printMatches(matches)
  cols = cell(0);
  
  for ii = 1 : length(matches)
    cols{ii, 1} = matches(ii).name;
    
    for jj = 1 : length(matches(ii).match)
      cols{ii, 2 * jj} = sprintf('%.0f%%', matches(ii).match(jj).rate);
      cols{ii, 2 * jj + 1} = matches(ii).match(jj).name;
    end
  end
  
  [numrows, numcols] = size(cols);
  
  if numcols > 3
    alignLeft = true;
    
    for ii = 1 : numrows
      cols{ii, 1} = sprintf('%s%s', cols{ii, 1}, ':');
    end
  else
    alignLeft = false;
  end
  
  widths = zeros(1, numcols);
  
  for ii = 1 : numrows
    for jj = 1 : numcols
      if widths(jj) < length(cols{ii, jj})
        widths(jj) = length(cols{ii, jj});
      end
    end
  end
  
  separators = cell(numrows, numcols - 1);
  
  for ii = 1 : numrows
    for jj = 1 : numcols
      if jj == 1
        separators{ii, jj} = ' ';
      elseif jj < numcols
        if ~isempty(cols{ii, jj})
          if ~mod(jj, 2)
            separators{ii, jj} = ' ';
          elseif ~isempty(cols{ii, jj + 1})
            separators{ii, jj} = ', ';
          end
        else
          separators{ii, jj} = '';
        end
      end
      
      if ~isempty(cols{ii, jj}) && length(cols{ii, jj}) < widths(jj)
        if jj == 1
          if alignLeft
            cols{ii, jj} = sprintf(sprintf('%%s%%%ds', widths(jj) - length(cols{ii, jj})), cols{ii, jj}, ' ');
          else
            cols{ii, jj} = sprintf(sprintf('%%%ds%%s', widths(jj) - length(cols{ii, jj})), ' ', cols{ii, jj});
          end
        elseif ~mod(jj, 2)
          cols{ii, jj} = sprintf(sprintf('%%%ds%%s', widths(jj) - length(cols{ii, jj})), ' ', cols{ii, jj});
        else
          cols{ii, jj} = sprintf(sprintf('%%s%%%ds', widths(jj) - length(cols{ii, jj})), cols{ii, jj}, ' ');
        end
      end
    end
  end
  
  for ii = 1 : numrows
    for jj = 1 : numcols
      fprintf('%s', cols{ii, jj});
      
      if jj < numcols
        fprintf('%s', separators{ii, jj});
      end
    end
    
    fprintf('\n');
  end
end