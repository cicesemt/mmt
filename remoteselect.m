function remoteselect(path)
  info = localinfo();
  
  if ~isempty(info)
    if length(path) > 1
      if path(length(path)) == filesep
        path = path(1 : length(path) - 1);
      end
    end
    
    files = dir(path);
    
    if ~isempty(files)
      ats_files = cell(1, length(files));
      ats_count = 0;
      
      for ii = 1 : length(files)
        file = files(ii);
        
        if ~strcmp(file.name, '.') && ~strcmp(file.name, '..') && ~file.isdir
          ats_file = atsfile(sprintf('%s%s%s', path, filesep, file.name));
          
          if ~isempty(ats_file)
            if ats_file.aduSerialNumber ~= info.aduSerialNumber && ats_file.samplingRate == info.samplingRate && ats_file.measurementUnit == info.measurementUnit
              ats_count = ats_count + 1;
              ats_files{ats_count} = ats_file;
            end
          end
        end
      end
      
      if ats_count > 0
        for ii = 1 : ats_count
          hx = [];
          hy = [];
          
          pivot = ats_files{ii};
          
          if strcmpi(pivot.channelType, 'HX')
            hx = pivot;
          elseif strcmpi(pivot.channelType, 'HY')
            hy = pivot;
          else
            continue;
          end
          
          for jj = 1 : ats_count
            if ii ~= jj
              file = ats_files{jj};
              
              if file.aduSerialNumber == pivot.aduSerialNumber && file.xmlVersion == pivot.xmlVersion && file.runNumber == pivot.runNumber && strcmpi(file.boardType, pivot.boardType) && file.samplingRate == pivot.samplingRate && strcmpi(file.measurementUnit, pivot.measurementUnit)
                if file.channelNumber ~= pivot.channelNumber && ~strcmpi(file.channelType, pivot.channelType)
                  if strcmpi(file.header.systemType, pivot.header.systemType) && file.startTime == pivot.startTime
                    
                    distance = sqrt((file.header.latitude - pivot.header.latitude)^2 + (file.header.longitude - pivot.header.longitude)^2 + (file.header.elevation - pivot.header.elevation)^2);
                    
                    if distance < 20
                      if strcmpi(file.channelType, 'HX')
                        if isempty(hx)
                          hx = file;
                        end
                      elseif strcmpi(file.channelType, 'HY')
                        if isempty(hy)
                          hy = file;
                        end
                      end
                    end
                    
                  end
                end
              end
              
            end
          end
          
          if ~isempty(hx) && ~isempty(hy)
            startTime = hx.startTime;
            stopTime  = min(hx.stopTime, hy.stopTime);
            
            if startTime < info.stopTime && info.startTime < stopTime
              hx.folder = path;
              hy.folder = path;
              
              [mtdir, ~] = mkdir('.mt');
              
              if mtdir
                fp = fopen(sprintf('%s%s%s', '.mt', filesep, 'remote'), 'wt');
                
                if fp ~= -1
                  fprintf(fp, '%s%s%s\n', hx.folder, filesep, hx.name);
                  fprintf(fp, '%s%s%s\n', hy.folder, filesep, hy.name);
                  
                  fclose(fp);
                end
              end
              
              break;
            end
          end
          
        end
      end
    end
  end
end