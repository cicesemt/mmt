function [varargout] = state(varargin)
  name = mfilename();
  defaultValue = '';
  
  currentSurvey = [];
  path = [];
  reset = false;
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-path')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          path = varargin{k};
          varargin(:, k) = [];
        end
      elseif strcmpi(varargin{k}, '-reset')
        varargin(:, k) = [];
        reset = true;
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSurvey)
    if isempty(path)
      path = pwd();
    end
    
    currentSurvey = survey(path);
  end
  
  if ~isempty(currentSurvey)
    dname = sprintf('%s%s%s', currentSurvey.folder, filesep, '.mt');
    fname = sprintf('%s%s%s', dname, filesep, name);
    
    if reset
      value = saveValue(dname, fname, defaultValue);
    elseif ~isempty(varargin)
      value = saveValue(dname, fname, varargin{1});
    else
      value = loadValue(fname, defaultValue);
      
      if nargout == 0
        if ~isempty(value)
          fprintf('%s\n', value);
        else
          value = suggestValue(currentSurvey);
          
          if ~isempty(value)
            fprintf('%s\n', value);
          end
        end
      end
    end
    
    if nargout > 0
      if ~isempty(value)
        varargout{1} = value;
      else
        value = suggestValue(currentSurvey);
        
        if ~isempty(value)
          varargout{1} = value;
        end
      end
    end
  end
end

function value = saveValue(dname, fname, value)
  [mtdir, ~] = mkdir(dname);
  
  if mtdir
    fp = fopen(fname, 'wt');
    
    if fp ~= -1
      fprintf(fp, '%s\n', value);
      fclose(fp);
    end
  end
end

function value = loadValue(fname, defaultValue)
  value = defaultValue;
  
  fp = fopen(fname, 'rt');
  
  if fp ~= -1
    v = fgetl(fp);
    
    if v ~= -1
      value = v;
    end
    
    fclose(fp);
  end
end

function value = suggestValue(currentSurvey)
  currentSite = currentSurvey.currentSite;
  
  if ~isempty(currentSite)
    [~, state, ~, ~] = latlon2place(currentSite.latitude, currentSite.longitude);
  else
    [~, state, ~, ~] = latlon2place(currentSurvey.latitude, currentSurvey.longitude);
  end
  
  if ~isempty(state)
    value = state;
  else
    value = '';
  end
end