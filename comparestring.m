function [value] = comparestring(string1, string2)
  if isempty(string1) && isempty(string2)
    value = 0;
    return;
  end
  
  if isempty(string1) && ~isempty(string2)
    value = -1;
    return;
  end
  
  if ~isempty(string1) && isempty(string2)
    value = 1;
    return;
  end
  
  if length(string1) < length(string2)
    str1 = string1;
    str2 = string2(1 : length(str1));
  elseif length(string1) > length(string2)
    str2 = string2;
    str1 = string1(1 : length(str2));
  else
    str1 = string1;
    str2 = string2;
  end
  
  lt = str1 < str2;
  
  for ii = 1 : length(lt)
    if lt(ii)
      value = -1;
      return;
    end
  end
  
  gt = str1 > str2;
  
  for ii = 1 : length(gt)
    if gt(ii)
      value = 1;
      return;
    end
  end
  
  if length(string1) < length(string2)
    value = -1;
    return;
  end
  
  if length(string1) > length(string2)
    value = 1;
    return;
  end
  
  value = 0;
end