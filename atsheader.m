function [varargout] = atsheader(varargin)
  files = [];
  
  flag.raw = false;
  flag.all = false;
  flag.specific = false;
  flag.length = false;
  flag.version = false;
  flag.numberOfSamples = false;
  flag.frequency = false;
  flag.startTime = false;
  flag.mvFactor = false;
  flag.gmtOffset = false;
  flag.originalFrequency = false;
  flag.aduSerialNumber = false;
  flag.adcSerialNumber = false;
  flag.channelNumber = false;
  flag.chopper = false;
  flag.channelType = false;
  flag.sensorType = false;
  flag.sensorSerialNumber = false;
  flag.posX1 = false;
  flag.posY1 = false;
  flag.posZ1 = false;
  flag.posX2 = false;
  flag.posY2 = false;
  flag.posZ2 = false;
  flag.dipoleLength = false;
  flag.angle = false;
  flag.probeRes = false;
  flag.dcOffset = false;
  flag.gainStage1 = false;
  flag.gainStage2 = false;
  flag.latitude = false;
  flag.longitude = false;
  flag.elevation = false;
  flag.latLongType = false;
  flag.addCoordType = false;
  flag.gaussRefMeridian = false;
  flag.hochwert = false;
  flag.rechtswert = false;
  flag.gpsStat = false;
  flag.gpsAccuracy = false;
  flag.utcOffset = false;
  flag.systemType = false;
  flag.surveyHeaderName = false;
  flag.measurementType = false;
  flag.dcOffsetCorrValue = false;
  flag.dcOffsetCorrOn = false;
  flag.inputDivOn = false;
  flag.notUsedVar = false;
  flag.selfTestResult = false;
  flag.reserved5 = false;
  flag.calFreqs = false;
  flag.calEntryLength = false;
  flag.calVersion = false;
  flag.calStartAddress = false;
  flag.lfFilters = false;
  flag.aduCalFilename = false;
  flag.aduCalTime = false;
  flag.sensorCalFilename = false;
  flag.sensorCalTime = false;
  flag.powerlineFreq1 = false;
  flag.powerlineFreq2 = false;
  flag.hfFilters = false;
  flag.originalMvFactor = false;
  flag.unsedVar = false;
  flag.boardType = false;
  flag.client = false;
  flag.contractor = false;
  flag.area = false;
  flag.surveyID = false;
  flag.operator = false;
  flag.reserved = false;
  flag.xmlHeader = false;
  flag.comments = false;
  flag.weather = false;
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        files = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-raw')
        varargin(:, k) = [];
        flag.raw = true;
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-length')
        varargin(:, k) = [];
        flag.specific = true;
        flag.length = true;
      elseif strcmpi(varargin{k}, '-version')
        varargin(:, k) = [];
        flag.specific = true;
        flag.version = true;
      elseif strcmpi(varargin{k}, '-numberOfSamples')
        varargin(:, k) = [];
        flag.specific = true;
        flag.numberOfSamples = true;
      elseif strcmpi(varargin{k}, '-frequency')
        varargin(:, k) = [];
        flag.specific = true;
        flag.frequency = true;
      elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.startTime = true;
      elseif strcmpi(varargin{k}, '-mvFactor')
        varargin(:, k) = [];
        flag.specific = true;
        flag.mvFactor = true;
      elseif strcmpi(varargin{k}, '-gmtOffset')
        varargin(:, k) = [];
        flag.specific = true;
        flag.gmtOffset = true;
      elseif strcmpi(varargin{k}, '-originalFrequency')
        varargin(:, k) = [];
        flag.specific = true;
        flag.originalFrequency = true;
      elseif strcmpi(varargin{k}, '-aduSerialNumber')
        varargin(:, k) = [];
        flag.specific = true;
        flag.aduSerialNumber = true;
      elseif strcmpi(varargin{k}, '-adcSerialNumber')
        varargin(:, k) = [];
        flag.specific = true;
        flag.adcSerialNumber = true;
      elseif strcmpi(varargin{k}, '-channelNumber')
        varargin(:, k) = [];
        flag.specific = true;
        flag.channelNumber = true;
      elseif strcmpi(varargin{k}, '-chopper')
        varargin(:, k) = [];
        flag.specific = true;
        flag.chopper = true;
      elseif strcmpi(varargin{k}, '-channelType')
        varargin(:, k) = [];
        flag.specific = true;
        flag.channelType = true;
      elseif strcmpi(varargin{k}, '-sensorType')
        varargin(:, k) = [];
        flag.specific = true;
        flag.sensorType = true;
      elseif strcmpi(varargin{k}, '-sensorSerialNumber')
        varargin(:, k) = [];
        flag.specific = true;
        flag.sensorSerialNumber = true;
      elseif strcmpi(varargin{k}, '-posX1')
        varargin(:, k) = [];
        flag.specific = true;
        flag.posX1 = true;
      elseif strcmpi(varargin{k}, '-posY1')
        varargin(:, k) = [];
        flag.specific = true;
        flag.posY1 = true;
      elseif strcmpi(varargin{k}, '-posZ1')
        varargin(:, k) = [];
        flag.specific = true;
        flag.posZ1 = true;
      elseif strcmpi(varargin{k}, '-posX2')
        varargin(:, k) = [];
        flag.specific = true;
        flag.posX2 = true;
      elseif strcmpi(varargin{k}, '-posY2')
        varargin(:, k) = [];
        flag.specific = true;
        flag.posY2 = true;
      elseif strcmpi(varargin{k}, '-posZ2')
        varargin(:, k) = [];
        flag.specific = true;
        flag.posZ2 = true;
      elseif strcmpi(varargin{k}, '-dipoleLength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.dipoleLength = true;
      elseif strcmpi(varargin{k}, '-angle')
        varargin(:, k) = [];
        flag.specific = true;
        flag.angle = true;
      elseif strcmpi(varargin{k}, '-probeRes')
        varargin(:, k) = [];
        flag.specific = true;
        flag.probeRes = true;
      elseif strcmpi(varargin{k}, '-dcOffset')
        varargin(:, k) = [];
        flag.specific = true;
        flag.dcOffset = true;
      elseif strcmpi(varargin{k}, '-gainStage1')
        varargin(:, k) = [];
        flag.specific = true;
        flag.gainStage1 = true;
      elseif strcmpi(varargin{k}, '-gainStage2')
        varargin(:, k) = [];
        flag.specific = true;
        flag.gainStage2 = true;
      elseif strcmpi(varargin{k}, '-latitude')
        varargin(:, k) = [];
        flag.specific = true;
        flag.latitude = true;
      elseif strcmpi(varargin{k}, '-longitude')
        varargin(:, k) = [];
        flag.specific = true;
        flag.longitude = true;
      elseif strcmpi(varargin{k}, '-elevation')
        varargin(:, k) = [];
        flag.specific = true;
        flag.elevation = true;
      elseif strcmpi(varargin{k}, '-latLongType')
        varargin(:, k) = [];
        flag.specific = true;
        flag.latLongType = true;
      elseif strcmpi(varargin{k}, '-addCoordType')
        varargin(:, k) = [];
        flag.specific = true;
        flag.addCoordType = true;
      elseif strcmpi(varargin{k}, '-gaussRefMeridian')
        varargin(:, k) = [];
        flag.specific = true;
        flag.gaussRefMeridian = true;
      elseif strcmpi(varargin{k}, '-hochwert')
        varargin(:, k) = [];
        flag.specific = true;
        flag.hochwert = true;
      elseif strcmpi(varargin{k}, '-rechtswert')
        varargin(:, k) = [];
        flag.specific = true;
        flag.rechtswert = true;
      elseif strcmpi(varargin{k}, '-gpsStat')
        varargin(:, k) = [];
        flag.specific = true;
        flag.gpsStat = true;
      elseif strcmpi(varargin{k}, '-gpsAccuracy')
        varargin(:, k) = [];
        flag.specific = true;
        flag.gpsAccuracy = true;
      elseif strcmpi(varargin{k}, '-utcOffset')
        varargin(:, k) = [];
        flag.specific = true;
        flag.utcOffset = true;
      elseif strcmpi(varargin{k}, '-systemType')
        varargin(:, k) = [];
        flag.specific = true;
        flag.systemType = true;
      elseif strcmpi(varargin{k}, '-surveyHeaderName')
        varargin(:, k) = [];
        flag.specific = true;
        flag.surveyHeaderName = true;
      elseif strcmpi(varargin{k}, '-measurementType')
        varargin(:, k) = [];
        flag.specific = true;
        flag.measurementType = true;
      elseif strcmpi(varargin{k}, '-dcOffsetCorrValue')
        varargin(:, k) = [];
        flag.specific = true;
        flag.dcOffsetCorrValue = true;
      elseif strcmpi(varargin{k}, '-dcOffsetCorrOn')
        varargin(:, k) = [];
        flag.specific = true;
        flag.dcOffsetCorrOn = true;
      elseif strcmpi(varargin{k}, '-inputDivOn')
        varargin(:, k) = [];
        flag.specific = true;
        flag.inputDivOn = true;
      elseif strcmpi(varargin{k}, '-notUsedVar')
        varargin(:, k) = [];
        flag.specific = true;
        flag.notUsedVar = true;
      elseif strcmpi(varargin{k}, '-selfTestResult')
        varargin(:, k) = [];
        flag.specific = true;
        flag.selfTestResult = true;
      elseif strcmpi(varargin{k}, '-reserved5')
        varargin(:, k) = [];
        flag.specific = true;
        flag.reserved5 = true;
      elseif strcmpi(varargin{k}, '-calFreqs')
        varargin(:, k) = [];
        flag.specific = true;
        flag.calFreqs = true;
      elseif strcmpi(varargin{k}, '-calEntryLength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.calEntryLength = true;
      elseif strcmpi(varargin{k}, '-calVersion')
        varargin(:, k) = [];
        flag.specific = true;
        flag.calVersion = true;
      elseif strcmpi(varargin{k}, '-calStartAddress')
        varargin(:, k) = [];
        flag.specific = true;
        flag.calStartAddress = true;
      elseif strcmpi(varargin{k}, '-lfFilters')
        varargin(:, k) = [];
        flag.specific = true;
        flag.lfFilters = true;
      elseif strcmpi(varargin{k}, '-aduCalFilename')
        varargin(:, k) = [];
        flag.specific = true;
        flag.aduCalFilename = true;
      elseif strcmpi(varargin{k}, '-aduCalTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.aduCalTime = true;
      elseif strcmpi(varargin{k}, '-sensorCalFilename')
        varargin(:, k) = [];
        flag.specific = true;
        flag.sensorCalFilename = true;
      elseif strcmpi(varargin{k}, '-sensorCalTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.sensorCalTime = true;
      elseif strcmpi(varargin{k}, '-powerlineFreq1')
        varargin(:, k) = [];
        flag.specific = true;
        flag.powerlineFreq1 = true;
      elseif strcmpi(varargin{k}, '-powerlineFreq2')
        varargin(:, k) = [];
        flag.specific = true;
        flag.powerlineFreq2 = true;
      elseif strcmpi(varargin{k}, '-hfFilters')
        varargin(:, k) = [];
        flag.specific = true;
        flag.hfFilters = true;
      elseif strcmpi(varargin{k}, '-originalMvFactor')
        varargin(:, k) = [];
        flag.specific = true;
        flag.originalMvFactor = true;
      elseif strcmpi(varargin{k}, '-unsedVar')
        varargin(:, k) = [];
        flag.specific = true;
        flag.unsedVar = true;
      elseif strcmpi(varargin{k}, '-boardType')
        varargin(:, k) = [];
        flag.specific = true;
        flag.boardType = true;
      elseif strcmpi(varargin{k}, '-client')
        varargin(:, k) = [];
        flag.specific = true;
        flag.client = true;
      elseif strcmpi(varargin{k}, '-contractor')
        varargin(:, k) = [];
        flag.specific = true;
        flag.contractor = true;
      elseif strcmpi(varargin{k}, '-area')
        varargin(:, k) = [];
        flag.specific = true;
        flag.area = true;
      elseif strcmpi(varargin{k}, '-surveyID')
        varargin(:, k) = [];
        flag.specific = true;
        flag.surveyID = true;
      elseif strcmpi(varargin{k}, '-operator')
        varargin(:, k) = [];
        flag.specific = true;
        flag.operator = true;
      elseif strcmpi(varargin{k}, '-reserved')
        varargin(:, k) = [];
        flag.specific = true;
        flag.reserved = true;
      elseif strcmpi(varargin{k}, '-xmlHeader')
        varargin(:, k) = [];
        flag.specific = true;
        flag.xmlHeader = true;
      elseif strcmpi(varargin{k}, '-comments')
        varargin(:, k) = [];
        flag.specific = true;
        flag.comments = true;
      elseif strcmpi(varargin{k}, '-weather')
        varargin(:, k) = [];
        flag.specific = true;
        flag.weather = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.length = true;
    flag.version = true;
    flag.numberOfSamples = true;
    flag.frequency = true;
    flag.startTime = true;
    flag.mvFactor = true;
    flag.gmtOffset = true;
    flag.originalFrequency = true;
    flag.aduSerialNumber = true;
    flag.adcSerialNumber = true;
    flag.channelNumber = true;
    flag.chopper = true;
    flag.channelType = true;
    flag.sensorType = true;
    flag.sensorSerialNumber = true;
    flag.posX1 = true;
    flag.posY1 = true;
    flag.posZ1 = true;
    flag.posX2 = true;
    flag.posY2 = true;
    flag.posZ2 = true;
    flag.dipoleLength = true;
    flag.angle = true;
    flag.probeRes = true;
    flag.dcOffset = true;
    flag.gainStage1 = true;
    flag.gainStage2 = true;
    flag.latitude = true;
    flag.longitude = true;
    flag.elevation = true;
    flag.latLongType = true;
    flag.addCoordType = true;
    flag.gaussRefMeridian = true;
    flag.hochwert = true;
    flag.rechtswert = true;
    flag.gpsStat = true;
    flag.gpsAccuracy = true;
    flag.utcOffset = true;
    flag.systemType = true;
    flag.surveyHeaderName = true;
    flag.measurementType = true;
    flag.dcOffsetCorrValue = true;
    flag.dcOffsetCorrOn = true;
    flag.inputDivOn = true;
    flag.notUsedVar = true;
    flag.selfTestResult = true;
    flag.reserved5 = true;
    flag.calFreqs = true;
    flag.calEntryLength = true;
    flag.calVersion = true;
    flag.calStartAddress = true;
    flag.lfFilters = true;
    flag.aduCalFilename = true;
    flag.aduCalTime = true;
    flag.sensorCalFilename = true;
    flag.sensorCalTime = true;
    flag.powerlineFreq1 = true;
    flag.powerlineFreq2 = true;
    flag.hfFilters = true;
    flag.originalMvFactor = true;
    flag.unsedVar = true;
    flag.boardType = true;
    flag.client = true;
    flag.contractor = true;
    flag.area = true;
    flag.surveyID = true;
    flag.operator = true;
    flag.reserved = true;
    flag.xmlHeader = true;
    flag.comments = true;
    flag.weather = true;
  elseif ~flag.specific
    flag.numberOfSamples = true;
    flag.frequency = true;
    flag.startTime = true;
    flag.mvFactor = true;
    flag.originalFrequency = true;
    flag.aduSerialNumber = true;
    flag.channelNumber = true;
    flag.channelType = true;
    flag.sensorType = true;
    flag.posX1 = true;
    flag.posY1 = true;
    flag.posZ1 = true;
    flag.posX2 = true;
    flag.posY2 = true;
    flag.posZ2 = true;
    flag.gainStage1 = true;
    flag.gainStage2 = true;
    flag.latitude = true;
    flag.longitude = true;
    flag.elevation = true;
    flag.systemType = true;
    flag.measurementType = true;
    flag.selfTestResult = true;
    flag.originalMvFactor = true;
    flag.boardType = true;
  end
  
  if isempty(files)
    files = ats(varargin{:});
  end
  
  if ~isempty(files)
    if nargout > 0
      for ii = 1 : length(files)
        varargout{1} = [varargout{1} buildHeader(files(ii), flag)];
      end
    else
      for ii = 1 : length(files)
        printHeader(files(ii), flag);
      end
    end
  end
end

function header = buildHeader(file, flag)
  header = [];
  
  if flag.length
    header.length = file.header.length;
  end
  
  if flag.version
    header.version = file.header.version;
  end
  
  if flag.numberOfSamples
    header.numberOfSamples = file.header.numberOfSamples;
  end
  
  if flag.frequency
    header.frequency = file.header.frequency;
  end
  
  if flag.startTime
    header.startTime = file.header.startTime;
  end
  
  if flag.mvFactor
    header.mvFactor = file.header.mvFactor;
  end
  
  if flag.gmtOffset
    header.gmtOffset = file.header.gmtOffset;
  end
  
  if flag.originalFrequency
    header.originalFrequency = file.header.originalFrequency;
  end
  
  if flag.aduSerialNumber
    header.aduSerialNumber = file.header.aduSerialNumber;
  end
  
  if flag.adcSerialNumber
    header.adcSerialNumber = file.header.adcSerialNumber;
  end
  
  if flag.channelNumber
    header.channelNumber = file.header.channelNumber;
  end
  
  if flag.chopper
    header.chopper = file.header.chopper;
  end
  
  if flag.channelType
    header.channelType = file.header.channelType;
  end
  
  if flag.sensorType
    header.sensorType = file.header.sensorType;
  end
  
  if flag.sensorSerialNumber
    header.sensorSerialNumber = file.header.sensorSerialNumber;
  end
  
  if flag.posX1
    header.posX1 = file.header.posX1;
  end
  
  if flag.posY1
    header.posY1 = file.header.posY1;
  end
  
  if flag.posZ1
    header.posZ1 = file.header.posZ1;
  end
  
  if flag.posX2
    header.posX2 = file.header.posX2;
  end
  
  if flag.posY2
    header.posY2 = file.header.posY2;
  end
  
  if flag.posZ2
    header.posZ2 = file.header.posZ2;
  end
  
  if flag.dipoleLength
    header.dipoleLength = file.header.dipoleLength;
  end
  
  if flag.angle
    header.angle = file.header.angle;
  end
  
  if flag.probeRes
    header.probeRes = file.header.probeRes;
  end
  
  if flag.dcOffset
    header.dcOffset = file.header.dcOffset;
  end
  
  if flag.gainStage1
    header.gainStage1 = file.header.gainStage1;
  end
  
  if flag.gainStage2
    header.gainStage2 = file.header.gainStage2;
  end
  
  if flag.latitude
    header.latitude = file.header.latitude;
  end
  
  if flag.longitude
    header.longitude = file.header.longitude;
  end
  
  if flag.elevation
    header.elevation = file.header.elevation;
  end
  
  if flag.latLongType
    header.latLongType = file.header.latLongType;
  end
  
  if flag.addCoordType
    header.addCoordType = file.header.addCoordType;
  end
  
  if flag.gaussRefMeridian
    header.gaussRefMeridian = file.header.gaussRefMeridian;
  end
  
  if flag.hochwert
    header.hochwert = file.header.hochwert;
  end
  
  if flag.rechtswert
    header.rechtswert = file.header.rechtswert;
  end
  
  if flag.gpsStat
    header.gpsStat = file.header.gpsStat;
  end
  
  if flag.gpsAccuracy
    header.gpsAccuracy = file.header.gpsAccuracy;
  end
  
  if flag.utcOffset
    header.utcOffset = file.header.utcOffset;
  end
  
  if flag.systemType
    header.systemType = file.header.systemType;
  end
  
  if flag.surveyHeaderName
    header.surveyHeaderName = file.header.surveyHeaderName;
  end
  
  if flag.measurementType
    header.measurementType = file.header.measurementType;
  end
  
  if flag.dcOffsetCorrValue
    header.dcOffsetCorrValue = file.header.dcOffsetCorrValue;
  end
  
  if flag.dcOffsetCorrOn
    header.dcOffsetCorrOn = file.header.dcOffsetCorrOn;
  end
  
  if flag.inputDivOn
    header.inputDivOn = file.header.inputDivOn;
  end
  
  if flag.notUsedVar
    header.notUsedVar = file.header.notUsedVar;
  end
  
  if flag.selfTestResult
    header.selfTestResult = file.header.selfTestResult;
  end
  
  if flag.reserved5
    header.reserved5 = file.header.reserved5;
  end
  
  if flag.calFreqs
    header.calFreqs = file.header.calFreqs;
  end
  
  if flag.calEntryLength
    header.calEntryLength = file.header.calEntryLength;
  end
  
  if flag.calVersion
    header.calVersion = file.header.calVersion;
  end
  
  if flag.calStartAddress
    header.calStartAddress = file.header.calStartAddress;
  end
  
  if flag.lfFilters
    header.lfFilters = file.header.lfFilters;
  end
  
  if flag.aduCalFilename
    header.aduCalFilename = file.header.aduCalFilename;
  end
  
  if flag.aduCalTime
    header.aduCalTime = file.header.aduCalTime;
  end
  
  if flag.sensorCalFilename
    header.sensorCalFilename = file.header.sensorCalFilename;
  end
  
  if flag.sensorCalTime
    header.sensorCalTime = file.header.sensorCalTime;
  end
  
  if flag.powerlineFreq1
    header.powerlineFreq1 = file.header.powerlineFreq1;
  end
  
  if flag.powerlineFreq2
    header.powerlineFreq2 = file.header.powerlineFreq2;
  end
  
  if flag.hfFilters
    header.hfFilters = file.header.hfFilters;
  end
  
  if flag.originalMvFactor
    header.originalMvFactor = file.header.originalMvFactor;
  end
  
  if flag.unsedVar
    header.unsedVar = file.header.unsedVar;
  end
  
  if flag.boardType
    header.boardType = file.header.boardType;
  end
  
  if flag.client
    header.client = file.header.client;
  end
  
  if flag.contractor
    header.contractor = file.header.contractor;
  end
  
  if flag.area
    header.area = file.header.area;
  end
  
  if flag.surveyID
    header.surveyID = file.header.surveyID;
  end
  
  if flag.operator
    header.operator = file.header.operator;
  end
  
  if flag.reserved
    header.reserved = file.header.reserved;
  end
  
  if flag.xmlHeader
    header.xmlHeader = file.header.xmlHeader;
  end
  
  if flag.comments
    header.comments = file.header.comments;
  end
  
  if flag.weather
    header.weather = file.header.weather;
  end
end

function printHeader(file, flag)
  name = cell(0);
  value = cell(0);
  
  if flag.length
    append_d('length:', file.header.length, flag.raw);
  end
  
  if flag.version
    append_d('version:', file.header.version, flag.raw);
  end
  
  if flag.numberOfSamples
    append_d('number of samples:', file.header.numberOfSamples, flag.raw);
  end
  
  if flag.frequency
    append_f('frequency:', file.header.frequency, 4, flag.raw);
  end
  
   if flag.startTime
     if flag.raw
       append('start time:', sprintf('%d', file.header.startTime));
     else
       append('start time:', datetime2string(posix2datetime(file.header.startTime)));
     end
   end
   
   if flag.mvFactor
     append_e('mv factor:', file.header.mvFactor, 8, flag.raw);
   end
   
   if flag.gmtOffset
     append_d('gmt offset:', file.header.gmtOffset, flag.raw);
   end
   
   if flag.originalFrequency
     append_f('original frequency:', file.header.originalFrequency, 4, flag.raw);
   end
   
   if flag.aduSerialNumber
     append_d('adu serial number:', file.header.aduSerialNumber, flag.raw);
   end
   
   if flag.adcSerialNumber
     append_d('adc serial number:', file.header.adcSerialNumber, flag.raw);
   end
   
   if flag.channelNumber
     append_d('channel number:', file.header.channelNumber, flag.raw);
   end
   
   if flag.chopper
     append_d('chopper:', file.header.chopper, flag.raw);
   end
   
   if flag.channelType
     append_s('channel type:', file.header.channelType);
   end
   
   if flag.sensorType
     append_s('sensor type:', file.header.sensorType);
   end
   
   if flag.sensorSerialNumber
     append_d('sensor serial number:', file.header.sensorSerialNumber, flag.raw);
   end
   
   if flag.posX1
     append_f('pos x1:', file.header.posX1, 4, flag.raw);
   end
   
   if flag.posY1
     append_f('pos y1:', file.header.posY1, 4, flag.raw);
   end
   
   if flag.posZ1
     append_f('pos z1:', file.header.posZ1, 4, flag.raw);
   end
   
   if flag.posX2
     append_f('pos x2:', file.header.posX2, 4, flag.raw);
   end
   
   if flag.posY2
     append_f('pos y2:', file.header.posY2, 4, flag.raw);
   end
   
   if flag.posZ2
     append_f('pos z2:', file.header.posZ2, 4, flag.raw);
   end
   
   if flag.dipoleLength
     append_f('dipole length:', file.header.dipoleLength, 4, flag.raw);
   end
   
   if flag.angle
     append_f('angle:', file.header.angle, 4, flag.raw);
   end
   
   if flag.probeRes
     append_f('probe res:', file.header.probeRes, 4, flag.raw);
   end
   
   if flag.dcOffset
     append_f('dc offset:', file.header.dcOffset, 4, flag.raw);
   end
   
   if flag.gainStage1
     append_f('gain stage1:', file.header.gainStage1, 4, flag.raw);
   end
   
   if flag.gainStage2
     append_f('gain stage2:', file.header.gainStage2, 4, flag.raw);
   end
   
   if flag.latitude
     if flag.raw
       append('latitude:', sprintf('%d', file.header.latitude));
     else
       append('latitude:', latitude2string(file.header.latitude));
     end
   end
   
   if flag.longitude
     if flag.raw
       append('longitude:', sprintf('%d', file.header.longitude));
     else
       append('longitude:', longitude2string(file.header.longitude));
     end
   end
   
   if flag.elevation
     if flag.raw
       append('elevation:', sprintf('%d', file.header.elevation));
     else
       append('elevation:', elevation2string(file.header.elevation));
     end
   end
   
   if flag.latLongType
     append_c('lat long type:', file.header.latLongType);
   end
   
   if flag.addCoordType
     append_d('add coord type:', file.header.addCoordType, flag.raw);
   end
   
   if flag.gaussRefMeridian
     append_d('gauss ref meridian:', file.header.gaussRefMeridian, flag.raw);
   end
   
   if flag.hochwert
     append_e('hochwert:', file.header.hochwert, 8, flag.raw);
   end
   
   if flag.rechtswert
     append_e('rechtswert:', file.header.rechtswert, 8, flag.raw);
   end
   
   if flag.gpsStat
     append_c('gps stat:', file.header.gpsStat);
   end
   
   if flag.gpsAccuracy
     append_d('gps accuracy:', file.header.gpsAccuracy, flag.raw);
   end
   
   if flag.utcOffset
     append_d('utc offset:', file.header.utcOffset, flag.raw);
   end
   
   if flag.systemType
     append_s('system type:', file.header.systemType);
   end
   
   if flag.surveyHeaderName
     append_s('survey header name:', file.header.surveyHeaderName);
   end
   
   if flag.measurementType
     append_s('measurement type:', file.header.measurementType);
   end
   
   if flag.dcOffsetCorrValue
     append_e('dc offset corr value:', file.header.dcOffsetCorrValue, 8, flag.raw);
   end
   
   if flag.dcOffsetCorrOn
     append_d('dc offset corr on:', file.header.dcOffsetCorrOn, flag.raw);
   end
   
   if flag.inputDivOn
     append_d('input div on:', file.header.inputDivOn, flag.raw);
   end
   
   if flag.notUsedVar
     append_d('not used var:', file.header.notUsedVar, flag.raw);
   end
   
   if flag.selfTestResult
     append_s('self test result:', file.header.selfTestResult);
   end
   
   if flag.reserved5
     append_s('reserved5:', file.header.reserved5);
   end
   
   if flag.calFreqs
     append_d('cal freqs:', file.header.calFreqs, flag.raw);
   end
   
   if flag.calEntryLength
     append_d('cal entry length:', file.header.calEntryLength, flag.raw);
   end
   
   if flag.calVersion
     append_d('cal version:', file.header.calVersion, flag.raw);
   end
   
   if flag.calStartAddress
     append_d('cal start address:', file.header.calStartAddress, flag.raw);
   end
   
   if flag.lfFilters
     append_s('lf filters:', file.header.lfFilters);
   end
   
   if flag.aduCalFilename
     append_s('adu cal filename:', file.header.aduCalFilename);
   end
   
   if flag.aduCalTime
     append_d('adu cal time:', file.header.aduCalTime, flag.raw);
   end
   
   if flag.sensorCalFilename
     append_s('sensor cal filename:', file.header.sensorCalFilename);
   end
   
   if flag.sensorCalTime
     append_d('sensor cal time:', file.header.sensorCalTime, flag.raw);
   end
   
   if flag.powerlineFreq1
     append_f('powerline freq1:', file.header.powerlineFreq1, 4, flag.raw);
   end
   
   if flag.powerlineFreq2
     append_f('powerline freq2:', file.header.powerlineFreq2, 4, flag.raw);
   end
   
   if flag.hfFilters
     append_s('hf filters:', file.header.hfFilters);
   end
   
   if flag.originalMvFactor
     append_e('original mv factor:', file.header.originalMvFactor, 8, flag.raw);
   end
   
   if flag.unsedVar
     append_d('unsed var:', file.header.unsedVar, flag.raw);
   end
   
   if flag.boardType
     append_s('board type:', file.header.boardType);
   end
   
   if flag.client
     append_s('client:', file.header.client);
   end
   
   if flag.contractor
     append_s('contractor:', file.header.contractor);
   end
   
   if flag.area
     append_s('area:', file.header.area);
   end
   
   if flag.surveyID
     append_s('survey id:', file.header.surveyID);
   end
   
   if flag.operator
     append_s('operator:', file.header.operator);
   end
   
   if flag.reserved
     append_s('reserved:', file.header.reserved);
   end
   
   if flag.xmlHeader
     append_s('xml header:', file.header.xmlHeader);
   end
   
   if flag.comments
     append_s('comments:', file.header.comments);
   end
   
   if flag.weather
     append_s('weather:', file.header.weather);
   end
  
  width = 0;
  
  for ii = 1 : length(name)
    if width < length(name{ii})
      width = length(name{ii});
    end
  end
  
  for ii = 1 : length(name)
    if length(name{ii}) < width
      name{ii} = sprintf(sprintf('%%s%%%ds', width - length(name{ii})), name{ii}, ' ');
    end
  end
  
  for ii = 1 : length(name)
    fprintf('%s %s\n', name{ii}, value{ii});
  end
  
  function append(n, v)
    name{length(name) + 1} = n;
    value{length(value) + 1} = v;
  end
  
  function append_d(n, v, r)
    if r || v < 1000
      append(n, sprintf('%d', v));
    else
      append(n, thousandsep(v, 0));
    end
  end
  
  function append_f(n, v, d, r)
    if r
      append(n, sprintf('%f', v));
    elseif v == fix(v)
      append(n, thousandsep(v, 0));
    else
      append(n, thousandsep(v, d));
    end
  end
  
  function append_e(n, v, d, r)
    if r
      append(n, sprintf('%e', v));
    elseif v == fix(v)
      append(n, thousandsep(v, 0));
    else
      append(n, thousandsep(v, d));
    end
  end
  
  function append_c(n, v)
    append(n, sprintf('%c', v));
  end
  
  function append_s(n, v)
    append(n, v);
  end
end