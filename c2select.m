function c2files = c2select(args, sites, bands, ex, ey, hx, hy, hz, nonrobust)
  c2files = cell(0);
  
  for ii = 1 : length(args)
    files = c2find(args{ii});
    
    if ~isempty(files)
      for jj = 1 : length(files)
        duplicated = false;
        
        for kk = 1 : length(c2files)
          if strcmpi(sprintf('%s%s%s', files(jj).folder, filesep, files(jj).name), sprintf('%s%s%s', c2files{kk}.folder, filesep, c2files{kk}.name))
            duplicated = true;
            break;
          end
        end
        
        if ~duplicated
          siteflag = true;
          bandflag = true;
          
          if ~isempty(sites)
            siteflag = false;
            
            for kk = 1 : length(sites)
              if strcmpi(files(jj).site, sites{kk})
                siteflag = true;
                break;
              end
            end
          end
          
          if ~isempty(bands)
            bandflag = false;
            
            for kk = 1 : length(bands)
              if strcmpi(files(jj).frequencyBand, bands{kk})
                bandflag = true;
                break;
              end
            end
          end
          
          if siteflag && bandflag
            ignore = true;
            
            if files(jj).stage == 1
              if files(jj).input == 1
                if hx
                  ignore = false;
                end
              elseif files(jj).input == 2
                if hy
                  ignore = false;
                end
              end
            elseif files(jj).stage == 2
              if files(jj).output == 1
                if ex
                  ignore = false;
                end
              elseif files(jj).output == 2
                if ey
                  ignore = false;
                end
              elseif files(jj).output == 3
                if hz
                  ignore = false;
                end
              end
            end
            
            if ~ignore
              ignore = true;
              
              if nonrobust
                if files(jj).nonrobust
                  ignore = false;
                end
              else
                if files(jj).bounded
                  ignore = false;
                end
              end
              
              if ~ignore
                c2files{length(c2files) + 1} = files(jj);
              end
            end
          end
        end
      end
    end
  end
end