function value = floatattribute(scope, name, defaultValue, varargin)
  value = [];
  
  dname = [];
  fname = [];
  
  if ~isempty(name)
    if strcmpi(scope, 'survey')
      currentSurvey = survey();
      
      if ~isempty(currentSurvey)
        dname = sprintf('%s%s.mt', currentSurvey.folder, filesep);
      end
    elseif strcmpi(scope, 'site')
      currentSite = site();
      
      if ~isempty(currentSite)
        dname = sprintf('%s%s.mt', currentSite.folder, filesep);
      end
    elseif strcmpi(scope, 'band')
      currentBand = band();
      
      if ~isempty(currentBand)
        dname = sprintf('%s%s.mt', currentBand.folder, filesep);
      end
    elseif strcmpi(scope, 'local')
      currentLocal = local();
      
      if ~isempty(currentLocal)
        dname = '.mt';
      end
    elseif strcmpi(scope, 'global')
      dname = mfilename('fullpath');
      dname = dname(1 : length(dname) - length(mfilename()));
      
      if length(dname) > 1 && strcmp(dname(length(dname)), filesep)
        dname = dname(1 : length(dname) - 1);
      end
      
      dname = sprintf('%s%s.mt', dname, filesep);
    end
    
    if ~isempty(dname)
      fname = sprintf('%s%s%s', dname, filesep, name);
    end
  end
  
  if ~isempty(fname)
    value = defaultValue;
    
    if ~isempty(varargin)
      v = value;
      
      if ischar(varargin{1}) || isstring(varargin{1})
        v = str2double(varargin{1});
      elseif isnumeric(varargin{1})
        v = varargin{1};
      end
      
      if ~isnan(v)
        value = v;
      end
      
      [mtdir, ~] = mkdir(dname);
      
      if mtdir
        fp = fopen(fname, 'wt');
        
        if fp ~= -1
          fprintf(fp, '%.12f\n', value);
          fclose(fp);
        end
      end
    else
      fp = fopen(fname, 'rt');
      
      if fp ~= -1
        v = fgetl(fp);
        
        if ischar(v) || isstring(v)
          v = str2double(v);
          
          if ~isnan(v)
            value = v;
          end
        end
        
        fclose(fp);
      end
    end
  end
end