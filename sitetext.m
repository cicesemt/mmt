function [varargout] = sitetext(varargin)
  currentSite = [];
  path = [];
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSite = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-path')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          path = varargin{k};
          varargin(:, k) = [];
        end
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSite)
    if isempty(path)
      path = pwd();
    end
    
    currentSite = site(path);
  end
  
  if ~isempty(currentSite)
    if nargout > 0
      varargout{1} = info(currentSite, varargin{:});
    else
      info(currentSite, varargin{:});
    end
  end
end