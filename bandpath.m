function bandpath(varargin)
  currentBand = [];
  
  flag.depth = 2;
  flag.default = false;
  flag.long = false;
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentBand = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-survey')
        varargin(:, k) = [];
        flag.depth = 0;
      elseif strcmpi(varargin{k}, '-site')
        varargin(:, k) = [];
        flag.depth = 1;
      elseif strcmpi(varargin{k}, '-band')
        varargin(:, k) = [];
        flag.depth = 2;
      elseif strcmpi(varargin{k}, '-channel')
        varargin(:, k) = [];
        flag.depth = 3;
      elseif strcmpi(varargin{k}, '-format')
        varargin(:, k) = [];
        
        if k <= length(varargin)
          format = varargin{k};
          varargin(:, k) = [];
          
          if strcmpi(format, 'default')
            flag.default = true;
          elseif strcmpi(format, 'long')
            flag.long = true;
          end
        end
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentBand)
    currentBand = band(varargin{:});
  end
  
  if ~isempty(currentBand)
    if flag.default || flag.long
      formatBand(currentBand, flag);
    else
      printBand(currentBand, flag);
    end
  end
end

function formatBand(currentBand, flag)
  if flag.default
    long2default(currentBand);
  elseif flag.long
    default2long(currentBand);
  end
end

function printBand(currentBand, flag)
  if flag.depth >= 0
    [parentPath, bandName] = fileparts(currentBand.folder);
    [parentPath, siteName] = fileparts(parentPath);
    [~, surveyName] = fileparts(parentPath);
    
    fprintf('%s\n', surveyName);
    
    if flag.depth >= 1
      upAndRight = 9492;
      horizontal = 9472;
      
      fprintf('%c%c %s\n', upAndRight, horizontal, siteName);
      
      if flag.depth >= 2
        fprintf('   %c%c %s\n', upAndRight, horizontal, bandName);
        
        for ii = 1 : length(currentBand.channels)
          printChannel(currentBand.channels(ii), ii == length(currentBand.channels), flag);
        end
      end
    end
  end
end

function printChannel(currentChannel, lastChannel, flag)
  if flag.depth >= 3
    name = currentChannel.name;
    
    upAndRight = 9492;
    verticalAndRight = 9500;
    horizontal = 9472;
    
    if lastChannel
      right = upAndRight;
    else
      right = verticalAndRight;
    end
    
    fprintf('      %c%c %s\n', right, horizontal, name);
  end
end

function default2long(currentBand)
  [path, name] = fileparts(currentBand.folder);
  
  if length(name) == length('meas_YYYY-MM-DD_HH-MM-SS')
    meas = name(1 : 4);
    
    if strcmpi(meas, 'meas')
      u0 = name( 5 :  5);
      u1 = name(16 : 16);
      
      if strcmp(u0, '_') && strcmp(u1, '_')
        h0 = name(10 : 10);
        h1 = name(13 : 13);
        h2 = name(19 : 19);
        h3 = name(22 : 22);
        
        if strcmp(h0, '-') && strcmp(h1, '-') && strcmp(h2, '-') && strcmp(h3, '-')
          year   = str2double(name( 6 :  9));
          month  = str2double(name(11 : 12));
          day    = str2double(name(14 : 15));
          hour   = str2double(name(17 : 18));
          minute = str2double(name(20 : 21));
          second = str2double(name(23 : 24));
          
          if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute) && ~isnan(second)
            year   = name( 6 :  9);
            month  = name(11 : 12);
            day    = name(14 : 15);
            hour   = name(17 : 18);
            minute = name(20 : 21);
            second = name(23 : 24);
            
            source = currentBand.folder;
            target = sprintf('%s%s%s_%s-%s-%s_%s-%s-%s_%s', path, filesep, meas, year, month, day, hour, minute, second, currentBand.name);
            
            if strcmp(source, pwd())
              changedirectory = true;
            else
              changedirectory = false;
            end
            
            if movefile(source, target) && changedirectory
              cd(target);
            end
          end
        end
      end
    end
  end
end

function long2default(currentBand)
  [path, name] = fileparts(currentBand.folder);
  
  if length(name) >= length('meas_YYYY-MM-DD_HH-MM-SS_?U')
    meas = name(1 : 4);
    
    if strcmpi(meas, 'meas')
      u0 = name( 5 :  5);
      u1 = name(16 : 16);
      u2 = name(25 : 25);
      
      if strcmp(u0, '_') && strcmp(u1, '_') && strcmp(u2, '_')
        h0 = name(10 : 10);
        h1 = name(13 : 13);
        h2 = name(19 : 19);
        h3 = name(22 : 22);
        
        measurementUnit = name(length(name) : length(name));
        
        if strcmp(h0, '-') && strcmp(h1, '-') && strcmp(h2, '-') && strcmp(h3, '-') && (strcmpi(measurementUnit, 'H') || strcmpi(measurementUnit, 'S'))
          year   = str2double(name( 6 :  9));
          month  = str2double(name(11 : 12));
          day    = str2double(name(14 : 15));
          hour   = str2double(name(17 : 18));
          minute = str2double(name(20 : 21));
          second = str2double(name(23 : 24));
          
          samplingRate = str2double(name(26 : length(name) - 1));
          
          if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute) && ~isnan(second) && ~isnan(samplingRate)
            if samplingRate == currentBand.samplingRate && measurementUnit == currentBand.measurementUnit
              year   = name( 6 :  9);
              month  = name(11 : 12);
              day    = name(14 : 15);
              hour   = name(17 : 18);
              minute = name(20 : 21);
              second = name(23 : 24);
              
              source = currentBand.folder;
              target = sprintf('%s%s%s_%s-%s-%s_%s-%s-%s', path, filesep, meas, year, month, day, hour, minute, second);
              
              if strcmp(source, pwd())
                changedirectory = true;
              else
                changedirectory = false;
              end
              
              if movefile(source, target) && changedirectory
                cd(target);
              end
            end
          end
        end
      end
    end
  end
end