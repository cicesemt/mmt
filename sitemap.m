function sitemap(varargin)
  currentSite = [];
  currentSurvey = [];
  
  if ~isempty(varargin)
    currentSite = site(varargin{:});
    
    if ~isempty(currentSite)
      currentSurvey = survey(fileparts(currentSite.folder));
    end
  else
    currentSurvey = survey();
    
    if ~isempty(currentSurvey)
      currentSite = currentSurvey.currentSite;
    end
  end
  
  if ~isempty(currentSurvey)
    if ~isempty(currentSite)
      [parentFolder, ~] = fileparts(currentSite.folder);
      [~, surveyName] = fileparts(parentFolder);
      
      matches = struct(...
        'name', {}, ...
        'folder', {}, ...
        'bytes', {}, ...
        'numberOfSamples', {}, ...
        'latitude', {}, ...
        'longitude', {}, ...
        'elevation', {}, ...
        'startTime', {}, ...
        'stopTime', {}, ...
        'bands', {}, ...
        'currentBand', {} ...
      );
      
      for ii = 1 : length(currentSurvey.sites)
        if ~strcmp(currentSite.folder, currentSurvey.sites(ii).folder)
          if currentSite.startTime < currentSurvey.sites(ii).stopTime
            if currentSite.stopTime > currentSurvey.sites(ii).startTime
              matches(length(matches) + 1) = currentSurvey.sites(ii);
            end
          end
        end
      end
      
      minlat = currentSite.latitude  - (0.05 * (1000*60*60));
      maxlat = currentSite.latitude  + (0.05 * (1000*60*60));
      minlon = currentSite.longitude - (0.05 * (1000*60*60));
      maxlon = currentSite.longitude + (0.05 * (1000*60*60));
      
      minlat = minlat / (1000*60*60);
      maxlat = maxlat / (1000*60*60);
      minlon = minlon / (1000*60*60);
      maxlon = maxlon / (1000*60*60);
      
      zoomFactor = 0.25;
      
      while true
        latinc = (maxlat - minlat) * zoomFactor * 0.5;
        loninc = (maxlon - minlon) * zoomFactor * 0.5;
        
        if minlat - latinc > -90 && maxlat + latinc < 90
          if minlon - loninc > -180 && maxlon + loninc < 180
            minlat = minlat - latinc;
            maxlat = maxlat + latinc;
            minlon = minlon - loninc;
            maxlon = maxlon + loninc;
            
            if maxlat - minlat > 0.1 || maxlon - minlon > 0.1
              break;
            end
          else
            break;
          end
        else
          break;
        end
      end
      
      latlim = [minlat maxlat];
      lonlim = [minlon maxlon];
      
      f = figure();
      f.set('NumberTitle', 'off');
      f.set('ToolBar', 'none');
      f.set('MenuBar', 'none');
      f.set('Name', surveyName);
      
      f.UserData.survey = currentSurvey;
      f.UserData.site = currentSite;
      f.UserData.matches = matches;
      f.UserData.latlim = latlim;
      f.UserData.lonlim = lonlim;
      f.UserData.zoomFactor = zoomFactor;
      
      show(f);
      
      set(f, 'WindowScrollWheelFcn', @WindowScrollWheelFcn);
    end
  end
end

function show(figure)
  cla(figure.Children);
  
  survey = figure.UserData.survey;
  site = figure.UserData.site;
  matches = figure.UserData.matches;
  latlim = figure.UserData.latlim;
  lonlim = figure.UserData.lonlim;
  
  load coastlines coastlat coastlon;
  
  a = worldmap(latlim, lonlim);
  
  a.FontSize = 10;
  a.FontWeight = 'normal';
  
  a.Title.String = 'Map';
  a.Title.FontSize = 14;
  a.Title.FontWeight = 'bold';
  
  setm(a, 'FFaceColor', 'white');
  plotm(coastlat, coastlon, 'Color', 'k');
  
  if latlim(2) - latlim(1) < 0.05 && lonlim(2) - lonlim(1) < 0.05
    showText = true;
  else
    showText = false;
  end
  
  name = site.name;
  lat = site.latitude / (1000*60*60);
  lon = site.longitude / (1000*60*60);
  
  color = [0.0000 0.4470 0.7410];
  markerFaceColor = [0.0000 0.4470 0.7410];
  textColor = [0.5 0.5 0.5];
  
  p = plotm(lat, lon, 'Color', color, 'LineStyle', 'none', 'Marker', 'o', 'MarkerFaceColor', markerFaceColor, 'DisplayName', name);
  
  if showText
    textm(lat, lon, sprintf('  %s', name), 'Color', textColor);
  end
  
  p.UserData.local = true;
  p.UserData.remote = false;
  
  for ii = 1 : length(matches)
    name = matches(ii).name;
    lat = matches(ii).latitude / (1000*60*60);
    lon = matches(ii).longitude / (1000*60*60);
    
    color = [0.0000 0.4470 0.7410];
    markerFaceColor = 'none';
    textColor = [0.5 0.5 0.5];
    
    p = plotm(lat, lon, 'Color', color, 'LineStyle', 'none', 'Marker', 'o', 'MarkerFaceColor', markerFaceColor, 'DisplayName', name);
    
    if showText
      textm(lat, lon, sprintf('  %s', name), 'Color', textColor);
    end
    
    p.UserData.local = false;
    p.UserData.remote = true;
  end
  
  for ii = 1 : length(survey.sites)
    ignore = false;
    
    if site.latitude == survey.sites(ii).latitude && site.longitude == survey.sites(ii).longitude
      ignore = true;
    else
      for jj = 1 : length(matches)
        if matches(jj).latitude == survey.sites(ii).latitude && matches(jj).longitude == survey.sites(ii).longitude
          ignore = true;
          break;
        end
      end
    end
    
    if ~ignore
      name = survey.sites(ii).name;
      lat = survey.sites(ii).latitude / (1000*60*60);
      lon = survey.sites(ii).longitude / (1000*60*60);
      
      color = [0.5 0.5 0.5];
      markerFaceColor = 'none';
      textColor = [0.75 0.75 0.75];
      
      p = plotm(lat, lon, 'Color', color, 'LineStyle', 'none', 'Marker', 'o', 'MarkerFaceColor', markerFaceColor, 'DisplayName', name);
      
      if showText
        textm(lat, lon, sprintf('  %s', name), 'Color', textColor);
      end
      
      p.UserData.local = false;
      p.UserData.remote = false;
    end
  end
  
  for ii = 1 : length(figure.Children.Children)
    set(figure.Children.Children(ii), 'ButtonDownFcn', @ButtonDownFcn);
    
    if strcmp(figure.Children.Children(ii).Type, 'line') && strcmp(figure.Children.Children(ii).LineStyle, 'none')
      if figure.Children.Children(ii).UserData.local
        figure.Children.Children(ii).Annotation.LegendInformation.IconDisplayStyle = 'on';
      elseif figure.Children.Children(ii).UserData.remote
        figure.Children.Children(ii).Annotation.LegendInformation.IconDisplayStyle = 'on';
      else
        figure.Children.Children(ii).Annotation.LegendInformation.IconDisplayStyle = 'off';
      end
    elseif ~strcmp(figure.Children.Children(ii).Type, 'text')
      figure.Children.Children(ii).Annotation.LegendInformation.IconDisplayStyle = 'off';
    end
  end
  
  legend();
end

function repaint(figure)
  legend('off');
  show(figure);
end

function WindowScrollWheelFcn(figure, event)
  if event.VerticalScrollCount ~= 0
    for ii = 1 : length(figure.Children)
      if strcmp(figure.Children(ii).Type, 'axes')
        axes = figure.Children(ii);
        
        currentPoint = axes.CurrentPoint;

        lat = currentPoint(1, 1);
        lon = currentPoint(1, 2);

        [lat, lon] = minvtran(lat, lon);
        
        latlim = getm(axes, 'MapLatLimit');
        lonlim = getm(axes, 'MapLonLimit');
        
        if lat > latlim(1) && lat < latlim(2)
          if lon > lonlim(1) && lon < lonlim(2)
            clat = latlim(1) + ((latlim(2) - latlim(1)) / 2);
            clon = lonlim(1) + ((lonlim(2) - lonlim(1)) / 2);
            
            flat = (lat - clat) / ((latlim(2) - latlim(1)) / 2);
            flon = (lon - clon) / ((lonlim(2) - lonlim(1)) / 2);
            
            if event.VerticalScrollCount < 0
              zoomIn(figure, axes, flat, flon);
            elseif event.VerticalScrollCount > 0
              zoomOut(figure, axes, flat, flon);
            end
          end
        end
        
        break;
      end
    end
  end
end

function zoomIn(figure, axes, flat, flon)
  latlim = getm(axes, 'MapLatLimit');
  lonlim = getm(axes, 'MapLonLimit');
  
  minlat = latlim(1);
  maxlat = latlim(2);
  minlon = lonlim(1);
  maxlon = lonlim(2);
  
  zoomFactor = figure.UserData.zoomFactor;
  
  if flat >= 0
    latinc = ((maxlat - minlat) / (1 + zoomFactor)) * zoomFactor * ( 0.5 * flat + 0.5);
    latdec = ((maxlat - minlat) / (1 + zoomFactor)) * zoomFactor * (-0.5 * flat + 0.5);
  else
    latinc = ((maxlat - minlat) / (1 + zoomFactor)) * zoomFactor * (-0.5 * -flat + 0.5);
    latdec = ((maxlat - minlat) / (1 + zoomFactor)) * zoomFactor * ( 0.5 * -flat + 0.5);
  end
  
  if flon >= 0
    loninc = ((maxlon - minlon) / (1 + zoomFactor)) * zoomFactor * ( 0.5 * flon + 0.5);
    londec = ((maxlon - minlon) / (1 + zoomFactor)) * zoomFactor * (-0.5 * flon + 0.5);
  else
    loninc = ((maxlon - minlon) / (1 + zoomFactor)) * zoomFactor * (-0.5 * -flon + 0.5);
    londec = ((maxlon - minlon) / (1 + zoomFactor)) * zoomFactor * ( 0.5 * -flon + 0.5);
  end
  
  if minlat + latinc < maxlat - latdec
    if minlon + loninc < maxlon - londec
      minlat = minlat + latinc;
      maxlat = maxlat - latdec;
      minlon = minlon + loninc;
      maxlon = maxlon - londec;
      
      figure.UserData.latlim = [minlat maxlat];
      figure.UserData.lonlim = [minlon maxlon];
      
      repaint(figure);
    end
  end
end

function zoomOut(figure, axes, flat, flon)
  latlim = getm(axes, 'MapLatLimit');
  lonlim = getm(axes, 'MapLonLimit');
  
  minlat = latlim(1);
  maxlat = latlim(2);
  minlon = lonlim(1);
  maxlon = lonlim(2);
  
  zoomFactor = figure.UserData.zoomFactor;
  
  if flat >= 0
    latinc = (maxlat - minlat) * zoomFactor * (-0.5 * flat + 0.5);
    latdec = (maxlat - minlat) * zoomFactor * ( 0.5 * flat + 0.5);
  else
    latinc = (maxlat - minlat) * zoomFactor * ( 0.5 * -flat + 0.5);
    latdec = (maxlat - minlat) * zoomFactor * (-0.5 * -flat + 0.5);
  end
  
  if flon >= 0
    loninc = (maxlon - minlon) * zoomFactor * (-0.5 * flon + 0.5);
    londec = (maxlon - minlon) * zoomFactor * ( 0.5 * flon + 0.5);
  else
    loninc = (maxlon - minlon) * zoomFactor * ( 0.5 * -flon + 0.5);
    londec = (maxlon - minlon) * zoomFactor * (-0.5 * -flon + 0.5);
  end
  
  if minlat - latdec > -90 && maxlat + latinc < 90
    if minlon - londec > -180 && maxlon + loninc < 180
      minlat = minlat - latdec;
      maxlat = maxlat + latinc;
      minlon = minlon - londec;
      maxlon = maxlon + loninc;
      
      figure.UserData.latlim = [minlat maxlat];
      figure.UserData.lonlim = [minlon maxlon];
      
      repaint(figure);
    end
  end
end

function ButtonDownFcn(object, ~)
  axes = object.Parent;
  figure = axes.Parent;
  
  if strcmp(object.Type, 'line') && strcmp(object.LineStyle, 'none')
    lat = object.XData;
    lon = object.YData;
    
    [lat, lon] = minvtran(lat, lon);
    
    objectName = textm(lat, lon, sprintf('  %s', object.DisplayName), 'Color', [0.635 0.078 0.184]);
  else
    objectName = [];
  end
  
  lat = axes.CurrentPoint(1, 1);
  lon = axes.CurrentPoint(1, 2);
  
  [lat1, lon1] = minvtran(lat, lon);
  
  dragLine = plotm([lat1 lat1], [lon1 lon1], 'Color', [0.75 0.75 0.75], 'LineStyle', ':', 'Marker', '.');
  dragLine.Annotation.LegendInformation.IconDisplayStyle = 'off';
  
  figure.WindowButtonMotionFcn = @WindowButtonMotionFcn;
  figure.WindowButtonUpFcn = @WindowButtonUpFcn;
  
  function WindowButtonMotionFcn(~, ~)
    delete(dragLine);
    
    lat = axes.CurrentPoint(1, 1);
    lon = axes.CurrentPoint(1, 2);
    
    [lat2, lon2] = minvtran(lat, lon);
    
    dragLine = plotm([lat1 lat2], [lon1 lon2], 'Color', [0.75 0.75 0.75], 'LineStyle', ':', 'Marker', '.');
    dragLine.Annotation.LegendInformation.IconDisplayStyle = 'off';
  end
  
  function WindowButtonUpFcn(~, ~)
    figure.WindowButtonMotionFcn = [];
    figure.WindowButtonUpFcn = [];
    
    delete(dragLine);
    
    if ~isempty(objectName)
      delete(objectName);
    end
    
    lat = axes.CurrentPoint(1, 1);
    lon = axes.CurrentPoint(1, 2);
    
    [lat2, lon2] = minvtran(lat, lon);
    
    if lat1 ~= lat2 || lon1 ~= lon2
      latlim = figure.UserData.latlim;
      lonlim = figure.UserData.lonlim;
      
      minlat = latlim(1) - (lat2 - lat1);
      maxlat = latlim(2) - (lat2 - lat1);
      minlon = lonlim(1) - (lon2 - lon1);
      maxlon = lonlim(2) - (lon2 - lon1);
      
      figure.UserData.latlim = [minlat maxlat];
      figure.UserData.lonlim = [minlon maxlon];
      
      repaint(figure);
    end
  end
end