function folder = atsfolder(filename)
  folder = [];
  
  name = [];
  parent = [];
  path = [];
  
  file = dir(filename);
  
  if length(file) == 1 && file.isdir
    path = file.folder;
  else
    for ii = 1 : length(file)
      f = file(ii);
      
      if strcmp(f.name, '.') && f.isdir
        path = f.folder;
        break;
      end
    end
  end
  
  if ~isempty(path)
    k = strfind(path, filesep);
    
    if ~isempty(k)
      name = path(k(length(k)) + 1 : length(path));
      
      if length(k) > 1
        parent = path(1 : k(length(k)) - 1);
      else
        parent = path(1 : k(length(k)));
      end
    end
    
    folder.name = name;
    folder.parent = parent;
    folder.path = path;
    folder.defaultFormat = false;
    folder.longFormat = false;
    
    if ~isempty(name)
      if length(name) == length('meas_YYYY-MM-DD_HH-MM-SS')
        meas = name(1 : 4);
        
        if strcmpi(meas, 'meas')
          u0 = name( 5 :  5);
          u1 = name(16 : 16);
          
          if strcmp(u0, '_') && strcmp(u1, '_')
            h0 = name(10 : 10);
            h1 = name(13 : 13);
            h2 = name(19 : 19);
            h3 = name(22 : 22);
            
            if strcmp(h0, '-') && strcmp(h1, '-') && strcmp(h2, '-') && strcmp(h3, '-')
              year = str2double(name(6 : 9));
              month = str2double(name(11 : 12));
              day = str2double(name(14 : 15));
              hour = str2double(name(17 : 18));
              minute = str2double(name(20 : 21));
              second = str2double(name(23 : 24));
              
              if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute) && ~isnan(second)
                folder.defaultFormat = true;
                folder.meas = meas;
                folder.year = year;
                folder.month = month;
                folder.day = day;
                folder.hour = hour;
                folder.minute = minute;
                folder.second = second;
              end
            end
          end
        end
      elseif length(name) >= length('meas_YYYY-MM-DD_HH-MM-SS_?U')
        meas = name(1 : 4);
        measurementUnit = name(length(name) : length(name));
        
        if strcmpi(meas, 'meas') && (strcmpi(measurementUnit, 'H') || strcmpi(measurementUnit, 'S'))
          u0 = name( 5 :  5);
          u1 = name(16 : 16);
          u2 = name(25 : 25);
          
          if strcmp(u0, '_') && strcmp(u1, '_') && strcmp(u2, '_')
            h0 = name(10 : 10);
            h1 = name(13 : 13);
            h2 = name(19 : 19);
            h3 = name(22 : 22);
            
            if strcmp(h0, '-') && strcmp(h1, '-') && strcmp(h2, '-') && strcmp(h3, '-')
              year = str2double(name(6 : 9));
              month = str2double(name(11 : 12));
              day = str2double(name(14 : 15));
              hour = str2double(name(17 : 18));
              minute = str2double(name(20 : 21));
              second = str2double(name(23 : 24));
              samplingRate = str2double(name(26 : length(name) - 1));
              
              if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute) && ~isnan(second) && ~isnan(samplingRate)
                folder.longFormat = true;
                folder.meas = meas;
                folder.year = year;
                folder.month = month;
                folder.day = day;
                folder.hour = hour;
                folder.minute = minute;
                folder.second = second;
                folder.samplingRate = samplingRate;
                folder.measurementUnit = measurementUnit;
              end
            end
          end
        end
      end
    end
  end
end