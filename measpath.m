function measpath()
  localInfo = localinfo();
  remoteInfo = remoteinfo();
  
  if ~isempty(localInfo) && ~isempty(remoteInfo)
    printMeasPath(localInfo, remoteInfo);
  elseif ~isempty(localInfo)
    printLocalPath(localInfo);
  end
end

function printMeasPath(localInfo, remoteInfo)
  [localSurveyPath, localBandName] = fileparts(localInfo.folder);
  [localSurveyPath, localSiteName] = fileparts(localSurveyPath);
  [~, localSurveyName] = fileparts(localSurveyPath);
  
  [remoteSurveyPath, remoteBandName] = fileparts(remoteInfo.folder);
  [remoteSurveyPath, remoteSiteName] = fileparts(remoteSurveyPath);
  
  if strcmp(localSurveyPath, remoteSurveyPath)
    upAndRight = 9492;
    verticalAndRight = 9500;
    vertical = 9474;
    horizontal = 9472;
    
    fprintf('%s\n', localSurveyName);
    fprintf('%c%c %s\n', verticalAndRight, horizontal, localSiteName);
    fprintf('%c  %c%c %s\n', vertical, upAndRight, horizontal, localBandName);
    fprintf('%c%c %s\n', upAndRight, horizontal, remoteSiteName);
    fprintf('   %c%c %s\n', upAndRight, horizontal, remoteBandName);
  end
end

function printLocalPath(localInfo)
  [parentPath, bandName] = fileparts(localInfo.folder);
  [parentPath, siteName] = fileparts(parentPath);
  [~, surveyName] = fileparts(parentPath);
  
  upAndRight = 9492;
  horizontal = 9472;
  
  fprintf('%s\n', surveyName);
  fprintf('%c%c %s\n', upAndRight, horizontal, siteName);
  fprintf('   %c%c %s\n', upAndRight, horizontal, bandName);
end