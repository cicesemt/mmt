function remotepath()
  remoteInfo = remoteinfo();
  
  if ~isempty(remoteInfo)
    printRemotePath(remoteInfo);
  end
end

function printRemotePath(remoteInfo)
  [parentPath, bandName] = fileparts(remoteInfo.folder);
  [parentPath, siteName] = fileparts(parentPath);
  [~, surveyName] = fileparts(parentPath);
  
  upAndRight = 9492;
  horizontal = 9472;
  
  fprintf('%s\n', surveyName);
  fprintf('%c%c %s\n', upAndRight, horizontal, siteName);
  fprintf('   %c%c %s\n', upAndRight, horizontal, bandName);
end