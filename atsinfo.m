function [varargout] = atsinfo(filename)
  if nargout > 0
    varargout{1} = [];
  end
  
  file = atsfile(filename);
  
  if ~isempty(file)
    maskinfo = tmaskinfo();
    
    info.name              = file.name;
    info.folder            = file.folder;
    info.bytes             = file.bytes;
    info.aduSerialNumber   = file.aduSerialNumber;
    info.xmlVersion        = file.xmlVersion;
    info.channelNumber     = file.channelNumber;
    info.runNumber         = file.runNumber;
    info.channelType       = file.channelType;
    info.boardType         = file.boardType;
    info.samplingRate      = file.samplingRate;
    info.measurementUnit   = file.measurementUnit;
    info.numberOfSamples   = file.numberOfSamples;
    info.frequency         = file.frequency;
    
    if ~isempty(maskinfo)
      info.samplesAfterMask = file.numberOfSamples - (maskinfo.totalSeconds * file.frequency);
    end
    
    info.startTime    = file.startTime;
    info.stopTime     = file.stopTime;
    info.days         = file.days;
    info.hours        = file.hours;
    info.minutes      = file.minutes;
    info.seconds      = file.seconds;
    info.totalSeconds = file.totalSeconds;
    
    if nargout > 0
      varargout{1} = info;
    else
      fprintf('name:               %s\n', info.name);
      fprintf('folder:             %s\n', info.folder);
      fprintf('bytes:              %s\n', thousandsep(info.bytes, 0));
      fprintf('adu serial number:  %d\n', info.aduSerialNumber);
      fprintf('xml version:        %d\n', info.xmlVersion);
      fprintf('channel number:     %d\n', info.channelNumber);
      fprintf('run number:         %d\n', info.runNumber);
      fprintf('channel type:       %s\n', info.channelType);
      fprintf('board type:         %s\n', info.boardType);
      fprintf('sampling rate:      %d\n', info.samplingRate);
      fprintf('measurement unit:   %s\n', info.measurementUnit);
      fprintf('number of samples:  %s\n', thousandsep(info.numberOfSamples, 0));
      fprintf('frequency:          %f\n', info.frequency);
      
      if ~isempty(maskinfo)
        fprintf('samples after mask: %s\n', thousandsep(info.samplesAfterMask, 0));
      end
      
      fprintf('start time:         %s (%d)\n', datestr(datetime(info.startTime, 'convertfrom', 'posixtime'), 'mmmm dd yyyy HH:MM:SS'), info.startTime);
      fprintf('stop time:          %s (%d)\n', datestr(datetime(info.stopTime, 'convertfrom', 'posixtime'), 'mmmm dd yyyy HH:MM:SS'), info.stopTime);
      fprintf('time length:        ');
      
      if info.days == 1
        fprintf('1 day ');
      else
        fprintf('%d days ', info.days);
      end
      
      if info.hours == 1
        fprintf('1 hour ');
      else
        fprintf('%d hours ', info.hours);
      end
      
      if info.minutes == 1
        fprintf('1 minute ');
      else
        fprintf('%d minutes ', info.minutes);
      end
      
      if info.seconds == 1
        fprintf('1 second\n');
      else
        fprintf('%d seconds\n', info.seconds);
      end
      
      fprintf('total seconds:      %s\n', thousandsep(info.totalSeconds, 0));
    end
  end
end