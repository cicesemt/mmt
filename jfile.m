function jfile = jfile(filename)
  jfile = [];
  
  file = dir(filename);
  
  if length(file) == 1 && length(file.name) > 2 && ~file.isdir
    filetype = file.name(length(file.name) - 1 : length(file.name));
    
    if strcmpi(filetype, '.J')
      fp = fopen(sprintf('%s%s%s', file.folder, filesep, file.name), 'r');
      
      if fp ~= -1
        site = [];
        band = [];
        
        samplingRate = [];
        measurementUnit = [];
        
        while ~feof(fp)
          l = deblank(sprintf('%s', fgetl(fp)));
          
          if length(l) > 13 && strcmpi('#filnam=', l(1 : 8))
            l = l(9 : length(l));
            u = strfind(l, '_');
            
            if ~isempty(u) && (strcmpi(l(length(l) - 4 : length(l)), 'H.ATS') || strcmpi(l(length(l) - 4 : length(l)), 'S.ATS'))
              samplingRate = str2double(l(u(length(u)) + 1 : length(l) - 5));
              
              if ~isnan(samplingRate)
                band = samplingRate;
                measurementUnit = l(length(l) - 4 : length(l) - 4);
                break;
              else
                samplingRate = [];
              end
            end
          end
        end
        
        if ~isempty(band)
          while ~feof(fp)
            l = deblank(sprintf('%s', fgetl(fp)));
            
            if strcmpi(l, file.name(1 : length(file.name) - 2))
              site = l;
              break;
            end
          end
          
          if ~isempty(site)
            zxx.value  = [];
            zxx.period = [];
            zxx.error  = [];
            
            zxy.value  = [];
            zxy.period = [];
            zxy.error  = [];
            
            zyx.value  = [];
            zyx.period = [];
            zyx.error  = [];
            
            zyy.value  = [];
            zyy.period = [];
            zyy.error  = [];
            
            tzx.value  = [];
            tzx.period = [];
            tzx.error  = [];
            
            tzy.value  = [];
            tzy.period = [];
            tzy.error  = [];
            
            rxx.value  = [];
            rxx.period = [];
            rxx.upper  = [];
            rxx.lower  = [];
            
            rxy.value  = [];
            rxy.period = [];
            rxy.upper  = [];
            rxy.lower  = [];
            
            ryx.value  = [];
            ryx.period = [];
            ryx.upper  = [];
            ryx.lower  = [];
            
            ryy.value  = [];
            ryy.period = [];
            ryy.upper  = [];
            ryy.lower  = [];
            
            pxx.value  = [];
            pxx.period = [];
            pxx.upper  = [];
            pxx.lower  = [];
            
            pxy.value  = [];
            pxy.period = [];
            pxy.upper  = [];
            pxy.lower  = [];
            
            pyx.value  = [];
            pyx.period = [];
            pyx.upper  = [];
            pyx.lower  = [];
            
            pyy.value  = [];
            pyy.period = [];
            pyy.upper  = [];
            pyy.lower  = [];
            
            while ~feof(fp)
              l = deblank(sprintf('%s', fgetl(fp)));
              
              t = strsplit(l);
              t = char(t(1));
              
              n = fscanf(fp, '%d', 1);
              fgetl(fp);
              
              if strcmpi(t, 'ZXX')
                dat = fscanf(fp, '%f', [6, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                zxx.value  = complex(dat(2, :), dat(3, :));
                zxx.period = dat(1, :);
                zxx.error  = dat(4, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'ZXY')
                dat = fscanf(fp, '%f', [6, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                zxy.value  = complex(dat(2, :), dat(3, :));
                zxy.period = dat(1, :);
                zxy.error  = dat(4, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'ZYX')
                dat = fscanf(fp, '%f', [6, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                zyx.value  = complex(dat(2, :), dat(3, :));
                zyx.period = dat(1, :);
                zyx.error  = dat(4, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'ZYY')
                dat = fscanf(fp, '%f', [6, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                zyy.value  = complex(dat(2, :), dat(3, :));
                zyy.period = dat(1, :);
                zyy.error  = dat(4, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'TZX')
                dat = fscanf(fp, '%f', [6, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                tzx.value  = complex(dat(2, :), dat(3, :));
                tzx.period = dat(1, :);
                tzx.error  = dat(4, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'TZY')
                dat = fscanf(fp, '%f', [6, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                tzy.value  = complex(dat(2, :), dat(3, :));
                tzy.period = dat(1, :);
                tzy.error  = dat(4, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'RXX')
                dat = fscanf(fp, '%f', [9, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0 && dat(5,k1) == 0 && dat(6,k1) == 0 && dat(7,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                rxx.value  = dat(2, :);
                rxx.period = dat(1, :);
                rxx.upper  = dat(4, :);
                rxx.lower  = dat(5, :);
                
                pxx.value  = dat(3, :);
                pxx.period = dat(1, :);
                pxx.upper  = dat(6, :);
                pxx.lower  = dat(7, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'RXY')
                dat = fscanf(fp, '%f', [9, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0 && dat(5,k1) == 0 && dat(6,k1) == 0 && dat(7,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                rxy.value  = dat(2, :);
                rxy.period = dat(1, :);
                rxy.upper  = dat(4, :);
                rxy.lower  = dat(5, :);
                
                pxy.value  = dat(3, :);
                pxy.period = dat(1, :);
                pxy.upper  = dat(6, :);
                pxy.lower  = dat(7, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'RYX')
                dat = fscanf(fp, '%f', [9, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0 && dat(5,k1) == 0 && dat(6,k1) == 0 && dat(7,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                ryx.value  = dat(2, :);
                ryx.period = dat(1, :);
                ryx.upper  = dat(4, :);
                ryx.lower  = dat(5, :);
                
                pyx.value  = dat(3, :);
                pyx.period = dat(1, :);
                pyx.upper  = dat(6, :);
                pyx.lower  = dat(7, :);
                
                fgetl(fp);
              elseif strcmpi(t, 'RYY')
                dat = fscanf(fp, '%f', [9, n]);
                
                k1 = 1;
                [~, k2] = size(dat);
                
                while k1 <= k2
                  if dat(:,k1) == -999.0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  elseif dat(2,k1) == 0 && dat(3,k1) == 0 && dat(4,k1) == 0 && dat(5,k1) == 0 && dat(6,k1) == 0 && dat(7,k1) == 0
                    dat(:,k1) = [];
                    k2 = k2 - 1;
                  else
                    k1 = k1 + 1;
                  end
                end
                
                ryy.value  = dat(2, :);
                ryy.period = dat(1, :);
                ryy.upper  = dat(4, :);
                ryy.lower  = dat(5, :);
                
                pyy.value  = dat(3, :);
                pyy.period = dat(1, :);
                pyy.upper  = dat(6, :);
                pyy.lower  = dat(7, :);
                
                fgetl(fp);
              else
                break;
              end
            end
            
            jfile.name   = file.name;
            jfile.folder = file.folder;
            jfile.bytes  = file.bytes;
            jfile.site   = site;
            jfile.band   = band;
            
            jfile.samplingRate = samplingRate;
            jfile.measurementUnit = upper(measurementUnit);
            jfile.frequencyBand = sprintf('%d%s', samplingRate, measurementUnit);
            
            jfile.zxx = zxx;
            jfile.zxy = zxy;
            jfile.zyx = zyx;
            jfile.zyy = zyy;
            
            jfile.tzx = tzx;
            jfile.tzy = tzy;
            
            jfile.rxx = rxx;
            jfile.rxy = rxy;
            jfile.ryx = ryx;
            jfile.ryy = ryy;
            
            jfile.pxx = pxx;
            jfile.pxy = pxy;
            jfile.pyx = pyx;
            jfile.pyy = pyy;
          end
        end
        
        fclose(fp);
      end
    end
  end
  
end