function [changed, mask] = tmaskadd(startTime, stopTime)
  changed = false;
  mask = [];
  
  if startTime < stopTime
    mask = tmask();
    
    if ~isempty(mask)
      start = mask(:, 1);
      stop = mask(:, 2);
      
      k = length(start) + 1;
       
      for ii = 1 : length(start)
        if startTime <= start(ii)
          k = ii;
          break;
        end
      end
      
      if k > 1
        if startTime <= stop(k - 1)
          startTime = start(k - 1);
          k = k - 1;
        end
      end
      
      while k <= length(start)
        if stopTime < start(k)
          for ii = length(start) : -1 : k
            start(ii + 1) = start(ii);
            stop(ii + 1) = stop(ii);
          end
          
          start(k) = startTime;
          stop(k) = stopTime;
          changed = true;
          
          break;
        elseif stopTime <= stop(k)
          if start(k) ~= startTime
            start(k) = startTime;
            changed = true;
          end
          
          break;
        else
          start(k) = [];
          stop(k) = [];
        end
      end
      
      if k > length(start)
        start(k) = startTime;
        stop(k) = stopTime;
        changed = true;
      end
    else
      start = startTime;
      stop = stopTime;
      changed = true;
    end
  end
  
  if changed
    mask = zeros(length(start), 2);
    
    mask(:, 1) = start;
    mask(:, 2) = stop;
    
    channels = local();
    
    if ~isempty(channels)
      [mtdir, ~] = mkdir('.mt');
      
      if mtdir
        fp = fopen(sprintf('%s%s%s', '.mt', filesep, 'tmask'), 'wt');
        
        if fp ~= -1
          [n, ~] = size(mask);
          
          for ii = 1 : n
            fprintf(fp, '%d\t%d\n', mask(ii, 1), mask(ii, 2));
          end
          
          fclose(fp);
        end
      end
    end
    
    broadcast();
  end
end

function broadcast()
  f = get(groot, 'Children');
  
  for ii = 1 : length(f)
    if ~isfield(f(ii).UserData, 'id') || ~isfield(f(ii).UserData, 'folder')
      continue;
    end
    
    if ~strcmp(f(ii).UserData.id, 'atsplot')
      continue;
    end
    
    if ~strcmp(f(ii).UserData.folder, pwd())
      continue;
    end
    
    e.Modifier = {};
    e.Key = 'r';
    
    f(ii).KeyPressFcn(f(ii), e);
  end
end