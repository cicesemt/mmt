function [direction, angle] = directioncalc(lat1, lon1, lat2, lon2)
  direction = [];
  angle = [];
  
  if lat1 ~= lat2 || lon1 ~= lon2
    lat1 = (lat1 / (1000*60*60)) * (pi / 180);
    lon1 = (lon1 / (1000*60*60)) * (pi / 180);
    lat2 = (lat2 / (1000*60*60)) * (pi / 180);
    lon2 = (lon2 / (1000*60*60)) * (pi / 180);
    
    angle = asin((lat2 - lat1) / ((lat2 - lat1)^2 + (lon2 - lon1)^2)^0.5) * (180 / pi);
    
    if lat2 > lat1
      direction = 'N';
      
      if lon2 > lon1
        if angle <= 22.5
          direction = 'E';
        elseif angle <= 67.5
          direction = 'NE';
        end
      elseif lon2 < lon1
        if angle <= 22.5
          direction = 'W';
        elseif angle <= 67.5
          direction = 'NW';
        end
      end
    elseif lat2 < lat1
      direction = 'S';
      
      if lon2 > lon1
        if angle >= -22.5
          direction = 'E';
        elseif angle >= -67.5
          direction = 'SE';
        end
      elseif lon2 < lon1
        if angle >= -22.5
          direction = 'W';
        elseif angle >= -67.5
          direction = 'SW';
        end
      end
    else
      if lon2 > lon1
        direction = 'E';
      elseif lon2 < lon1
        direction = 'W';
      end
    end
  end
end