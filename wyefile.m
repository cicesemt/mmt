function wye = wyefile(filename)
  wye = [];
  
  file = dir(filename);
  
  if length(file) == 1 && ~file.isdir && length(file.name) >= length('Y?.000')
    y = file.name(1);
    soundingName = file.name(2 : length(file.name) - 4);
    dot = file.name(length(file.name) - 3 : length(file.name) - 3);
    soundingNumber = str2double(file.name(length(file.name) - 2 : length(file.name)));
    
    if (y == 'Y' || y == 'y') && dot == '.' && ~isnan(soundingNumber) && file.bytes > 7 * 2
      blocks = struct(...
        'offset', {}, ...
        'samplingFrequency', {}, ...
        'samplingSeconds', {}, ...
        'header', {}, ...
        'timeLength', {}, ...
        'numberOfSegments', {}, ...
        'segments', {}...
      );
      
      bands = struct(...
        'name', {}, ...
        'samplingFrequency', {}, ...
        'samplingSeconds', {}, ...
        'decimationExponent', {}, ...
        'numberOfSamples', {}, ...
        'timeLength', {} ...
      );
      
      success = false;
      
      fp = fopen(sprintf('%s%s%s', file.folder, filesep, file.name), 'r');
      
      if fp ~= -1
        offset = 0;
        
        while ~feof(fp)
          [header, headerLength] = fread(fp, 7, 'int16');
          
          if headerLength ~= 7
            break;
          end
          
          decimationExponent = header(1);
          segmentNumber = header(2);
          settings = header(3);
          xlength = header(4);
          ylength = header(5);
          numberOfSamples = header(6);
          numberOfChannels = header(7);
          
          status = fseek(fp, numberOfSamples * numberOfChannels * 2, 'cof');
          
          if status ~= 0
            break;
          end
          
          segment.offset = offset;
          segment.samplingFrequency = (192 * 2^10) / (2^decimationExponent);
          segment.samplingSeconds = numberOfSamples / segment.samplingFrequency;
          segment.header.decimationExponent = decimationExponent;
          segment.header.segmentNumber = segmentNumber;
          segment.header.settings = settings;
          segment.header.xlength = xlength;
          segment.header.ylength = ylength;
          segment.header.numberOfSamples = numberOfSamples;
          segment.header.numberOfChannels = numberOfChannels;
          segment.timeLength.days = 0;
          segment.timeLength.hours = 0;
          segment.timeLength.minutes = 0;
          segment.timeLength.seconds = 0;
          
          segment.timeLength.seconds = segment.samplingSeconds;
          segment.timeLength.days = fix(segment.timeLength.seconds / 86400);
          segment.timeLength.seconds = mod(segment.timeLength.seconds, 86400);
          segment.timeLength.hours = fix(segment.timeLength.seconds / 3600);
          segment.timeLength.seconds = mod(segment.timeLength.seconds, 3600);
          segment.timeLength.minutes = fix(segment.timeLength.seconds / 60);
          segment.timeLength.seconds = mod(segment.timeLength.seconds, 60);
          
          newBlock = true;
          
          if ~isempty(blocks)
            k = length(blocks);
            
            previousSegment = blocks(k).segments(blocks(k).numberOfSegments);
            
            if previousSegment.header.decimationExponent == segment.header.decimationExponent
              if previousSegment.header.segmentNumber + 1 == segment.header.segmentNumber
                if previousSegment.header.settings == segment.header.settings
                  if previousSegment.header.xlength == segment.header.xlength
                    if previousSegment.header.ylength == segment.header.ylength
                      if previousSegment.header.numberOfChannels == segment.header.numberOfChannels
                        blocks(k).samplingSeconds = blocks(k).samplingSeconds + segment.samplingSeconds;
                        blocks(k).header.numberOfSamples = blocks(k).header.numberOfSamples + segment.header.numberOfSamples;
                        
                        blocks(k).timeLength.seconds = blocks(k).samplingSeconds;
                        blocks(k).timeLength.days = fix(blocks(k).timeLength.seconds / 86400);
                        blocks(k).timeLength.seconds = mod(blocks(k).timeLength.seconds, 86400);
                        blocks(k).timeLength.hours = fix(blocks(k).timeLength.seconds / 3600);
                        blocks(k).timeLength.seconds = mod(blocks(k).timeLength.seconds, 3600);
                        blocks(k).timeLength.minutes = fix(blocks(k).timeLength.seconds / 60);
                        blocks(k).timeLength.seconds = mod(blocks(k).timeLength.seconds, 60);
                        
                        blocks(k).numberOfSegments = blocks(k).numberOfSegments + 1;
                        blocks(k).segments(blocks(k).numberOfSegments) = segment;
                        
                        newBlock = false;
                      end
                    end
                  end
                end
              end
            end
          end
          
          if newBlock
            block.offset = segment.offset;
            block.samplingFrequency = segment.samplingFrequency;
            block.samplingSeconds = segment.samplingSeconds;
            block.header.decimationExponent = segment.header.decimationExponent;
            block.header.blockNumber = length(blocks);
            block.header.settings = segment.header.settings;
            block.header.xlength = segment.header.xlength;
            block.header.ylength = segment.header.ylength;
            block.header.numberOfSamples = segment.header.numberOfSamples;
            block.header.numberOfChannels = segment.header.numberOfChannels;
            block.timeLength.days = segment.timeLength.days;
            block.timeLength.hours = segment.timeLength.hours;
            block.timeLength.minutes = segment.timeLength.minutes;
            block.timeLength.seconds = segment.timeLength.seconds;
            block.numberOfSegments = 1;
            block.segments = segment;
            
            blocks(block.header.blockNumber + 1) = block;
          end
          
          newBand = true;
          
          for ii = 1 : length(bands)
            if bands(ii).decimationExponent == segment.header.decimationExponent
              bands(ii).samplingSeconds = bands(ii).samplingSeconds + segment.samplingSeconds;
              bands(ii).numberOfSamples = bands(ii).numberOfSamples + segment.header.numberOfSamples;
              
              bands(ii).timeLength.seconds = bands(ii).samplingSeconds;
              bands(ii).timeLength.days = fix(bands(ii).timeLength.seconds / 86400);
              bands(ii).timeLength.seconds = mod(bands(ii).timeLength.seconds, 86400);
              bands(ii).timeLength.hours = fix(bands(ii).timeLength.seconds / 3600);
              bands(ii).timeLength.seconds = mod(bands(ii).timeLength.seconds, 3600);
              bands(ii).timeLength.minutes = fix(bands(ii).timeLength.seconds / 60);
              bands(ii).timeLength.seconds = mod(bands(ii).timeLength.seconds, 60);
              
              newBand = false;
              break;
            end
          end
          
          if newBand
            band.name = sprintf('%dK', 192 / (2^segment.header.decimationExponent));
            band.samplingFrequency = segment.samplingFrequency;
            band.samplingSeconds = segment.samplingSeconds;
            band.decimationExponent = segment.header.decimationExponent;
            band.numberOfSamples = segment.header.numberOfSamples;
            band.timeLength.days = segment.timeLength.days;
            band.timeLength.hours = segment.timeLength.hours;
            band.timeLength.minutes = segment.timeLength.minutes;
            band.timeLength.seconds = segment.timeLength.seconds;
            
            bands(length(bands) + 1) = band;
          end
          
          offset = offset + 7 * 2;
          offset = offset + segment.header.numberOfSamples * segment.header.numberOfChannels * 2;
        end
        
        if ~isempty(blocks) && offset == file.bytes
          success = true;
        end
        
        fclose(fp);
      end
      
      if success
        wye.name = file.name;
        wye.folder = file.folder;
        wye.bytes = file.bytes;
        wye.soundingName = soundingName;
        wye.soundingNumber = soundingNumber;
        wye.samplingSeconds = 0;
        wye.numberOfSamples = 0;
        wye.timeLength.days = 0;
        wye.timeLength.hours = 0;
        wye.timeLength.minutes = 0;
        wye.timeLength.seconds = 0;
        wye.numberOfBands = length(bands);
        wye.bands = bands;
        wye.numberOfBlocks = length(blocks);
        wye.blocks = blocks;
        
        for ii = 1 : wye.numberOfBands
          wye.samplingSeconds = wye.samplingSeconds + wye.bands(ii).samplingSeconds;
          wye.numberOfSamples = wye.numberOfSamples + wye.bands(ii).numberOfSamples;
        end
        
        wye.timeLength.seconds = wye.samplingSeconds;
        wye.timeLength.days = fix(wye.timeLength.seconds / 86400);
        wye.timeLength.seconds = mod(wye.timeLength.seconds, 86400);
        wye.timeLength.hours = fix(wye.timeLength.seconds / 3600);
        wye.timeLength.seconds = mod(wye.timeLength.seconds, 3600);
        wye.timeLength.minutes = fix(wye.timeLength.seconds / 60);
        wye.timeLength.seconds = mod(wye.timeLength.seconds, 60);
      end
    end
  end
end