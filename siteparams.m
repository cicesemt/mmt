function [varargout] = siteparams(varargin)
  currentSite = [];
  
  flag.reset = false;
  flag.all = false;
  flag.specific = false;
  flag.loc = false;
  flag.lat = false;
  flag.long = false;
  flag.elev = false;
  flag.xutm = false;
  flag.yutm = false;
  flag.datum = false;
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSite = varargin{k};
        varargin(:, k) = [];
      elseif strcmpi(varargin{k}, '-reset')
        varargin(:, k) = [];
        flag.reset = true;
      elseif strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-loc')
        varargin(:, k) = [];
        flag.specific = true;
        flag.loc = true;
      elseif strcmpi(varargin{k}, '-lat')
        varargin(:, k) = [];
        flag.specific = true;
        flag.lat = true;
      elseif strcmpi(varargin{k}, '-long')
        varargin(:, k) = [];
        flag.specific = true;
        flag.long = true;
      elseif strcmpi(varargin{k}, '-elev')
        varargin(:, k) = [];
        flag.specific = true;
        flag.elev = true;
      elseif strcmpi(varargin{k}, '-xutm')
        varargin(:, k) = [];
        flag.specific = true;
        flag.xutm = true;
      elseif strcmpi(varargin{k}, '-yutm')
        varargin(:, k) = [];
        flag.specific = true;
        flag.yutm = true;
      elseif strcmpi(varargin{k}, '-datum')
        varargin(:, k) = [];
        flag.specific = true;
        flag.datum = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.loc = true;
    flag.lat = true;
    flag.long = true;
    flag.elev = true;
    flag.xutm = true;
    flag.yutm = true;
    flag.datum = true;
  elseif ~flag.specific
    flag.loc = true;
    flag.lat = true;
    flag.long = true;
    flag.elev = true;
    flag.xutm = true;
    flag.yutm = true;
    flag.datum = true;
  end
  
  if isempty(currentSite)
    currentSite = site(varargin{:});
  end
  
  if ~isempty(currentSite)
    if flag.reset
      resetParams(currentSite, flag);
    end
    
    if nargout > 0
      varargout{1} = buildParams(currentSite, flag);
    elseif ~flag.reset
      printParams(currentSite, flag);
    end
  end
end

function resetParams(currentSite, flag)
  if flag.loc
    loc(currentSite, '-reset');
  end
  
  if flag.lat
    lat(currentSite, '-reset');
  end
  
  if flag.long
    long(currentSite, '-reset');
  end
  
  if flag.elev
    elev(currentSite, '-reset');
  end
  
  if flag.xutm
    xutm(currentSite, '-reset');
  end
  
  if flag.yutm
    yutm(currentSite, '-reset');
  end
  
  if flag.datum
    datum(currentSite, '-reset');
  end
end

function params = buildParams(currentSite, flag)
  params = [];
  
  if flag.loc
    params.loc = loc(currentSite);
  end
  
  if flag.lat
    params.lat = lat(currentSite);
  end
  
  if flag.long
    params.long = long(currentSite);
  end
  
  if flag.elev
    params.elev = elev(currentSite);
  end
  
  if flag.xutm
    params.xutm = xutm(currentSite);
  end
  
  if flag.yutm
    params.yutm = yutm(currentSite);
  end
  
  if flag.datum
    params.datum = datum(currentSite);
  end
end

function printParams(currentSite, flag)
  names = cell(0);
  values = cell(0);
  
  if flag.loc
    names{length(names) + 1} = 'loc:';
    values{length(values) + 1} = loc(currentSite);
  end
  
  if flag.lat
    names{length(names) + 1} = 'lat:';
    values{length(values) + 1} = lat(currentSite);
  end
  
  if flag.long
    names{length(names) + 1} = 'long:';
    values{length(values) + 1} = long(currentSite);
  end
  
  if flag.elev
    names{length(names) + 1} = 'elev:';
    values{length(values) + 1} = elev(currentSite);
  end
  
  if flag.xutm
    names{length(names) + 1} = 'xutm:';
    values{length(values) + 1} = xutm(currentSite);
  end
  
  if flag.yutm
    names{length(names) + 1} = 'yutm:';
    values{length(values) + 1} = yutm(currentSite);
  end
  
  if flag.datum
    names{length(names) + 1} = 'datum:';
    values{length(values) + 1} = datum(currentSite);
  end
  
  width = 0;
  
  for ii = 1 : length(names)
    if width < length(names{ii})
      width = length(names{ii});
    end
  end
  
  for ii = 1 : length(names)
    if length(names{ii}) < width
      names{ii} = sprintf(sprintf('%%s%%%ds', width - length(names{ii})), names{ii}, ' ');
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s %s\n', names{ii}, values{ii});
  end
end