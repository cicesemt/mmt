function [r, p] = rdet(zxx, zxy, zyx, zyy)
  [value, period, error] = zdet(zxx, zxy, zyx, zyy);
  
  omega = 2 * pi ./ period;
  mu    = 4 * pi * 1.0e-7;
  
  rvalue = (abs(value).^2) ./ (omega * mu);
  rerror = (2 * abs(value) .* error) ./ (omega * mu);
  
  pvalue  = angle(value) * (180 / pi);
  perror = (error ./ abs(value)) * (180 / pi);
  
  r.value  = rvalue;
  r.period = period;
  r.upper  = rvalue + rerror;
  r.lower  = rvalue - rerror;

  p.value  = pvalue;
  p.period = period;
  p.upper  = pvalue + perror;
  p.lower  = pvalue - perror;
end