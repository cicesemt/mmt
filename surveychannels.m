function surveychannels(varargin)
  currentSurvey = [];
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if isstruct(varargin{k})
        currentSurvey = varargin{k};
        varargin(:, k) = [];
      else
        k = k + 1;
      end
    end
  end
  
  if isempty(currentSurvey)
    currentSurvey = survey(varargin{:});
  end
  
  if ~isempty(currentSurvey)
    printChannels(currentSurvey);
  end
end

function printChannels(currentSurvey)
  names = cell(0);
  
  for ii = 1 : length(currentSurvey.sites)
    for jj = 1 : length(currentSurvey.sites(ii).bands)
      for kk = 1 : length(currentSurvey.sites(ii).bands(jj).channels)
        ignore = false;
        
        for ll = 1 : length(names)
          if strcmpi(names{ll}, currentSurvey.sites(ii).bands(jj).channels(kk).channelType)
            ignore = true;
            break;
          end
        end
        
        if ~ignore
          names{length(names) + 1} = currentSurvey.sites(ii).bands(jj).channels(kk).channelType;
        end
      end
    end
  end
  
  for ii = 1 : length(names)
    fprintf('%s\n', names{ii});
  end
end