function despike()
  info = ats('info', '-ex');
  
  if ~isempty(info)
    fname = sprintf('%s%s%s', info.folder, filesep, info.name);
    
    samplingFrequency = info.samplingFrequency;
    startTime = info.startTime;
    stopTime = info.stopTime;
    
    data = atsdata(fname, true);
    
    maxValue = max(data);
    meanValue = mean(data);
    minValue = min(data);
    
    height = maxValue - minValue;
    meanHeight = height / 2;
    tolerance = 0.075;
    
    top = meanValue + meanHeight * tolerance;
    bottom = meanValue - meanHeight * tolerance;
    grace = top - bottom;
    
    index = 1;
    topCount = 0;
    bottomCount = 0;
    
    for ii = startTime : stopTime - 1
      for jj = 1 : samplingFrequency
        index = index + 1;
        
        if index > length(data)
          break;
        end
        
        if data(index) > data(index - 1)
          if data(index) - data(index - 1) > grace
            tmaskadd(ii, ii + 1);
            topCount = topCount + 1;
          end
        elseif data(index) < data(index - 1)
          if data(index - 1) - data(index) > grace
            tmaskadd(ii, ii + 1);
            bottomCount = bottomCount + 1;
          end
        end
      end
    end
    
%     fprintf('%f\t%f\t%f\n', maxValue, meanValue, minValue);
%     fprintf('%f\t%f\n', height, meanHeight);
%     fprintf('%f\t%f\t\n', top, bottom);
%     fprintf('%d\t%d\n', topCount, bottomCount);
%     fprintf('%f\n', grace);
  end
end