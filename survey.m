function [varargout] = survey(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    if strcmpi(varargin{1}, 'info')
      varargin(:, 1) = [];
      surveyinfo(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'sites')
      varargin(:, 1) = [];
      surveysites(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'bands')
      varargin(:, 1) = [];
      surveybands(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'channels')
      varargin(:, 1) = [];
      surveychannels(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'path')
      varargin(:, 1) = [];
      surveypath(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'match')
      varargin(:, 1) = [];
      surveymatch(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'compass')
      varargin(:, 1) = [];
      surveycompass(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'timeline')
      varargin(:, 1) = [];
      surveytimeline(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'samples')
      varargin(:, 1) = [];
      surveysamples(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'elevation')
      varargin(:, 1) = [];
      surveyelevation(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'map')
      varargin(:, 1) = [];
      surveymap(varargin{:});
      return;
    end
    
    if strcmpi(varargin{1}, 'params')
      varargin(:, 1) = [];
      
      if nargout > 0
        varargout{1} = surveyparams(varargin{:});
      else
        surveyparams(varargin{:});
      end
      
      return;
    end
    
    if strcmpi(varargin{1}, 'text')
      varargin(:, 1) = [];
      
      if nargout > 0
        varargout{1} = surveytext(varargin{:});
      else
        surveytext(varargin{:});
      end
      
      return;
    end
  end
  
  if ~isempty(varargin)
    path = varargin{1};
  else
    path = pwd();
  end
  
  currentSurvey = loadSurvey(path);
  
  if ~isempty(currentSurvey)
    if nargout > 0
      varargout{1} = currentSurvey;
    else
      fprintf('%s\n', currentSurvey.name);
    end
  end
end

function currentSurvey = loadSurvey(path)
  currentSurvey = [];
  
  files = dir(path);
  
  if ~isempty(files)
    currentPath = [];
    
    sites = struct(...
      'name', {}, ...
      'folder', {}, ...
      'bytes', {}, ...
      'numberOfSamples', {}, ...
      'latitude', {}, ...
      'longitude', {}, ...
      'elevation', {}, ...
      'startTime', {}, ...
      'stopTime', {}, ...
      'bands', {}, ...
      'currentBand', {}...
    );
    
    startings = zeros(0);
    
    for ii = 1 : length(files)
      file = files(ii);
      
      if strcmp(file.name, '.')
        currentPath = file.folder;
      elseif ~strcmp(file.name, '..') && file.isdir
        currentSite = site(sprintf('%s%s%s', file.folder, filesep, file.name));
        
        if ~isempty(currentSite) && strcmp(currentSite.folder, sprintf('%s%s%s', file.folder, filesep, file.name))
          sites(length(sites) + 1) = currentSite;
          startings(length(startings) + 1) = currentSite.startTime;
        end
      end
    end
    
    if ~isempty(sites)
      [path, ~] = fileparts(sites(1).folder);
      [~, name] = fileparts(path);
      
      currentSurvey.name = name;
      currentSurvey.folder = path;
      currentSurvey.bytes = sites(1).bytes;
      currentSurvey.numberOfSamples = sites(1).numberOfSamples;
      currentSurvey.latitude = sites(1).latitude;
      currentSurvey.longitude = sites(1).longitude;
      currentSurvey.elevation = sites(1).elevation;
      currentSurvey.startTime = sites(1).startTime;
      currentSurvey.stopTime = sites(1).stopTime;
      currentSurvey.sites = struct(...
        'name', {}, ...
        'folder', {}, ...
        'bytes', {}, ...
        'numberOfSamples', {}, ...
        'latitude', {}, ...
        'longitude', {}, ...
        'elevation', {}, ...
        'startTime', {}, ...
        'stopTime', {}, ...
        'bands', {}, ...
        'currentBand', {}...
      );
      currentSurvey.currentSite = [];
      
      for ii = 2 : length(sites)
        currentSurvey.bytes = currentSurvey.bytes + sites(ii).bytes;
        currentSurvey.numberOfSamples = currentSurvey.numberOfSamples + sites(ii).numberOfSamples;
        currentSurvey.latitude = currentSurvey.latitude + sites(ii).latitude;
        currentSurvey.longitude = currentSurvey.longitude + sites(ii).longitude;
        currentSurvey.elevation = currentSurvey.elevation + sites(ii).elevation;
        
        if currentSurvey.startTime > sites(ii).startTime
          currentSurvey.startTime = sites(ii).startTime;
        end
        
        if currentSurvey.stopTime < sites(ii).stopTime
          currentSurvey.stopTime = sites(ii).stopTime;
        end
      end
      
      currentSurvey.latitude = round(currentSurvey.latitude / length(sites));
      currentSurvey.longitude = round(currentSurvey.longitude / length(sites));
      currentSurvey.elevation = round(currentSurvey.elevation / length(sites));
      
      [~, index] = sort(startings);
      
      for ii = 1 : length(index)
        currentSurvey.sites(ii) = sites(index(ii));
        
        if ~isempty(currentSurvey.sites(ii).currentBand)
          currentSurvey.currentSite = currentSurvey.sites(ii);
        elseif strcmp(currentSurvey.sites(ii).folder, pwd())
          currentSurvey.currentSite = currentSurvey.sites(ii);
        end
      end
      
      while true
        swap = false;
        
        for ii = 2 : length(currentSurvey.sites)
          if currentSurvey.sites(ii - 1).startTime == currentSurvey.sites(ii).startTime
            if comparestring(currentSurvey.sites(ii - 1).name, currentSurvey.sites(ii).name) == 1
              swp = currentSurvey.sites(ii - 1);
              currentSurvey.sites(ii - 1) = currentSurvey.sites(ii);
              currentSurvey.sites(ii) = swp;
              
              swap = true;
            end
          end
        end
        
        if ~swap
          break;
        end
      end
    elseif ~isempty(currentPath)
      currentSite = site(currentPath);
      
      if ~isempty(currentSite)
        [parentPath, ~] = fileparts(currentSite.folder);
        currentSurvey = survey(parentPath);
      end
    end
  end
end