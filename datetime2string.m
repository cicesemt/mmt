function output = datetime2string(input)
  output = datestr(input, 'yyyy/mm/dd HH:MM:SS');
end