function [varargout] = tmask(varargin)
  if nargout > 0
    varargout{1} = [];
  end
  
  command.info = false;
  command.zones = false;
  command.add = false;
  command.remove = false;
  command.clear = false;
  
  startTime = [];
  stopTime = [];
  
  if nargin > 0
    if strcmpi(varargin{1}, 'info')
      varargin(:, 1) = [];
      command.info = true;
    elseif strcmpi(varargin{1}, 'zones')
      varargin(:, 1) = [];
      command.zones = true;
    elseif strcmpi(varargin{1}, 'add')
      varargin(:, 1) = [];
      command.add = true;
    elseif strcmpi(varargin{1}, 'remove')
      varargin(:, 1) = [];
      command.remove = true;
    elseif strcmpi(varargin{1}, 'clear')
      varargin(:, 1) = [];
      command.clear = true;
    end
    
    if command.add || command.remove
      if length(varargin) == 2
        startTime = varargin{1};
        stopTime = varargin{2};
        
        varargin(:, 2) = [];
        varargin(:, 1) = [];
        
        if isnan(str2double(startTime))
          startTime = string2posixtime(startTime);
        else
          startTime = fix(str2double(startTime));
        end
        
        if isnan(str2double(stopTime))
          stopTime = string2posixtime(stopTime);
        else
          stopTime = fix(str2double(stopTime));
        end
      end
    end
  end
  
  if command.info
    if nargout > 0
      varargout{1} = tmaskinfo(varargin{:});
    else
      tmaskinfo(varargin{:});
    end
  elseif command.zones
    if nargout > 0
      varargout{1} = tmaskzones(varargin{:});
    else
      tmaskzones(varargin{:});
    end
  elseif command.add
    if ~isempty(startTime) && ~isempty(stopTime)
      [~, mask] = tmaskadd(startTime, stopTime);
      
      if nargout > 0
        varargout{1} = mask;
      end
    end
  elseif command.remove
    if ~isempty(startTime) && ~isempty(stopTime)
      [~, mask] = tmaskremove(startTime, stopTime);
      
      if nargout > 0
        varargout{1} = mask;
      end
    end
  elseif command.clear
    tmaskclear();
  elseif nargout > 0
    fp = fopen(sprintf('%s%s%s', '.mt', filesep, 'tmask'), 'rt');
    
    if fp ~= -1
      start = zeros(0);
      stop = zeros(0);
      
      while ~feof(fp)
        l = deblank(sprintf('%s', fgetl(fp)));
        
        if ~isempty(l)
          values = sscanf(l, '%d');
          
          if length(values) == 2
            start(length(start) + 1) = values(1);
            stop(length(stop) + 1) = values(2);
          end
        end
      end
      
      if ~isempty(start) && length(start) == length(stop)
        start = start';
        stop = stop';
        
        mask = zeros(length(start), 2);
        
        mask(:, 1) = start;
        mask(:, 2) = stop;
        
        varargout{1} = mask;
      end
      
      fclose(fp);
    end
  else
    info = tmaskinfo();
    
    if ~isempty(info)
      fprintf('%d%%\n', round(info.timeSpan))
    end
  end
end