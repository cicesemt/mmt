function output = string2posixtime(input)
  output = [];
  
  if ~isempty(input)
    if isstring(input)
      input = sprintf('%s', input);
    end
    
    if ischar(input)
      if strlength(input) == strlength('YYYY/MM/DD HH:MM:SS')
        c1 = input( 5 :  5);
        c2 = input( 8 :  8);
        c3 = input(11 : 11);
        c4 = input(14 : 14);
        c5 = input(17 : 17);
        
        if strcmp(c1, '-') || strcmp(c1, '/') || strcmp(c1, '.') || strcmp(c1, ':') || strcmp(c1, '_') || strcmp(c1, ' ')
          if strcmp(c2, '-') || strcmp(c2, '/') || strcmp(c2, '.') || strcmp(c2, ':') || strcmp(c2, '_') || strcmp(c2, ' ')
            if strcmp(c3, '-') || strcmp(c3, '/') || strcmp(c3, '.') || strcmp(c3, ':') || strcmp(c3, '_') || strcmp(c3, ' ')
              if strcmp(c4, '-') || strcmp(c4, '/') || strcmp(c4, '.') || strcmp(c4, ':') || strcmp(c4, '_') || strcmp(c4, ' ')
                if strcmp(c5, '-') || strcmp(c5, '/') || strcmp(c5, '.') || strcmp(c5, ':') || strcmp(c5, '_') || strcmp(c5, ' ')
                  year   = str2double(input( 1 :  4));
                  month  = str2double(input( 6 :  7));
                  day    = str2double(input( 9 : 10));
                  hour   = str2double(input(12 : 13));
                  minute = str2double(input(15 : 16));
                  second = str2double(input(18 : 19));
                  
                  if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute) && ~isnan(second)
                    output = posixtime(datetime(year, month, day, hour, minute, second));
                  end
                end
              end
            end
          end
        end
      elseif strlength(input) == strlength('YY/MM/DD HH:MM:SS')
        c1 = input( 3 :  3);
        c2 = input( 6 :  6);
        c3 = input( 9 :  9);
        c4 = input(12 : 12);
        c5 = input(15 : 15);
        
        if strcmp(c1, '-') || strcmp(c1, '/') || strcmp(c1, '.') || strcmp(c1, ':') || strcmp(c1, '_') || strcmp(c1, ' ')
          if strcmp(c2, '-') || strcmp(c2, '/') || strcmp(c2, '.') || strcmp(c2, ':') || strcmp(c2, '_') || strcmp(c2, ' ')
            if strcmp(c3, '-') || strcmp(c3, '/') || strcmp(c3, '.') || strcmp(c3, ':') || strcmp(c3, '_') || strcmp(c3, ' ')
              if strcmp(c4, '-') || strcmp(c4, '/') || strcmp(c4, '.') || strcmp(c4, ':') || strcmp(c4, '_') || strcmp(c4, ' ')
                if strcmp(c5, '-') || strcmp(c5, '/') || strcmp(c5, '.') || strcmp(c5, ':') || strcmp(c5, '_') || strcmp(c5, ' ')
                  year   = str2double(input( 1 :  2));
                  month  = str2double(input( 4 :  5));
                  day    = str2double(input( 7 :  8));
                  hour   = str2double(input(10 : 11));
                  minute = str2double(input(13 : 14));
                  second = str2double(input(16 : 17));
                  
                  if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute) && ~isnan(second)
                    output = posixtime(datetime(year + 2000, month, day, hour, minute, second));
                  end
                end
              end
            end
          end
        end
      elseif strlength(input) == strlength('YYYY/MM/DD HH:MM')
        c1 = input( 5 :  5);
        c2 = input( 8 :  8);
        c3 = input(11 : 11);
        c4 = input(14 : 14);
        
        if strcmp(c1, '-') || strcmp(c1, '/') || strcmp(c1, '.') || strcmp(c1, ':') || strcmp(c1, '_') || strcmp(c1, ' ')
          if strcmp(c2, '-') || strcmp(c2, '/') || strcmp(c2, '.') || strcmp(c2, ':') || strcmp(c2, '_') || strcmp(c2, ' ')
            if strcmp(c3, '-') || strcmp(c3, '/') || strcmp(c3, '.') || strcmp(c3, ':') || strcmp(c3, '_') || strcmp(c3, ' ')
              if strcmp(c4, '-') || strcmp(c4, '/') || strcmp(c4, '.') || strcmp(c4, ':') || strcmp(c4, '_') || strcmp(c4, ' ')
                year   = str2double(input( 1 :  4));
                month  = str2double(input( 6 :  7));
                day    = str2double(input( 9 : 10));
                hour   = str2double(input(12 : 13));
                minute = str2double(input(15 : 16));
                
                if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute)
                  output = posixtime(datetime(year, month, day, hour, minute, 0));
                end
              end
            end
          end
        end
      elseif strlength(input) == strlength('YY/MM/DD HH:MM')
        c1 = input( 3 :  3);
        c2 = input( 6 :  6);
        c3 = input( 9 :  9);
        c4 = input(12 : 12);
        
        if strcmp(c1, '-') || strcmp(c1, '/') || strcmp(c1, '.') || strcmp(c1, ':') || strcmp(c1, '_') || strcmp(c1, ' ')
          if strcmp(c2, '-') || strcmp(c2, '/') || strcmp(c2, '.') || strcmp(c2, ':') || strcmp(c2, '_') || strcmp(c2, ' ')
            if strcmp(c3, '-') || strcmp(c3, '/') || strcmp(c3, '.') || strcmp(c3, ':') || strcmp(c3, '_') || strcmp(c3, ' ')
              if strcmp(c4, '-') || strcmp(c4, '/') || strcmp(c4, '.') || strcmp(c4, ':') || strcmp(c4, '_') || strcmp(c4, ' ')
                year   = str2double(input( 1 :  2));
                month  = str2double(input( 4 :  5));
                day    = str2double(input( 7 :  8));
                hour   = str2double(input(10 : 11));
                minute = str2double(input(13 : 14));
                
                if ~isnan(year) && ~isnan(month) && ~isnan(day) && ~isnan(hour) && ~isnan(minute)
                  output = posixtime(datetime(year + 2000, month, day, hour, minute, 0));
                end
              end
            end
          end
        end
      end
    end
  end
end