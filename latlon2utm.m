function [easting, northing, zone, datum] = latlon2utm(latitude, longitude)
  latitude = latitude / (1000*60*60);
  longitude = longitude / (1000*60*60);
  
  zoneNumber = fix(longitude / 6 + 31);
  
  if latitude < -72
    zoneLetter = 'C';
  elseif latitude < -64
    zoneLetter='D';
  elseif latitude < -56
    zoneLetter='E';
  elseif latitude < -48
    zoneLetter='F';
  elseif latitude < -40
    zoneLetter='G';
  elseif latitude < -32
    zoneLetter='H';
  elseif latitude < -24
    zoneLetter='J';
  elseif latitude < -16
    zoneLetter='K';
  elseif latitude < -8
    zoneLetter='L';
  elseif latitude < 0
    zoneLetter='M';
  elseif latitude < 8
    zoneLetter='N';
  elseif latitude < 16
    zoneLetter='P';
  elseif latitude < 24
    zoneLetter='Q';
  elseif latitude < 32
    zoneLetter='R';
  elseif latitude < 40
    zoneLetter='S';
  elseif latitude < 48
    zoneLetter='T';
  elseif latitude < 56
    zoneLetter='U';
  elseif latitude < 64
    zoneLetter='V';
  elseif latitude < 72
    zoneLetter='W';
  else
    zoneLetter='X';
  end
  
  latitude = latitude * (pi / 180);
  longitude = longitude * (pi / 180);
  
  sa = 6378137.000000;
  sb = 6356752.314245;
  
  e2 = (sa^2 - sb^2)^0.5 / sb;
  e4 = e2^2;
  c = sa^2 / sb;
  
  s = zoneNumber * 6 - 183;
  delta = longitude - s * (pi / 180);
  
  a = cos(latitude) * sin(delta);
  epsilon = 0.5 * log((1 +  a) / (1 - a));
  nu = atan(tan(latitude) / cos(delta)) - latitude;
  v = (c / (1 + e4 * cos(latitude)^2)^0.5) * 0.9996;
  ta = (e4 / 2) * epsilon^2 * cos(latitude)^2;
  a1 = sin(2 * latitude);
  a2 = a1 * cos(latitude)^2;
  j2 = latitude + a1 / 2;
  j4 = (3 * j2 + a2) / 4;
  j6 = (5 * j4 + a2 * cos(latitude)^2) / 3;
  alfa = (3 / 4) * e4;
  beta = (5 / 3) * alfa^2;
  gama = (35 / 27) * alfa^3;
  Bm = 0.9996 * c * (latitude - alfa * j2 + beta * j4 - gama * j6);
  
  easting = epsilon * v * (1 + ta / 3) + 500000;
  northing = nu * v * (1 + ta) + Bm;
  
  if northing < 0
    northing = 9999999 + northing;
  end
  
  zone = sprintf('%d%s', zoneNumber, zoneLetter);
  datum = 'WGS84';
end