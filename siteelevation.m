function siteelevation(varargin)
  currentSite = site(varargin{:});
  
  if ~isempty(currentSite)
    [parentFolder, ~] = fileparts(currentSite.folder);
    [~, surveyName] = fileparts(parentFolder);
    
    name = currentSite.name;
    elevation = currentSite.elevation;
    
    ymin = 0;
    yuni = {'Centimeters' 'Centimeters' 'Meters' 'Meters' 'Meters' 'Kilometres'};
    ydiv = {1 1 100 100 100 100000};
    yind = 1;
    ymax = elevation;
    
    for ii = 1 : 6
      if elevation < 10^ii
        yind = ii;
        
        for jj = 10^(ii-1) : 10^(ii-1) : 10^ii
          if elevation < jj
            ymax = jj;
            
            break;
          end
        end
        
        break;
      end
    end
    
    step = (ymax - ymin) / 4;
    
    y0 = ymin;
    y1 = ymin + step;
    y2 = ymin + step + step;
    y3 = ymax - step;
    y4 = ymax;
    
    label = yuni{yind};
    
    if mod(ymax/ydiv{yind}, 4) == 0
      digits = 0;
    elseif mod(ymax/ydiv{yind}, 2) == 0
      digits = 1;
    else
      digits = 2;
    end
    
    label0 = sprintf(sprintf('%%.%df', digits), y0/ydiv{yind});
    label1 = sprintf(sprintf('%%.%df', digits), y1/ydiv{yind});
    label2 = sprintf(sprintf('%%.%df', digits), y2/ydiv{yind});
    label3 = sprintf(sprintf('%%.%df', digits), y3/ydiv{yind});
    label4 = sprintf(sprintf('%%.%df', digits), y4/ydiv{yind});
    
    f = figure();
    a = axes();
    
    f.set('NumberTitle', 'off');
    f.set('ToolBar', 'none');
    f.set('MenuBar', 'none');
    f.set('Name', surveyName);
    
    a.FontSize = 10;
    a.FontWeight = 'normal';
    
    a.Title.String = 'Elevation';
    a.Title.FontSize = 14;
    a.Title.FontWeight = 'bold';
    
    a.XLabel.String = 'Site';
    a.XLabel.FontSize = 12;
    a.XLabel.FontWeight = 'bold';
    
    a.XLim = [0 2];
    a.XTick = 1;
    a.XTickLabel = {name};
    
    a.YLabel.String = label;
    a.YLabel.FontSize = 12;
    a.YLabel.FontWeight = 'bold';
    
    a.YLim = [ymin ymax];
    a.YTick = ymin : step : ymax;
    a.YTickLabel = {label0 label1 label2 label3 label4};
    a.YGrid = 'on';
    
    a.Box = 'on';
    hold(a, 'on');
    
    fposition = get(f, 'Position');
    fwidth = fposition(3);
    
    aposition = get(a, 'Position');
    awidth = aposition(3) * fwidth;
    
    lwidth = round(awidth / 3);
    
    x = [1 1];
    y = [0 elevation];
    
    plot(x, y, 'Color', [0 0.4470 0.7410], 'LineStyle', '-', 'LineWidth', lwidth);
  end
end