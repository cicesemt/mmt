function [varargout] = tmaskzones(varargin)
  flag.all = false;
  flag.specific = false;
  flag.startTime = false;
  flag.stopTime = false;
  flag.timeSpan = false;
  flag.timeLength = false;
  flag.totalSeconds = false;
  
  if nargout > 0
    varargout{1} = [];
  end
  
  if nargin > 0
    k = 1;
    
    while k <= length(varargin)
      if strcmpi(varargin{k}, '-all')
        varargin(:, k) = [];
        flag.all = true;
      elseif strcmpi(varargin{k}, '-start') || strcmpi(varargin{k}, '-startTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.startTime = true;
      elseif strcmpi(varargin{k}, '-stop') || strcmpi(varargin{k}, '-stopTime')
        varargin(:, k) = [];
        flag.specific = true;
        flag.stopTime = true;
      elseif strcmpi(varargin{k}, '-timeSpan')
        varargin(:, k) = [];
        flag.specific = true;
        flag.timeSpan = true;
      elseif strcmpi(varargin{k}, '-timeLength')
        varargin(:, k) = [];
        flag.specific = true;
        flag.timeLength = true;
      elseif strcmpi(varargin{k}, '-totalSeconds')
        varargin(:, k) = [];
        flag.specific = true;
        flag.totalSeconds = true;
      else
        k = k + 1;
      end
    end
  end
  
  if flag.all
    flag.startTime = true;
    flag.stopTime = true;
    flag.timeSpan = true;
    flag.timeLength = true;
    flag.totalSeconds = true;
  elseif ~flag.specific
    flag.startTime = true;
    flag.stopTime = true;
    flag.timeSpan = true;
  end
  
  mask = tmask();
  
  if ~isempty(mask)
    zones = buildZones(mask);
    
    if nargout > 0
      varargout{1} = zones;
    else
      printZones(zones, flag);
    end
  end
end

function zones = buildZones(mask)
  zones = struct(...
    'startTime', {}, ...
    'stopTime', {}, ...
    'timeSpan', {}, ...
    'days', {}, ...
    'hours', {}, ...
    'minutes', {}, ...
    'seconds', {}, ...
    'totalSeconds', {}...
  );
  
  startTime = [];
  stopTime = [];
  
  channels = local();
  
  if ~isempty(channels)
    smaller = channels(1);
    
    for ii = 2 : length(channels)
      if smaller.numberOfSamples > channels(ii).numberOfSamples
        smaller = channels(ii);
      end
    end
    
    startTime = smaller.startTime;
    stopTime = smaller.stopTime;
  end
  
  [rows, ~] = size(mask);
  
  for ii = 1 : rows
    zone.startTime = mask(ii, 1);
    zone.stopTime = mask(ii, 2);
    zone.timeSpan = 0;
    zone.days = 0;
    zone.hours = 0;
    zone.minutes = 0;
    zone.seconds = 0;
    zone.totalSeconds = zone.stopTime - zone.startTime;
    
    zone.seconds = zone.totalSeconds;
    zone.days = fix(zone.seconds / 86400);
    zone.seconds = mod(zone.seconds, 86400);
    zone.hours = fix(zone.seconds / 3600);
    zone.seconds = mod(zone.seconds, 3600);
    zone.minutes = fix(zone.seconds / 60);
    zone.seconds = mod(zone.seconds, 60);
    
    if ~isempty(startTime) && ~isempty(stopTime)
      zone.timeSpan = rate(startTime, stopTime, zone.startTime, zone.stopTime);
    end
    
    zones(ii) = zone;
  end
end

function printZones(zones, flag)
  for ii = 1 : length(zones)
    fprintf('zone %d\n', ii);
    printZone(zones(ii), flag);
  end
end

function printZone(zone, flag)
  name = cell(0);
  value = cell(0);
  raw = cell(0);
  
  if flag.startTime
    name{length(name) + 1} = 'start time:';
    value{length(value) + 1} = datetime2string(posix2datetime(zone.startTime));
    raw{length(raw) + 1} = sprintf('%d', zone.startTime);
  end
  
  if flag.stopTime
    name{length(name) + 1} = 'stop time:';
    value{length(value) + 1} = datetime2string(posix2datetime(zone.stopTime));
    raw{length(raw) + 1} = sprintf('%d', zone.stopTime);
  end
  
  if flag.timeSpan
    name{length(name) + 1} = 'time span:';
    value{length(value) + 1} = sprintf('%d%%', round(zone.timeSpan));
    raw{length(raw) + 1} = '';
  end
  
  if flag.timeLength
    name{length(name) + 1} = 'time length:';
    value{length(value) + 1} = timeLength(zone.days, zone.hours, zone.minutes, zone.seconds);
    raw{length(raw) + 1} = '';
  end
  
  if flag.totalSeconds
    name{length(name) + 1} = 'total seconds:';
    value{length(value) + 1} = thousandsep(zone.totalSeconds, 0);
    raw{length(raw) + 1} = '';
  end
  
  nameWidth = 0;
  valueWidth = 0;
  rawWidth = 0;
  
  for ii = 1 : length(name)
    if nameWidth < length(name{ii})
      nameWidth = length(name{ii});
    end
    
    if ~isempty(raw{ii})
      if valueWidth < length(value{ii})
        valueWidth = length(value{ii});
      end
      
      if rawWidth < length(raw{ii})
        rawWidth = length(raw{ii});
      end
    end
  end
  
  for ii = 1 : length(name)
    if length(name{ii}) < nameWidth
      name{ii} = sprintf(sprintf('%%s%%%ds', nameWidth - length(name{ii})), name{ii}, ' ');
    end
    
    if ~isempty(raw{ii})
      if length(value{ii}) < valueWidth
        value{ii} = sprintf(sprintf('%%s%%%ds', valueWidth - length(value{ii})), value{ii}, ' ');
      end
      
      if length(raw{ii}) < rawWidth
        raw{ii} = sprintf(sprintf('%%%ds%%s', rawWidth - length(raw{ii})), ' ', raw{ii});
      end
    end
  end
  
  for ii = 1 : length(name)
    fprintf(' %s ', name{ii});
    
    if ~isempty(value{ii})
      fprintf('%s', value{ii});
    end
    
    if ~isempty(raw{ii})
      fprintf(' [%s]', raw{ii});
    end
    
    fprintf('\n');
  end
end

function timeLength = timeLength(days, hours, minutes, seconds)
  timeLength = '';
  
  if days > 0
    if days == 1
      timeLength = '1 day ';
    else
      timeLength = sprintf('%d days ', days);
    end
  end
  
  if hours == 1
    timeLength = sprintf('%s1 hour ', timeLength);
  else
    timeLength = sprintf('%s%d hours ', timeLength, hours);
  end
  
  if minutes == 1
    timeLength = sprintf('%s1 minute ', timeLength);
  else
    timeLength = sprintf('%s%d minutes ', timeLength, minutes);
  end
  
  if seconds == 1
    timeLength = sprintf('%s1 second', timeLength);
  else
    timeLength = sprintf('%s%d seconds', timeLength, seconds);
  end
end

function rate = rate(startTime1, stopTime1, startTime2, stopTime2)
  rate = 0;
  
  if startTime1 < stopTime2
    if stopTime1 > startTime2
      if startTime1 > startTime2
        startTime2 = startTime1;
      end
      
      if stopTime1 < stopTime2
        stopTime2 = stopTime1;
      end
      
      rate = ((stopTime2 - startTime2) / (stopTime1 - startTime1)) * 100;
    end
  end
end